import React, { Component } from 'react';
import Firebase from 'firebase';



class resumenItem extends Component {

    constructor() {
        super();
        this.state = {
        };
    }

    componentWillMount() {
        var candidatoRef = Firebase.database().ref('snap_presidentes/candidatos/' + this.props.id + '/total_global');
        candidatoRef.on("value", snap => {
            this.setState({ votos: snap.val() ? snap.val() : 0 });
        })
    }

    render() {
        return (
            <div className="card">
                <div className="card-block">
                    <div className="h1 text-muted text-right mb-2">
                        <img src={'candidatos/' + this.props.candidato.foto} width="35%" />
                    </div>
                    <div className="h4 mb-0">{this.state.votos}</div>
                    <small className="text-muted text-uppercase font-weight-bold">votos</small>
                    <div className="progress progress-xs mt-1 mb-0">
                        <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%" }}></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default resumenItem;
