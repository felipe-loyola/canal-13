import React, { Component } from 'react';
import Firebase from 'firebase';
import Candidato from './resumenItem'


class resumen extends Component {

    constructor() {
        super();
        this.state = {
            candidatos: {
                "110007": {
                    nombre: "Eduardo Artés B.",
                    foto: "Artes.jpg",
                    partido: "Unión Patriótica",
                    color: "Red"
                },
                "110004": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    partido: "Independiente",
                    color: "Teal"
                },
                "110002": {
                    nombre: "José Antonio Kast",
                    foto: "Kast.jpg",
                    partido: "Independiente",
                    color: "Purple"
                },
                "110008": {
                    nombre: "Alejandro Navarro B.",
                    foto: "Navarro.jpg",
                    partido: "País",
                    color: "Amber"
                },
                "110001": {
                    nombre: "Carolina Goic B.",
                    foto: "Goic.jpg",
                    partido: "Demócrata Cristiano",
                    color: "Green"
                },
                "110006": {
                    nombre: "M. Enríquez-Ominami",
                    foto: "Ominami.jpg",
                    partido: "Progresista",
                    color: "Grey"
                },
                "110005": {
                    nombre: "Beatriz Sánchez M.",
                    foto: "Sanchez.jpg",
                    partido: "Frente Amplio",
                    color: "Brown"
                },
                "110003": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Piñera.jpg",
                    partido: "Chile Vamos",
                    color: "Blue"
                }
            }
        };
    }

    componentDidMount() {
        var count = 0;
        var candidatoRef = Firebase.database().ref('snap_presidentes');
        Object.keys(this.state.candidatos).map((id, index) => (
            candidatoRef.child(id).child('total_global').on("value", snap => {
                count = count + snap.val() ? snap.val() : 0;
            })
        ))
        this.setState({ total_votos: count })
    }

    render() {
        return (
            <div className="card-group">
                {this.state.candidatos ?
                    Object.values(this.state.candidatos).map((c, index) => (
                        <Candidato candidato={c} key={index} id={Object.keys(this.state.candidatos)[index]} total={this.state.total_votos}/>
                    ))
                    :
                    ""
                }
            </div>
        )
    }
}

export default resumen;
