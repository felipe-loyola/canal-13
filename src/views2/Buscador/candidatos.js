import React, { Component } from 'react';
import LaddaButton, { S, EXPAND_RIGHT } from 'react-ladda';
import Firebase from 'firebase';
import Candidato from './candidatoITem'
import Sortable from 'react-sortablejs';

class candidatos extends Component {

    constructor() {
        super();
        this.state = {
            candidatos: {
                "110007": {
                    nombre: "Eduardo Artés B.",
                    foto: "Artes.jpg",
                    partido: "Unión Patriótica",
                    color: "Red"
                },
                "110004": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    partido: "Independiente",
                    color: "Teal"
                },
                "110002": {
                    nombre: "José Antonio Kast",
                    foto: "Kast.jpg",
                    partido: "Independiente",
                    color: "Purple"
                },
                "110008": {
                    nombre: "Alejandro Navarro B.",
                    foto: "Navarro.jpg",
                    partido: "País",
                    color: "Amber"
                },
                "110001": {
                    nombre: "Carolina Goic B.",
                    foto: "Goic.jpg",
                    partido: "Demócrata Cristiano",
                    color: "Green"
                },
                "110006": {
                    nombre: "M. Enríquez-Ominami",
                    foto: "Ominami.jpg",
                    partido: "Progresista",
                    color: "Grey"
                },
                "110005": {
                    nombre: "Beatriz Sánchez M.",
                    foto: "Sanchez.jpg",
                    partido: "Frente Amplio",
                    color: "Brown"
                },
                "110003": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Piñera.jpg",
                    partido: "Chile Vamos",
                    color: "Blue"
                }
            }
        };
    }

    render() {
        return (
            <section className="container" style={{ width: "60%" }}>
                <Sortable className="row active-with-click">
                    {
                        Object.values(this.state.candidatos).map((c, index) => (
                            <div className="col-md-3 col-sm-6 col-xs-12" key={index}>
                                <Candidato
                                    id={Object.keys(this.state.candidatos)[index]}
                                    candidato={c}
                                    results={this.props.results}
                                    type={this.props.type} />
                            </div>
                        ))
                    }
                </Sortable>
            </section>
        )
    }
}

export default candidatos;
