import React, { Component } from 'react';
import LaddaButton, { S, EXPAND_RIGHT } from 'react-ladda';
import Locales from '../../datasets/tabla_locales.json'
import Firebase from 'firebase';
import Autocomplete from "react-autocomplete"
import LocalesDataset from '../../datasets/locales'
import PaisesDatasets from '../../datasets/paises'
import ComunasDatasets from '../../datasets/comunas'
import ConsuladosDataset from '../../datasets/consulados'
import Circunscripcion from '../../datasets/circunscripcion'
import Distritos from '../../datasets/distritos'
import CandidatosList from './candidatos'
import Resumen from './resumen'


class main extends Component {

    constructor() {
        super();
        this.state = {
            calls: [],
            loadingData: false,
            loadingSend: false,
            countLlamadas: 1000,
            busqueda: '',
            typing: false,
            typingTimeOut: 0,
            results: {},
            loader: "none"
        };
    }

    renderOptions() {
        return (
            <div style={{ width: "50%", left: "0", right: "0", margin: "auto", marginBottom: "5%" }}>
                <label className="label-cbx" style={{ marginRight: "3%" }}>
                    <input id="cbx" type="radio" className="invisible" checked={this.state.selected === "pais"} onClick={() => { this.setState({ selected: "pais", busqueda: "" }) }} />
                    <div className="checkbox">
                        <svg width="20px" height="20px" viewBox="0 0 20 20">
                            <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                            <polyline points="4 11 8 15 16 6"></polyline>
                        </svg>
                    </div>
                    <span>País</span>
                </label>
                <label className="label-cbx" style={{ marginRight: "3%" }}>
                    <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "nombre_consulado"} onClick={() => { this.setState({ selected: "nombre_consulado", busqueda: "" }) }} />
                    <div className="checkbox">
                        <svg width="20px" height="20px" viewBox="0 0 20 20">
                            <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                            <polyline points="4 11 8 15 16 6"></polyline>
                        </svg>
                    </div>
                    <span>Consulado</span>
                </label>
                <label className="label-cbx" style={{ marginRight: "3%" }}>
                    <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "circunscripcionsenatorial"} onClick={() => { this.setState({ selected: "circunscripcionsenatorial", busqueda: "" }) }} />
                    <div className="checkbox">
                        <svg width="20px" height="20px" viewBox="0 0 20 20">
                            <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                            <polyline points="4 11 8 15 16 6"></polyline>
                        </svg>
                    </div>
                    <span>Circunscripcion Senatorial</span>
                </label>
                <label className="label-cbx" style={{ marginRight: "3%" }}>
                    <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "nombre_distrito"} onClick={() => { this.setState({ selected: "nombre_distrito", busqueda: "" }) }} />
                    <div className="checkbox">
                        <svg width="20px" height="20px" viewBox="0 0 20 20">
                            <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                            <polyline points="4 11 8 15 16 6"></polyline>
                        </svg>
                    </div>
                    <span>Distrito</span>
                </label>
                <label className="label-cbx" style={{ marginRight: "3%" }}>
                    <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "nombre_comuna"} onClick={() => { this.setState({ selected: "nombre_comuna", busqueda: "" }) }} />
                    <div className="checkbox">
                        <svg width="20px" height="20px" viewBox="0 0 20 20">
                            <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                            <polyline points="4 11 8 15 16 6"></polyline>
                        </svg>
                    </div>
                    <span>Comuna</span>
                </label>
            </div>
        )
    }

    search(b) {
        this.setState({ loader: "" })
        var resultados = {}
        const contex = this;
        var localesRef = Firebase.database().ref('locales');
        localesRef
            .orderByChild(this.state.selected)
            .equalTo(b).on("value", snap => {
                console.log(snap.val())
                this.setState({ loader: "none", results: snap.val(), count: snap.numChildren() })
            });
    }

    datos() {
        switch (this.state.selected) {
            case "pais":
                return PaisesDatasets;
                break;
            case "nombre_comuna":
                return ComunasDatasets;
                break;
            case "nombre_consulado":
                return ConsuladosDataset;
                break;
            case "circunscripcionsenatorial":
                return Circunscripcion;
                break;
            case "nombre_distrito":
                return Distritos;
                break;
            default:
                return [{ id: "sad", label: "Seleccione un tipo de busqueda" }]
        }
    }

    render() {
        return (
            <div className="animated fadeIn">
                <div className="form-group" >
                    <Resumen />
                    {
                        this.renderOptions()
                    }
                    <div className="row" style={{ zIndex: "2" }}>
                        <fieldset className="field-container">
                            <Autocomplete
                                items={this.datos()}
                                shouldItemRender={(item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1}
                                getItemValue={item => item.label}
                                renderItem={(item, highlighted) =>
                                    <div
                                        key={item.id}
                                        style={{ backgroundColor: highlighted ? '#eee' : 'transparent', fontSize: "35px", textAlign: "center", border: "1px, solid", cursor: "pointer" }}
                                    >
                                        {item.label}
                                    </div>
                                }
                                value={this.state.busqueda}
                                onChange={e => { this.setState({ busqueda: e.target.value }) }}
                                onSelect={busqueda => { this.setState({ busqueda }); this.search(busqueda) }}
                            />
                            <div className="icons-container">
                                <div className="icon-search"></div>
                                <div className="icon-close">
                                    <div className="x-up"></div>
                                    <div className="x-down"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div className="row" style={this.state.results ? { zIndex: "1" } : { display: "none" }}>
                        <CandidatosList results={this.state.results} type={this.state.selected} />
                    </div>
                    <div style={{ display: this.state.loader, position: "absolute", width: "200px", height: "200px", top: "0", bottom: "0", left: "0", right: "0", margin: "auto", zIndex: "1" }}>
                        <div className="item item-1"></div>
                        <div className="item item-2"></div>
                        <div className="item item-3"></div>
                        <div className="item item-4"></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default main;
