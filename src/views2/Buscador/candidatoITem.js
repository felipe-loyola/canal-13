import React, { Component } from 'react';
import LaddaButton, { S, EXPAND_RIGHT } from 'react-ladda';
import Firebase from 'firebase';



class candidatoItem extends Component {

    constructor() {
        super();
        this.state = {
            count: 0,
            votos: 0
        };
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.results).length > 0 && nextProps.results !== this.props.results) {
            this.setState({ count: 0 })
            var count = 0;
            switch (this.props.type) {
                case "nombre_comuna":
                    Object.values(nextProps.results).map((r) => {
                        var candidatoRef = Firebase.database().ref('snap_presidentes/candidatos/' + nextProps.id + '/paises/'+r.pais_id +'/comunas/' + r.comuna_id + '/total_locales');
                        candidatoRef.on("value", snap => {
                            this.setState({ votos : snap.val() });
                        })
                    })
                    break;

                default:
                    break;
            }

        }

    }


    render() {
        return (
            <article className={"material-card " + this.props.candidato.color}>
                <h2>
                    <span>{this.props.candidato.nombre}</span>
                    <strong>
                        <i className="fa fa-fw fa-star"></i>{this.state.votos} votos
                    </strong>
                </h2>
                <div className="mc-content">
                    <div className="img-container">
                        <img className="img-responsive" src={'candidatos/' + this.props.candidato.foto} width="100%" />
                    </div>
                    <div className="mc-description">

                    </div>
                </div>
                <div className="mc-footer">
                    <h4>
                        Votos
                    </h4>
                    <span style={{ position: "absolute", fontSize: "50px", right: "30px", top: "5px" }}>
                        {
                            this.state.votos
                        }
                    </span>
                </div>
            </article >
        )
    }
}

export default candidatoItem;
