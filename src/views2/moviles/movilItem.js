import React, { Component } from 'react';
import Firebase from 'firebase';

class movilItem extends Component {

    constructor() {
        super();
        this.state = {
            full: false,
            icon: 0,
            totalGeneral: 412390,
            totalComuna: 89767,
            totalLocal: 5123,
            totalMesa: 32,
            candidatos: {
                "01": {
                    nombre: "Eduardo Artés B.",
                    foto: "Artes.jpg",
                    votos: "4325"
                },
                "02": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    votos: "632"
                },
                "03": {
                    nombre: "José Antonio Kast",
                    foto: "Kast.jpg",
                    votos: "841"
                },
                "04": {
                    nombre: "Alejandro Navarro B.",
                    foto: "Navarro.jpg",
                    votos: "143"
                },
                "05": {
                    nombre: "Carolina Goic B.",
                    foto: "Goic.jpg",
                    votos: "7543"
                },
                "06": {
                    nombre: "M. Enríquez-Ominami",
                    foto: "Ominami.jpg",
                    votos: "73"
                },
                "07": {
                    nombre: "Beatriz Sánchez M.",
                    foto: "Sanchez.jpg",
                    votos: "325"
                },
                "08": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Piñera.jpg",
                    votos: "894"
                }
            }
        };
    }

    componentWillMount() {

    }


    renderIcon() {
        switch (this.state.icon) {
            case 0:
                return (
                    <i className="fa fa-bars"></i>
                )
            case 1:
                return (
                    <i className="fa fa-refresh fa-spin"></i>
                )
            case 2:
                return (
                    <i className="fa fa-arrow-left" aria-hidden="true"></i>
                )
        }
    }

    showFull(e) {
        if (this.state.full) {
            this.setState({
                full: false,
                icon: 1
            })
            const contex = this;
            window.setTimeout(function () {
                contex.setState({ icon: 0 })
            }, 800);
        } else {
            this.setState({
                full: true,
                icon: 1
            })
            const contex = this;
            window.setTimeout(function () {
                contex.setState({ icon: 2 })
            }, 800);
        }
    }




    render() {
        var card = "material-card " + this.props.movil.color;
        var cardFull = "material-card " + this.props.movil.color + " mc-active";
        return (
            <article className={this.state.full ? cardFull : card} style={this.state.full ? { height: "640px" } : {}}>
                <h2>
                    <span>{this.props.movil.nombre}</span>
                    <strong>

                    </strong>
                    <div style={this.state.full ? { display: "none" } : {}}>
                        <i className="fa fa-fw fa-star"></i>
                        <span style={{ position: "absolute", right: "15px", top: "30px", fontSize: "50px" }}>Saklasd 3</span>
                        <span style={{ position: "absolute", right: "25px", bottom: "0" }}>mesa</span>
                    </div>
                </h2>
                <div className="mc-content">
                    <div className="img-container">
                        <img className="img-responsive" src={'moviles/' + this.props.movil.foto} width="100%" />
                    </div>
                    <div className="mc-description">
                        <div className="form-group" style={{ top: "45px", position: "absolute", right: "0", left: "0" }}>
                            <table className="table table-responsive table-hover table-outline mb-0">
                                <tbody>
                                    {
                                        Object.values(this.state.candidatos).map((c) => (
                                            <tr>
                                                <td className="text-center">
                                                    <div className="avatar">
                                                        <img src={"candidatos/"+c.foto} className="img-avatar" alt="admin@bootstrapmaster.com" />
                                                        <span className="avatar-status badge-success"></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="clearfix">
                                                        <div className="float-left">
                                                            <strong>{c.votos}</strong>
                                                        </div>
                                                    </div>
                                                    <div className="progress progress-xs">
                                                        <div className="progress-bar bg-success" style={{ width: "50%" }} role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <a className="mc-btn-action" onClick={(e) => { this.showFull(e) }}>
                    {this.renderIcon()}
                </a>
                <div className="mc-footer">
                    <h4>total de votos:</h4>
                    <span style={{ position: "absolute", fontSize: "50px", right: "30px", top: "5px" }}>
                        123123433
                    </span>
                </div>
            </article >
        )
    }
}

export default movilItem;
