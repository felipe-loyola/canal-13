import React, { Component } from 'react';
import Firebase from 'firebase';
import MovilItem from './movilItem'
import Sortable from 'react-sortablejs';


class movilesList extends Component {

    constructor() {
        super();
        this.state = {
            moviles: {
                "01": {
                    nombre: "Movil 1",
                    foto: "movil.jpg",
                    color: "Red"
                },
                "02": {
                    nombre: "Movil 2",
                    foto: "movil.jpg",
                    color: "Teal"
                },
                "03": {
                    nombre: "Movil 3",
                    foto: "movil.jpg",
                    color: "Purple"
                }
            }
        };
    }

    componentWillMount() {

    }


    render() {
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <section className="container" style={{ width: "80%" }}>
                        <Sortable className="row active-with-click">
                            {
                                Object.values(this.state.moviles).map((m, index) => (
                                    <div className="col-md-3 col-sm-6 col-xs-12" key={index}>
                                        <MovilItem movil={m} />
                                    </div>
                                ))
                            }
                        </Sortable>
                    </section>

                </div>
            </div >
        )
    }
}

export default movilesList;
