import React, { Component } from 'react';
import { withRouter } from "react-router-dom";

import Firebase from 'firebase';



class login extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            login: false,
            error: "",
            successEmail: "",
            client: {},
            loading: "none"
        }
        this.handleConnect = this.handleConnect.bind(this);
        this.handleRequestPassword = this.handleRequestPassword.bind(this);
    }


    handleConnect() {
        this.setState({ loading: "" })
        Firebase
            .auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(user => {
                this.props.history.push("/");
            })
            .catch(error => {
                switch (error.code) {
                    case "auth/invalid-email":
                        this.setState({ error: "Dirección de correo electrónico no válida", loading: "none" })
                        break;
                    case "auth/user-not-found":
                        this.setState({ error: "Dirección de correo ingresada no corresponde a un usuario registrado", loading: "none" })
                        break;
                    case "auth/wrong-password":
                        this.setState({ error: "Contraseña ingresada es incorrecta", loading: "none" })
                        break;
                    default:
                        break;
                }
            });
    }

    handleRequestPassword() {
        Firebase.auth().sendPasswordResetEmail(this.state.email)
            .then(() => this.setState({ successEmail: "se ha enviado un mail con los pasos a seguir para restrablecer su contraseña", error: "" }))
            .catch(error => {
                switch (error.code) {
                    case "auth/invalid-email":
                        this.setState({ error: "Dirección de correo electrónico no válida", successEmail: "" })
                        break;
                    case "auth/user-not-found":
                        this.setState({ error: "Dirección de correo ingresada no corresponde a un usuario registrado", successEmail: "" })
                        break;
                    case "auth/wrong-password":
                        this.setState({ error: "Contraseña ingresada es incorrecta", successEmail: "" })
                        break;
                    default:
                        this.setState({ successEmail: "" })
                        break;
                }
            });
    }



    render() {
        return (
            <div className="app flex-row align-items-center">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card-group mb-0">
                                <div className="card p-4">
                                    <div className="card-block">
                                        <h1>Iniciar Sesión</h1>
                                        <p className="text-muted">Ingrese con su cuenta</p>
                                        <div className="input-group mb-3">
                                            <span className="input-group-addon"><i className="icon-user"></i></span>
                                            <input onChange={(e) => { this.setState({ email: e.target.value }) }} value={this.state.email} type="text" className="form-control" placeholder="Email" required />
                                        </div>
                                        <div className="input-group mb-4">
                                            <span className="input-group-addon"><i className="icon-lock"></i></span>
                                            <input onChange={(e) => { this.setState({ password: e.target.value }) }} value={this.state.password} onKeyPress={(e) => e.which === 13 ? this.handleConnect() : ""} type="password" className="form-control" placeholder="Contraseña" />
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <button type="button" style={{ cursor: "pointer" }} onClick={() => { this.setState({ loading: true }); this.handleConnect() }} className="btn btn-primary px-4">
                                                    <i className="fa fa-refresh fa-spin" style={{ display: this.state.loading }} /> Iniciar Sesión
                        </button>
                                            </div>
                                            <div className="col-6 text-right">
                                                <button type="button" style={{ cursor: "pointer" }} onClick={this.handleRequestPassword} className="btn btn-link px-0">¿Olvidó su contraseña?</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card card-inverse card-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                                    <div className="card-block text-center">
                                        <div>
                                            <img src={'/img/logo_blanco.png'} className="img-login" width="90%" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="alert alert-dismissible alert-danger" style={{ display: this.state.error ? "" : "none", position: "absolute", margin: "auto", left: "0", right: "0", width: "35%" }}  >
                        <button type="button" className="close" onClick={() => this.setState({ error: "" })} data-dismiss="alert">&times;</button>
                        <strong>Oh Error!</strong> <a className="alert-link">{this.state.error}</a>.
          </div>
                    <div className="alert alert-dismissible alert-success" style={{ display: this.state.successEmail ? "" : "none", position: "absolute", margin: "auto", left: "0", right: "0", width: "35%" }}  >
                        <button type="button" className="close" onClick={() => this.setState({ successEmail: "" })} data-dismiss="alert">&times;</button>
                        <strong>Correcto!</strong> <a className="alert-link">{this.state.successEmail}</a>.
          </div>
                </div>
            </div >
        );
    }
}
export default login;