import React, { Component } from 'react';
import Firebase from 'firebase';
import Diputado from './diputado'
import Listado from './listado'

class diputados extends Component {

    constructor() {
        super();
        this.state = {
            count: 0,
            votos: 0
        };
    }

    renderDiputado(d, index) {
        if (d.COD_ZONA === parseInt(this.props.distrito)) {
            var actived = false;
            this.props.activados.map((a) => {
                if (a.codCand === d.COD_CAND) {
                    actived = true;
                }
            })
            return (
                <div key={index} className="col-md-2" style={{ marginBottom: "10px" }}>
                    <Diputado candidato={d} distrito={this.props.distrito} actived={actived} />
                </div>
            )
        }
    }

    render() {
        return (
            <div className="form-group row">
                {
                    this.props.activados ?
                        Object.values(Listado).map((d, index) => (
                            this.renderDiputado(d, index)
                        ))
                        :
                        ""
                }
            </div>
        )
    }
}

export default diputados;
