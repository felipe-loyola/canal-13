const candidatos = {
    "130942":{
      "COD_CAND" : 130942,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "RODRIGO ANTONIO CUEVAS TRONCOSO"
    },
    "130943":{
      "COD_CAND" : 130943,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "CAROLINA ADRIANA HERRERA CALQUIN"
    },
    "130944":{
      "COD_CAND" : 130944,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "CAMILO ADOLFO NELZON MEDINA CUSI"
    },
    "130945":{
      "COD_CAND" : 130945,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "ROSSANA VIRGINIA ALVARADO PINTO"
    },
    "130946":{
      "COD_CAND" : 130946,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "VLADO MIROSEVIC VERDUGO"
    },
    "130947":{
      "COD_CAND" : 130947,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "CAMILA  NAVARRO AGUAYO"
    },
    "130948":{
      "COD_CAND" : 130948,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "IVAN  FIGUEROA LLONA"
    },
    "130949":{
      "COD_CAND" : 130949,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "HECTOR  MERIDA CESPEDES"
    },
    "130950":{
      "COD_CAND" : 130950,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "LUIS ROCAFULL LOPEZ"
    },
    "130951":{
      "COD_CAND" : 130951,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "IVAN PAREDES FIERRO"
    },
    "130952":{
      "COD_CAND" : 130952,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "ANGELO ALEJANDRO CARRASCO ARIAS"
    },
    "130953":{
      "COD_CAND" : 130953,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "CAROLINA ELIANA VIDELA OSORIO"
    },
    "130954":{
      "COD_CAND" : 130954,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "ANDREA MACARENA MURILLO NEUMANN"
    },
    "130955":{
      "COD_CAND" : 130955,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "RICHARD VILDOSO ROJAS"
    },
    "130956":{
      "COD_CAND" : 130956,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "HECTOR HERMOGENES RIVEROS VIDAL"
    },
    "130957":{
      "COD_CAND" : 130957,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "PATRICIO LOPEZ BERRIOS"
    },
    "130958":{
      "COD_CAND" : 130958,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "NINO BALTOLU RASERA"
    },
    "130959":{
      "COD_CAND" : 130959,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "LORENA JIMENEZ WATT"
    },
    "130960":{
      "COD_CAND" : 130960,
      "COD_ZONA": 6001,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "MARCELO ZARA PIZARRO"
    },
    "130001":{
      "COD_CAND" : 130001,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "CARLOS EDUARDO SANTELICES LAGOS"
    },
    "130002":{
      "COD_CAND" : 130002,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "MYLA ETELVINA CHAVEZ FAJARDO"
    },
    "130003":{
      "COD_CAND" : 130003,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "SEBASTIAN  VERGARA ALFARO"
    },
    "130004":{
      "COD_CAND" : 130004,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "MIGUEL ALFREDO ROCHA SARMIENTO"
    },
    "130005":{
      "COD_CAND" : 130005,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "RODRIGO OLIVA VICENTELO"
    },
    "130006":{
      "COD_CAND" : 130006,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "ROXANA  VIGUERAS CHERRES"
    },
    "130007":{
      "COD_CAND" : 130007,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "RAFAEL  MONTES  GONZALEZ"
    },
    "130008":{
      "COD_CAND" : 130008,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "HECTOR  TICUNA VILCA"
    },
    "130009":{
      "COD_CAND" : 130009,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "CAROLINA  VASQUEZ ESPINOZA"
    },
    "130010":{
      "COD_CAND" : 130010,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "MANUEL  SANCHEZ LOYOLA"
    },
    "130011":{
      "COD_CAND" : 130011,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "HUGO  GUTIERREZ GALVEZ"
    },
    "130012":{
      "COD_CAND" : 130012,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "PATRICIO  MARTINEZ FUENTES"
    },
    "130013":{
      "COD_CAND" : 130013,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "DANISA ASTUDILLO PEIRETTI"
    },
    "130014":{
      "COD_CAND" : 130014,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "JUAN MARROQUIN GUZMAN"
    },
    "130015":{
      "COD_CAND" : 130015,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "MARIELA BASUALTO AVALOS"
    },
    "130016":{
      "COD_CAND" : 130016,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "ANA MARIA LUKSIC ROMERO"
    },
    "130017":{
      "COD_CAND" : 130017,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PABLO ORNALDO VALENZUELA HUANCA"
    },
    "130018":{
      "COD_CAND" : 130018,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "RENZO TRISOTTI MARTINEZ"
    },
    "130019":{
      "COD_CAND" : 130019,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "GABRIELA MONSALVEZ ROJAS"
    },
    "130020":{
      "COD_CAND" : 130020,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "NATAN OLIVOS NUNEZ"
    },
    "130021":{
      "COD_CAND" : 130021,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "RAMON GALLEGUILLOS CASTILLO"
    },
    "130022":{
      "COD_CAND" : 130022,
      "COD_ZONA": 6002,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "FREDDY MARCOS ARANEDA BARAHONA"
    },
    "130023":{
      "COD_CAND" : 130023,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "JANETT SHIRLEY GUERRA ABURTO"
    },
    "130024":{
      "COD_CAND" : 130024,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "CLAUDIO ANDRES SANCHEZ BUSTOS"
    },
    "130025":{
      "COD_CAND" : 130025,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "GALIA AGUILERA  CABALLERO"
    },
    "130026":{
      "COD_CAND" : 130026,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "DANIEL  VARGAS DOWNING"
    },
    "130027":{
      "COD_CAND" : 130027,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "NANCY  LANZARINI WALKER"
    },
    "130028":{
      "COD_CAND" : 130028,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "NESTOR  VERA ROJAS"
    },
    "130029":{
      "COD_CAND" : 130029,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "FERNANDO  SAN ROMAN BASCUNAN"
    },
    "130030":{
      "COD_CAND" : 130030,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "SANDRA  CARMONA PIZARRO"
    },
    "130031":{
      "COD_CAND" : 130031,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "CATALINA  PEREZ SALINAS"
    },
    "130032":{
      "COD_CAND" : 130032,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "PABLO ALEJANDRO HERRERA MONARDES"
    },
    "130033":{
      "COD_CAND" : 130033,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "FREDDY CRISTIAN ANTILEF LEIVA"
    },
    "130034":{
      "COD_CAND" : 130034,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "EDGAR  MUNOZ RETAMALES"
    },
    "130035":{
      "COD_CAND" : 130035,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CONSTANTINO  ZAFIROPULOS BOSSY"
    },
    "130036":{
      "COD_CAND" : 130036,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "HARRY  ROBLEDO CORTES"
    },
    "130037":{
      "COD_CAND" : 130037,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "PAOLA ANDREA HIGUERAS LABRA"
    },
    "130038":{
      "COD_CAND" : 130038,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "SACHA IVAN RAZMILIC BURGOS"
    },
    "130039":{
      "COD_CAND" : 130039,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "RODRIGO JAVIER BORNE CARNARTON"
    },
    "130040":{
      "COD_CAND" : 130040,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "SERGIO JAVIER PIZARRO GOMEZ"
    },
    "130041":{
      "COD_CAND" : 130041,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "ANTONINO PARISI FERNANDEZ"
    },
    "130042":{
      "COD_CAND" : 130042,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "ESTEBAN  ZLATAR FRANULIC"
    },
    "130043":{
      "COD_CAND" : 130043,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "DANIEL ERNESTO ADARO SILVA"
    },
    "130044":{
      "COD_CAND" : 130044,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "CAROLINA ELIZABETH MOBAREC VALENCIA"
    },
    "130045":{
      "COD_CAND" : 130045,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "ESTEBAN JORGE VELASQUEZ NUNEZ"
    },
    "130046":{
      "COD_CAND" : 130046,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "DIELA MARIA ALVAREZ ORTEGA"
    },
    "130047":{
      "COD_CAND" : 130047,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "MARCELA XIMENA HERNANDO PEREZ"
    },
    "130048":{
      "COD_CAND" : 130048,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "MARCOS ANDRES ESPINOSA MONARDES"
    },
    "130049":{
      "COD_CAND" : 130049,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "SOL ROMERO DEL VILLAR"
    },
    "130050":{
      "COD_CAND" : 130050,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "LUIS CAPRIOGLIO RABELLO"
    },
    "130051":{
      "COD_CAND" : 130051,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "ALEJANDRA POZO CORTEZ"
    },
    "130052":{
      "COD_CAND" : 130052,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "JAIME ARAYA GUERRERO"
    },
    "130053":{
      "COD_CAND" : 130053,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "VALENTIN HUMBERTO VOLTA VALENCIA"
    },
    "130054":{
      "COD_CAND" : 130054,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "SONYA IVONNE LANGENBACH RUIZ"
    },
    "130055":{
      "COD_CAND" : 130055,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "MIRIAM CAROLINA GONZALEZ BERNAL"
    },
    "130056":{
      "COD_CAND" : 130056,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "CARLOS ENRIQUE LOPEZ VEGA"
    },
    "130057":{
      "COD_CAND" : 130057,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "IGNACIO LEON CUEVAS"
    },
    "130058":{
      "COD_CAND" : 130058,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "PAULINA NUNEZ URRUTIA"
    },
    "130059":{
      "COD_CAND" : 130059,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "JOSE MIGUEL CASTRO BASCUNAN"
    },
    "130060":{
      "COD_CAND" : 130060,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "PABLO TOLOZA FERNANDEZ"
    },
    "130061":{
      "COD_CAND" : 130061,
      "COD_ZONA": 6003,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "CLAUDIA MENESES OLIVA"
    },
    "130062":{
      "COD_CAND" : 130062,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "JOSE CAMPOS VELIZ"
    },
    "130063":{
      "COD_CAND" : 130063,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "ULISES CARABANTES AHUMADA"
    },
    "130064":{
      "COD_CAND" : 130064,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "FABIOLA ESCARLEN EWERT DAZA"
    },
    "130065":{
      "COD_CAND" : 130065,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "HECTOR ALEJANDRO ZAMORA GARCIA"
    },
    "130066":{
      "COD_CAND" : 130066,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "INTI ELEODORO SALAMANCA FERNANDEZ"
    },
    "130067":{
      "COD_CAND" : 130067,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "JUAN MANUEL ANTONIO DIAZ MICHEA"
    },
    "130068":{
      "COD_CAND" : 130068,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "LUIS ALBERTO ACUNA CASTILLO"
    },
    "130069":{
      "COD_CAND" : 130069,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "PATRICIA ERIKA ALVAREZ OLAVE"
    },
    "130070":{
      "COD_CAND" : 130070,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "MARIO  VARAS PIZARRO"
    },
    "130071":{
      "COD_CAND" : 130071,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "SANDRA  PENA MARAMBIO"
    },
    "130072":{
      "COD_CAND" : 130072,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "JAIME MULET MARTINEZ"
    },
    "130073":{
      "COD_CAND" : 130073,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "YHANSS DELGADO QUEVEDO"
    },
    "130074":{
      "COD_CAND" : 130074,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CLARA MENESES COLOMA"
    },
    "130075":{
      "COD_CAND" : 130075,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "SANDRA GUERRA TURCATI"
    },
    "130076":{
      "COD_CAND" : 130076,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "JOHN MARTINEZ GALLARDO"
    },
    "130077":{
      "COD_CAND" : 130077,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "PEDRO  CID CID"
    },
    "130078":{
      "COD_CAND" : 130078,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "MARIO  RIVAS SILVA"
    },
    "130079":{
      "COD_CAND" : 130079,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "DANIELLA  CICARDINI MILLA"
    },
    "130080":{
      "COD_CAND" : 130080,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "JUAN RUBEN SANTANA CASTILLO"
    },
    "130081":{
      "COD_CAND" : 130081,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "JUAN CARLOS MELENDEZ MACKENZIE"
    },
    "130082":{
      "COD_CAND" : 130082,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "JORGE  HIDALGO HIDALGO"
    },
    "130083":{
      "COD_CAND" : 130083,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "PILAR SOTO RIVAS"
    },
    "130084":{
      "COD_CAND" : 130084,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "VITALIA MARICEL MUNOZ CASTILLO"
    },
    "130085":{
      "COD_CAND" : 130085,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "RAUL PALACIOS AUSPONT"
    },
    "130086":{
      "COD_CAND" : 130086,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "JOSE IGNACIO DIAZ MALDONADO"
    },
    "130087":{
      "COD_CAND" : 130087,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "FABIOLA CRISTINA ASMAD REYES"
    },
    "130088":{
      "COD_CAND" : 130088,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "SEBASTIAN MANUEL CAMPOS CAMPILLAY"
    },
    "130089":{
      "COD_CAND" : 130089,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "SOFIA CID VERSALOVIC"
    },
    "130090":{
      "COD_CAND" : 130090,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "RENE AEDO ORMENO"
    },
    "130091":{
      "COD_CAND" : 130091,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "NELLY GALEB BOU"
    },
    "130092":{
      "COD_CAND" : 130092,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "NICOLAS NOMAN GARRIDO"
    },
    "130093":{
      "COD_CAND" : 130093,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "CARLOS VILCHES GUZMAN"
    },
    "130094":{
      "COD_CAND" : 130094,
      "COD_ZONA": 6004,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "JUAN ANTONIO PEREZ MARIN"
    },
    "130095":{
      "COD_CAND" : 130095,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "JULIO MOISES ZAPATA RODRIGUEZ"
    },
    "130096":{
      "COD_CAND" : 130096,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "NELSON RODRIGO GALVEZ PEREZ"
    },
    "130097":{
      "COD_CAND" : 130097,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "PATRICIA AGUIRRE GONZALEZ"
    },
    "130098":{
      "COD_CAND" : 130098,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "MAGALLANES ESPINOSA BERTRAN"
    },
    "130099":{
      "COD_CAND" : 130099,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "MARISOL CESPEDES AGUIRRE"
    },
    "130100":{
      "COD_CAND" : 130100,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "DANIELLA  CORTES BAUER"
    },
    "130101":{
      "COD_CAND" : 130101,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "JAIME ENRIQUE FERNANDEZ MADRID"
    },
    "130102":{
      "COD_CAND" : 130102,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "AGUSTIN  GONZALEZ MORENO"
    },
    "130103":{
      "COD_CAND" : 130103,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "TIRSO ANTONIO GONZALEZ ALQUINTA"
    },
    "130104":{
      "COD_CAND" : 130104,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "YERKO AVALOS VILLARROEL"
    },
    "130105":{
      "COD_CAND" : 130105,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "ALEJANDRO TELLO ARATA"
    },
    "130106":{
      "COD_CAND" : 130106,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "NIDIA MILLA IRARRAZABAL"
    },
    "130107":{
      "COD_CAND" : 130107,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CAROLINA TORRES GONZALEZ"
    },
    "130108":{
      "COD_CAND" : 130108,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "PEDRO ARAYA AVILA"
    },
    "130109":{
      "COD_CAND" : 130109,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "GABRIEL CABALLERO BERRIOS"
    },
    "130110":{
      "COD_CAND" : 130110,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "PEDRO ANTONIO CASTILLO CASTILLO"
    },
    "130111":{
      "COD_CAND" : 130111,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PEDRO ANTONIO VELASQUEZ SEGUEL"
    },
    "130112":{
      "COD_CAND" : 130112,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "ANGELA ERCILIA ROJAS BARRIOS"
    },
    "130113":{
      "COD_CAND" : 130113,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "JORGE ADRIAN MONSALVE VARGAS"
    },
    "130114":{
      "COD_CAND" : 130114,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "ERNESTO VELASCO RODRIGUEZ"
    },
    "130115":{
      "COD_CAND" : 130115,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "CESAR ANTONIO VALENCIA CORTES"
    },
    "130116":{
      "COD_CAND" : 130116,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "DANIEL  NUNEZ ARANCIBIA"
    },
    "130117":{
      "COD_CAND" : 130117,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "ELISA CORTES ARAYA"
    },
    "130118":{
      "COD_CAND" : 130118,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "RAUL  SALDIVAR AUGER"
    },
    "130119":{
      "COD_CAND" : 130119,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "LUIS  LEMUS ARACENA"
    },
    "130120":{
      "COD_CAND" : 130120,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "MIGUEL ANGEL ALVARADO RAMIREZ"
    },
    "130121":{
      "COD_CAND" : 130121,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "ALEJANDRA MARGARITA DIAZ MALUENDA"
    },
    "130122":{
      "COD_CAND" : 130122,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "MATIAS  WALKER PRIETO"
    },
    "130123":{
      "COD_CAND" : 130123,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "PEDRO  PRADO MORENO"
    },
    "130124":{
      "COD_CAND" : 130124,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "CECILIA  TIRADO SOTO"
    },
    "130125":{
      "COD_CAND" : 130125,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "CRISTIAN  GALLEGUILLOS VEGA"
    },
    "130126":{
      "COD_CAND" : 130126,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "ADRIANA  VERGARA MANQUE"
    },
    "130127":{
      "COD_CAND" : 130127,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "DENIS  CORTES AGUILERA"
    },
    "130128":{
      "COD_CAND" : 130128,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "CAROLINA INES HENRIQUEZ GUTIERREZ"
    },
    "130129":{
      "COD_CAND" : 130129,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "SERGIO GAHONA SALAZAR"
    },
    "130130":{
      "COD_CAND" : 130130,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "IVON GUERRA AGUILERA"
    },
    "130131":{
      "COD_CAND" : 130131,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "JUAN MANUEL FUENZALIDA COBO"
    },
    "130132":{
      "COD_CAND" : 130132,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "ERNESTINA SARMIENTO RIVERA"
    },
    "130133":{
      "COD_CAND" : 130133,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "CARLOS CRUZ-COKE CARVALLO"
    },
    "130134":{
      "COD_CAND" : 130134,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 89,
      "GLOSA_CAND": "ROBERTO VEGA CAMPUSANO"
    },
    "130135":{
      "COD_CAND" : 130135,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 90,
      "GLOSA_CAND": "FRANCISCO EGUIGUREN CORREA"
    },
    "130136":{
      "COD_CAND" : 130136,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 91,
      "GLOSA_CAND": "FRANCESCA FIGARI SCHENCK"
    },
    "130137":{
      "COD_CAND" : 130137,
      "COD_ZONA": 6005,
      "CAN_ORDEN": 92,
      "GLOSA_CAND": "ANDREA GUZMAN HERRERA"
    },
    "130138":{
      "COD_CAND" : 130138,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "GABRIEL ARANGUIZ BALLESTEROS"
    },
    "130139":{
      "COD_CAND" : 130139,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "JULIO OMAR LOBOS LOBOS"
    },
    "130140":{
      "COD_CAND" : 130140,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "NIBALDO GABRIEL PINTO SILVA"
    },
    "130141":{
      "COD_CAND" : 130141,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "OSCAR ANDRES ARANCIBIA ARANCIBIA"
    },
    "130142":{
      "COD_CAND" : 130142,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "MARGGORIE ZUMARAN OYANEDEL"
    },
    "130143":{
      "COD_CAND" : 130143,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "MARIANELLA BENAVIDES CARDENAS"
    },
    "130144":{
      "COD_CAND" : 130144,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "DIEGO IBANEZ COTRONEO"
    },
    "130145":{
      "COD_CAND" : 130145,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "JOEL ANDRES GONZALEZ VEGA"
    },
    "130146":{
      "COD_CAND" : 130146,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "JAVIERA TOLEDO MUNOZ"
    },
    "130147":{
      "COD_CAND" : 130147,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "ALEJANDRO GERMAN VERGARA ARIAS"
    },
    "130148":{
      "COD_CAND" : 130148,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "LIZET DE LOS ANGELES BRIONES PEREZ"
    },
    "130149":{
      "COD_CAND" : 130149,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "LUIS SOTO PEREZ"
    },
    "130150":{
      "COD_CAND" : 130150,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CLAUDIA ARCOS DUARTE"
    },
    "130151":{
      "COD_CAND" : 130151,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "FIDEL RODRIGO CUETO ROSALES"
    },
    "130152":{
      "COD_CAND" : 130152,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "ELIZABETH  ARMSTRONG GONZALEZ"
    },
    "130153":{
      "COD_CAND" : 130153,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "RODRIGO  SILVA ALFARO"
    },
    "130154":{
      "COD_CAND" : 130154,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "DONNA  HERRERA BARRERA"
    },
    "130155":{
      "COD_CAND" : 130155,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "ASHLY  VENEGAS CLOBARES"
    },
    "130156":{
      "COD_CAND" : 130156,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "JEANNE KATHERINE CROOCKER VILLA"
    },
    "130157":{
      "COD_CAND" : 130157,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "IGOR  CONTRERAS JERIA"
    },
    "130158":{
      "COD_CAND" : 130158,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "MANUEL ANTONIO CARDENAS CANCINO"
    },
    "130159":{
      "COD_CAND" : 130159,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "FRANCISCA  CHAHUAN GAJARDO"
    },
    "130160":{
      "COD_CAND" : 130160,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "MAURICIO  RIFFO MENDEZ"
    },
    "130161":{
      "COD_CAND" : 130161,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "CARLOS LEMUS CID"
    },
    "130162":{
      "COD_CAND" : 130162,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "PABLO RECABAL MATURANA"
    },
    "130163":{
      "COD_CAND" : 130163,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "EDITH ESTAY OLGUIN"
    },
    "130164":{
      "COD_CAND" : 130164,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "SONIA CUBILLOS ESPINOZA"
    },
    "130165":{
      "COD_CAND" : 130165,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "CHRISTIAN URIZAR MUNOZ"
    },
    "130166":{
      "COD_CAND" : 130166,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "MARCELO SCHILLING RODRIGUEZ"
    },
    "130167":{
      "COD_CAND" : 130167,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "CYNTHIA LORENA  BURGOS SANCHEZ"
    },
    "130168":{
      "COD_CAND" : 130168,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "MARIO FRANCISCO PEREZ NAVARRO"
    },
    "130169":{
      "COD_CAND" : 130169,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "PEDRO  AVILA SILVA"
    },
    "130170":{
      "COD_CAND" : 130170,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "CAROLINA MARZAN PINTO"
    },
    "130171":{
      "COD_CAND" : 130171,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "XIMENA MARIA RIVILLO OROSTICA"
    },
    "130172":{
      "COD_CAND" : 130172,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "CAMILO SANCHEZ PIZARRO"
    },
    "130173":{
      "COD_CAND" : 130173,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "MANUEL RIESCO LARRAIN"
    },
    "130174":{
      "COD_CAND" : 130174,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "DANIEL ANGEL VERDESSI BELEMMI"
    },
    "130175":{
      "COD_CAND" : 130175,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "WALESKA JACQUELINE CASTILLO LOPEZ"
    },
    "130176":{
      "COD_CAND" : 130176,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "ULISES  TOBAR CASSI"
    },
    "130177":{
      "COD_CAND" : 130177,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 89,
      "GLOSA_CAND": "MARIO LUIS FUENTES ROMERO"
    },
    "130178":{
      "COD_CAND" : 130178,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 90,
      "GLOSA_CAND": "PATRICIO ANTONIO BARROS VIGUERA"
    },
    "130179":{
      "COD_CAND" : 130179,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 91,
      "GLOSA_CAND": "JAVIER MALDONADO CORREA"
    },
    "130180":{
      "COD_CAND" : 130180,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 92,
      "GLOSA_CAND": "PABLO KAST SOMMERHOFF"
    },
    "130181":{
      "COD_CAND" : 130181,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 93,
      "GLOSA_CAND": "MARCELA GUTIERREZ GARCES"
    },
    "130182":{
      "COD_CAND" : 130182,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 94,
      "GLOSA_CAND": "CAMILA FLORES OPORTO"
    },
    "130183":{
      "COD_CAND" : 130183,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 95,
      "GLOSA_CAND": "ANDRES LONGTON HERRERA"
    },
    "130184":{
      "COD_CAND" : 130184,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 96,
      "GLOSA_CAND": "LUIS PARDO SAINZ"
    },
    "130185":{
      "COD_CAND" : 130185,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 97,
      "GLOSA_CAND": "GIOVANNI CALDERON BASSI"
    },
    "130186":{
      "COD_CAND" : 130186,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 98,
      "GLOSA_CAND": "ERIKA MUNOZ BRAVO"
    },
    "130187":{
      "COD_CAND" : 130187,
      "COD_ZONA": 6006,
      "CAN_ORDEN": 99,
      "GLOSA_CAND": "GUSTAVO ALESSANDRI BALMACEDA"
    },
    "130188":{
      "COD_CAND" : 130188,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 100,
      "GLOSA_CAND": "TRINIDAD MORAN HERRERA"
    },
    "130189":{
      "COD_CAND" : 130189,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 101,
      "GLOSA_CAND": "ROSARIO PEREZ IZQUIERDO"
    },
    "130190":{
      "COD_CAND" : 130190,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 102,
      "GLOSA_CAND": "MARIA JOSE HOFFMANN OPAZO"
    },
    "130191":{
      "COD_CAND" : 130191,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 103,
      "GLOSA_CAND": "OSVALDO  URRUTIA SOTO"
    },
    "130192":{
      "COD_CAND" : 130192,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 104,
      "GLOSA_CAND": "JORGE  CASTRO MUNOZ"
    },
    "130193":{
      "COD_CAND" : 130193,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 105,
      "GLOSA_CAND": "ANNETTE  RAPU ZAMORA"
    },
    "130194":{
      "COD_CAND" : 130194,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 106,
      "GLOSA_CAND": "PILAR SOBERADO NUNEZ"
    },
    "130195":{
      "COD_CAND" : 130195,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "IVAN ROBERTO ROJAS OVANDO"
    },
    "130196":{
      "COD_CAND" : 130196,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "MYRNA SEPULVEDA ZAMUDIO"
    },
    "130197":{
      "COD_CAND" : 130197,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "VICTOR MANUEL VILLALOBOS SOTO"
    },
    "130198":{
      "COD_CAND" : 130198,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "NICOLA HADWA SHAHWAN"
    },
    "130199":{
      "COD_CAND" : 130199,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "NICOLL ADRIANA ROJAS LABRA"
    },
    "130200":{
      "COD_CAND" : 130200,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "MARCOS ANTONIO MONTECINOS YANEZ"
    },
    "130201":{
      "COD_CAND" : 130201,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "JORGE  RAULD GONZALEZ"
    },
    "130202":{
      "COD_CAND" : 130202,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "MILKO  CARACCIOLO SOTO"
    },
    "130203":{
      "COD_CAND" : 130203,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "CAMILA  ROJAS VALDERRAMA"
    },
    "130204":{
      "COD_CAND" : 130204,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "JORGE  BRITO HASBUN"
    },
    "130205":{
      "COD_CAND" : 130205,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "MABEL LUCILA ZUNIGA VALENCIA"
    },
    "130206":{
      "COD_CAND" : 130206,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "JOHN  PARADA MONTERO"
    },
    "130207":{
      "COD_CAND" : 130207,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "JUAN PABLO PAONESSA BARRIA"
    },
    "130208":{
      "COD_CAND" : 130208,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "JORGE RUBEN CANALES GATICA"
    },
    "130209":{
      "COD_CAND" : 130209,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "DANN ALEJANDRO ESPINOZA POZO"
    },
    "130210":{
      "COD_CAND" : 130210,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "ALVARO  SANHUEZA MARIPANGUE"
    },
    "130211":{
      "COD_CAND" : 130211,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "LEONARDO  CONTRERAS NEIRA"
    },
    "130212":{
      "COD_CAND" : 130212,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "CARLOS HORACIO VIO VICUNA"
    },
    "130213":{
      "COD_CAND" : 130213,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "JAIME ROBERTO ESPINOZA TOBAR"
    },
    "130214":{
      "COD_CAND" : 130214,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "PASTORA ANTONIETA VALDERRAMA VASQUEZ"
    },
    "130215":{
      "COD_CAND" : 130215,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "LUCAS BLASET PEREZ"
    },
    "130216":{
      "COD_CAND" : 130216,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "RODRIGO CUEVAS BENITEZ"
    },
    "130217":{
      "COD_CAND" : 130217,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "CRESCENCIO  LOPEZ PEREZ"
    },
    "130218":{
      "COD_CAND" : 130218,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "KAREN  PICHUNANTE CANSECO"
    },
    "130219":{
      "COD_CAND" : 130219,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "JORGE ESTAY OLGUIN"
    },
    "130220":{
      "COD_CAND" : 130220,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "JORGE RENAULT MANRIQUEZ"
    },
    "130221":{
      "COD_CAND" : 130221,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "LUIS  QUIJADA LLANCAS"
    },
    "130222":{
      "COD_CAND" : 130222,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "CLARISA  RIOS INOSTROZA"
    },
    "130223":{
      "COD_CAND" : 130223,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "JEAN PIERRE GUTIERREZ ECHEVERRIA"
    },
    "130224":{
      "COD_CAND" : 130224,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "MARCIA TAPIA NAVIA"
    },
    "130225":{
      "COD_CAND" : 130225,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "ANA ARAYA ROMERO"
    },
    "130226":{
      "COD_CAND" : 130226,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "BARBARA  GONZALEZ CID"
    },
    "130227":{
      "COD_CAND" : 130227,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "JUAN ENRIQUE SOTO ROJAS"
    },
    "130228":{
      "COD_CAND" : 130228,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "RODRIGO GONZALEZ TORRES"
    },
    "130229":{
      "COD_CAND" : 130229,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "KATHERINE  ARAYA MATUS"
    },
    "130230":{
      "COD_CAND" : 130230,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "MARIO  SOTO BENAVIDES"
    },
    "130231":{
      "COD_CAND" : 130231,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "JORGE  COULON LARRANAGA"
    },
    "130232":{
      "COD_CAND" : 130232,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "EDSON  CHAVEZ SEVERINO"
    },
    "130233":{
      "COD_CAND" : 130233,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "RICARDO BRAVO OLIVA"
    },
    "130234":{
      "COD_CAND" : 130234,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 89,
      "GLOSA_CAND": "MARCELO  DIAZ DIAZ"
    },
    "130235":{
      "COD_CAND" : 130235,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 90,
      "GLOSA_CAND": "GIGLIOLA ELINA CENTONZIO ROSSEL"
    },
    "130236":{
      "COD_CAND" : 130236,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 91,
      "GLOSA_CAND": "IRMA DEL CARMEN TRONCOSO SAAVEDRA"
    },
    "130237":{
      "COD_CAND" : 130237,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 92,
      "GLOSA_CAND": "VICTOR MARCELO TORRES JELDES"
    },
    "130238":{
      "COD_CAND" : 130238,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 93,
      "GLOSA_CAND": "MARIELLA VALDES AVILA"
    },
    "130239":{
      "COD_CAND" : 130239,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 94,
      "GLOSA_CAND": "MARIEL FERNANDA TRIGO VALLEJOS"
    },
    "130240":{
      "COD_CAND" : 130240,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 95,
      "GLOSA_CAND": "JENNIFER VIOLETA LEIVA LABARCA"
    },
    "130241":{
      "COD_CAND" : 130241,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 96,
      "GLOSA_CAND": "MAURICIO EDUARDO LILLO CAVAGNARO"
    },
    "130242":{
      "COD_CAND" : 130242,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 97,
      "GLOSA_CAND": "RENE LUES ESCOBAR"
    },
    "130243":{
      "COD_CAND" : 130243,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 98,
      "GLOSA_CAND": "ANDRES CELIS MONTT"
    },
    "130244":{
      "COD_CAND" : 130244,
      "COD_ZONA": 6007,
      "CAN_ORDEN": 99,
      "GLOSA_CAND": "JOSE  EGIDO ARRIOLA"
    },
    "130823":{
      "COD_CAND" : 130823,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "CAMILO  LAGOS MIRANDA"
    },
    "130824":{
      "COD_CAND" : 130824,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "ALEXANDER GABRIEL ARANGUIZ MEZA"
    },
    "130825":{
      "COD_CAND" : 130825,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "BERTA DEL CARMEN MONTECINOS DURAN"
    },
    "130826":{
      "COD_CAND" : 130826,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "MANUEL ENRIQUE MUNOZ PALACIOS"
    },
    "130827":{
      "COD_CAND" : 130827,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "MAURICIO ANTONIO SANTANDER VIDELA"
    },
    "130828":{
      "COD_CAND" : 130828,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "HUGO CIPRIANO NORAMBUENA RODRIGUEZ"
    },
    "130829":{
      "COD_CAND" : 130829,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "MICHAEL GABRIEL GUERRA MUNOZ"
    },
    "130830":{
      "COD_CAND" : 130830,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "GREMIDIA ESTELA SAGARDIA ARIAS"
    },
    "130831":{
      "COD_CAND" : 130831,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "DAVID BLANCO URZUA"
    },
    "130832":{
      "COD_CAND" : 130832,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "CLAUDIA NATHALIE MIX JIMENEZ"
    },
    "130833":{
      "COD_CAND" : 130833,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "RICARDO ANGEL CAMARGO BRITO"
    },
    "130834":{
      "COD_CAND" : 130834,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "MARIA TERESA ALVAREZ AGUILAR"
    },
    "130835":{
      "COD_CAND" : 130835,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "RICARDO IVAN MALDONADO OLIVARES"
    },
    "130836":{
      "COD_CAND" : 130836,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CRISTOBAL MARDONES DIAZ"
    },
    "130837":{
      "COD_CAND" : 130837,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "DORIS GONZALEZ LEMUNAO"
    },
    "130838":{
      "COD_CAND" : 130838,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "MARIA JOSE LEIVA JOPIA"
    },
    "130839":{
      "COD_CAND" : 130839,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PABLO VIDAL ROJAS"
    },
    "130840":{
      "COD_CAND" : 130840,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "MARCELA SANDOVAL OSORIO"
    },
    "130841":{
      "COD_CAND" : 130841,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "VALDEMAR  SANHUEZA YEVENES"
    },
    "130842":{
      "COD_CAND" : 130842,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "MAURICIO ALVAREZ SEPULVEDA"
    },
    "130843":{
      "COD_CAND" : 130843,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "PAOLA  MARCELLI ASTUDILLO"
    },
    "130844":{
      "COD_CAND" : 130844,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "ANA MARIA MOLINA VILLARREAL"
    },
    "130845":{
      "COD_CAND" : 130845,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "JUAN HERNANDEZ MARTINEZ"
    },
    "130846":{
      "COD_CAND" : 130846,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "SILVIO  RIVERA DURAN"
    },
    "130847":{
      "COD_CAND" : 130847,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "VICTOR  ITURRIETA RIOS"
    },
    "130848":{
      "COD_CAND" : 130848,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "MARIA EUGENIA PARRA TORO"
    },
    "130849":{
      "COD_CAND" : 130849,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "MACARENA  PARRA PARRA"
    },
    "130850":{
      "COD_CAND" : 130850,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "ANDRES SANTANDER ORTEGA"
    },
    "130851":{
      "COD_CAND" : 130851,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "GABRIELA CARRASCO TOBAR"
    },
    "130852":{
      "COD_CAND" : 130852,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "PEPE AUTH STEWART"
    },
    "130853":{
      "COD_CAND" : 130853,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "MARCELA GEOVANNA CONCHA VILLANUEVA"
    },
    "130854":{
      "COD_CAND" : 130854,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "ALEJANDRO ZISIS DRINBERG"
    },
    "130855":{
      "COD_CAND" : 130855,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "MIRIAM DEL CARMEN BAZAEZ GUERRA"
    },
    "130856":{
      "COD_CAND" : 130856,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "LORENA PIZARRO SIERRA"
    },
    "130857":{
      "COD_CAND" : 130857,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "CARMEN HERTZ CADIZ"
    },
    "130858":{
      "COD_CAND" : 130858,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "GABRIEL  SILBER ROMO"
    },
    "130859":{
      "COD_CAND" : 130859,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "VICTOR OSORIO REYES"
    },
    "130860":{
      "COD_CAND" : 130860,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "ALEJANDRA MONASTERIO RIVAS"
    },
    "130861":{
      "COD_CAND" : 130861,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "CECILIA VALDES LEON"
    },
    "130862":{
      "COD_CAND" : 130862,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 89,
      "GLOSA_CAND": "EVELYN DIANA GOMEZ HIDALGO"
    },
    "130863":{
      "COD_CAND" : 130863,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 90,
      "GLOSA_CAND": "ALEJANDRA BRAVO HIDALGO"
    },
    "130864":{
      "COD_CAND" : 130864,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 91,
      "GLOSA_CAND": "SEBASTIAN  LAFAURIE RIVERA"
    },
    "130865":{
      "COD_CAND" : 130865,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 92,
      "GLOSA_CAND": "PABLO  GALDAMES DIAZ"
    },
    "130866":{
      "COD_CAND" : 130866,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 93,
      "GLOSA_CAND": "MARIO  DESBORDES JIMENEZ"
    },
    "130867":{
      "COD_CAND" : 130867,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 94,
      "GLOSA_CAND": "ANDREA  OJEDA MIRANDA"
    },
    "130868":{
      "COD_CAND" : 130868,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 95,
      "GLOSA_CAND": "PERLA SEGUEL TORRES"
    },
    "130869":{
      "COD_CAND" : 130869,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 96,
      "GLOSA_CAND": "PATRICIO MELERO ABAROA"
    },
    "130870":{
      "COD_CAND" : 130870,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 97,
      "GLOSA_CAND": "PIA  MARGARIT BAHAMONDE"
    },
    "130871":{
      "COD_CAND" : 130871,
      "COD_ZONA": 6008,
      "CAN_ORDEN": 98,
      "GLOSA_CAND": "JOAQUIN  LAVIN LEON"
    },
    "130872":{
      "COD_CAND" : 130872,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "ANDREA  CONDEMARIN FUENTES"
    },
    "130873":{
      "COD_CAND" : 130873,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "ALAN  FANCELLI ESMAR"
    },
    "130874":{
      "COD_CAND" : 130874,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "ALEJANDRA SOLANGE NAVARRETE CARRASCO"
    },
    "130875":{
      "COD_CAND" : 130875,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "PABLO FRANCISCO GONZALEZ VERA"
    },
    "130876":{
      "COD_CAND" : 130876,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "VALESKA DE LAS NIEVES OYARCE PENA"
    },
    "130877":{
      "COD_CAND" : 130877,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "ROBERTO JULIO MARTINEZ MALDONADO"
    },
    "130878":{
      "COD_CAND" : 130878,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "JORGE EDUARDO ROSALES VARGAS"
    },
    "130879":{
      "COD_CAND" : 130879,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "MAITE ORSINI PASCAL"
    },
    "130880":{
      "COD_CAND" : 130880,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "PABLO ALEXIS PADILLA RUBIO"
    },
    "130881":{
      "COD_CAND" : 130881,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "ERICK MARCO ANTONIO CONOMAN GARAY"
    },
    "130882":{
      "COD_CAND" : 130882,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "KIMBERLY GREDY SEGUEL VILLAGRAN"
    },
    "130883":{
      "COD_CAND" : 130883,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "CATALINA VIDAL MENDEZ"
    },
    "130884":{
      "COD_CAND" : 130884,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "DIEGO  VILLAS MONTECINOS"
    },
    "130885":{
      "COD_CAND" : 130885,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "SANDRA ELENA URIBE FLORES"
    },
    "130886":{
      "COD_CAND" : 130886,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "ALEJANDRO ARAVENA EGANA"
    },
    "130887":{
      "COD_CAND" : 130887,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "GUIDO GONZALEZ VALENZUELA"
    },
    "130888":{
      "COD_CAND" : 130888,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "VALERIA SOTO CARRASCO"
    },
    "130889":{
      "COD_CAND" : 130889,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "ANA MARIA ARIAS AGURTO"
    },
    "130890":{
      "COD_CAND" : 130890,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "GONZALO NAVARRETE MUNOZ"
    },
    "130891":{
      "COD_CAND" : 130891,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "CRISTINA GIRARDI LAVIN"
    },
    "130892":{
      "COD_CAND" : 130892,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "ALEJANDRA SAA CARRASCO"
    },
    "130893":{
      "COD_CAND" : 130893,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "KAROL  CARIOLA OLIVA"
    },
    "130894":{
      "COD_CAND" : 130894,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "BORIS ANTHONY BARRERA MORENO"
    },
    "130895":{
      "COD_CAND" : 130895,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "KARINA DELFINO MUSSA"
    },
    "130896":{
      "COD_CAND" : 130896,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "FRANCISCO FLORES RIQUELME"
    },
    "130897":{
      "COD_CAND" : 130897,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "PILAR DURAN BARONTI"
    },
    "130898":{
      "COD_CAND" : 130898,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "CRISTIAN BOWEN GARFIAS"
    },
    "130899":{
      "COD_CAND" : 130899,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "RODRIGO ALBORNOZ POLLMANN"
    },
    "130900":{
      "COD_CAND" : 130900,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "HUMBERTO SANHUEZA BARRIGA"
    },
    "130901":{
      "COD_CAND" : 130901,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "CAROLINA MORA SAA"
    },
    "130902":{
      "COD_CAND" : 130902,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "MIJAIL BONITO LOVIO"
    },
    "130903":{
      "COD_CAND" : 130903,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "SEBASTIAN KEITEL BIANCHI"
    },
    "130904":{
      "COD_CAND" : 130904,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "JORGE DURAN ESPINOZA"
    },
    "130905":{
      "COD_CAND" : 130905,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "KATHERINE  MARTORELL AWAD"
    },
    "130906":{
      "COD_CAND" : 130906,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "ERIKA OLIVERA DE LA FUENTE"
    },
    "130907":{
      "COD_CAND" : 130907,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "CLAUDIA  NOGUEIRA FERNANDEZ"
    },
    "130908":{
      "COD_CAND" : 130908,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "LORETO LETELIER BARRERA"
    },
    "130909":{
      "COD_CAND" : 130909,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "CRISTOBAL LETURIA INFANTE"
    },
    "130910":{
      "COD_CAND" : 130910,
      "COD_ZONA": 6009,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "EDDY  ROLDAN CABRERA"
    },
    "130634":{
      "COD_CAND" : 130634,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "ANDREA  DUARTE VIDAL"
    },
    "130635":{
      "COD_CAND" : 130635,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "RAUL ALBERTO VILLAVICENCIO MARABOLI"
    },
    "130636":{
      "COD_CAND" : 130636,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "CAROLINA DE LA PAZ HINOJOSA CANALES"
    },
    "130637":{
      "COD_CAND" : 130637,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "JORGE ALBERTO ARROYO SALGADO"
    },
    "130638":{
      "COD_CAND" : 130638,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "JULIAN ALCAYAGA OLIVARES"
    },
    "130639":{
      "COD_CAND" : 130639,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "PATRICIA ELENA VALENZUELA SANDOVAL"
    },
    "130640":{
      "COD_CAND" : 130640,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "RODOLFO JAVIER CHELME BUSTOS"
    },
    "130641":{
      "COD_CAND" : 130641,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "GIORGIO JACKSON DRAGO"
    },
    "130642":{
      "COD_CAND" : 130642,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "NATALIA CASTILLO MUNOZ"
    },
    "130643":{
      "COD_CAND" : 130643,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "GONZALO WINTER ETCHEBERRY"
    },
    "130644":{
      "COD_CAND" : 130644,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "FRANCISCO  FIGUEROA CERDA"
    },
    "130645":{
      "COD_CAND" : 130645,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "PATRICIO ANTONIO NEIRA PEZOA"
    },
    "130646":{
      "COD_CAND" : 130646,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "ROSARIO ANDREA OLIVARES SAAVEDRA"
    },
    "130647":{
      "COD_CAND" : 130647,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "JUAN CARLOS SANTIBANEZ NORAMBUENA"
    },
    "130648":{
      "COD_CAND" : 130648,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "CHRISTIAN  PULGAR TOLEDO"
    },
    "130649":{
      "COD_CAND" : 130649,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "ALBERTO MAYOL MIRANDA"
    },
    "130650":{
      "COD_CAND" : 130650,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "FABIAN VASQUEZ SILVA"
    },
    "130651":{
      "COD_CAND" : 130651,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "SERGIO GOMEZ CELEDON"
    },
    "130652":{
      "COD_CAND" : 130652,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "ANGELICA CORNEJO ZUNIGA"
    },
    "130653":{
      "COD_CAND" : 130653,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "CARLA GONZALEZ MORALES"
    },
    "130654":{
      "COD_CAND" : 130654,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "NINOZKA SANCHEZ CARO"
    },
    "130655":{
      "COD_CAND" : 130655,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "CHRISTIAN JORQUERA TAPIA"
    },
    "130656":{
      "COD_CAND" : 130656,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "JULIA  URQUIETA OLIVARES"
    },
    "130657":{
      "COD_CAND" : 130657,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "JAVIERA  OLIVARES MARDONES"
    },
    "130658":{
      "COD_CAND" : 130658,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "RAMON  FARIAS PONCE"
    },
    "130659":{
      "COD_CAND" : 130659,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "VERONICA PAULINA PINILLA MARTINEZ"
    },
    "130660":{
      "COD_CAND" : 130660,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "MAYA  FERNANDEZ ALLENDE"
    },
    "130661":{
      "COD_CAND" : 130661,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "FRANCYS  FOIX FUENTEALBA"
    },
    "130662":{
      "COD_CAND" : 130662,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "VALESKA  NARANJO DAWSON"
    },
    "130663":{
      "COD_CAND" : 130663,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "CAROLA ANDREA SOLIS ARAYA"
    },
    "130664":{
      "COD_CAND" : 130664,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "CLAUDIO  ARRIAGADA MACAYA"
    },
    "130665":{
      "COD_CAND" : 130665,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "EVA JIMENEZ URIZAR"
    },
    "130666":{
      "COD_CAND" : 130666,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "NICOLAS MUNOZ MONTES"
    },
    "130667":{
      "COD_CAND" : 130667,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "TITO JAIME MONJE PINCHEIRA"
    },
    "130668":{
      "COD_CAND" : 130668,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "PEDRO ENRIQUE GARCIA ASPILLAGA"
    },
    "130669":{
      "COD_CAND" : 130669,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "ANA MARIA HERNANDEZ SAN MARTIN"
    },
    "130670":{
      "COD_CAND" : 130670,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "MARCELA  SABAT FERNANDEZ"
    },
    "130671":{
      "COD_CAND" : 130671,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "ROSA OYARCE SUAZO"
    },
    "130672":{
      "COD_CAND" : 130672,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "SEBASTIAN TORREALBA ALVARADO"
    },
    "130673":{
      "COD_CAND" : 130673,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 89,
      "GLOSA_CAND": "JORGE ALESSANDRI VERGARA"
    },
    "130674":{
      "COD_CAND" : 130674,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 90,
      "GLOSA_CAND": "CAROLINA LAVIN ALIAGA"
    },
    "130675":{
      "COD_CAND" : 130675,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 91,
      "GLOSA_CAND": "JULIO ISAMIT DIAZ"
    },
    "130676":{
      "COD_CAND" : 130676,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 92,
      "GLOSA_CAND": "LUCIANO CRUZ-COKE CARVALLO"
    },
    "130677":{
      "COD_CAND" : 130677,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 93,
      "GLOSA_CAND": "LUIS LARRAIN STIEB"
    },
    "130678":{
      "COD_CAND" : 130678,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 94,
      "GLOSA_CAND": "CECILIA AGUAYO IPINZA"
    },
    "130679":{
      "COD_CAND" : 130679,
      "COD_ZONA": 6010,
      "CAN_ORDEN": 95,
      "GLOSA_CAND": "DAUNO TOTORO NAVARRO"
    },
    "130680":{
      "COD_CAND" : 130680,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "NICOLAS MENARE MORALES"
    },
    "130681":{
      "COD_CAND" : 130681,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "MIGUEL ANGEL RENDON ESCOBAR"
    },
    "130682":{
      "COD_CAND" : 130682,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "ELIANA MELENDEZ MOLINA"
    },
    "130683":{
      "COD_CAND" : 130683,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "LILIAN DEL CARMEN QUILAQUEO SOZA"
    },
    "130684":{
      "COD_CAND" : 130684,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ANA INES MARDONES CONTRERAS"
    },
    "130685":{
      "COD_CAND" : 130685,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "JOHANNA DUARTE NARVAEZ"
    },
    "130686":{
      "COD_CAND" : 130686,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "MARCELA YANEZ BRAVO"
    },
    "130687":{
      "COD_CAND" : 130687,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "TOMAS HIRSCH GOLDSCHMIDT"
    },
    "130688":{
      "COD_CAND" : 130688,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "FERNANDO ANDRES ENCINA WAISSBLUTH"
    },
    "130689":{
      "COD_CAND" : 130689,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "SOLEDAD ALAMOS FUENZALIDA"
    },
    "130690":{
      "COD_CAND" : 130690,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "MANUELA ALEJANDRA VELOSO DORNER"
    },
    "130691":{
      "COD_CAND" : 130691,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "NATALIA  GARRIDO TORO"
    },
    "130692":{
      "COD_CAND" : 130692,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CAMILA  IANISZEWSKI CONTRERAS"
    },
    "130693":{
      "COD_CAND" : 130693,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "WILLEM SCHUITEMAKER TRUFFELLO"
    },
    "130694":{
      "COD_CAND" : 130694,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "ARTURO MUNOZ ZUNIGA"
    },
    "130695":{
      "COD_CAND" : 130695,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "LUISA PARRA CASTRO"
    },
    "130696":{
      "COD_CAND" : 130696,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PABLO PRADO PULIDO"
    },
    "130697":{
      "COD_CAND" : 130697,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "LUCIA RIVERA HAUENSTEIN"
    },
    "130698":{
      "COD_CAND" : 130698,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "RENE  JOFRE DELGADO"
    },
    "130699":{
      "COD_CAND" : 130699,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "ENRIQUE LUIS ACCORSI OPAZO"
    },
    "130700":{
      "COD_CAND" : 130700,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "PATRICIA ANITA COCAS GONZALEZ"
    },
    "130701":{
      "COD_CAND" : 130701,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "MAURICIO JAVIER ABU-GHOSH PARHAM"
    },
    "130702":{
      "COD_CAND" : 130702,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "FERNANDO ATRIA LEMAITRE"
    },
    "130703":{
      "COD_CAND" : 130703,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "VIVIENNE  BACHELET NORELLI"
    },
    "130704":{
      "COD_CAND" : 130704,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "TAMARA MOYA MOYANO"
    },
    "130705":{
      "COD_CAND" : 130705,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "CRISTOBAL ACEVEDO FERRER"
    },
    "130706":{
      "COD_CAND" : 130706,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "PAULINA PINEIRO UGARTE"
    },
    "130707":{
      "COD_CAND" : 130707,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "NELSON HADAD HERESY"
    },
    "130708":{
      "COD_CAND" : 130708,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "JULIO ENRIQUE MADRID FUENTES"
    },
    "130709":{
      "COD_CAND" : 130709,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "CECILIA GONZALEZ HANSEN"
    },
    "130710":{
      "COD_CAND" : 130710,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "FRANCISCO UNDURRAGA GAZITUA"
    },
    "130711":{
      "COD_CAND" : 130711,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "GONZALO  FUENZALIDA FIGUEROA"
    },
    "130712":{
      "COD_CAND" : 130712,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "CATALINA DEL REAL MIHOVILOVIC"
    },
    "130713":{
      "COD_CAND" : 130713,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "KARIN  LUCK URBAN"
    },
    "130714":{
      "COD_CAND" : 130714,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "PABLO  TERRAZAS LAGOS"
    },
    "130715":{
      "COD_CAND" : 130715,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "BARBARA  SOTO SILVA"
    },
    "130716":{
      "COD_CAND" : 130716,
      "COD_ZONA": 6011,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "GUILLERMO  RAMIREZ DIEZ"
    },
    "130717":{
      "COD_CAND" : 130717,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "DAVID  HENRIQUEZ ESPINOZA"
    },
    "130718":{
      "COD_CAND" : 130718,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "JOSE ANTONIO  HENRIQUEZ MUNIZ"
    },
    "130719":{
      "COD_CAND" : 130719,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "VICTOR HUGO ORTIZ AVENDANO"
    },
    "130720":{
      "COD_CAND" : 130720,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "GLADYS LORENA PALMA VALLEJOS"
    },
    "130721":{
      "COD_CAND" : 130721,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "MARIBEL  MARTINEZ LOPEZ"
    },
    "130722":{
      "COD_CAND" : 130722,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "CLAUDIO  HERRERA SAN MARTIN"
    },
    "130723":{
      "COD_CAND" : 130723,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "PABLINA MELLA DIAZ"
    },
    "130724":{
      "COD_CAND" : 130724,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "MIGUEL  CRISPI SERRANO"
    },
    "130725":{
      "COD_CAND" : 130725,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "ANDREA STELLA SALAZAR NAVIA"
    },
    "130726":{
      "COD_CAND" : 130726,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "KARINA LORETTA OLIVA PEREZ"
    },
    "130727":{
      "COD_CAND" : 130727,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "LUIS MARIANO RENDON ESCOBAR"
    },
    "130728":{
      "COD_CAND" : 130728,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "JOSE ANSELMO HIDALGO ZAMORA"
    },
    "130729":{
      "COD_CAND" : 130729,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "PAMELA JILES MORENO"
    },
    "130730":{
      "COD_CAND" : 130730,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "VALENTINA OLIVARES GRAY"
    },
    "130731":{
      "COD_CAND" : 130731,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "SUSANA ROSA HERNANDEZ COLLAO"
    },
    "130732":{
      "COD_CAND" : 130732,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "EDUARDO GUTIERREZ GONZALEZ"
    },
    "130733":{
      "COD_CAND" : 130733,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "JORGE ARAVENA EGANA"
    },
    "130734":{
      "COD_CAND" : 130734,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "PATRICIO ALTAMIRANO ARANCIBIA"
    },
    "130735":{
      "COD_CAND" : 130735,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "GLORIA ANGELICA GALLARDO VOSS"
    },
    "130736":{
      "COD_CAND" : 130736,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "CAMILA  VALLEJO DOWLING"
    },
    "130737":{
      "COD_CAND" : 130737,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "AMARO  LABRA SEPULVEDA"
    },
    "130738":{
      "COD_CAND" : 130738,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "OSVALDO  ANDRADE LARA"
    },
    "130739":{
      "COD_CAND" : 130739,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "MARIA JOSE BECERRA MORO"
    },
    "130740":{
      "COD_CAND" : 130740,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "LORENA PATRICIA BUSTAMANTE MEDINA"
    },
    "130741":{
      "COD_CAND" : 130741,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "JAIME FRANCISCO ESCUDERO RAMOS"
    },
    "130742":{
      "COD_CAND" : 130742,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "MELISSA ESTEFANIA VARAS MONTERO"
    },
    "130743":{
      "COD_CAND" : 130743,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "PAZ ANDREA SUAREZ BRIONES"
    },
    "130744":{
      "COD_CAND" : 130744,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "NICOLE VANESSA VALDEBENITO TORRES"
    },
    "130745":{
      "COD_CAND" : 130745,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "MARCELA CANALES HIPP"
    },
    "130746":{
      "COD_CAND" : 130746,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "LEOPOLDO  PEREZ LAHSEN"
    },
    "130747":{
      "COD_CAND" : 130747,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "XIMENA OSSANDON IRARRAZABAL"
    },
    "130748":{
      "COD_CAND" : 130748,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "OLGA  GONZALEZ DEL RIEGO GARCIA"
    },
    "130749":{
      "COD_CAND" : 130749,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "MICHELLE  ZUNINO PALACIOS"
    },
    "130750":{
      "COD_CAND" : 130750,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "CLAUDIA  LANGE FARIAS"
    },
    "130751":{
      "COD_CAND" : 130751,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "MARIA TERESA LAVIN ALIAGA"
    },
    "130752":{
      "COD_CAND" : 130752,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "KARLA  JANA VALENZUELA"
    },
    "130753":{
      "COD_CAND" : 130753,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "ALVARO  CARTER FERNANDEZ"
    },
    "130754":{
      "COD_CAND" : 130754,
      "COD_ZONA": 6012,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "PATRICIO  MUNOZ CARDENAS"
    },
    "130755":{
      "COD_CAND" : 130755,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "MARCIA ALEJANDRA SOTO DIAZ"
    },
    "130756":{
      "COD_CAND" : 130756,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "ANA MARIA TOLEDO GUERRA"
    },
    "130757":{
      "COD_CAND" : 130757,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "PATRICIO ALFONSO JIMENEZ GONZALEZ"
    },
    "130758":{
      "COD_CAND" : 130758,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "OSCAR FELIPE VARGAS COLOMA"
    },
    "130759":{
      "COD_CAND" : 130759,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "CRISTIAN MARCELO DIAZ MUNOZ"
    },
    "130760":{
      "COD_CAND" : 130760,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "JUAN MANUEL CASAS QUINTRIQUEO"
    },
    "130761":{
      "COD_CAND" : 130761,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "GAEL FERNANDA YEOMANS ARAYA"
    },
    "130762":{
      "COD_CAND" : 130762,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "CESAR ALEJANDRO SOLIS SANCHEZ"
    },
    "130763":{
      "COD_CAND" : 130763,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "IRMA EDITH PEREZ LLANQUIN"
    },
    "130764":{
      "COD_CAND" : 130764,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "MAURICIO ALEJANDRO CARRASCO NUNEZ"
    },
    "130765":{
      "COD_CAND" : 130765,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "GABRIEL ESTEBAN BRAVO MUNOZ"
    },
    "130766":{
      "COD_CAND" : 130766,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "CARLOS  ASTUDILLO LOPEZ"
    },
    "130767":{
      "COD_CAND" : 130767,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "GUILLERMO  ARAVENA MONDACA"
    },
    "130768":{
      "COD_CAND" : 130768,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "GASPAR ORTIZ CARDENAS"
    },
    "130769":{
      "COD_CAND" : 130769,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "CRISTIAN  BAEZA FIGUEROA"
    },
    "130770":{
      "COD_CAND" : 130770,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "ZAIDA  FALCES SALAZAR"
    },
    "130771":{
      "COD_CAND" : 130771,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "GUILLERMO TEILLIER DEL VALLE"
    },
    "130772":{
      "COD_CAND" : 130772,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "JAVIERA PAZ REYES JARA"
    },
    "130773":{
      "COD_CAND" : 130773,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "TUCAPEL FRANCISCO JIMENEZ FUENTES"
    },
    "130774":{
      "COD_CAND" : 130774,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "CAMILA  BRUNA FAUNDES"
    },
    "130775":{
      "COD_CAND" : 130775,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "DANIEL MELO CONTRERAS"
    },
    "130776":{
      "COD_CAND" : 130776,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "JAIME FUENTEALBA MALDONADO"
    },
    "130777":{
      "COD_CAND" : 130777,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "IVAN FUENTES CASTILLO"
    },
    "130778":{
      "COD_CAND" : 130778,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "RODOLFO  SEGUEL MOLINA"
    },
    "130779":{
      "COD_CAND" : 130779,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "GEORGINA MARIA VILLABLANCA MARTINEZ"
    },
    "130780":{
      "COD_CAND" : 130780,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "TAMARA MORA MORA"
    },
    "130781":{
      "COD_CAND" : 130781,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "LUIS ACEVEDO QUINTANILLA"
    },
    "130782":{
      "COD_CAND" : 130782,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "CRISTHIAN  MOREIRA BARROS"
    },
    "130783":{
      "COD_CAND" : 130783,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "ALVARO PILLADO IRRIBARRA"
    },
    "130784":{
      "COD_CAND" : 130784,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "VERONICA GARRIDO BELLO"
    },
    "130785":{
      "COD_CAND" : 130785,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "LORETO  SEGUEL KING"
    },
    "130786":{
      "COD_CAND" : 130786,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "EDUARDO  DURAN SALINAS"
    },
    "130787":{
      "COD_CAND" : 130787,
      "COD_ZONA": 6013,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "MACARENA  DONOSO ROJAS"
    },
    "130788":{
      "COD_CAND" : 130788,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "MARISELA  SANTIBANEZ NOVOA"
    },
    "130789":{
      "COD_CAND" : 130789,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "SUSANA  SANCHEZ GONGORA"
    },
    "130790":{
      "COD_CAND" : 130790,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "JOSE LUIS STARK AGUILERA"
    },
    "130791":{
      "COD_CAND" : 130791,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "MARCIA MILLAQUEO IBARRA"
    },
    "130792":{
      "COD_CAND" : 130792,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "CLAUDIO PATRICIO MONTERO ARRATIA"
    },
    "130793":{
      "COD_CAND" : 130793,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "MARCELO VERA PAVEZ"
    },
    "130794":{
      "COD_CAND" : 130794,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "DAVID ALEXIS SALDANA VERGARA"
    },
    "130795":{
      "COD_CAND" : 130795,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "FRANCISCO VALDES REYES"
    },
    "130796":{
      "COD_CAND" : 130796,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "ALEJANDRO IGNACIO IRARRAZABAL SALDIAS"
    },
    "130797":{
      "COD_CAND" : 130797,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "CARLOS ALBERTO NAVARRO GALLINATO"
    },
    "130798":{
      "COD_CAND" : 130798,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "JUAN DOMINGO VILLAVICENCIO PASTEN"
    },
    "130799":{
      "COD_CAND" : 130799,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "ALENA  GUTIERREZ MORENO"
    },
    "130800":{
      "COD_CAND" : 130800,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "VICTOR RICARDO  CERDA MORALES"
    },
    "130801":{
      "COD_CAND" : 130801,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "RENATO  GARIN GONZALEZ"
    },
    "130802":{
      "COD_CAND" : 130802,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "ROBERTO CARLOS OVALLE MOLINA"
    },
    "130803":{
      "COD_CAND" : 130803,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "MARIO FAJARDO PEREZ"
    },
    "130804":{
      "COD_CAND" : 130804,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "DENNY FLORES BUSTOS"
    },
    "130805":{
      "COD_CAND" : 130805,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "ESTEBAN CACERES OLAVE"
    },
    "130806":{
      "COD_CAND" : 130806,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "CLARA RIOS LUEIZA"
    },
    "130807":{
      "COD_CAND" : 130807,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "LEONARDO  SOTO FERRADA"
    },
    "130808":{
      "COD_CAND" : 130808,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "RAUL  LEIVA CARVAJAL"
    },
    "130809":{
      "COD_CAND" : 130809,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "YOVANNA  FUENTES PINO"
    },
    "130810":{
      "COD_CAND" : 130810,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "MARCIA  GONZALEZ NAVARRO"
    },
    "130811":{
      "COD_CAND" : 130811,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "MARIO  GEBAUER BRINGAS"
    },
    "130812":{
      "COD_CAND" : 130812,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "ANGEL RICARDO BOZAN RAMOS"
    },
    "130813":{
      "COD_CAND" : 130813,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "DIEGO EDUARDO CALDERON GAJARDO"
    },
    "130814":{
      "COD_CAND" : 130814,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "PAULA  ZUNIGA CALDERON"
    },
    "130815":{
      "COD_CAND" : 130815,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "DAVID  MORALES NORDETTI"
    },
    "130816":{
      "COD_CAND" : 130816,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "ALEJANDRA  NOVOA SANDOVAL"
    },
    "130817":{
      "COD_CAND" : 130817,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "ANGELICA PINO SAN MARTIN"
    },
    "130818":{
      "COD_CAND" : 130818,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "ANTONIO  HORVATH GUTIERREZ"
    },
    "130819":{
      "COD_CAND" : 130819,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "JUAN ANTONIO COLOMA ALAMOS"
    },
    "130820":{
      "COD_CAND" : 130820,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "JAIME  BELLOLIO AVARIA"
    },
    "130821":{
      "COD_CAND" : 130821,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "ISABEL MARGARITA LABBE MARTINEZ"
    },
    "130822":{
      "COD_CAND" : 130822,
      "COD_ZONA": 6014,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "JUANA  SILVA MUNOZ"
    },
    "130245":{
      "COD_CAND" : 130245,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "FILOMENA  CARMEN URBINA CAMPOS"
    },
    "130246":{
      "COD_CAND" : 130246,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "BENJAMIN NELSON ARANEDA GONZALEZ"
    },
    "130247":{
      "COD_CAND" : 130247,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "VICTOR HUGO CAMILLA REYES"
    },
    "130248":{
      "COD_CAND" : 130248,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "ANDRES ESTEBAN MORENO BILBAO"
    },
    "130249":{
      "COD_CAND" : 130249,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "CRISTIAN ALFREDO ROJAS DONOSO"
    },
    "130250":{
      "COD_CAND" : 130250,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "CRISTIAN ALEJANDRO PALMA SANHUEZA"
    },
    "130251":{
      "COD_CAND" : 130251,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "KARIN RODRIGO BELMAR GUZMAN"
    },
    "130252":{
      "COD_CAND" : 130252,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "MARIA MARGARITA CRESPO URETA"
    },
    "130253":{
      "COD_CAND" : 130253,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "JOAQUIN ARDUENGO NAREDO"
    },
    "130254":{
      "COD_CAND" : 130254,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "GRACIELA FILOMENA ISLA PEREZ"
    },
    "130255":{
      "COD_CAND" : 130255,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "ANDRES ALFONSO GUILLERMO LEAL ALVARADO"
    },
    "130256":{
      "COD_CAND" : 130256,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "ALFREDO VALENZUELA BRICENO"
    },
    "130257":{
      "COD_CAND" : 130257,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CATHERINE FIELDHOUSE ALARCON"
    },
    "130258":{
      "COD_CAND" : 130258,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CAROL HOLE BELL"
    },
    "130259":{
      "COD_CAND" : 130259,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "CARLOS ENRIQUE ORTEGA AEDO"
    },
    "130260":{
      "COD_CAND" : 130260,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "GUIDO MIRANDA ROJAS"
    },
    "130261":{
      "COD_CAND" : 130261,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "EMMANUEL LARA ACEVEDO"
    },
    "130262":{
      "COD_CAND" : 130262,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "PABLO CAROCA VASQUEZ"
    },
    "130263":{
      "COD_CAND" : 130263,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "MARIANA CONCHA MUNOZ"
    },
    "130264":{
      "COD_CAND" : 130264,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "LESLIE DIAZ SILVA"
    },
    "130265":{
      "COD_CAND" : 130265,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "FELIPE LETELIER NORAMBUENA"
    },
    "130266":{
      "COD_CAND" : 130266,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "EDUARDO JAVIER VERGARA BOLBARAN"
    },
    "130267":{
      "COD_CAND" : 130267,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "JUAN LUIS CASTRO GONZALEZ"
    },
    "130268":{
      "COD_CAND" : 130268,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "OSCAR  AVILA MENDEZ"
    },
    "130269":{
      "COD_CAND" : 130269,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "RODOLFO ARTURO NUNEZ BUSTAMANTE"
    },
    "130270":{
      "COD_CAND" : 130270,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "FRANCISCO ALEJANDRO PARRAGUEZ LEIVA"
    },
    "130271":{
      "COD_CAND" : 130271,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "JUAN CARLOS OLMOS CASTRO"
    },
    "130272":{
      "COD_CAND" : 130272,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "ALEJANDRO ANSELMO SULE FERNANDEZ"
    },
    "130273":{
      "COD_CAND" : 130273,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "ANDRES LORCA SAAVEDRA"
    },
    "130274":{
      "COD_CAND" : 130274,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "JORGE  FREI TOLEDO"
    },
    "130275":{
      "COD_CAND" : 130275,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "RAUL  SOTO MARDONES"
    },
    "130276":{
      "COD_CAND" : 130276,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "JAVIER MACAYA DANUS"
    },
    "130277":{
      "COD_CAND" : 130277,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "PAULINA ORELLANA AUIL"
    },
    "130278":{
      "COD_CAND" : 130278,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "ISSA  KORT GARRIGA"
    },
    "130279":{
      "COD_CAND" : 130279,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "IVONNE MANGELSDORFF GALEB"
    },
    "130280":{
      "COD_CAND" : 130280,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "PAMELA MEDINA SCHULZ"
    },
    "130281":{
      "COD_CAND" : 130281,
      "COD_ZONA": 6015,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "DIEGO SCHALPER SEPULVEDA"
    },
    "130282":{
      "COD_CAND" : 130282,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "NILZA MARCELA ROJAS ESCHMANN"
    },
    "130283":{
      "COD_CAND" : 130283,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "JOSE GREGORIO ARGOMEDO SERRE"
    },
    "130284":{
      "COD_CAND" : 130284,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "RUBEN EDUARDO ANDINO MALDONADO"
    },
    "130285":{
      "COD_CAND" : 130285,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "ADRIANA ROMAN TAPIA"
    },
    "130286":{
      "COD_CAND" : 130286,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "RICARDO LISBOA HENRIQUEZ"
    },
    "130287":{
      "COD_CAND" : 130287,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "CLAUDIO CANEPA BLUMENBERG"
    },
    "130288":{
      "COD_CAND" : 130288,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "MARIA TERESA HUALA MOLINA"
    },
    "130289":{
      "COD_CAND" : 130289,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "ALEJANDRA  SEPULVEDA ORBENES"
    },
    "130290":{
      "COD_CAND" : 130290,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "LISETTE HEDWIG BOSSHARD PENA"
    },
    "130291":{
      "COD_CAND" : 130291,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "VICTOR  BUSTAMANTE GALAZ"
    },
    "130292":{
      "COD_CAND" : 130292,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "ALEXANDRA  DIAZ MUNOZ"
    },
    "130293":{
      "COD_CAND" : 130293,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "KATHERINE ROJAS PENAILILLO"
    },
    "130294":{
      "COD_CAND" : 130294,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "COSME  MELLADO PINO"
    },
    "130295":{
      "COD_CAND" : 130295,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CAMILO ARTURO GARCIA DE LA BARRA"
    },
    "130296":{
      "COD_CAND" : 130296,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "MORIN  CONTRERAS CONCHA"
    },
    "130297":{
      "COD_CAND" : 130297,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "SERGIO ARIEL SALAZAR MEZA"
    },
    "130298":{
      "COD_CAND" : 130298,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "CAROLINA ELSA ICELA CUCUMIDES CALDERON"
    },
    "130299":{
      "COD_CAND" : 130299,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "JUAN CARLOS LATORRE CARMONA"
    },
    "130300":{
      "COD_CAND" : 130300,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "MACARENA ZAPATA MARMOLEJO"
    },
    "130301":{
      "COD_CAND" : 130301,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "PATRICIA ANDREA IBACETA SANTOS"
    },
    "130302":{
      "COD_CAND" : 130302,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "SEBASTIAN LLANTEN MORALES"
    },
    "130303":{
      "COD_CAND" : 130303,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "CHRISTIAN ANDRES GONZALEZ GONZALEZ"
    },
    "130304":{
      "COD_CAND" : 130304,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "JULIO  IBARRA MALDONADO"
    },
    "130305":{
      "COD_CAND" : 130305,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "MAURICIO BOZA GONZALEZ"
    },
    "130306":{
      "COD_CAND" : 130306,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "RAMON  BARROS MONTERO"
    },
    "130307":{
      "COD_CAND" : 130307,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "VIRGINIA  TRONCOSO HELLMAN"
    },
    "130308":{
      "COD_CAND" : 130308,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "VANESSA  CABELLO PARADA"
    },
    "130309":{
      "COD_CAND" : 130309,
      "COD_ZONA": 6016,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "SAMUEL FLORES GARRIDO"
    },
    "130310":{
      "COD_CAND" : 130310,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "FERNANDO ADILIO LEAL ARAVENA"
    },
    "130311":{
      "COD_CAND" : 130311,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "PAMELA ANDREA VALENZUELA CASTILLO"
    },
    "130312":{
      "COD_CAND" : 130312,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "IVONNE CECILIA SAN LUIS GONZALEZ"
    },
    "130313":{
      "COD_CAND" : 130313,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "JOSE MANUEL ARAYA ARAYA"
    },
    "130314":{
      "COD_CAND" : 130314,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ROSA DEL CARMEN RIQUELME ANDRADES"
    },
    "130315":{
      "COD_CAND" : 130315,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "GENARO MALDONADO PALMA"
    },
    "130316":{
      "COD_CAND" : 130316,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "NATALY  ROJAS SEGUEL"
    },
    "130317":{
      "COD_CAND" : 130317,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "VALERIA  ORTEGA CADENA"
    },
    "130318":{
      "COD_CAND" : 130318,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "MARCELO  RIOSECO PAIS"
    },
    "130319":{
      "COD_CAND" : 130319,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "IVAN  SEPULVEDA SEPULVEDA"
    },
    "130320":{
      "COD_CAND" : 130320,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "FLORCITA ALARCON ROJAS"
    },
    "130321":{
      "COD_CAND" : 130321,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "JUAN HERRERA FUENTES"
    },
    "130322":{
      "COD_CAND" : 130322,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "GABRIELA  TORRES PINCHEIRA"
    },
    "130323":{
      "COD_CAND" : 130323,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "IVAN MORAN MORAN"
    },
    "130324":{
      "COD_CAND" : 130324,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "JUAN FRANCISCO PULGAR CASTILLO"
    },
    "130325":{
      "COD_CAND" : 130325,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "CARLOS IVAN MUNOZ ARAYA"
    },
    "130326":{
      "COD_CAND" : 130326,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "LILIAN TAMARA UNDA ESCALONA"
    },
    "130327":{
      "COD_CAND" : 130327,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "MAURICIO ANDRES AGUILERA MUNOZ"
    },
    "130328":{
      "COD_CAND" : 130328,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "VALERIA BEATRIZ FANNY ARANCIBIA LUCO"
    },
    "130329":{
      "COD_CAND" : 130329,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "ALEXIS SEPULVEDA SOTO"
    },
    "130330":{
      "COD_CAND" : 130330,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "JUAN ENRIQUE NORAMBUENA MATUS"
    },
    "130331":{
      "COD_CAND" : 130331,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "JORGE CESPEDES POZO"
    },
    "130332":{
      "COD_CAND" : 130332,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "BORIS DURAN REYES"
    },
    "130333":{
      "COD_CAND" : 130333,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "ALEJANDRA SUAZO DURAN"
    },
    "130334":{
      "COD_CAND" : 130334,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "BEATRIZ VILLENA ROCO"
    },
    "130335":{
      "COD_CAND" : 130335,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "HENRY BERNARD VARAS CONCHA"
    },
    "130336":{
      "COD_CAND" : 130336,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "ROBERTO EDUARDO LEON RAMIREZ"
    },
    "130337":{
      "COD_CAND" : 130337,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "PABLO LORENZINI BASSO"
    },
    "130338":{
      "COD_CAND" : 130338,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "CLAUDIA ANDREA SANCHEZ SOTO"
    },
    "130339":{
      "COD_CAND" : 130339,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "GRACIELA TORRES INOSTROZA"
    },
    "130340":{
      "COD_CAND" : 130340,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "VIVIANA ALEJANDRA GULPPI RUBIO"
    },
    "130341":{
      "COD_CAND" : 130341,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "CELSO MORALES MUNOZ"
    },
    "130342":{
      "COD_CAND" : 130342,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "ELENA  ROJAS ALVARADO"
    },
    "130343":{
      "COD_CAND" : 130343,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "PEDRO PABLO ALVAREZ-SALAMANCA RAMIREZ"
    },
    "130344":{
      "COD_CAND" : 130344,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "GERMAN VERDUGO SOTO"
    },
    "130345":{
      "COD_CAND" : 130345,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "ELIAS VISTOSO URRUTIA"
    },
    "130346":{
      "COD_CAND" : 130346,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "GONZALO  MONTERO VIVEROS"
    },
    "130347":{
      "COD_CAND" : 130347,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "HUGO  REY MARTINEZ"
    },
    "130348":{
      "COD_CAND" : 130348,
      "COD_ZONA": 6017,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "PABLO  PRIETO LORCA"
    },
    "130349":{
      "COD_CAND" : 130349,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "LORENA ARELLANO NAVARRO"
    },
    "130350":{
      "COD_CAND" : 130350,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "MARIA VERONICA SAAVEDRA HENRIQUEZ"
    },
    "130351":{
      "COD_CAND" : 130351,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "SAMUEL ABELARDO JIMENEZ MORAGA"
    },
    "130352":{
      "COD_CAND" : 130352,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "EUDALDO HERNAN GONZALEZ MANRIQUEZ"
    },
    "130353":{
      "COD_CAND" : 130353,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ALEXANDER MITJAEW PANASEWITSCH"
    },
    "130354":{
      "COD_CAND" : 130354,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "CARLOS ALBERTO TILLERIA GOMEZ"
    },
    "130355":{
      "COD_CAND" : 130355,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "LILIAN KARINA CANCINO HENRIQUEZ"
    },
    "130356":{
      "COD_CAND" : 130356,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "NATALIA ALEJANDRA VILLALOBOS CAMPOS"
    },
    "130357":{
      "COD_CAND" : 130357,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "SERGIO CARVAJAL CADIZ"
    },
    "130358":{
      "COD_CAND" : 130358,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "PAOLA ANDREA CABEZAS ZUNIGA"
    },
    "130359":{
      "COD_CAND" : 130359,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "GONZALO EDUARDO JARA ESPINOZA"
    },
    "130360":{
      "COD_CAND" : 130360,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "ALVARO ANDRES ARRIETA LEIVA"
    },
    "130361":{
      "COD_CAND" : 130361,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "JAIME  NARANJO ORTIZ"
    },
    "130362":{
      "COD_CAND" : 130362,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "GUILLERMO ARTURO CERONI FUENTES"
    },
    "130363":{
      "COD_CAND" : 130363,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "MARIA ANTONIETA ROJAS GONZALEZ"
    },
    "130364":{
      "COD_CAND" : 130364,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "FRANCISCA BASCUNAN PENA"
    },
    "130365":{
      "COD_CAND" : 130365,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "RODOLFO  ARANDA VILLANUEVA"
    },
    "130366":{
      "COD_CAND" : 130366,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "MANUEL JOSE MATTA ARAGAY"
    },
    "130367":{
      "COD_CAND" : 130367,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "CLAUDIA ARAVENA LAGOS"
    },
    "130368":{
      "COD_CAND" : 130368,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "PABLO GUTIERREZ PAREJA"
    },
    "130369":{
      "COD_CAND" : 130369,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "FELIPE  CISTERNAS SOBARZO"
    },
    "130370":{
      "COD_CAND" : 130370,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "ALEJANDRA RAMIREZ VERDUGO"
    },
    "130371":{
      "COD_CAND" : 130371,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "IGNACIO URRUTIA BONILLA"
    },
    "130372":{
      "COD_CAND" : 130372,
      "COD_ZONA": 6018,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "ROLANDO  RENTERIA MOLLER"
    },
    "130373":{
      "COD_CAND" : 130373,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "CRISTIAN ANTONIO QUIROZ REYES"
    },
    "130374":{
      "COD_CAND" : 130374,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "RICHARD MANUEL PINCHEIRA AEDO"
    },
    "130375":{
      "COD_CAND" : 130375,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "CARLOS HERNAN ASTETE ALVAREZ"
    },
    "130376":{
      "COD_CAND" : 130376,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "FANNY ESTHER ARAYA BAHAMONDE"
    },
    "130377":{
      "COD_CAND" : 130377,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ENRIQUE EDIS JARA RIVERA"
    },
    "130378":{
      "COD_CAND" : 130378,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "JOSE LUIS GUZMAN FERNANDEZ"
    },
    "130379":{
      "COD_CAND" : 130379,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "CAROLINA  MONSALVES SANHUEZA"
    },
    "130380":{
      "COD_CAND" : 130380,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "XIMENA  LARENAS HENRIQUEZ"
    },
    "130381":{
      "COD_CAND" : 130381,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "JOVINESSA  MUJICA MARTINEZ"
    },
    "130382":{
      "COD_CAND" : 130382,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "ROCIO GUZMAN MAGANA"
    },
    "130383":{
      "COD_CAND" : 130383,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "HUGO GUINEZ MARDONES"
    },
    "130384":{
      "COD_CAND" : 130384,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "ALEJANDRA SEPULVEDA RIOS"
    },
    "130385":{
      "COD_CAND" : 130385,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "MARIA JOSE RUBILAR ESCANDON"
    },
    "130386":{
      "COD_CAND" : 130386,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CARLOS ABEL JARPA WEVAR"
    },
    "130387":{
      "COD_CAND" : 130387,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "JULIA  TORRES MATAMALA"
    },
    "130388":{
      "COD_CAND" : 130388,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "MARIA LORETO CARVAJAL AMBIADO"
    },
    "130389":{
      "COD_CAND" : 130389,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "MANUEL EDUARDO BELLO NUNEZ"
    },
    "130390":{
      "COD_CAND" : 130390,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "GINA MARIA HIDALGO CANCINO"
    },
    "130391":{
      "COD_CAND" : 130391,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "SOLEDAD TOHA VELOSO"
    },
    "130392":{
      "COD_CAND" : 130392,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "JORGE EDUARDO SABAG VILLALOBOS"
    },
    "130393":{
      "COD_CAND" : 130393,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "PATRICIA WALEWESKA SALDIAS CARRENO"
    },
    "130394":{
      "COD_CAND" : 130394,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "GRACIELA HAYDEE SUAREZ PEREZ"
    },
    "130395":{
      "COD_CAND" : 130395,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "MARCIA ISABEL WALL TORO"
    },
    "130396":{
      "COD_CAND" : 130396,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "GUSTAVO SANHUEZA DUENAS"
    },
    "130397":{
      "COD_CAND" : 130397,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "MARGARITA LETELIER CORTES"
    },
    "130398":{
      "COD_CAND" : 130398,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "CRISTOBAL MARTINEZ RAMIREZ"
    },
    "130399":{
      "COD_CAND" : 130399,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "FRANK SAUERBAUM MUNOZ"
    },
    "130400":{
      "COD_CAND" : 130400,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "JACQUELINE  GUINEZ NUNEZ"
    },
    "130401":{
      "COD_CAND" : 130401,
      "COD_ZONA": 6019,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "HORACIO BORQUEZ CONTI"
    },
    "130402":{
      "COD_CAND" : 130402,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "HERNAN ORLANDO PINO SEGUEL"
    },
    "130403":{
      "COD_CAND" : 130403,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "MANUEL DAVID LOPEZ CARTES"
    },
    "130404":{
      "COD_CAND" : 130404,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "RODOLFO  RUIZ MEDEL"
    },
    "130405":{
      "COD_CAND" : 130405,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "MARIO ALEJANDRO JARA ESCOBAR"
    },
    "130406":{
      "COD_CAND" : 130406,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ROBERTO ANDRES FRANCESCONI RIQUELME"
    },
    "130407":{
      "COD_CAND" : 130407,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "RENZO ITALO GALGANI FONSECA"
    },
    "130408":{
      "COD_CAND" : 130408,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "DAMARIS ILIA HERNANDEZ MUNOZ"
    },
    "130409":{
      "COD_CAND" : 130409,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "JORGE EDUARDO AGUAYO CERRO"
    },
    "130410":{
      "COD_CAND" : 130410,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "VIVIANA  NAVARRO BRAIN"
    },
    "130411":{
      "COD_CAND" : 130411,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "FELIX GONZALEZ GATICA"
    },
    "130412":{
      "COD_CAND" : 130412,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "RAQUEL REBOLLEDO FLORES"
    },
    "130413":{
      "COD_CAND" : 130413,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "PAULA OPAZO NOVA"
    },
    "130414":{
      "COD_CAND" : 130414,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "ELIZABETH MUJICA ZEPEDA"
    },
    "130415":{
      "COD_CAND" : 130415,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CRISTIAN  CUEVAS ZAMBRANO"
    },
    "130416":{
      "COD_CAND" : 130416,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "LORETO MUNOZ GUTIERREZ"
    },
    "130417":{
      "COD_CAND" : 130417,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "CAMILO  RIFFO QUINTANA"
    },
    "130418":{
      "COD_CAND" : 130418,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "CLAUDIA  MONJE VIDAL"
    },
    "130419":{
      "COD_CAND" : 130419,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "MAURICIO GONZALO PEREZ DURAN"
    },
    "130420":{
      "COD_CAND" : 130420,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "ALEJANDRA  FLORES DE LA VEGA"
    },
    "130421":{
      "COD_CAND" : 130421,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "MAURICIO  SALGADO ROJAS"
    },
    "130422":{
      "COD_CAND" : 130422,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "MIGUEL ANGEL PEZO MORALES"
    },
    "130423":{
      "COD_CAND" : 130423,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "MARIA MAGDALENA GONZALEZ REBOLLEDO"
    },
    "130424":{
      "COD_CAND" : 130424,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "VICTORIA INES FARINA CONCHA"
    },
    "130425":{
      "COD_CAND" : 130425,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "FERNANDO ANTINAO JELVES"
    },
    "130426":{
      "COD_CAND" : 130426,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "CRISTIAN CAMPOS JARA"
    },
    "130427":{
      "COD_CAND" : 130427,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "FABIOLA PATRICIA LAGOS LIZAMA"
    },
    "130428":{
      "COD_CAND" : 130428,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "BERTA ANGELICA BELMAR RUIZ"
    },
    "130429":{
      "COD_CAND" : 130429,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "LILY  ONATE MUNOZ"
    },
    "130430":{
      "COD_CAND" : 130430,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "GASTON SAAVEDRA CHANDIA"
    },
    "130431":{
      "COD_CAND" : 130431,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "VANIA SALAZAR SALAZAR"
    },
    "130432":{
      "COD_CAND" : 130432,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "JAIME TOHA GONZALEZ"
    },
    "130433":{
      "COD_CAND" : 130433,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "JOSE MIGUEL ORTIZ NOVOA"
    },
    "130434":{
      "COD_CAND" : 130434,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "MARCELO OMAR CHAVEZ VELASQUEZ"
    },
    "130435":{
      "COD_CAND" : 130435,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "KARLA ANDREA MUNOZ RAMIREZ"
    },
    "130436":{
      "COD_CAND" : 130436,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "GABRIEL ENRIQUE ALBISTUR GOMEZ"
    },
    "130437":{
      "COD_CAND" : 130437,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "BALDRAMINA DEL CARMEN HENRIQUEZ COFRE"
    },
    "130438":{
      "COD_CAND" : 130438,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "PAOLA DEL CARMEN BUSTOS MATUS"
    },
    "130439":{
      "COD_CAND" : 130439,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 87,
      "GLOSA_CAND": "FELIPE  HERNANDEZ VIDAL"
    },
    "130440":{
      "COD_CAND" : 130440,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 88,
      "GLOSA_CAND": "JOSE ALEXIS LARA FLORES"
    },
    "130441":{
      "COD_CAND" : 130441,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 89,
      "GLOSA_CAND": "MACARENA ALEJANDRA AGUSTO RODRIGUEZ"
    },
    "130442":{
      "COD_CAND" : 130442,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 90,
      "GLOSA_CAND": "FRANCESCA PARODI OPPLIGER"
    },
    "130443":{
      "COD_CAND" : 130443,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 91,
      "GLOSA_CAND": "CLAUDIO EGUILUZ RODRIGUEZ"
    },
    "130444":{
      "COD_CAND" : 130444,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 92,
      "GLOSA_CAND": "FRANCESCA MUNOZ GONZALEZ"
    },
    "130445":{
      "COD_CAND" : 130445,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 93,
      "GLOSA_CAND": "LEONIDAS ROMERO SAEZ"
    },
    "130446":{
      "COD_CAND" : 130446,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 94,
      "GLOSA_CAND": "GABRIEL TORRES HERMOSILLA"
    },
    "130447":{
      "COD_CAND" : 130447,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 95,
      "GLOSA_CAND": "ENRIQUE VAN RYSSELBERGHE HERRERA"
    },
    "130448":{
      "COD_CAND" : 130448,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 96,
      "GLOSA_CAND": "SERGIO BOBADILLA MUNOZ"
    },
    "130449":{
      "COD_CAND" : 130449,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 97,
      "GLOSA_CAND": "CLAUDIA HURTADO ESPINOZA"
    },
    "130450":{
      "COD_CAND" : 130450,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 98,
      "GLOSA_CAND": "JORGE ULLOA AGUILLON"
    },
    "130451":{
      "COD_CAND" : 130451,
      "COD_ZONA": 6020,
      "CAN_ORDEN": 99,
      "GLOSA_CAND": "ELIAS  RAMOS MUNOZ"
    },
    "130452":{
      "COD_CAND" : 130452,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "MIGUEL ANGEL PALTA SARMIENTO"
    },
    "130453":{
      "COD_CAND" : 130453,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "DOMINGO  MONTECINOS OSSES"
    },
    "130454":{
      "COD_CAND" : 130454,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "ALFONSO BELMAR RODRIGUEZ"
    },
    "130455":{
      "COD_CAND" : 130455,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "JORGE BUSTOS MELLADO"
    },
    "130456":{
      "COD_CAND" : 130456,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "SANDRA OLIVA JIMENEZ"
    },
    "130457":{
      "COD_CAND" : 130457,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "TAMARA BELEN ORTIZ CASTILLO"
    },
    "130458":{
      "COD_CAND" : 130458,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "CARLA BOSSELIN ZUNIGA"
    },
    "130459":{
      "COD_CAND" : 130459,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "SANDRA VELASQUEZ PALMA"
    },
    "130460":{
      "COD_CAND" : 130460,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "JOSE PEREZ ARRIAGADA"
    },
    "130461":{
      "COD_CAND" : 130461,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "OSCAR  CABRERA ORTIZ"
    },
    "130462":{
      "COD_CAND" : 130462,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "MANUEL MONSALVE BENAVIDES"
    },
    "130463":{
      "COD_CAND" : 130463,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "ROBERTO POBLETE ZAPATA"
    },
    "130464":{
      "COD_CAND" : 130464,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "JORGE GONZALEZ CASTILLO"
    },
    "130465":{
      "COD_CAND" : 130465,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "RODRIGO DAROCH YANEZ"
    },
    "130466":{
      "COD_CAND" : 130466,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "JOANNA PEREZ OLEA"
    },
    "130467":{
      "COD_CAND" : 130467,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "JUAN BENEDICTO MACAYA MIRANDA"
    },
    "130468":{
      "COD_CAND" : 130468,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PATRICIO  PINILLA VALENCIA"
    },
    "130469":{
      "COD_CAND" : 130469,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "CRISTOBAL URRUTICOECHEA RIOS"
    },
    "130470":{
      "COD_CAND" : 130470,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "MARLENE GUZMAN KRAMER"
    },
    "130471":{
      "COD_CAND" : 130471,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "IVAN NORAMBUENA FARIAS"
    },
    "130472":{
      "COD_CAND" : 130472,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "MARIA CAROLINA RIOS AYCAGUER"
    },
    "130473":{
      "COD_CAND" : 130473,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "RENE NUNEZ AVILA"
    },
    "130474":{
      "COD_CAND" : 130474,
      "COD_ZONA": 6021,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "CARLA CEBALLOS SANTIBANEZ"
    },
    "130475":{
      "COD_CAND" : 130475,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "EVARISTO RENE CURICAL NANCO"
    },
    "130476":{
      "COD_CAND" : 130476,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "JENNY BEATRIZ RIVERA MUNOZ"
    },
    "130477":{
      "COD_CAND" : 130477,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "ANDREA PARRA SAUTEREL"
    },
    "130478":{
      "COD_CAND" : 130478,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "GUILLERMO JARAMILLO SALAZAR"
    },
    "130479":{
      "COD_CAND" : 130479,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ALEJANDRO BLAMEY ALEGRIA"
    },
    "130480":{
      "COD_CAND" : 130480,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "ANDREA SAAVEDRA TEIGUE"
    },
    "130481":{
      "COD_CAND" : 130481,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "PAMELA SOLEDAD OVALLE QUIROZ"
    },
    "130482":{
      "COD_CAND" : 130482,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "MARIO ARTIDORO VENEGAS CARDENAS"
    },
    "130483":{
      "COD_CAND" : 130483,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "JUAN FRANCISCO CHODIMAN TORRES"
    },
    "130484":{
      "COD_CAND" : 130484,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "RAYEN  INGLES HUECHE"
    },
    "130485":{
      "COD_CAND" : 130485,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "GREGORIO DE LA MAZA LARRAIN"
    },
    "130486":{
      "COD_CAND" : 130486,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "GLORIA NAVEILLAN ARRIAGADA"
    },
    "130487":{
      "COD_CAND" : 130487,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "DIEGO  PAULSEN KEHR"
    },
    "130488":{
      "COD_CAND" : 130488,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "MARIA SOLEDAD CASTILLO HENRIQUEZ"
    },
    "130489":{
      "COD_CAND" : 130489,
      "COD_ZONA": 6022,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "JORGE  RATHGEB SCHIFFERLI"
    },
    "130490":{
      "COD_CAND" : 130490,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "GUILLERMO ISMAEL ALLENDE GONZALEZ"
    },
    "130491":{
      "COD_CAND" : 130491,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "ERNESTO JAVIER LAVANDERO PEREIRA"
    },
    "130492":{
      "COD_CAND" : 130492,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "PAMELA ALEJANDRA GARCES ZUNIGA"
    },
    "130493":{
      "COD_CAND" : 130493,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "PAOLA ALEJANDRA SANTIBANEZ RIFFO"
    },
    "130494":{
      "COD_CAND" : 130494,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "NILLS MARY CID CACERES"
    },
    "130495":{
      "COD_CAND" : 130495,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "FELIPE FABIAN VALDEBENITO LEIVA"
    },
    "130496":{
      "COD_CAND" : 130496,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "JOSE  ANCALAO GAVILAN"
    },
    "130497":{
      "COD_CAND" : 130497,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "LISSETTE  GODOY ANTIQUEO"
    },
    "130498":{
      "COD_CAND" : 130498,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "CATALINA VALESKA VALENZUELA MAUREIRA"
    },
    "130499":{
      "COD_CAND" : 130499,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "ANA GRICELLE ABARCA ABARCA"
    },
    "130500":{
      "COD_CAND" : 130500,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "IVAN  CERDA ZUNIGA"
    },
    "130501":{
      "COD_CAND" : 130501,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "EDGARDO HUGO SEPULVEDA HACHIGUR"
    },
    "130502":{
      "COD_CAND" : 130502,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "ERIKA MARIA CHAVEZ CONA"
    },
    "130503":{
      "COD_CAND" : 130503,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CLAUDIA LORENA SUBIABRE MAYORGA"
    },
    "130504":{
      "COD_CAND" : 130504,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "VICTOR  SEGUEL HENRIQUEZ"
    },
    "130505":{
      "COD_CAND" : 130505,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "RICHARD CAIFAL PIUTRIN"
    },
    "130506":{
      "COD_CAND" : 130506,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "RICARDO  CELIS ARAYA"
    },
    "130507":{
      "COD_CAND" : 130507,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "JOSE FRANCISCO MONTALVA FEUERHAKE"
    },
    "130508":{
      "COD_CAND" : 130508,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "FERNANDO MEZA MONCADA"
    },
    "130509":{
      "COD_CAND" : 130509,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "MARIO HERNAN GONZALEZ REBOLLEDO"
    },
    "130510":{
      "COD_CAND" : 130510,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "ANDREA MERCADO BETANZO"
    },
    "130511":{
      "COD_CAND" : 130511,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "RODRIGO GONZALEZ LOPEZ"
    },
    "130512":{
      "COD_CAND" : 130512,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "XIMENA ONATE AVILA"
    },
    "130513":{
      "COD_CAND" : 130513,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "TAMARA TORRES HUECHUCURA"
    },
    "130514":{
      "COD_CAND" : 130514,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "ANDRES ALFONSO JOUANNET VALDERRAMA"
    },
    "130515":{
      "COD_CAND" : 130515,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "HILDA HORTA RIVERA"
    },
    "130516":{
      "COD_CAND" : 130516,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "LUIS CAMPOS LEAL"
    },
    "130517":{
      "COD_CAND" : 130517,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "ANDRES MOLINA MAGOFKE"
    },
    "130518":{
      "COD_CAND" : 130518,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "SEBASTIAN ALVAREZ RAMIREZ"
    },
    "130519":{
      "COD_CAND" : 130519,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "MARISOL ROZAS VALENZUELA"
    },
    "130520":{
      "COD_CAND" : 130520,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "RENE MANUEL GARCIA GARCIA"
    },
    "130521":{
      "COD_CAND" : 130521,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "MIGUEL  MELLADO SUAZO"
    },
    "130522":{
      "COD_CAND" : 130522,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "LAURA  SAN CRISTOBAL VASQUEZ"
    },
    "130523":{
      "COD_CAND" : 130523,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "HENRY LEAL BIZAMA"
    },
    "130524":{
      "COD_CAND" : 130524,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "KURT  HORTA CACERES"
    },
    "130525":{
      "COD_CAND" : 130525,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "RENE SAFFIRIO ESPINOZA"
    },
    "130526":{
      "COD_CAND" : 130526,
      "COD_ZONA": 6023,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "PATRICIO RIVAS GATICA"
    },
    "130911":{
      "COD_CAND" : 130911,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "PABLO ANDRES MOYA NUNEZ"
    },
    "130912":{
      "COD_CAND" : 130912,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "SILVIA BERTA GUALA CARRASCO"
    },
    "130913":{
      "COD_CAND" : 130913,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "LUIS GUILLERMO SCHWAIGER RIVERA"
    },
    "130914":{
      "COD_CAND" : 130914,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "CARLA ANDREA AMTMANN FECCI"
    },
    "130915":{
      "COD_CAND" : 130915,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "VLADIMIR ALBERTO RIESCO BAHAMONDES"
    },
    "130916":{
      "COD_CAND" : 130916,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "MARIA ANGELICA ALVEAR MONTECINOS"
    },
    "130917":{
      "COD_CAND" : 130917,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "BAYRON MANUEL VELASQUEZ PAREDES"
    },
    "130918":{
      "COD_CAND" : 130918,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "VICENTE JAVIER GOMEZ SAN MARTIN"
    },
    "130919":{
      "COD_CAND" : 130919,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "CARI LEMU ALVAREZ TORRES"
    },
    "130920":{
      "COD_CAND" : 130920,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "HERNAN JOSE MANUEL URRUTIA MILLAS"
    },
    "130921":{
      "COD_CAND" : 130921,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "JAVIERA PAZ ARIAS GUTIERREZ"
    },
    "130922":{
      "COD_CAND" : 130922,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "CLAUDIA BERNARDITA CHAVEZ GONZALEZ"
    },
    "130923":{
      "COD_CAND" : 130923,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "MARCELA ALEJANDRA GARRIDO ISLA"
    },
    "130924":{
      "COD_CAND" : 130924,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "FRANCISCO JAVIER NUNEZ MARTINEZ"
    },
    "130925":{
      "COD_CAND" : 130925,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "PAULA  GUROVICH MIRET"
    },
    "130926":{
      "COD_CAND" : 130926,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "MARCOS  ILABACA CERDA"
    },
    "130927":{
      "COD_CAND" : 130927,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PATRICIO ROSAS BARRIENTOS"
    },
    "130928":{
      "COD_CAND" : 130928,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "ABERNEGO JOB MARDONES RIQUELME"
    },
    "130929":{
      "COD_CAND" : 130929,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "ENRIQUE  JARAMILLO ASTUDILLO"
    },
    "130930":{
      "COD_CAND" : 130930,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "CARLA MAILEN SOTO GALLARDO"
    },
    "130931":{
      "COD_CAND" : 130931,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "IVAN ALBERTO FLORES GARCIA"
    },
    "130932":{
      "COD_CAND" : 130932,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "MYRTHA CARMEN PERALTA VEGAS"
    },
    "130933":{
      "COD_CAND" : 130933,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "OSCAR SANDRINO LEIVA GONZALEZ"
    },
    "130934":{
      "COD_CAND" : 130934,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "MARLYN MAGDALENA LEUPIN FERNANDEZ"
    },
    "130935":{
      "COD_CAND" : 130935,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "JUAN CARLOS TOBAR SALA"
    },
    "130936":{
      "COD_CAND" : 130936,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "EDUARDO SALAS CERDA"
    },
    "130937":{
      "COD_CAND" : 130937,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "PATRICIA MARTINEZ TORRES"
    },
    "130938":{
      "COD_CAND" : 130938,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "BERNARDO BERGER FETT"
    },
    "130939":{
      "COD_CAND" : 130939,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "ALVARO VARGAS SALAS"
    },
    "130940":{
      "COD_CAND" : 130940,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "GASTON VON MUHLENBROCK ZAMORA"
    },
    "130941":{
      "COD_CAND" : 130941,
      "COD_ZONA": 6024,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "ANN HUNTER GUTIERREZ"
    },
    "130527":{
      "COD_CAND" : 130527,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "RUTH SEEMANN BARRIA"
    },
    "130528":{
      "COD_CAND" : 130528,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "JOSE MANUEL SANTANA ABURTO"
    },
    "130529":{
      "COD_CAND" : 130529,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "SONIA ELIZABETH SILVA TORRES"
    },
    "130530":{
      "COD_CAND" : 130530,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "ALICIA NOEMI MARIN SOTOMAYOR"
    },
    "130531":{
      "COD_CAND" : 130531,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "PAMELA BEATRIZ LEAL GATICA"
    },
    "130532":{
      "COD_CAND" : 130532,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "GERMAN RODRIGO CARTES OYARZO"
    },
    "130533":{
      "COD_CAND" : 130533,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "CATALINA ANDREA YOVANE GARCIA"
    },
    "130534":{
      "COD_CAND" : 130534,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "MARIA IGNACIA BUONO-CORE BARROILHET"
    },
    "130535":{
      "COD_CAND" : 130535,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "CLAUDE  ROUTHIER CARTES"
    },
    "130536":{
      "COD_CAND" : 130536,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "JORGE RENE TEJEDA ROA"
    },
    "130537":{
      "COD_CAND" : 130537,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "CAROLINA LETICIA DUPUY GUARDA"
    },
    "130538":{
      "COD_CAND" : 130538,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "RODRIGO ANDRES MORENO ORELLANA"
    },
    "130539":{
      "COD_CAND" : 130539,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "RODRIGO ALEJANDRO TOLEDO DEL RIO"
    },
    "130540":{
      "COD_CAND" : 130540,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "KASSANDRA TABATTA MUNOZ AVILA"
    },
    "130541":{
      "COD_CAND" : 130541,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "ANGELICA MARIA HENRIQUEZ FERNANDEZ"
    },
    "130542":{
      "COD_CAND" : 130542,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "MARIA JOSE LEVICOI OVANDO"
    },
    "130543":{
      "COD_CAND" : 130543,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "ALEJANDRA DANIELA ARCOS ROSAS"
    },
    "130544":{
      "COD_CAND" : 130544,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "MARGARITA PEREZ VALDES"
    },
    "130545":{
      "COD_CAND" : 130545,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "CATALINA VELASQUEZ FLORES"
    },
    "130546":{
      "COD_CAND" : 130546,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "FIDEL ESPINOZA SANDOVAL"
    },
    "130547":{
      "COD_CAND" : 130547,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "EMILIA IRIS NUYADO ANCAPICHUN"
    },
    "130548":{
      "COD_CAND" : 130548,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "GUSTAVO SALVO PEREIRA"
    },
    "130549":{
      "COD_CAND" : 130549,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "SERGIO RODRIGO OJEDA URIBE"
    },
    "130550":{
      "COD_CAND" : 130550,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "KARLA BENAVIDES HENRIQUEZ"
    },
    "130551":{
      "COD_CAND" : 130551,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "MARIANELA HUENCHOR HUENCHOR"
    },
    "130552":{
      "COD_CAND" : 130552,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "MIGUEL ARREDONDO ORELLANA"
    },
    "130553":{
      "COD_CAND" : 130553,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "JAVIER HERNANDEZ HERNANDEZ"
    },
    "130554":{
      "COD_CAND" : 130554,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "SORAYA SAID TEUBER"
    },
    "130555":{
      "COD_CAND" : 130555,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "AMANDA MILOSEVICH PEPPER"
    },
    "130556":{
      "COD_CAND" : 130556,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "HARRY JURGENSEN RUNDSHAGEN"
    },
    "130557":{
      "COD_CAND" : 130557,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "ANDREA ROSMANICH ROJAS"
    },
    "130558":{
      "COD_CAND" : 130558,
      "COD_ZONA": 6025,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "LUIS  ABARCA AZOCAR"
    },
    "130559":{
      "COD_CAND" : 130559,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "RAUL OLIVA CAMADRO"
    },
    "130560":{
      "COD_CAND" : 130560,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "GONZALO VALENZUELA OYARZO"
    },
    "130561":{
      "COD_CAND" : 130561,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "MARIA GLADIS OYARZO ZUNIGA"
    },
    "130562":{
      "COD_CAND" : 130562,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "PAULA MILLACURA TORRES"
    },
    "130563":{
      "COD_CAND" : 130563,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "OSCAR PATRICIO MACIAS MACIAS"
    },
    "130564":{
      "COD_CAND" : 130564,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "FRANCISCO BUCAT OVIEDO"
    },
    "130565":{
      "COD_CAND" : 130565,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "ALEJANDRO JAVIER BERNALES MALDONADO"
    },
    "130566":{
      "COD_CAND" : 130566,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "IVONNE LECAROS SANDOVAL"
    },
    "130567":{
      "COD_CAND" : 130567,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "RICARDO CACERES OLAVE"
    },
    "130568":{
      "COD_CAND" : 130568,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "SUSAN ANGELICA PONCE JARA"
    },
    "130569":{
      "COD_CAND" : 130569,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "SANDRA PAOLA VEGA OYARZO"
    },
    "130570":{
      "COD_CAND" : 130570,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "EDUARDO ANTONIO OCAMPO CASTILLO"
    },
    "130571":{
      "COD_CAND" : 130571,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "ALEXIS MARCELO BARRIA MALDONADO"
    },
    "130572":{
      "COD_CAND" : 130572,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CAROLINA ALEJANDRA PINO COLLADO"
    },
    "130573":{
      "COD_CAND" : 130573,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "MARIA ORIANA PEREZ BAEZ"
    },
    "130574":{
      "COD_CAND" : 130574,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "LUIS FELIPE VERGARA MALDONADO"
    },
    "130575":{
      "COD_CAND" : 130575,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "NELSON GUZMAN CACERES"
    },
    "130576":{
      "COD_CAND" : 130576,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "MARVA  VARGAS ALVARADO"
    },
    "130577":{
      "COD_CAND" : 130577,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "JOSE ROBERTO ABURTO BARRIA"
    },
    "130578":{
      "COD_CAND" : 130578,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "ZANDRA ESTHER PARISI FERNANDEZ"
    },
    "130579":{
      "COD_CAND" : 130579,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 70,
      "GLOSA_CAND": "MARIA PATRICIA PASCUALES GUTIERREZ"
    },
    "130580":{
      "COD_CAND" : 130580,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 71,
      "GLOSA_CAND": "HECTOR LEONARDO AGUILERA MAYORGA"
    },
    "130581":{
      "COD_CAND" : 130581,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 72,
      "GLOSA_CAND": "ERIKA ANDREA GUAIQUIN GUAIQUIN"
    },
    "130582":{
      "COD_CAND" : 130582,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 73,
      "GLOSA_CAND": "ANA MARIA CALISTO RAMIREZ"
    },
    "130583":{
      "COD_CAND" : 130583,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 74,
      "GLOSA_CAND": "CLAUDIO OYARZUN CARCAMO"
    },
    "130584":{
      "COD_CAND" : 130584,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 75,
      "GLOSA_CAND": "GLORIA PATRICIA GONZALEZ SAEZ"
    },
    "130585":{
      "COD_CAND" : 130585,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 76,
      "GLOSA_CAND": "JENNY  ALVAREZ VERA"
    },
    "130586":{
      "COD_CAND" : 130586,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 77,
      "GLOSA_CAND": "RAMON  ESPINOZA SANDOVAL"
    },
    "130587":{
      "COD_CAND" : 130587,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 78,
      "GLOSA_CAND": "CARLOS EDUARDO CONTRERAS OYARZUN"
    },
    "130588":{
      "COD_CAND" : 130588,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 79,
      "GLOSA_CAND": "CLAUDIO MARTINEZ LABRA"
    },
    "130589":{
      "COD_CAND" : 130589,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 80,
      "GLOSA_CAND": "GABRIEL HECTOR ASCENCIO MANSILLA"
    },
    "130590":{
      "COD_CAND" : 130590,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 81,
      "GLOSA_CAND": "ALEJANDRO SANTANA TIRACHINI"
    },
    "130591":{
      "COD_CAND" : 130591,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 82,
      "GLOSA_CAND": "CARLOS IGNACIO KUSCHEL SILVA"
    },
    "130592":{
      "COD_CAND" : 130592,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 83,
      "GLOSA_CAND": "JOSE  SEGURA DIAZ"
    },
    "130593":{
      "COD_CAND" : 130593,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 84,
      "GLOSA_CAND": "MARISOL TURRES FIGUEROA"
    },
    "130594":{
      "COD_CAND" : 130594,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 85,
      "GLOSA_CAND": "FRANCISCO  MUNOZ LE-BRETON"
    },
    "130595":{
      "COD_CAND" : 130595,
      "COD_ZONA": 6026,
      "CAN_ORDEN": 86,
      "GLOSA_CAND": "ORIETA  GONZALEZ MATURANA"
    },
    "130596":{
      "COD_CAND" : 130596,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "EDUARDO  ROMO LAFOY"
    },
    "130597":{
      "COD_CAND" : 130597,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "SARA  MARTINEZ MONDELO"
    },
    "130598":{
      "COD_CAND" : 130598,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "VICTOR MANUEL BORQUEZ FINCKE"
    },
    "130599":{
      "COD_CAND" : 130599,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "MARISOL LUSDEMIA PINILLA VEJAR"
    },
    "130600":{
      "COD_CAND" : 130600,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "ELSON BORQUEZ YANEZ"
    },
    "130601":{
      "COD_CAND" : 130601,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "PEDRO ANTONIO VERGARA ROJAS"
    },
    "130602":{
      "COD_CAND" : 130602,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "JESSICA ANDREA TORRES BORQUEZ"
    },
    "130603":{
      "COD_CAND" : 130603,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "TAMARA ANDREA ESPINOZA GUTIERREZ"
    },
    "130604":{
      "COD_CAND" : 130604,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "JORGE CALDERON NUNEZ"
    },
    "130605":{
      "COD_CAND" : 130605,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "MARISOL MARTINEZ SANCHEZ"
    },
    "130606":{
      "COD_CAND" : 130606,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "ROXANA PEY TUMANOFF"
    },
    "130607":{
      "COD_CAND" : 130607,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "RENE OSVALDO ALINCO BUSTOS"
    },
    "130608":{
      "COD_CAND" : 130608,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "MIGUEL ANGEL CALISTO AGUILA"
    },
    "130609":{
      "COD_CAND" : 130609,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "CARMEN GLORIA MARTINEZ CARDENAS"
    },
    "130610":{
      "COD_CAND" : 130610,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "RENE ANSELMO LEGUE CARDENAS"
    },
    "130611":{
      "COD_CAND" : 130611,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "NESTOR  MERA MUNOZ"
    },
    "130612":{
      "COD_CAND" : 130612,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "PATRICIO  HENRIQUEZ BARRIENTOS"
    },
    "130613":{
      "COD_CAND" : 130613,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "GEOCONDA  NAVARRETE ARRATIA"
    },
    "130614":{
      "COD_CAND" : 130614,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 68,
      "GLOSA_CAND": "ARACELY  LEUQUEN URIBE"
    },
    "130615":{
      "COD_CAND" : 130615,
      "COD_ZONA": 6027,
      "CAN_ORDEN": 69,
      "GLOSA_CAND": "CECILIO AGUILAR GALINDO"
    },
    "130616":{
      "COD_CAND" : 130616,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 50,
      "GLOSA_CAND": "GABRIEL BORIC FONT"
    },
    "130617":{
      "COD_CAND" : 130617,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 51,
      "GLOSA_CAND": "CAROLINA SALDIVIA SALAZAR"
    },
    "130618":{
      "COD_CAND" : 130618,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 52,
      "GLOSA_CAND": "JUAN MANUEL MANCILLA CARDENAS"
    },
    "130619":{
      "COD_CAND" : 130619,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 53,
      "GLOSA_CAND": "DENISSE GUERRA MARTINEZ"
    },
    "130620":{
      "COD_CAND" : 130620,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 54,
      "GLOSA_CAND": "MARIO ENRIQUE  ESQUIVEL LIZONDO"
    },
    "130621":{
      "COD_CAND" : 130621,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 55,
      "GLOSA_CAND": "MARLA GLADYS VARGAS OJEDA"
    },
    "130622":{
      "COD_CAND" : 130622,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 56,
      "GLOSA_CAND": "JORGE EMILIO PAREDES LEIVA"
    },
    "130623":{
      "COD_CAND" : 130623,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 57,
      "GLOSA_CAND": "VLADIMIRO MIMICA CARCAMO"
    },
    "130624":{
      "COD_CAND" : 130624,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 58,
      "GLOSA_CAND": "DOMINGO ANTONIO RUBILAR RUIZ"
    },
    "130625":{
      "COD_CAND" : 130625,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 59,
      "GLOSA_CAND": "FRANCISCO ENRIQUE ALARCON NAVARRO"
    },
    "130626":{
      "COD_CAND" : 130626,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 60,
      "GLOSA_CAND": "KARIM ANTONIO BIANCHI RETAMALES"
    },
    "130627":{
      "COD_CAND" : 130627,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 61,
      "GLOSA_CAND": "JUAN ENRIQUE MORANO CORNEJO"
    },
    "130628":{
      "COD_CAND" : 130628,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 62,
      "GLOSA_CAND": "CARLOS  MANDRIAZA MUNOZ"
    },
    "130629":{
      "COD_CAND" : 130629,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 63,
      "GLOSA_CAND": "EUGENIA  MANCILLA MACIAS"
    },
    "130630":{
      "COD_CAND" : 130630,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 64,
      "GLOSA_CAND": "JUAN JOSE ARCOS SRDANOVIC"
    },
    "130631":{
      "COD_CAND" : 130631,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 65,
      "GLOSA_CAND": "HELGA  NEUMANN CALISTO"
    },
    "130632":{
      "COD_CAND" : 130632,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 66,
      "GLOSA_CAND": "NICOLAS COGLER GALINDO"
    },
    "130633":{
      "COD_CAND" : 130633,
      "COD_ZONA": 6028,
      "CAN_ORDEN": 67,
      "GLOSA_CAND": "SANDRA  AMAR MANCILLA"
    }
  }
  
  export default candidatos;