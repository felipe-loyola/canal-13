import React, { Component } from 'react';
import Firebase from 'firebase';
import PresidenteItem from './presidenteItem'
import Listado from './Listado'
import Sortable from 'react-sortablejs';
import LocalesDataset from '../../datasets/locales'
import PaisesDatasets from '../../datasets/paises'
import ComunasDatasets from '../../datasets/comunas'
import ConsuladosDataset from '../../datasets/consulados'

import Autocomplete from "react-autocomplete"
import Select from 'react-select';


const style = {
    background: "#FFF",
    border: "1px solid #bbb",
    paddingTop: "15px",
    paddingLeft: "30px",
    borderRadius: "5px"
}

class presidentesList extends Component {

    constructor() {
        super();
        this.state = {
            candidatos: {
                "110007": {
                    nombre: "Eduardo Artés B.",
                    foto: "Artes.jpg",
                    partido: "Unión Patriótica",
                    color: "Red"
                },
                "110004": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    partido: "Independiente",
                    color: "Teal"
                },
                "110002": {
                    nombre: "José Antonio Kast",
                    foto: "Kast.jpg",
                    partido: "Independiente",
                    color: "Purple"
                },
                "110008": {
                    nombre: "Alejandro Navarro B.",
                    foto: "Navarro.jpg",
                    partido: "País",
                    color: "Amber"
                },
                "110001": {
                    nombre: "Carolina Goic B.",
                    foto: "Goic.jpg",
                    partido: "DC",
                    color: "Green"
                },
                "110006": {
                    nombre: "M. Enríquez-Ominami",
                    foto: "Ominami.jpg",
                    partido: "Progresista",
                    color: "Grey"
                },
                "110005": {
                    nombre: "Beatriz Sánchez M.",
                    foto: "Sanchez.jpg",
                    partido: "Frente Amplio",
                    color: "Brown"
                },
                "110003": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Pinera.jpg",
                    partido: "Chile Vamos",
                    color: "Blue"
                }
            },
            total: 0,
            selected: "todo"
        };
    }

    getVotos(id) {
        var arr = this.state.candidatos;
        var total_votos = this.state.total;
        var candidatoClient = Firebase.database().ref('snap_presidentes/candidatos/' + id);
        candidatoClient.on("value", snap => {
            var total = 0;
            Object.values(snap.val().paises).map((p, indexPais) => {
                var total_comuna = 0;
                Object.values(p.comunas).map((c, indexComunas) => {
                    var total_locales = 0;
                    Object.values(c.locales).map((l) => {
                        var total_local = 0;
                        Object.values(l.mesas).map((m) => {
                            total_local = m + total_local;
                        })
                        total_locales = total_local + total_locales;
                    })
                    total_comuna = total_locales + total_comuna;
                })
                total = total_comuna + total;
            })
            arr[id].votos = total
            total_votos = total_votos + total;
            this.setState({ candidatos: arr, total: total_votos })
        })
    }

    componentWillMount() {
        Object.keys(this.state.candidatos).map((c, index) => {
            this.getVotos(c);
        })        
    }

    updateVotos(id, valor, filtro) {
        var arr = Object.assign([], this.state.candidatos);
        var total_votos = this.state.total;
        var candidatoClient = Firebase.database().ref('snap_presidentes/candidatos/' + id);
        switch (filtro) {
            case "nombre_consulado":
                candidatoClient.on("value", snap => {
                    var total = 0;
                    Object.values(snap.val().paises).map((p, indexPais) => {
                        var total_comuna = 0;
                        Object.values(p.comunas).map((c, indexComunas) => {
                            var total_locales = 0;
                            if (Object.keys(p.comunas)[indexComunas] === String(valor)) {
                                Object.values(c.locales).map((l) => {
                                    var total_local = 0;
                                    Object.values(l.mesas).map((m) => {
                                        total_local = m + total_local;
                                    })
                                    total_locales = total_local + total_locales;
                                })
                                total_comuna = total_locales + total_comuna;
                            }
                        })
                        total = total_comuna + total;
                    })
                    arr[id].votos = total
                    total_votos = total_votos + total;
                    this.setState({ candidatos: arr, total: total_votos })
                });
                break;
            case "nombre_comuna":
                candidatoClient.on("value", snap => {
                    var total = 0;
                    Object.values(snap.val().paises).map((p, indexPais) => {
                        var total_comuna = 0;
                        Object.values(p.comunas).map((c, indexComunas) => {
                            var total_locales = 0;
                            if (Object.keys(p.comunas)[indexComunas] === String(valor)) {
                                Object.values(c.locales).map((l) => {
                                    var total_local = 0;
                                    Object.values(l.mesas).map((m) => {
                                        total_local = m + total_local;
                                    })
                                    total_locales = total_local + total_locales;
                                })
                                total_comuna = total_locales + total_comuna;
                            }
                        })
                        total = total_comuna + total;
                    })
                    arr[id].votos = total
                    total_votos = total_votos + total;
                    this.setState({ candidatos: arr, total: total_votos })
                });
                break;
            case "nombre_local":
                candidatoClient.on("value", snap => {
                    var total = 0;
                    Object.values(snap.val().paises).map((p, indexPais) => {
                        var total_comuna = 0;
                        if (Object.keys(snap.val().paises)[indexPais] === "8056") {
                            Object.values(p.comunas).map((c, indexComunas) => {
                                var total_locales = 0;
                                Object.values(c.locales).map((l, indexLocales) => {
                                    var total_local = 0;
                                    if (Object.keys(c.locales)[indexLocales] === String(valor)) {
                                        Object.values(l.mesas).map((m) => {
                                            total_local = m + total_local;
                                        })
                                        total_locales = total_local + total_locales;
                                    }
                                })
                                total_comuna = total_locales + total_comuna;
                            })
                        }
                        total = total_comuna + total;
                    })
                    arr[id].votos = total
                    total_votos = total_votos + total;
                    this.setState({ candidatos: arr, total: total_votos })
                })
                break;
            case "todo":
                candidatoClient.on("value", snap => {
                    var total = 0;
                    Object.values(snap.val().paises).map((p, indexPais) => {
                        var total_comuna = 0;
                        Object.values(p.comunas).map((c, indexComunas) => {
                            var total_locales = 0;
                            Object.values(c.locales).map((l) => {
                                var total_local = 0;
                                Object.values(l.mesas).map((m) => {
                                    total_local = m + total_local;
                                })
                                total_locales = total_local + total_locales;
                            })
                            total_comuna = total_locales + total_comuna;
                        })
                        total = total_comuna + total;
                    })
                    arr[id].votos = total
                    total_votos = total_votos + total;
                    this.setState({ candidatos: arr, total: total_votos })
                })
                break;
            case "pais":
                candidatoClient.on("value", snap => {
                    var total = 0;
                    Object.values(snap.val().paises).map((p, indexPais) => {
                        var total_comuna = 0;
                        if (Object.keys(snap.val().paises)[indexPais] === String(valor)) {
                            Object.values(p.comunas).map((c, indexComunas) => {
                                var total_locales = 0;
                                Object.values(c.locales).map((l) => {
                                    var total_local = 0;
                                    Object.values(l.mesas).map((m) => {
                                        total_local = m + total_local;
                                    })
                                    total_locales = total_local + total_locales;
                                })
                                total_comuna = total_locales + total_comuna;
                            })
                        }
                        total = total_comuna + total;
                    })
                    arr[id].votos = total
                    total_votos = total_votos + total;
                    this.setState({ candidatos: arr, total: total_votos })
                })
            default:
                break;
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextState.value !== this.state.value) {
            Object.keys(this.state.candidatos).map((c, index) => {
                this.updateVotos(c, nextState.value, nextState.selected);
            })
        }
    }

    setTotal() {
        var total = 0;
        Object.values(this.state.candidatos).map((c, index) => {
            total = total + c.votos
        })
        return total;
    }

    datos() {
        switch (this.state.selected) {
            case "pais":
                return PaisesDatasets;
                break;
            case "nombre_comuna":
                return ComunasDatasets;
                break;
            case "nombre_consulado":
                return ConsuladosDataset;
                break;
            case "nombre_local":
                return LocalesDataset;
                break;
            default:
                return [{ id: "sad", label: "Seleccione un tipo de busqueda" }]
        }
    }


    handleSelectChange(value) {
        this.setState({ value: value })
    }


    render() {
        var total_votos = this.setTotal();
        return (
            <div className="animated fadeIn" >
                <div className="form-group row">
                    <div className="col-md-2" style={style}>
                        <h5 style={{ borderBottom: "1px solid #bbb" }}>Filtros</h5>
                        <div className="row">
                            <label className="label-cbx" style={{ marginRight: "3%" }}>
                                <input id="cbx" type="radio" className="invisible" checked={this.state.selected === "todo"} onClick={() => { this.setState({ selected: "todo", busqueda: "" }); }} />
                                <div className="checkbox">
                                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                                        <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                                        <polyline points="4 11 8 15 16 6"></polyline>
                                    </svg>
                                </div>
                                <span>Todo</span>
                            </label>
                        </div>
                        <div className="row">
                            <label className="label-cbx" style={{ marginRight: "3%" }}>
                                <input id="cbx" type="radio" className="invisible" checked={this.state.selected === "pais"} onClick={() => { this.setState({ selected: "pais", busqueda: "" }) }} />
                                <div className="checkbox">
                                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                                        <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                                        <polyline points="4 11 8 15 16 6"></polyline>
                                    </svg>
                                </div>
                                <span>País</span>
                            </label>
                        </div>
                        <div className="row">
                            <label className="label-cbx" style={{ marginRight: "3%" }}>
                                <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "nombre_consulado"} onClick={() => { this.setState({ selected: "nombre_consulado", busqueda: "" }) }} />
                                <div className="checkbox">
                                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                                        <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                                        <polyline points="4 11 8 15 16 6"></polyline>
                                    </svg>
                                </div>
                                <span>Consulado</span>
                            </label>
                        </div>
                        <div className="row">
                            <label className="label-cbx" style={{ marginRight: "3%" }}>
                                <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "nombre_comuna"} onClick={() => { this.setState({ selected: "nombre_comuna", busqueda: "" }) }} />
                                <div className="checkbox">
                                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                                        <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                                        <polyline points="4 11 8 15 16 6"></polyline>
                                    </svg>
                                </div>
                                <span>Comuna</span>
                            </label>
                        </div>
                        <div className="row">
                            <label className="label-cbx" style={{ marginRight: "3%" }}>
                                <input id="cbx" type="checkbox" className="invisible" checked={this.state.selected === "nombre_local"} onClick={() => { this.setState({ selected: "nombre_local", busqueda: "" }) }} />
                                <div className="checkbox">
                                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                                        <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                                        <polyline points="4 11 8 15 16 6"></polyline>
                                    </svg>
                                </div>
                                <span>Local</span>
                            </label>
                        </div>
                        <hr />
                        <div className="row">
                            <div className="section" style={{ width: "95%" }}>
                                <Select simpleValue value={this.state.value}
                                    placeholder="Seleccione formularios asignados"
                                    options={this.datos()}
                                    onChange={(e) => { this.setState({ value: e }) }} />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-10">
                        <section className="container" style={{ width: "90%" }}>
                            <Sortable className="row active-with-click"
                                tag="ul" // Defaults to "div"
                                onChange={(order, sortable, evt) => {
                                    this.setState({ items: order });
                                }}
                            >
                                {
                                    this.state.candidatos ?
                                        Object.values(this.state.candidatos).map((c, index) => (
                                            <div className="col-md-3 col-sm-4 col-xs-12" key={index}>
                                                <PresidenteItem candidato={c} id={Object.keys(this.state.candidatos)[index]} total={total_votos} />
                                            </div>
                                        ))
                                        :
                                        ""
                                }
                            </Sortable>
                        </section>
                    </div>
                </div>
            </div >
        )
    }
}

export default presidentesList;
