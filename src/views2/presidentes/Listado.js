const candidatos = {
    "01": {
        nombre: "Eduardo Artés Brichetti",
        foto: "Artes.jpg",
        partido: "Unión Patriótica"
    },
    "02": {
        nombre: "Alejandro Guillier Álvarez",
        foto: "Guillier.jpg",
        partido: "Independiente"
    },
    "03": {
        nombre: " José Antonio Kast",
        foto: "Kast.jpg",
        partido: "Independiente"
    },
    "04": {
        nombre: "Alejandro Navarro Brain",
        foto: "Navarro.jpg",
        partido: "País"
    },
    "05": {
        nombre: "Carolina Goic Boroevic",
        foto: "Goic.jpg",
        partido: "Demócrata Cristiano"
    },
    "06": {
        nombre: "Marco Enríquez-Ominami",
        foto: "Ominami.jpg",
        partido: "Progresista"
    },
    "07": {
        nombre: "Beatriz Sánchez Muñoz",
        foto: "Sanchez.jpg",
        partido: "Frente Amplio"
    },
    "08": {
        nombre: "Sebastián Piñera Echenique",
        foto: "Piñera.jpg",
        partido: "Chile Vamos"
    }
}

export default candidatos;