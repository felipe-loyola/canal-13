import React, { Component } from 'react';
import Firebase from 'firebase';
import Locales from '../../datasets/tabla_locales.json'
import LaddaButton, { S, EXPAND_RIGHT } from 'react-ladda';



class firebase extends Component {

    constructor() {
        super();
        this.state = {
            loadingData: false,
            candidatos: {
                "110007": {
                    nombre: "Eduardo Artés B.",
                    foto: "Artes.jpg",
                    partido: "Unión Patriótica",
                    color: "Red"
                },
                "110004": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    partido: "Independiente",
                    color: "Teal"
                },
                "110002": {
                    nombre: "José Antonio Kast",
                    foto: "Kast.jpg",
                    partido: "Independiente",
                    color: "Purple"
                },
                "110008": {
                    nombre: "Alejandro Navarro B.",
                    foto: "Navarro.jpg",
                    partido: "País",
                    color: "Amber"
                },
                "110001": {
                    nombre: "Carolina Goic B.",
                    foto: "Goic.jpg",
                    partido: "Demócrata Cristiano",
                    color: "Green"
                },
                "110006": {
                    nombre: "M. Enríquez-Ominami",
                    foto: "Ominami.jpg",
                    partido: "Progresista",
                    color: "Grey"
                },
                "110005": {
                    nombre: "Beatriz Sánchez M.",
                    foto: "Sanchez.jpg",
                    partido: "Frente Amplio",
                    color: "Brown"
                },
                "110003": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Piñera.jpg",
                    partido: "Chile Vamos",
                    color: "Blue"
                }
            }
        };
    }

    setData() {
        var presiRef = Firebase.database();
        var mesas = ["1902", "1245", "0123", "189", "441", "2", "3"]
        Object.keys(this.state.candidatos).map((c) => {
            var mesas = {};
            Locales.map((l) => {
                if (l.pais === "CHILE") {
                    for (var index = 0; index < 20; index++) {
                        mesas[index] = Math.floor(Math.random() * (600 - 1) + 10)
                    }
                    presiRef.ref('snap_presidentes/' + c + '/' + l.comuna_id + '/' + l.local_id).set(mesas);
                }
            });
        })
        this.setState({ loadingData: false });
    }

    resetData() {
        const contex = this;
        setTimeout(function () {
            var presiRef = Firebase.database().ref('snap_presidentes').remove();
            contex.setState({ loadingReset: false })
        }, 1000);
    }

    sendFunctions() {
        var presiRef = Firebase.database();
        Object.keys(this.state.candidatos).map((c) => {
            var mesas = {};
            for (var com = 0; com < 5; com++) {
                setTimeout(function () {
                    for (var i = 0; i < 10; i++) {
                        window.setTimeout(function () {
                            for (var index = 0; index < 5; index++) {
                                mesas[index + "M"] = Math.floor(Math.random() * (10 - 1) + 1)
                            }
                            presiRef.ref('snap_presidentes/'+ c + '/paises/8056/comunas/' + "C" + com + '/locales/' + "L" + i + '/mesas').set(mesas).then(resp=>{
                                console.log("enviado")
                            });
                        }, 1000);
                    }
                }, 1000);
            }
        })
        this.setState({ loadingFunctions: false });
    }


    render() {
        return (
            <div className="col-md-12">
                <div className="form-group row">
                    <div className="col-md-4">
                        <LaddaButton
                            style={{ left: "1%", position: "relative" }}
                            loading={this.state.loadingData}
                            onClick={() => { this.setState({ loadingData: true }); this.setData() }}
                            data-color="green"
                            data-size={S}
                            data-style={EXPAND_RIGHT}
                            data-spinner-size={30}
                            data-spinner-color="#ddd"
                            data-spinner-lines={12}
                        >
                            Generar Datos
                        </LaddaButton>
                    </div>
                    <div className="col-md-4">
                        <LaddaButton
                            style={{ left: "1%", position: "relative" }}
                            loading={this.state.loadingReset}
                            onClick={() => { this.setState({ loadingReset: true }); this.resetData() }}
                            data-color="green"
                            data-size={S}
                            data-style={EXPAND_RIGHT}
                            data-spinner-size={30}
                            data-spinner-color="#ddd"
                            data-spinner-lines={12}
                        >
                            Limpiar tabla
                        </LaddaButton>
                    </div>
                    <div className="col-md-4">
                        <LaddaButton
                            style={{ left: "1%", position: "relative" }}
                            loading={this.state.loadingFunctions}
                            onClick={() => { this.setState({ loadingFunctions: true }); this.sendFunctions() }}
                            data-color="green"
                            data-size={S}
                            data-style={EXPAND_RIGHT}
                            data-spinner-size={30}
                            data-spinner-color="#ddd"
                            data-spinner-lines={12}
                        >
                            Prueba functions
                        </LaddaButton>
                    </div>
                </div>
            </div>
        )
    }
}

export default firebase;
