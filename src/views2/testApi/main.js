import React, { Component } from 'react';
import LaddaButton, { S, EXPAND_RIGHT } from 'react-ladda';
import Firebase from './firebase'

class main extends Component {

    constructor() {
        super();
        this.state = {
            calls: [],
            loadingData: false,
            loadingSend: false,
            countLlamadas: 1000
        };
    }

    getRandomCandidato() {
        return Math.floor(Math.random() * (5 - 1) + 1);
    }

    getRandomLocal() {
        return Math.floor(Math.random() * (5 - 1) + 1);
    }

    getRandomMesa() {
        return Math.floor(Math.random() * (5 - 1) + 1);
    }

    getRandomUsuario() {
        return Math.floor(Math.random() * (1950 - 50) + 50);
    }

    setData() {
        var data = {
            "candidatoId": this.getRandomCandidato(),
            "deviceTime": "1507776785",
            "localId": this.getRandomLocal(),
            "mesaId": this.getRandomMesa(),
            "eleccionId": 2,
            "method": "add",
            "userId": this.getRandomUsuario(),
            "votos": "1"
        }
        return data;
    }

    methodAdd(d) {
        var count = 0;
        const urlAddc13 = 'http://200.27.99.123:8080/operatio_votaciones/api/votes';
        const urlAdd = 'http://34.229.87.250:8080/operatio_votaciones/api/votes';
        const data = d
        fetch(urlAdd, {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "cache-control": "no-cache"
                //        "Authorization": "Bearer " 
            }
        })
            .then((response) => {
                return response.json();
            })
            .then(jsonResponse => {
                console.log(jsonResponse)
                this.setState({ loadingSend: false })
                return jsonResponse
            })
            .catch(error => {
                return false;
                console.log("error", error)
            });

    }

    setCalls() {
        const contex = this;
        this.setState({
            loadingData: !this.state.loadingData,
            progress: 0.5,
        });
        var llamadas = []
        for (var index = 0; index < this.state.countLlamadas; index++) {
            llamadas.push(this.setData())
        }
        setTimeout(function () {
            contex.setState({ calls: llamadas });
        }, 1000);
    }

    sendData() {
        console.time('Test performance');
        Promise.all(
            this.state.calls.map((c) => {
                var done = this.methodAdd(c)
            })
        ).then(() => { console.timeEnd('Test performance'); })
    }

    renderResumen() {
        if (this.state.calls) {
            var count = {
                candidato: {
                    1: 0,
                    2: 0,
                    3: 0,
                    4: 0
                },
                mesa: {
                    1: 0,
                    2: 0,
                    3: 0,
                    4: 0
                },
                local: {
                    1: 0,
                    2: 0,
                    3: 0,
                    4: 0
                }
            }
            this.state.calls.map((c) => {
                count.candidato[c.candidatoId] = count.candidato[c.candidatoId] + 1;
                count.mesa[c.mesaId] = count.mesa[c.mesaId] + 1;
                count.local[c.localId] = count.local[c.localId] + 1
            })
            return (
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <th>Candidato</th>
                            <td>{count.candidato[1]}</td>
                            <td>{count.candidato[2]}</td>
                            <td>{count.candidato[3]}</td>
                            <td>{count.candidato[4]}</td>
                        </tr>
                        <tr >
                            <th>Local</th>
                            <td>{count.local[1]}</td>
                            <td>{count.local[2]}</td>
                            <td>{count.local[3]}</td>
                            <td>{count.local[4]}</td>
                        </tr>
                        <tr >
                            <th>Mesa</th>
                            <td>{count.mesa[1]}</td>
                            <td>{count.mesa[2]}</td>
                            <td>{count.mesa[3]}</td>
                            <td>{count.mesa[4]}</td>
                        </tr>
                    </tbody>
                </table>
            )
        }
    }

    renderVotos() {
        if (this.state.calls) {
            return (
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Candidato</th>
                            <th>local</th>
                            <th>mesa</th>
                            <th>usuario</th>
                            <th>votos</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.calls.map((c, index) => (
                                <tr key={index}>
                                    <th scope="row">{c.candidatoId}</th>
                                    <td>{c.localId}</td>
                                    <td>{c.mesaId}</td>
                                    <td>{c.userId}</td>
                                    <td>1</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            )
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.calls !== this.state.calls) {
            this.setState({ loadingData: false })
        }
    }

    render() {
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <div className="text-left">
                        <LaddaButton
                            style={{ left: "1%", position: "relative" }}
                            loading={this.state.loadingData}
                            onClick={() => this.setCalls()}
                            data-color="green"
                            data-size={S}
                            data-style={EXPAND_RIGHT}
                            data-spinner-size={30}
                            data-spinner-color="#ddd"
                            data-spinner-lines={12}
                        >
                            Generar llamadas
                        </LaddaButton>
                    </div>

                    <div className="input-group" style={{ left: "0", right: "0", margin: "auto", width: "30%" }}>
                        <div className="input-group-addon">Cantidad:</div>
                        <input type="email" className="form-control" value={this.state.countLlamadas} onChange={(e) => { this.setState({ countLlamadas: e.target.value }) }} />
                    </div>

                    <div className="text-right">
                        <LaddaButton
                            style={{ right: "1%", position: "absolute" }}
                            loading={this.state.loadingSend}
                            onClick={() => { this.setState({ loadingSend: true }); this.sendData(); }}
                            data-color="green"
                            data-size={S}
                            data-style={EXPAND_RIGHT}
                            data-spinner-size={30}
                            data-spinner-color="#ddd"
                            data-spinner-lines={12}
                        >
                            Enviar llamadas
                        </LaddaButton>
                    </div>
                </div>
                <br />
                <div className="row">
                    {this.renderResumen()}
                </div>
                <br />
                <div className="row">
                    <Firebase />
                </div>
            </div>
        )
    }
}

export default main;
