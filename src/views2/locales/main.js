import React, { Component } from 'react';
import Firebase from 'firebase';
import Local from './local'
import Sortable from 'react-sortablejs';


class main extends Component {

    constructor() {
        super();
        this.state = {
            list: [],
            count: 0
        };
    }

    componentDidMount() {
        const contex = this;
        var mesasRef = Firebase.database().ref("status_mesas");
        mesasRef.on('value', snap => {
            contex.setLocales();
        })
    }

    componentWillMount() {
        const contex = this;
        var mesasRef = Firebase.database().ref("status_mesas");
        var periodistaRef = Firebase.database().ref("periodista_status");
        mesasRef.on('value', snap => {
            var arr = new Array;
            snap.forEach(function (childSnapshot) {
                arr.push(childSnapshot.key)
            });
            this.setState({ list: arr, count: this.state.count + 1 })
        })
    }

    setLocales() {
        const contex = this;
        var mesasRef = Firebase.database().ref("status_mesas");
        var periodistaRef = Firebase.database().ref("periodista_status");
        mesasRef.on('value', snap => {
            var arr = new Array;
            snap.forEach(function (childSnapshot) {
                arr.push(childSnapshot.key)
            });
            this.setState({list : arr})
        })
    }

    /*     componentWillUpdate(nextProps, nextState) {
            if (nextState.list !== this.state.list && nextState.count > 1) {
                window.location.reload();
            }
        } */

    render() {
        return (
            <div>
                <Sortable className="form-group row">
                    {
                        this.state.list.map((a, index) => (
                            <div className="col-md-6 col-sm-12" style={{ padding: "8px", marginTop : "8px" }} key={index}>
                                <Local localId={a} />
                            </div>
                        ))
                    }
                </Sortable>
            </div>
        )
    }
}

export default main;
