import React, { Component } from 'react';
import Firebase from 'firebase';
import Mesa from './mesa'

class local extends Component {

    constructor() {
        super();
        this.state = {
            mesas: []
        };
    }

    setNames(props) {
        var id = parseInt(props.localId);
        const contex = this;
        var localRef = Firebase.database().ref('locales');
        localRef.orderByChild("local_id").equalTo(id).on("value", resp => {
            this.setState({ name: resp.val()[id].nombre_local })
        });
        var id = parseInt(props.localId);
        var localRef = Firebase.database().ref('locales');
        localRef.orderByChild("local_id").equalTo(id).on("value", resp => {
            this.setState({ name: resp.val()[id].nombre_local, comuna: resp.val()[id].nombre_consulado ? resp.val()[id].nombre_consulado : resp.val()[id].nombre_comuna })
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setNames(nextProps);
        var mesasRef = Firebase.database().ref("status_mesas/" + nextProps.localId);
        mesasRef.on('value', snap => {
            var mesasArr = [];
            snap.forEach(function (childSnapshot) {
                if (childSnapshot.key !== "timestamp") {
                    mesasArr.push(childSnapshot.key);
                }
            });
            this.setState({ mesas: mesasArr });
        })
    }

    componentWillMount() {
        this.setNames(this.props);
        var mesasRef = Firebase.database().ref("status_mesas/" + this.props.localId);
        mesasRef.on('value', snap => {
            var mesasArr = [];
            snap.forEach(function (childSnapshot) {
                if (childSnapshot.key !== "timestamp") {
                    mesasArr.push(childSnapshot.key);
                }
            });
            this.setState({ mesas: mesasArr });
        })
    }


    render() {
        return (
            <div className="card">
                <div className="card-header" style={this.state.active ? { backgroundColor: "#20a8d8", color: "#FFF" } : {}}>
                    <h5>{this.state.name}</h5>
                    <strong>
                        <i className="fa fa-fw fa-star"></i>
                        {this.state.comuna}
                    </strong>
                </div>
                <div className="card-body" style={{ padding: "10px" }}>
                    <table className="table table-bordered table-sm table-hover">
                        <thead>
                            <th>
                                Mesa
                            </th>
                            <th>
                                Periodista
                            </th>
                            <th>
                                Goic
                            </th>
                            <th>
                                Kast
                            </th>
                            <th>
                                Piñera
                            </th>
                            <th>
                                Guiller
                            </th>
                            <th>
                                Sanchez
                            </th>
                            <th>
                                Ominami
                            </th>
                            <th>
                                Artes
                            </th>
                            <th>
                                Navarro
                            </th>
                        </thead>
                        <tbody>
                            {
                                this.state.mesas.map((m, index) => (
                                    <Mesa mesaId={m} localId={this.props.localId} key={index} />
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default local;
