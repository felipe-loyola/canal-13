import React, { Component } from 'react';
import Firebase from 'firebase';
import Senador from './senador'
import Listado from './listado'

class senadores extends Component {

    constructor() {
        super();
        this.state = {
            count: 0,
            votos: 0
        };
    }

    renderSenador(d, index) {
        if (d.COD_ZONA === parseInt(this.props.circunscripcion)) {
            var actived = false;
            this.props.activados.map((a) => {
                if (a.codCand === d.COD_CAND) {
                    actived = true;
                }
            })
            return (
                <div key={index} className="col-md-2" style={{ marginBottom: "10px" }}>
                    <Senador candidato={d} circunscripcion={this.props.circunscripcion} actived={actived} />
                </div>
            )
        }
    }


    render() {
        return (
            <div className="form-group row">
                {
                    this.props.activados ?
                        Object.values(Listado).map((d, index) => (
                            this.renderSenador(d, index)
                        ))
                        :
                        ""
                }
            </div>
        )
    }
}

export default senadores;
