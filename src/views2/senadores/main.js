import React, { Component } from 'react';
import Firebase from 'firebase';
import Senadores from './senadores'
import Circunscripcion from './circunscripciones'


class main extends Component {

    constructor() {
        super();
        this.state = {
            circunscripcion: Object.keys(Circunscripcion)[0]
        };
    }

    componentWillMount() {
        const contex = this;
        const url = 'http://10.12.1.123:8080/operatio_votaciones/api/candidatos/zona/' + "5001" + '/eleccion/5/graficar/1';
        fetch(url)
            .then(server => {
                return server.json();
            }).then(response => {
                contex.setState({ activados: response, ready: true })
            }).catch(err => console.log(err));
    }


    getNewActived(circunscripcion) {
        const contex = this;
        const url = 'http://10.12.1.123:8080/operatio_votaciones/api/candidatos/zona/' + circunscripcion + '/eleccion/5/graficar/1';
        fetch(url)
            .then(server => {
                return server.json();
            }).then(response => {
                contex.setState({ activados: response, ready: true })
            }).catch(err => console.log(err));
    }

    render() {
        return (
            <div >
                <div className="form-group row">
                    <div className="col-md-2" style={{background : "#FFF", border : "1px solid #20c2e0", borderRadius :"5px"}}>
                        <section className="section section--menu">
                            <span className="link-copy"></span>
                            <nav className="menu menu--ferdinand">
                                {
                                    Object.values(Circunscripcion).map((d, index) => (
                                        <li key={index} className={this.state.circunscripcion === Object.keys(Circunscripcion)[index] ? "menu__item menu__item--current" : "menu__item"}>
                                            <a onClick={() => {
                                                this.setState({ circunscripcion: Object.keys(Circunscripcion)[index], ready: false });
                                                this.getNewActived(Object.keys(Circunscripcion)[index])
                                            }}
                                                style={{ cursor: "pointer", textAlign: "left" }} className="menu__link">{d}</a>
                                        </li>
                                    ))
                                }
                            </nav>
                        </section>
                    </div>
                    <div className="col-md-10" style={{ width: "80%" }}>
                        <div>
                            {
                                this.state.ready ?
                                    <p><Senadores circunscripcion={this.state.circunscripcion} activados={this.state.activados} /></p>
                                    :
                                    ""
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default main;
