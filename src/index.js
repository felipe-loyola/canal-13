import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history';
import Header from './components/Header/Header';
// Containers
import Full from './containers/Full/'
import Firebase from 'firebase';

var config = {
  apiKey: "AIzaSyAUvLaW6Uurus6Eja6e7KrMbvuxXkZJYSo",
  authDomain: "operatio-votaciones-c484b.firebaseapp.com",
  databaseURL: "https://operatio-votaciones-c484b.firebaseio.com",
  projectId: "operatio-votaciones-c484b",
  storageBucket: "operatio-votaciones-c484b.appspot.com",
  messagingSenderId: "752721535433"
};
Firebase.initializeApp(config);

const history = createBrowserHistory();

ReactDOM.render((
  <HashRouter history={history}>
    <Switch>
      <Route path="/" name="Home" component={Full} />
    </Switch>
  </HashRouter>
), document.getElementById('root'))
