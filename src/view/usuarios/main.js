import React, { Component } from 'react';
import Firebase from 'firebase';
import UsuarioItem from './usuarioItem'
import Locales from '../../datasets/locales'
import Usuario from '../../datasets/usuarios'
import Periodistas from '../../datasets/periodistas'

var DataTable = require('react-data-components').DataTable;

class main extends Component {

    constructor() {
        super();
        this.state = {
            usuarios: [],
            tittles: [
                { title: 'Nombre', prop: 'name' },
                { title: 'Perfil', prop: 'profile' },
                { title: 'Comuna', prop: 'comuna' },
                { title: 'Periodista', prop: 'periodista' },
                { title: 'Local', prop: 'local' },
                { title: 'Mesa', prop: 'mesa' },
                { title: 'Última conexión', prop: 'timeStamp' },
                { title: "Versión", prop: "version" }
            ]
        }
    }

    DateToFormattedString(time) {
        var d = new Date(time)
        var yyyy = d.getFullYear().toString();
        var mm = (d.getMonth() + 1).toString();
        var dd = d.getDate().toString();
        var hh = d.getHours().toString();
        var mn = d.getMinutes().toString();
        return yyyy + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + (dd[1] ? dd : "0" + dd[0]) + " " + (hh[1] ? hh : "0" + hh[0]) + ':' + (mn[1] ? mn : "0" + mn[0]);
    };

    componentDidMount() {
        const contex = this;
        var periodistaRef = Firebase.database().ref("users").orderByChild("localId").on("value", snap => {
            if (snap.val()) {
                var items = [];
                var columns = [];
                snap.forEach(function (childSnapshot) {
                    if (childSnapshot.val().email) {
                        items.push(childSnapshot.val());
                        columns.push({
                            'name': Usuario[childSnapshot.val().email] ? Usuario[childSnapshot.val().email].nombre : childSnapshot.val().email,
                            'profile': Usuario[childSnapshot.val().email] ? Usuario[childSnapshot.val().email].perfil : "",
                            'comuna': Usuario[childSnapshot.val().email] ? Usuario[childSnapshot.val().email].comuna : "",
                            'periodista': childSnapshot.val().periodista ? Periodistas[childSnapshot.val().periodista].nombre : "",
                            'local': Locales[childSnapshot.val().localId] ? Locales[childSnapshot.val().localId].label : "Sin Local",
                            'mesa': childSnapshot.val().mesaId,
                            'timeStamp': childSnapshot.val().timeStamp ? contex.DateToFormattedString(childSnapshot.val().timeStamp) : "",
                            "version": childSnapshot.val().version ? "app: " + childSnapshot.val().version.app + " / " + "build: " + childSnapshot.val().version.build : ""
                        })
                    }
                });
                this.setState({ usuarios: items, data: columns });
            }
        })
    }


    render() {
        var h = window.innerHeight;
        return (
            <div className="animated fadeIn">
                <div className="animated fadeIn">
                    <div style={{ height: (h - 54), overflow: "auto", position: "absolute", top: "85", width: "98%" }}>
                        <DataTable
                            keys="name"
                            columns={this.state.tittles}
                            initialPageLength={150}
                            initialData={this.state.data}
                            initialSortBy={{ prop: 'timeStamp', order: 'descending' }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default main;
