import React, { Component } from 'react';
import Firebase from 'firebase';
import Locales from '../../datasets/locales'
import Periodistas from '../../datasets/periodistas'
import Usuario from '../../datasets/usuarios'
class usuarioItem extends Component {

    constructor() {
        super();
        this.state = {

        }
    }

    componentWillMount() {
    }

    DateToFormattedString(time) {
        var d = new Date(time)
        var yyyy = d.getFullYear().toString();
        var mm = (d.getMonth() + 1).toString();
        var dd = d.getDate().toString();
        var hh = d.getHours().toString();
        var mn = d.getMinutes().toString();
        return yyyy + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + (dd[1] ? dd : "0" + dd[0]) + " " + (hh[1] ? hh : "0" + hh[0]) + ':' + (mn[1] ? mn : "0" + mn[0]);
    };

    render() {
        return (
            <tr>
                <td>{Usuario[this.props.usuario.email] ? Usuario[this.props.usuario.email].Nombre : this.props.usuario.email  }</td>
                <td>{Usuario[this.props.usuario.email] ? Usuario[this.props.usuario.email].Perfil : ""  }</td>
                <td>{Usuario[this.props.usuario.email] ? Usuario[this.props.usuario.email].Comuna : "" }</td>
                <td>{this.props.usuario.periodista ? Periodistas[this.props.usuario.periodista].nombre : ""}</td>
                <td>{this.props.usuario.localId ? Locales[this.props.usuario.localId].label : "Sin Local"}</td>
                <td>{this.props.usuario.mesaId}</td>
                <td>{this.DateToFormattedString(this.props.usuario.timeStamp)}</td>
            </tr>
        );
    }
}

export default usuarioItem;
