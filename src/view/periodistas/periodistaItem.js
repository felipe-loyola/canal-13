import React, { Component } from 'react';


class periodistaItem extends Component {

    constructor() {
        super();
        this.state = {
            actived: false
        }
    }

    componentWillMount() {
    }

    componentWillReceiveProps(nextProps) {
        const contex = this;
        if (nextProps.periodista[110003] !== this.props.periodista[110003] || nextProps.periodista[110004] !== this.props.periodista[110004]) {
           this.setState({ actived: true })
        }
        setTimeout(function () {
            contex.setState({ actived: false });
        }, 30000);
    }

    render() {
        return (
            <tr style={this.state.actived ? { background: "#279766", color : "#FFF" } : {}}>
                <td style={{ borderLeft: "2px solid #4dbd74", textTransform: "uppercase" }}>{this.props.periodista.nombre}</td>
                <td>{this.props.periodista.comuna}</td>
                <td>{this.props.periodista.local}</td>
                <td>{this.props.periodista.mesa}</td>
                <td>{this.props.periodista[110003]}</td>
                <td>{this.props.periodista[110004]}</td>
            </tr>
        );
    }
}

export default periodistaItem;
