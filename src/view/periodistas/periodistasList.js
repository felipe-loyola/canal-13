import React, { Component } from 'react';
import Firebase from 'firebase';
import PeriodistaItem from './periodistaItem';
import Consolidado from './consolidado/main'


class periodistasList extends Component {

  constructor() {
    super();
    this.state = {
      periodistas: []
    }
  }

  componentDidMount() {
    var periodistaRef = Firebase.database().ref("periodistas").on("value", snap => {
      if (snap.val()) {
        var items = []
        snap.forEach(function (childSnapshot) {
          items.push(childSnapshot.val())
        });
        this.setState({ periodistas: items, data: snap.val() });
      }
    })
  }


  render() {
    var h = window.innerHeight;
    return (
      <div className="animated fadeIn">
        <br />
        <div style={{ height: (h - 227), overflow: "auto" }}>
          <table className="table table-sm table-responsive" style={{ cursor: "pointer", fontSize :"12px" }}>
            <thead>
              <tr>
                <th>Periodista</th>
                <th>Comuna</th>
                <th>Local</th>
                <th>Mesa</th>
                <th>Piñera</th>
                <th>Guillier</th>
              </tr>
            </thead>
            {
              this.state.periodistas.map((p, index) => (
                <tbody key={index}>
                  <PeriodistaItem periodista={p} />
                </tbody>
              ))
            }
          </table>
        </div>
        <div style={{ height: "173px", position: "absolute", left: "0", width: "100%" }}>
          <Consolidado />
        </div>
      </div>
    );
  }
}

export default periodistasList;
