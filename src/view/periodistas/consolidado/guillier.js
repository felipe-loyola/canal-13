import React, { Component } from 'react';
import Firebase from 'firebase';

const colors = {
    "Green": "#4CAF50",
    "Purple": "#9C27B0",
    "Blue": "#2196F3",
    "Teal": "#009688",
    "Brown": "#795548",
    "Grey": "#9E9E9E",
    "Red": "#F44336",
    "Amber": "#FFC107"
}

class guiller extends Component {

    constructor() {
        super();
        this.state = {
            full: false,
            icon: 0,
            totalGeneral: 412390,
            totalComuna: 89767,
            totalLocal: 5123,
            totalMesa: 32
        };
    }

    componentWillMount(){
        console.log( window.innerHeight)
    }


    format(number) {
        var n = parseInt(number);
        return n.toLocaleString("de-De", {});
    }

    render() {
        var h = window.innerHeight;
        return (
            <div>
                <div className="votos--guiller" style={ h > 700 ? { position: "relative", right: "462px", fontSize: "35px", float :"right !important" }:{ position: "relative", right: "323px", fontSize: "35px", float :"right !important" }}>
                    {this.format(this.props.candidato.votos)}
                </div>
                <div style={{position :"absolute", top : "0"}}>
                    <img src={'candidatos/' + this.props.candidato.foto} width="141" />
                </div>
            </div >
        )
    }
}

export default guiller;
