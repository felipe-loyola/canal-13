import React, { Component } from 'react';
import Firebase from 'firebase';

class barra extends Component {

    constructor() {
        super();
        this.state = {

        };
    }

    componentWillReceiveProps(nextProps) {
        var p = (nextProps.piñera * 100) / nextProps.total
        this.setState({ porcentajePiñera: p.toFixed(1) })
        var g = (nextProps.guiller * 100) / nextProps.total
        this.setState({ porcentajeGuiller: g.toFixed(1) })
    }


    componentWillMount() {

    }


    render() {
        return (
            <div className="text-center">
                <div style={{ width: "100%", height: "40px", background: "#FFF", position: "absolute", left: "0", right: "0", margin: "auto", top: "60px" }}>
                    <div style={{ width: !isNaN(this.state.porcentajePiñera) ? this.state.porcentajePiñera + "%" : "50%", background: "#1985ac", height: "40px" }}>
                        <p style={{ color: "#FFF", fontSize: "30px", position: "relative", zIndex: "10" }}>
                            {!isNaN(this.state.porcentajePiñera) && this.state.porcentajePiñera !== undefined ? this.state.porcentajePiñera + "%" : 0 + "%"}
                        </p>
                    </div>
                    <div style={{ width: !isNaN(this.state.porcentajeGuiller) ? this.state.porcentajeGuiller + "%" : "50%", background: "#f63c3a", height: "40px", top: "0", right: "0", position: "absolute" }}>
                        <p style={{ color: "#FFF", fontSize: "30px", position: "relative" }}>
                            {!isNaN(this.state.porcentajeGuiller) && this.state.porcentajeGuiller !== undefined ? this.state.porcentajeGuiller + "%" : 0 + "%"}
                        </p>
                    </div>
                </div>
            </div >
        )
    }
}

export default barra;
