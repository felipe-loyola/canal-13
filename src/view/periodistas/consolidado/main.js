import React, { Component } from 'react';
import Firebase from 'firebase';
import Guillier from './guillier'
import Piñera from './piñera'
import Barra from './barra'


class main extends Component {

    constructor() {
        super();
        this.state = {
            candidatos: {
                "110004": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    partido: "Independiente",
                    color: "Teal"
                },
                "110003": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Pinera.jpg",
                    partido: "Chile Vamos",
                    color: "Blue"
                }
            },
            total: 0,
            selected: "todo"
        };
    }

    getVotos(id) {
        var arr = this.state.candidatos;
        var total_votos = this.state.total;
        var candidatoClient = Firebase.database().ref('snap_presidentes/candidatos/' + id);
        candidatoClient.on("value", snap => {
            var total = 0;
            Object.values(snap.val().paises).map((p, indexPais) => {
                var total_comuna = 0;
                Object.values(p.comunas).map((c, indexComunas) => {
                    var total_locales = 0;
                    Object.values(c.locales).map((l) => {
                        var total_local = 0;
                        Object.values(l.mesas).map((m) => {
                            total_local = m + total_local;
                        })
                        total_locales = total_local + total_locales;
                    })
                    total_comuna = total_locales + total_comuna;
                })
                total = total_comuna + total;
            })
            arr[id].votos = total
            total_votos = total_votos + total;
            this.setState({ candidatos: arr, total: total_votos })
        })
    }

    componentDidMount() {
        Object.keys(this.state.candidatos).map((c, index) => {
            this.getVotos(c);
        })
        console.log("este",window.innerWidth)
    }

    setTotal() {
        var total = 0;
        Object.values(this.state.candidatos).map((c, index) => {
            total = total + c.votos
        })
        return total;
    }

    render() {
        var total_votos = this.setTotal();
        return (
            <div className="form-group row" style={{borderTop :"2px solid #CCC", padding :"3px"}}>
                <div className="col-md-3" style={window.innerWidth > 1500 ? { height: "100%", position: "relative", left: "325px" } : { height: "100%", position: "relative", left: "187px" }}>
                    <Piñera candidato={Object.values(this.state.candidatos)[0]} id={Object.keys(this.state.candidatos)[0]} />
                </div>
                <div className="col-md-6">
                    <Barra piñera={this.state.candidatos[110003].votos} guiller={this.state.candidatos[110004].votos} total={total_votos} />
                </div>
                <div className="col-md-3" style={window.innerWidth > 1000 ? { height: "100%", position: "relative", right: "9px" } : { height: "100%", position: "relative", right: "12px" }}>
                    <Guillier candidato={Object.values(this.state.candidatos)[1]} id={Object.keys(this.state.candidatos)[1]} />
                </div>
            </div >
        )
    }
}

export default main;
