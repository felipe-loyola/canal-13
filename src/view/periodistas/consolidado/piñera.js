import React, { Component } from 'react';
import Firebase from 'firebase';

const colors = {
    "Green": "#4CAF50",
    "Purple": "#9C27B0",
    "Blue": "#2196F3",
    "Teal": "#009688",
    "Brown": "#795548",
    "Grey": "#9E9E9E",
    "Red": "#F44336",
    "Amber": "#FFC107"
}

class piñera extends Component {

    constructor() {
        super();
        this.state = {
            full: false,
            icon: 0,
            totalGeneral: 412390,
            totalComuna: 89767,
            totalLocal: 5123,
            totalMesa: 32
        };
    }


    format(number) {
        var n = parseInt(number);
        return n.toLocaleString("de-De", {});
    }

    render() {
        return (
            <div>
                <div style={{ position: "absolute", left: "157px", width :"150px", height :"50px", fontSize :"35px"}}>
                    {this.format(this.props.candidato.votos)}
                </div>
                <div>
                    <img src={'candidatos/' + this.props.candidato.foto} width="141" />
                </div>
            </div >
        )
    }
}

export default piñera;
