import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  render() {
    return (

      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
            <li className="nav-title">
              Opciones
            </li>
            <li className={this.activeRoute("/candidatos")}>
              <Link to={'/candidatos'} className="nav-link" >
                <i className="fa fa-search" aria-hidden="true"></i> Consolidado
              </Link>
            </li>
            <li className={this.activeRoute("/mesas")}>
              <Link to={'/mesas'} className="nav-link"  >
                <i className="fa fa-check" aria-hidden="true"></i> Supervisores
              </Link>
            </li>
            <li className={this.activeRoute("/comunas")}>
              <Link to={'/comunas'} className="nav-link" >
                <i className="fa fa-globe" aria-hidden="true"></i> Comunas
            </Link>
            </li>
            <li className={this.activeRoute("/locales")}>
              <Link to={'/locales'} className="nav-link" >
                <i className="fa fa-truck" aria-hidden="true"></i> Locales
              </Link>
            </li>
            <li className={this.activeRoute("/presidentes")}>
              <Link to={'/presidentes'} className="nav-link" >
                <i className="fa fa-id-badge" aria-hidden="true"></i> Presidentes
              </Link>
            </li>
            <li className={this.activeRoute("/diputados")}>
              <Link to={'/diputados'} className="nav-link" >
                <i className="fa fa-id-badge" aria-hidden="true"></i> Diputados
              </Link>
            </li>
            <li className={this.activeRoute("/senadores")}>
              <Link to={'/senadores'} className="nav-link" >
                <i className="fa fa-id-badge" aria-hidden="true"></i> Senadores
              </Link>
            </li>
            <li className={this.activeRoute("/periodistas")}>
              <Link to={'/periodistas'} className="nav-link" >
                <i className="fa fa-microphone" aria-hidden="true"></i> Periodistas
              </Link>
            </li>
          </ul>
        </nav>
        <button className="sidebar-minimizer" type="button"></button>
      </div >
    )
  }
}

export default Sidebar;
