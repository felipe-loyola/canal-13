import React, { Component } from 'react';
import Firebase from 'firebase';
import PresidenteItem from './presidenteItem'
import Listado from './Listado'
import Sortable from 'react-sortablejs';
import LocalesDataset from '../../datasets/locales'
import PaisesDatasets from '../../datasets/paises'
import ComunasDatasets from '../../datasets/comunas'
import ConsuladosDataset from '../../datasets/consulados'

import Autocomplete from "react-autocomplete"
import Select from 'react-select';


const style = {
    background: "#FFF",
    border: "1px solid #bbb",
    paddingTop: "15px",
    paddingLeft: "30px",
    borderRadius: "5px"
}

class presidentesList extends Component {

    constructor() {
        super();
        this.state = {
            candidatos: {
                "110007": {
                    nombre: "Eduardo Artés B.",
                    foto: "Artes.jpg",
                    partido: "Unión Patriótica",
                    color: "Red"
                },
                "110004": {
                    nombre: "Alejandro Guillier A.",
                    foto: "Guillier.jpg",
                    partido: "Independiente",
                    color: "Teal"
                },
                "110002": {
                    nombre: "José Antonio Kast",
                    foto: "Kast.jpg",
                    partido: "Independiente",
                    color: "Purple"
                },
                "110008": {
                    nombre: "Alejandro Navarro B.",
                    foto: "Navarro.jpg",
                    partido: "País",
                    color: "Amber"
                },
                "110001": {
                    nombre: "Carolina Goic B.",
                    foto: "Goic.jpg",
                    partido: "DC",
                    color: "Green"
                },
                "110006": {
                    nombre: "M. Enríquez-Ominami",
                    foto: "Ominami.jpg",
                    partido: "Progresista",
                    color: "Grey"
                },
                "110005": {
                    nombre: "Beatriz Sánchez M.",
                    foto: "Sanchez.jpg",
                    partido: "Frente Amplio",
                    color: "Brown"
                },
                "110003": {
                    nombre: "Sebastián Piñera E.",
                    foto: "Pinera.jpg",
                    partido: "Chile Vamos",
                    color: "Blue"
                }
            },
            total: 0
        };
    }

    getVotos(id) {
        var arr = this.state.candidatos;
        var total_votos = this.state.total;
        var candidatoClient = Firebase.database().ref('snap_presidentes/candidatos/' + id);
        candidatoClient.on("value", snap => {
            var total = 0;
            Object.values(snap.val().paises).map((p, indexPais) => {
                var total_comuna = 0;
                Object.values(p.comunas).map((c, indexComunas) => {
                    var total_locales = 0;
                    Object.values(c.locales).map((l) => {
                        var total_local = 0;
                        Object.values(l.mesas).map((m) => {
                            total_local = m + total_local;
                        })
                        total_locales = total_local + total_locales;
                    })
                    total_comuna = total_locales + total_comuna;
                })
                total = total_comuna + total;
            })
            arr[id].votos = total
            total_votos = total_votos + total;
            this.setState({ candidatos: arr, total: total_votos })
        })
    }

    componentWillMount() {
        Object.keys(this.state.candidatos).map((c, index) => {
            this.getVotos(c);
        })
    }


    setTotal() {
        var total = 0;
        Object.values(this.state.candidatos).map((c, index) => {
            total = total + c.votos
        })
        return total;
    }



    render() {
        var total_votos = this.setTotal();
        return (
            <div style={{ padding: "15%" }}>
                <section className="container">
                    <Sortable className="row active-with-click">
                        {
                            this.state.candidatos ?
                                Object.values(this.state.candidatos).map((c, index) => (
                                    <div className="col-md-3 col-sm-6 col-xs-12" key={index} style={{ marginBottom: "2%" }}>
                                        <PresidenteItem candidato={c} id={Object.keys(this.state.candidatos)[index]} total={total_votos} />
                                    </div>
                                ))
                                :
                                ""
                        }
                    </Sortable>
                </section>
            </div>
        )
    }
}

export default presidentesList;
