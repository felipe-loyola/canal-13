import React, { Component } from 'react';
import Firebase from 'firebase';

const colors = {
    "Green": "#4CAF50",
    "Purple": "#9C27B0",
    "Blue": "#2196F3",
    "Teal": "#009688",
    "Brown": "#795548",
    "Grey" : "#9E9E9E",
    "Red" : "#F44336",
    "Amber" : "#FFC107"
}

class presidenteItem extends Component {

    constructor() {
        super();
        this.state = {
            full: false,
            icon: 0,
            totalGeneral: 412390,
            totalComuna: 89767,
            totalLocal: 5123,
            totalMesa: 32
        };
    }

    componentWillMount(){
        var p = (this.props.candidato.votos * 100) / this.props.total
        this.setState({ porcentaje: p.toFixed(1) })
    }

    componentWillReceiveProps(nextProps) {
        var p = (nextProps.candidato.votos * 100) / nextProps.total
        this.setState({ porcentaje: p.toFixed(1) })
    }

    render() {
        var porcentajeStyle = {
            position: "absolute",
            zIndex: "10",
            color: "#FFF",
            right: "3px",
            fontSize: "25px",
            top: "4px",
            border: "1px solid #FFF",
            padding: "5px",
            background: colors[this.props.candidato.color]
        }
        var card = "material-card " + this.props.candidato.color;
        var cardFull = "material-card " + this.props.candidato.color + " mc-active";
        return (
            <article className={this.state.full ? cardFull : card} style={this.state.full ? { height: "450px" } : {}}>
                <h2 className="text-center">
                    <span style={{ fontSize: "40px" }}>{this.props.candidato.votos}</span>
                </h2>
                <div style={porcentajeStyle}>
                    {!isNaN(this.state.porcentaje) && this.state.porcentaje !== undefined ? this.state.porcentaje + "%" : 0 + "%"}
                </div>
                <div className="mc-content">
                    <div className="img-container">
                        <img className="img-responsive" src={'candidatos/' + this.props.candidato.foto} width="100%" />
                    </div>
                    <div className="mc-description">
                        <div className="form-group" style={{ top: "45px", position: "absolute", right: "0", left: "0" }}>

                        </div>
                    </div>
                </div>
                {
                    /*<a className="mc-btn-action" onClick={(e) => { this.showFull(e) }}>
                    {this.renderIcon()}
                    </a> */
                }
            </article >
        )
    }
}

export default presidenteItem;
