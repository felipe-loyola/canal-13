import React, { Component } from 'react';
import Firebase from 'firebase';
import Senadores from './senadores'
import Circunscripcion from './circunscripciones'


class main extends Component {

    constructor() {
        super();
        this.state = {
            circunscripcion: Object.keys(Circunscripcion)[0]
        };
    }

    componentWillMount() {
        const contex = this;
        const url = 'http://34.229.87.250:8080/operatio_votaciones/api/candidatos/zona/' + "5001" + '/eleccion/5/grafica/1';
        fetch(url)
            .then(server => {
                return server.json();
            }).then(response => {
                contex.setState({ activados: response, ready: true })
            }).catch(err => console.log(err));
    }


    getNewActived(circunscripcion) {
        const contex = this;
        const url = 'http://34.229.87.250:8080/operatio_votaciones/api/candidatos/zona/' + circunscripcion + '/eleccion/5/grafica/1';
        fetch(url)
            .then(server => {
                return server.json();
            }).then(response => {
                contex.setState({ activados: response, ready: true })
            }).catch(err => console.log(err));
    }

    render() {
        return (
            <div >
                <div>
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item active">Senadores</li>
                        <li className="breadcrumb-item"><a href="/#/activar/diputados">Diputados</a></li>
                        <li className="breadcrumb-item"><a href="/#/mesas">Mesas</a></li>
                        <li className="breadcrumb-item"><a href="/#/candidatos">Presidenciales</a></li>
                    </ol>
                </div>
                <div className="form-group row">
                    <div className="col-md-2">
                        <section className="section section--menu">
                            <span className="link-copy"></span>
                            <nav className="menu menu--ferdinand">
                                {
                                    Object.values(Circunscripcion).map((d, index) => (
                                        <li key={index} className={this.state.circunscripcion === Object.keys(Circunscripcion)[index] ? "menu__item menu__item--current" : "menu__item"}>
                                            <a onClick={() => {
                                                this.setState({ circunscripcion: Object.keys(Circunscripcion)[index], ready: false });
                                                this.getNewActived(Object.keys(Circunscripcion)[index])
                                            }}
                                                style={{ cursor: "pointer", textAlign: "left" }} className="menu__link">{d}</a>
                                        </li>
                                    ))
                                }
                            </nav>
                        </section>
                    </div>
                    <div className="col-md-10" style={{ width: "80%" }}>
                        <div className="acc">
                            <dl>
                                <dt>
                                    <a className="acc_title acc_title_active">
                                        {this.state.name}
                                    </a>
                                    <dd className="acc_panel anim_in">
                                        {
                                            this.state.ready ?
                                                <p><Senadores circunscripcion={this.state.circunscripcion} activados={this.state.activados} /></p>
                                                :
                                                ""
                                        }
                                    </dd>
                                </dt>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default main;
