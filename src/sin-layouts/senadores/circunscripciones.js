const circunscripciones =
    {
        5001: "1a Circunscripcion",
        5002: "2a Circunscripcion",
        5003: "3a Circunscripcion",
        5004: "4a Circunscripcion",
        5005: "5a Circunscripcion",
        5006: "6a Circunscripcion",
        5007: "7a Circunscripcion",
        5008: "8a Circunscripcion",
        5009: "9a Circunscripcion",
        5010: "10a Circunscripcion",
        5011: "11a Circunscripcion",
        5012: "12a Circunscripcion",
        5013: "13a Circunscripcion",
        5014: "14a Circunscripcion",
        5015: "15a Circunscripcion"
    }

export default circunscripciones;