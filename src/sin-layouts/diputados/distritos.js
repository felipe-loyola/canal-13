const distritos =
    {
        6001: "1er Distrito"
        ,
        6002: "2° Distrito"
        ,
        6003: "3er Distrito"
        ,
        6004: "4° Distrito"
        ,
        6005: "5° Distrito"
        ,
        6006: "6° Distrito"
        ,
        6007: "7° Distrito"
        ,
        6008: "8° Distrito"
        ,
        6009: "9° Distrito"
        ,
        6010: "10° Distrito"
        ,
        6011: "11er Distrito"
        ,
        6012: "12° Distrito"
        ,
        6013: "13er Distrito"
        ,
        6014: "14° Distrito"
        ,
        6015: "15° Distrito"
        ,
        6016: "16° Distrito"
        ,
        6017: "17° Distrito"
        ,
        6018: "18° Distrito"
        ,
        6019: "19° Distrito"
        ,
        6020: "20° Distrito"
        ,
        6021: "21er Distrito"
        ,
        6022: "22° Distrito"
        ,
        6023: "23er Distrito"
        ,
        6024: "24° Distrito"
        ,
        6025: "25° Distrito"
        ,
        6026: "26° Distrito"
        ,
        6027: "27° Distrito"
        ,
        6028: "28° Distrito"
    }

export default distritos;