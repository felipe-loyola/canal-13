import React, { Component } from 'react';
import Firebase from 'firebase';
import Listado from './listado'


class diputado extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            votos: 0,
            actived: this.props.actived
        };
    }

    setActived(e) {
        const contex = this;
        const url = 'http://34.229.87.250:8080/operatio_votaciones/api/candidatos';
        var data = {
            "codCand": this.props.candidato.COD_CAND,
            "grafica": e 
        }
        console.log(data)
        fetch(url, {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "cache-control": "no-cache"
                //        "Authorization": "Bearer " 
            }
        })
            .then(server => {
                return server.json();
            }).then(response => {
                console.log('candidatos response', response);
            }).catch(err => console.log(err));
    }

    componentWillMount() {
        const contex = this;
        var diputadoRef = Firebase.database().ref('snap_diputados/distritos/' + this.props.distrito + "/candidatos/" + this.props.candidato.COD_CAND + "/comunas/");
        diputadoRef.on("value", snap => {
            var total_candidato = 0;
            snap.forEach(function (comuna) {
                var total_comuna = 0;
                Object.values(comuna.val()).map((locales) => {
                    var total_local = 0;
                    Object.values(locales).map((local) => {
                        var total_mesas = 0;
                        Object.values(local).map((mesas) => {
                            var total_mesa = 0;
                            Object.values(mesas).map((mesa) => {
                                total_mesa = total_mesa + mesa;
                            })
                            total_mesas = total_mesas + total_mesa
                        })
                        total_local = total_local + total_mesas;
                    })
                    total_comuna = total_comuna + total_local;
                })
                total_candidato = total_candidato + total_comuna;
            });
            this.setState({ votos: total_candidato, update: "0 0px 14px #00ff5a, 0 0 1px #208643 inset" })
            setTimeout(function () {
                contex.setState({ update: "" });
            }, 2000);
        })
    }

    render() {
        var styles = {
            border: "2px solid #CCC",
            borderRadius: "7px",
            height: "100%",
            color: "#FF5722",
            padding: "10px",
            paddingTop: "25px",
            boxShadow: this.state.update ? this.state.update : ""
        }
        return (
            <div style={styles} className="text-center">
                <div style={{ position: "relative", top: "-22px", left: "-48px", color: "darkgrey" }}>
                    <label className="label-cbx">
                        <input id="cbx" type="radio" className="invisible" checked={this.state.actived} onClick={() => { this.setState({ actived: !this.state.actived }); this.setActived(this.state.actived ? 0 : 1) }} />
                        <div className="checkbox">
                            <svg width="20px" height="20px" viewBox="0 0 20 20">
                                <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                                <polyline points="4 11 8 15 16 6"></polyline>
                            </svg>
                        </div>
                        <span>En pantalla</span>
                    </label>
                </div>
                {this.props.candidato.GLOSA_CAND}
                <h4 style={{ borderBottom: "1px solid rgba(0, 0, 0, 0.1)", marginTop: "10px", color: "#263238" }}>
                    {this.state.votos}
                </h4>
                <div className="text-muted">
                    votos
                </div>
            </div>
        )
    }
}

export default diputado;
