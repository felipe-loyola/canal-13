import React, { Component } from 'react';
import Firebase from 'firebase';


class mesa extends Component {

    constructor() {
        super();
        this.state = {
            periodista: "-",
            candidatos: {
                "110007": {
                    nombre: "Eduardo Artés B."
                },
                "110004": {
                    nombre: "Alejandro Guillier A."
                },
                "110002": {
                    nombre: "José Antonio Kast"
                },
                "110008": {
                    nombre: "Alejandro Navarro B."
                },
                "110001": {
                    nombre: "Carolina Goic B."
                },
                "110006": {
                    nombre: "M. Enríquez-Ominami"
                },
                "110005": {
                    nombre: "Beatriz Sánchez M."
                },
                "110003": {
                    nombre: "Sebastián Piñera E."
                }
            }
        };
    }

    componentWillMount(){
        
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            var id = parseInt(nextProps.localId);
            var presidentes = this.state.candidatos;
            var periodistaRef = Firebase.database().ref('periodista_status/' + nextProps.localId + "/" + nextProps.mesaId);
            periodistaRef.on("value", snap => {
                this.setState({ periodista: snap.val()? snap.val().name : "" })
            })
            var arrCandidatos = this.state.candidatos;
            var localesRef = Firebase.database().ref("locales");
            localesRef.child(nextProps.localId).once("value").then(resp => {
                Object.keys(this.state.candidatos).map((c) => {
                    var candidatoClient = Firebase.database().ref('snap_presidentes/candidatos/' + c + "/paises/" + resp.val().pais_id + "/comunas/" + resp.val().comuna_id + "/locales/" + nextProps.localId + "/mesas/" + nextProps.mesaId);
                    candidatoClient.on("value", snap => {
                        arrCandidatos[c].votos = snap.val() ? snap.val() : "-"
                        this.setState({ className: "table-success" });
                    });
                });
            })
            this.setState({ candidatos: arrCandidatos });
        }
    }

    componentWillUpdate(nextProps, nextState) {
        const contex = this;
        if (nextState.className === "table-success") {
            setTimeout(function () {
                contex.setState({ className: "" });
            }, 2000);
        }
    }

    render() {
        return (
            <tr>
                <td>
                    {this.props.mesaId}
                </td >
                <td>
                    {this.state.periodista}
                </td>
                <td className={this.state.className}>
                    {this.state.candidatos[110001].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110002].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110003].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110004].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110005].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110006].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110007].votos}
                </td >
                <td className={this.state.className}>
                    {this.state.candidatos[110008].votos}
                </td >
            </tr>
        )
    }
}

export default mesa;
