import React, { Component } from 'react';
import Firebase from 'firebase';


class voto extends Component {

    constructor() {
        super();
        this.state = {
            voto: "-"
        };
    }

    componentWillReceiveProps(nextProps) {
        var candidatoClient = Firebase.database()
            .ref('snap_presidentes/candidatos/' + nextProps.id + "/paises/" + nextProps.paisId + "/comunas/" + nextProps.comunaId + "/locales/" + nextProps.localId + "/mesas/" + nextProps.mesaId);
        candidatoClient.on("value", snap => {
            this.setState({ voto: snap.val() ? snap.val() : "-" });
        });
    }


    componentWillMount() {
        var candidatoClient = Firebase.database()
            .ref('snap_presidentes/candidatos/' + this.props.id + "/paises/" + this.props.paisId + "/comunas/" + this.props.comunaId + "/locales/" + this.props.localId + "/mesas/" + this.props.mesaId);
        candidatoClient.on("value", snap => {
            this.setState({ voto: snap.val() ? snap.val() : "-" });
        });
    }

    render() {
        return (
            <div>{this.state.voto}</div>
        )
    }
}

export default voto;
