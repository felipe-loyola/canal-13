import React, { Component } from 'react';
import Firebase from 'firebase';
import Local from './local'
import Sortable from 'react-sortablejs';


class main extends Component {

    constructor() {
        super();
        this.state = {
            list: [],
            menu: [],
            count: 0
        };
    }

    componentWillMount() {
        var SupervisorRef = Firebase.database().ref('supervisores');
        SupervisorRef.on('value', snap => {
            var array = []
            snap.forEach(function (childSnapshot) {
                array.push({ [childSnapshot.key]: childSnapshot.val() });
            });
            this.setState({ menu: array })
        })

    }

    setLocales(user, index) {
        const contex = this;
        this.setState({ list: [] })
        var mesasRef = Firebase.database().ref("status_mesas");
        var periodistaRef = Firebase.database().ref("periodista_status");
        mesasRef.on('value', snap => {
            var arr = new Array;
            snap.forEach(function (childSnapshot) {
                if (contex.state.menu[index][user][childSnapshot.key]) {
                    arr.push(childSnapshot.key)
                }
            });
            setTimeout(function () {
                contex.setState({ list: arr, count: contex.state.count + 1 })
            }, 200);
        })
    }


    render() {
        return (
            <div className="col-lg-12">
                <div className="row" >
                    <ul className="nav nav-tabs" style={{ position: "relative", left: "0", right: "0", margin: "auto" }}>
                        {
                            this.state.menu.map((m, index) => (
                                <li key={index} className="nav-item">
                                    <a className={this.state.actived === Object.keys(m)[0] ? "nav-link active" : "nav-link"} onClick={() => { this.setState({ actived: Object.keys(m)[0] }); this.setLocales(Object.keys(m)[0], index) }} >{Object.keys(m)[0]}</a>
                                </li>
                            ))
                        }
                    </ul>
                </div>
                <div className="row">
                    <div style={{ width: "100%", height: "100%", background: "#FFF", padding: "8px" }}>
                        <Sortable className="form-group row">
                            {
                                this.state.list.map((a, index) => (
                                    <div className="col-md-6 col-sm-12" key={index}>
                                        <Local localId={a} />
                                    </div>
                                ))
                            }
                        </Sortable>
                    </div>
                </div>
            </div >
        )
    }
}

export default main;
