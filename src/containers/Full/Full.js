import React, { Component } from 'react';
import { Link, Switch, Route, Redirect } from 'react-router-dom'
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';
import { withRouter } from "react-router-dom";
import Firebase from 'firebase';
import Periodistas from '../../view/periodistas/periodistasList'
import Users from '../../view/usuarios/main'
import Comunas from '../../view/comunas/comunasList'

class Full extends Component {

  constructor(props) {
    super(props);
    this.state = {
      login: true
    };
  }

  componentWillMount() {
    var user = Firebase.auth().currentUser;
    if (!user) {
      Firebase
        .auth()
        .signInWithEmailAndPassword("floyola@operatio.cl", "123456")
        .then(user => {
          console.log("login")
        })
        .catch(error => {
          console.log("error login")
        });
    }
  }

  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <div className="container-fluid">
              <Switch>
                <Route path="/comunas" name="Comunas" component={Comunas} />
                <Route path="/usuarios" name="Usuarios" component={Users} />
                <Route path="/periodistas" name="Periodistas" component={Periodistas} />
                <Redirect from="/" to="/periodistas" />
              </Switch>
            </div>
          </main>
          <Aside />
        </div>
      </div>
    );
  }
}

export default Full;
