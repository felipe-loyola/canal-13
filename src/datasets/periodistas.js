var periodistas = {
    p01: {
        nombre: "Alejandro Rivera"
    },
    p02: {
        nombre: "Alfonso Concha"
    },
    p03: {
        nombre: "Alvaro Paci"
    },
    p04: {
        nombre: "Andrea Chacon"
    },
    p05: {
        nombre: "Aranza Fernandez"
    },
    p06: {
        nombre: "Beatriz Apud"
    },
    p07: {
        nombre: "Carla Borquez"
    },
    p08: {
        nombre: "Carlos Zarate"
    },
    p09: {
        nombre: "Carolina Urrejola"
    },
    p10: {
        nombre: "Caroline Munoz"
    },
    p11: {
        nombre: "Catalina Guardia"
    },
    p12: {
        nombre: "Chantal Aguilar"
    },
    p13: {
        nombre: "Claudia Reilich"
    },
    p14: {
        nombre: "Comodin 1"
    },
    p15: {
        nombre: "Comodin 2"
    },
    p16: {
        nombre: "Comodin 3"
    },
    p17: {
        nombre: "Comodin 4"
    },
    p18: {
        nombre: "Comodin 5"
    },
    p19: {
        nombre: "Comodin 6"
    },
    p20: {
        nombre: "Comodin 7"
    },
    p21: {
        nombre: "Comodin 8"
    },
    p22: {
        nombre: "Comodin 9"
    },
    p23: {
        nombre: "Comodin 10"
    },
    p24: {
        nombre: "Cristian Pino"
    },
    p25: {
        nombre: "Cristina Gonzalez"
    },
    p26: {
        nombre: "Daniela Valenzuela"
    },
    p27: {
        nombre: "Danielle Thiers"
    },
    p28: {
        nombre: "Emilio Sutherland"
    },
    p29: {
        nombre: "Eugenio Figueroa"
    },
    p30: {
        nombre: "Extranjero"
    },
    p31: {
        nombre: "Felipe Espina"
    },
    p32: {
        nombre: "Felipe Varas"
    },
    p33: {
        nombre: "Flavia Cordella"
    },
    p34: {
        nombre: "Florencia Vial"
    },
    p35: {
        nombre: "Francisco Aleuanlli"
    },
    p36: {
        nombre: "Francisco Franulic"
    },
    p37: {
        nombre: "Germán Gatica"
    },
    p38: {
        nombre: "Hector Burgos"
    },
    p39: {
        nombre: "Jocelyn Cabrera"
    },
    p40: {
        nombre: "Jorge Hans"
    },
    p41: {
        nombre: "Jorge Iturrieta"
    },
    p42: {
        nombre: "Josefa Bustos"
    },
    p43: {
        nombre: "Karina Zuniga"
    },
    p44: {
        nombre: "Katherine Flores"
    },
    p45: {
        nombre: "Laura Padilla"
    },
    p46: {
        nombre: "Leo Castillo"
    },
    p47: {
        nombre: "Loreto Alvarez"
    },
    p48: {
        nombre: "Loreto Schaffeld"
    },
    p49: {
        nombre: "Marcelo Gonzalez"
    },
    p50: {
        nombre: "Maria Jose Soto"
    },
    p51: {
        nombre: "Maria Luisa Carrion"
    },
    p52: {
        nombre: "Maria Paz Sanchez"
    },
    p53: {
        nombre: "Marilyn Perez"
    },
    p54: {
        nombre: "Matias Gonzalez"
    },
    p55: {
        nombre: "Michelle Adam"
    },
    p56: {
        nombre: "Miguel Acuna"
    },
    p57: {
        nombre: "Monserrat Alvarez"
    },
    p58: {
        nombre: "Nancy Jara"
    },
    p59: {
        nombre: "Natalia Lopez"
    },
    p60: {
        nombre: "Nicolas Arce"
    },
    p61: {
        nombre: "Pablo Abarza"
    },
    p62: {
        nombre: "Pablo Alamos"
    },
    p63: {
        nombre: "Pablo Becerra"
    },
    p64: {
        nombre: "Pablo Gomez"
    },
    p65: {
        nombre: "Pablo Honorato"
    },
    p66: {
        nombre: "Paloma Avila"
    },
    p67: {
        nombre: "Patricio Martinez"
    },
    p68: {
        nombre: "Paula Godoy"
    },
    p69: {
        nombre: "Paulina Bravo"
    },
    p70: {
        nombre: "Polo Ramirez"
    },
    p71: {
        nombre: "Rita Diaz"
    },
    p72: {
        nombre: "Rodrigo Sepulveda"
    },
    p73: {
        nombre: "Rodrigo Vera"
    },
    p74: {
        nombre: "Sofia Obaid"
    },
    p75: {
        nombre: "Tomas Cancino"
    },
    p76: {
        nombre: "Vanessa Zuniga"
    },
    p77: {
        nombre: "Verioska Venegas"
    }
}

export default periodistas;