var locales = [
    {
      "value": 316,
      "label": "LICEO B-30 DE NINAS MARIA FRANCK DE MACDOUGALL"
    },
    {
      "value": 317,
      "label": "INSTITUTO DE MATEMATICAS PUCV MALAQUIAS MORALES MUNOZ"
    },
    {
      "value": 318,
      "label": "LICEO COEDUCACIONAL LA IGUALDAD"
    },
    {
      "value": 319,
      "label": "ESCUELA D-255 ALEMANIA"
    },
    {
      "value": 320,
      "label": "ESCUELA A-19 HERNAN OLGUIN"
    },
    {
      "value": 321,
      "label": "ESCUELA REPUBLICA DEL PARAGUAY"
    },
    {
      "value": 322,
      "label": "LICEO JUANA ROSS DE EDWARDS"
    },
    {
      "value": 323,
      "label": "INSTITUTO SUPERIOR DE COMERCIO FRANCISCO ARAYA BENNET"
    },
    {
      "value": 324,
      "label": "ESCUELA D-272 JUAN JOSE LATORRE"
    },
    {
      "value": 325,
      "label": "LICEO TECNICO PROFESIONAL BARON"
    },
    {
      "value": 326,
      "label": "COLEGIO LEONARDO MURIALDO"
    },
    {
      "value": 327,
      "label": "ESCUELA D-314 JOAQUIN EDWARDS BELLO"
    },
    {
      "value": 328,
      "label": "ESCUELA JUAN DE SAAVEDRA"
    },
    {
      "value": 329,
      "label": "ESCUELA D-250 GASPAR CABRALES"
    },
    {
      "value": 330,
      "label": "ESCUELA ERNESTO QUIROS WEBER"
    },
    {
      "value": 331,
      "label": "ESCUELA D-256 REPUBLICA DEL URUGUAY"
    },
    {
      "value": 332,
      "label": "COLEGIO SALESIANOS VALPARAISO"
    },
    {
      "value": 333,
      "label": "COLEGIO ARTURO EDWARDS (SEDE CARRERA)"
    },
    {
      "value": 334,
      "label": "ESCUELA BASICA RAMON BARROS LUCO D-270"
    },
    {
      "value": 335,
      "label": "LICEO MIXTO POLIVALENTE MATILDE BRANDAU DE ROSS"
    },
    {
      "value": 336,
      "label": "ESCUELA GRECIA (D-251)"
    },
    {
      "value": 337,
      "label": "LICEO TECNICO DE VALPARAISO"
    },
    {
      "value": 338,
      "label": "COLEGIO ARTURO EDWARDS (SEDE COLON)"
    },
    {
      "value": 339,
      "label": "SEMINARIO SAN RAFAEL"
    },
    {
      "value": 340,
      "label": "LICEO BICENTENARIO VALPARAISO (B-29)"
    },
    {
      "value": 341,
      "label": "COLEGIO SAN PEDRO NOLASCO"
    },
    {
      "value": 342,
      "label": "LICEO EDUARDO DE LA BARRA"
    },
    {
      "value": 343,
      "label": "SCUOLA ITALIANA ARTURO DELL'ORO"
    },
    {
      "value": 344,
      "label": "COLEGIO CARLOS COUSINO"
    },
    {
      "value": 345,
      "label": "LICEO PEDRO MONTT C-100"
    },
    {
      "value": 346,
      "label": "GIMNASIO PUCV CASA CENTRAL"
    },
    {
      "value": 347,
      "label": "COLEGIO INMACULADA CONCEPCION DE NUESTRA SENORA DE LOURDES"
    },
    {
      "value": 348,
      "label": "ESCUELA DE TRIPULANTES Y PORTUARIA DE VALPARAISO"
    },
    {
      "value": 349,
      "label": "COLEGIO AGUSTIN EDWARDS"
    },
    {
      "value": 350,
      "label": "UNIVERSIDAD DE PLAYA ANCHA FACULTAD DE CIENCIAS DE LA SALUD"
    },
    {
      "value": 351,
      "label": "ESCUELA BLAS CUEVAS RAMON ALLENDE"
    },
    {
      "value": 352,
      "label": "ESCUELA BASICA N�150 LAGUNA VERDE G-289"
    },
    {
      "value": 353,
      "label": "COLEGIO NUEVA ERA SIGLO XXI"
    },
    {
      "value": 354,
      "label": "COLEGIO E-268 REPUBLICA DE MEXICO"
    },
    {
      "value": 355,
      "label": "LICEO B-26 MARIA LUISA BOMBAL"
    },
    {
      "value": 356,
      "label": "ESCUELA E-266 CARABINERO PEDRO A. CARIAGA M."
    },
    {
      "value": 357,
      "label": "COLEGIO PATRICIO LYNCH"
    },
    {
      "value": 358,
      "label": "ESCUELA REPUBLICA ARABE SIRIA D-246"
    },
    {
      "value": 359,
      "label": "CENTRO EDUCATIVO REINO DE SUECIA"
    },
    {
      "value": 360,
      "label": "ESCUELA DIEGO PORTALES PALAZUELOS"
    },
    {
      "value": 361,
      "label": "COLEGIO ALBERTO HURTADO SEGUNDO"
    },
    {
      "value": 362,
      "label": "LICEO TECNOLOGICO ALFREDO NAZAR FERES"
    },
    {
      "value": 363,
      "label": "ESCUELA E-312 ESTADO DE ISRAEL"
    },
    {
      "value": 364,
      "label": "ESCUELA D-245 NACIONES UNIDAS"
    },
    {
      "value": 365,
      "label": "INSTITUTO TECNICO PROFESIONAL MARITIMO DE VALPARAISO (2)"
    },
    {
      "value": 366,
      "label": "COLEGIO MARIA AUXILIADORA"
    },
    {
      "value": 367,
      "label": "INSTITUTO TECNICO PROFESIONAL MARITIMO DE VALPARAISO (1)"
    },
    {
      "value": 368,
      "label": "COLEGIO GUARDIAMARINA RIQUELME"
    },
    {
      "value": 380,
      "label": "COLEGIO WINTERHILL VINA DEL MAR"
    },
    {
      "value": 381,
      "label": "ESCUELA BASICA D-316 REPUBLICA DEL ECUADOR"
    },
    {
      "value": 382,
      "label": "COLEGIO FRIENDLY HIGH SCHOOL"
    },
    {
      "value": 383,
      "label": "ESCUELA PRESIDENTE JOSE MANUEL BALMACEDA"
    },
    {
      "value": 384,
      "label": "COLEGIO INMACULADA DE LOURDES"
    },
    {
      "value": 385,
      "label": "ESCUELA D-346 TEODORO LOWEY"
    },
    {
      "value": 386,
      "label": "LICEO BENJAMIN VICUNA MACKENNA"
    },
    {
      "value": 387,
      "label": "ESCUELA LORD COCHRANE"
    },
    {
      "value": 388,
      "label": "ESCUELA BASICA CHORRILLOS E-323"
    },
    {
      "value": 389,
      "label": "LICEO A-33 GUILLERMO RIVERA COTAPOS"
    },
    {
      "value": 390,
      "label": "ESCUELA LIBERTADOR BERNARDO OHIGGINS"
    },
    {
      "value": 391,
      "label": "ESCUELA ESPECIAL JUANITA DE AGUIRRE CERDA"
    },
    {
      "value": 392,
      "label": "SEMINARIO SAN RAFAEL"
    },
    {
      "value": 393,
      "label": "LICEO SANTA TERESA DE LOS ANDES"
    },
    {
      "value": 394,
      "label": "COLEGIO JUANITA FERNANDEZ"
    },
    {
      "value": 395,
      "label": "LICEO INDUSTRIAL DE VINA DEL MAR"
    },
    {
      "value": 396,
      "label": "PAN AMERICAN COLLEGE VINA DEL MAR"
    },
    {
      "value": 397,
      "label": "ESCUELA ARTURO PRAT CHACON"
    },
    {
      "value": 398,
      "label": "COLEGIO CASTELIANO"
    },
    {
      "value": 399,
      "label": "ESCUELA PATRICIO LYNCH ZALDIVAR"
    },
    {
      "value": 400,
      "label": "SCUOLA ITALIANA ARTURO DELL' ORO VINA DEL MAR"
    },
    {
      "value": 401,
      "label": "COLEGIO ANA MARIA JANER"
    },
    {
      "value": 402,
      "label": "COLEGIO MIRAFLORES D-329"
    },
    {
      "value": 403,
      "label": "ESCUELA BASICA SANTA JULIA D-334"
    },
    {
      "value": 404,
      "label": "COLEGIO SAN IGNACIO (SEDE LUSITANIA)"
    },
    {
      "value": 405,
      "label": "ESTADIO MUNICIPAL SAUSALITO"
    },
    {
      "value": 406,
      "label": "CORPORACION DOCENTE SAINT DOMINIC"
    },
    {
      "value": 407,
      "label": "ESCUELA BASICA HUMBERTO VILCHES ALZAMORA"
    },
    {
      "value": 408,
      "label": "LICEO POLITECNICO JOSE FRANCISCO VERGARA ETCHEVERS"
    },
    {
      "value": 409,
      "label": "ESCUELA BASICA D-318 UNESCO"
    },
    {
      "value": 410,
      "label": "FACULTAD DE FILOSOFIA Y EDUCACION PUCV CAMPUS SAUSALITO"
    },
    {
      "value": 411,
      "label": "COLEGIO RUBEN CASTRO"
    },
    {
      "value": 412,
      "label": "LICEO A-36 BICENTENARIO DE VINA DEL MAR"
    },
    {
      "value": 413,
      "label": "COLEGIO REPUBLICA DE COLOMBIA"
    },
    {
      "value": 414,
      "label": "GIMNASIO POLIDEPORTIVO REGIONAL"
    },
    {
      "value": 415,
      "label": "COLEGIO MARIA AUXILIADORA"
    },
    {
      "value": 416,
      "label": "ESCUELA PRESIDENTE PEDRO AGUIRRE CERDA"
    },
    {
      "value": 557,
      "label": "ESCUELA PALESTINA"
    },
    {
      "value": 558,
      "label": "LICEO FERNANDO LAZCANO"
    },
    {
      "value": 720,
      "label": "COLEGIO SANTA LUISA DE CONCEPCION"
    },
    {
      "value": 721,
      "label": "COLEGIO MARINA DE CHILE"
    },
    {
      "value": 722,
      "label": "ESCUELA ESTHER HUNNEUS DE CLARO"
    },
    {
      "value": 723,
      "label": "LICEO REBECA MATTE BELLO"
    },
    {
      "value": 724,
      "label": "COLEGIO CAMILO HENRIQUEZ"
    },
    {
      "value": 725,
      "label": "ESCUELA DIEGO PORTALES PALAZUELOS"
    },
    {
      "value": 726,
      "label": "COLEGIO SANTA EUFRASIA"
    },
    {
      "value": 727,
      "label": "LICEO DE ADULTOS JOSE MANUEL BALMACEDA FERNANDEZ"
    },
    {
      "value": 728,
      "label": "COLEGIO TECNICO PROFESIONAL LOS ACACIOS"
    },
    {
      "value": 729,
      "label": "LICEO DE NINAS"
    },
    {
      "value": 730,
      "label": "LICEO TECNICO FEMENINO A-29 DE CONCEPCION"
    },
    {
      "value": 731,
      "label": "LICEO JUAN GREGORIO LAS HERAS"
    },
    {
      "value": 732,
      "label": "ESCUELA BASICA RENE LOUVEL BERT"
    },
    {
      "value": 733,
      "label": "COLEGIO BICENTENARIO REPUBLICA DEL BRASIL"
    },
    {
      "value": 734,
      "label": "LICEO JUAN MARTINEZ DE ROZAS"
    },
    {
      "value": 735,
      "label": "CENTRO INTEGRADO DE EDUCACION ESPECIAL"
    },
    {
      "value": 736,
      "label": "LICEO EXPERIMENTAL LUCILA GODOY ALCAYAGA"
    },
    {
      "value": 737,
      "label": "LICEO ESPANA"
    },
    {
      "value": 738,
      "label": "LICEO ENRIQUE MOLINA GARMENDIA"
    },
    {
      "value": 739,
      "label": "LICEO COMERCIAL ENRIQUE OYARZUN MONDACA"
    },
    {
      "value": 740,
      "label": "LICEO ANDALIEN"
    },
    {
      "value": 741,
      "label": "COLEGIO SALESIANO"
    },
    {
      "value": 742,
      "label": "LICEO COMERCIAL FEMENINO DE CONCEPCION"
    },
    {
      "value": 743,
      "label": "COLEGIO BIOBIO"
    },
    {
      "value": 744,
      "label": "LICEO REPUBLICA DEL ECUADOR"
    },
    {
      "value": 745,
      "label": "LICEO DOMINGO SANTA MARIA"
    },
    {
      "value": 746,
      "label": "LICEO REPUBLICA DE ISRAEL"
    },
    {
      "value": 747,
      "label": "LICEO INDUSTRIAL DE CONCEPCION"
    },
    {
      "value": 748,
      "label": "COLEGIO SAN PEDRO NOLASCO"
    },
    {
      "value": 749,
      "label": "GIMNASIO MUNICIPAL"
    },
    {
      "value": 750,
      "label": "INSTITUTO DE HUMANIDADES ALFREDO SILVA S"
    },
    {
      "value": 751,
      "label": "COLEGIO CREACION CONCEPCION"
    },
    {
      "value": 1323,
      "label": "LICEO MUNICIPAL SARA TRONCOSO TRONCOSO"
    },
    {
      "value": 1324,
      "label": "LICEO A-131 MEDIA HAYDEE AZOCAR MANSILLA"
    },
    {
      "value": 1325,
      "label": "LICEO A-131 MEDIA HAYDEE AZOCAR MANSILLA LOCAL: 2"
    },
    {
      "value": 1326,
      "label": "LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA"
    },
    {
      "value": 1327,
      "label": "LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 2"
    },
    {
      "value": 1328,
      "label": "LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 3"
    },
    {
      "value": 1329,
      "label": "LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 4"
    },
    {
      "value": 1330,
      "label": "LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 5"
    },
    {
      "value": 1331,
      "label": "GIMNASIO MUNICIPAL"
    },
    {
      "value": 1332,
      "label": "LICEO POLIVALENTE MODERNO CARDENAL CARO BASICA"
    },
    {
      "value": 1333,
      "label": "LICEO POETISA GABRIELA MISTRAL ENSENANZA BASICA"
    },
    {
      "value": 1334,
      "label": "LICEO POETISA GABRIELA MISTRAL ENSENANZA BASICA LOCAL: 2"
    },
    {
      "value": 1335,
      "label": "LICEO POETISA GABRIELA MISTRAL ENSENANZA BASICA LOCAL: 3"
    },
    {
      "value": 1336,
      "label": "ESCUELA JOAQUIN PRIETO VIAL"
    },
    {
      "value": 1337,
      "label": "ESCUELA CERRILLOS"
    },
    {
      "value": 1338,
      "label": "LICEO POLIVALENTE DR. LUIS VARGAS SALCEDO"
    },
    {
      "value": 1339,
      "label": "ESCUELA ESTRELLA REINA DE CHILE"
    },
    {
      "value": 1340,
      "label": "ESCUELA LOTHAR KOMMER BRUGER"
    },
    {
      "value": 1341,
      "label": "ESCUELA BASICA D-259 CONDORES DE PLATA"
    },
    {
      "value": 1342,
      "label": "ESCUELA BASICA PEDRO AGUIRRE CERDA"
    },
    {
      "value": 1344,
      "label": "ESCUELA 407 SARGENTO CANDELARIA"
    },
    {
      "value": 1345,
      "label": "ESCUELA FEDERICO ACEVEDO SALAZAR"
    },
    {
      "value": 1346,
      "label": "ESCUELA 380 DR. TREVISO GIRARDI TONELLI"
    },
    {
      "value": 1347,
      "label": "ESCUELA PROVINCIA DE ARAUCO"
    },
    {
      "value": 1348,
      "label": "ESCUELA MARIA LUISA BOMBAL"
    },
    {
      "value": 1349,
      "label": "ESCUELA MILLAHUE"
    },
    {
      "value": 1350,
      "label": "ESCUELA 1193 CARLOS BERRIOS VALDES"
    },
    {
      "value": 1351,
      "label": "ESCUELA 1193 CARLOS BERRIOS VALDES LOCAL: 2"
    },
    {
      "value": 1352,
      "label": "ESCUELA 377 CIUDAD SANTO DOMINGO DE GUZMAN"
    },
    {
      "value": 1353,
      "label": "LICEO LOS HEROES DE LA CONCEPCION"
    },
    {
      "value": 1354,
      "label": "ESCUELA ESPECIAL SGTO.CANDELARIA"
    },
    {
      "value": 1355,
      "label": "ESCUELA GENERAL RENE ESCAURIAZA"
    },
    {
      "value": 1356,
      "label": "ESCUELA PROF. MANUEL GUERRERO CEBALLOS"
    },
    {
      "value": 1357,
      "label": "ESCUELA REPUBLICA DE CROACIA"
    },
    {
      "value": 1358,
      "label": "ESCUELA HERMINDA DE LA VICTORIA"
    },
    {
      "value": 1359,
      "label": "ESCUELA BASICA PAULO FREIRE"
    },
    {
      "value": 1360,
      "label": "LICEO BICENTENARIO DE EXCELENCIA"
    },
    {
      "value": 1361,
      "label": "ESCUELA BASICA ALGARROBAL"
    },
    {
      "value": 1362,
      "label": "ESCUELA BASICA MARCOS GOOYCOLEA CORTES"
    },
    {
      "value": 1363,
      "label": "ESCUELA BASICA SANTA TERESA DEL CARMELO"
    },
    {
      "value": 1364,
      "label": "INSTITUTO CHACABUCO"
    },
    {
      "value": 1365,
      "label": "LICEO POLIVALENTE RIGOBERTO FONTT I."
    },
    {
      "value": 1366,
      "label": "ESCUELA PREMIO NOBEL PABLO NERUDA"
    },
    {
      "value": 1367,
      "label": "LICEO ESMERALDA"
    },
    {
      "value": 1368,
      "label": "ESCUELA ANDALIEN"
    },
    {
      "value": 1369,
      "label": "INSTITUTO CHACABUCO LOCAL: 2"
    },
    {
      "value": 1370,
      "label": "ESCUELA E-114 GENERAL BERNALES"
    },
    {
      "value": 1371,
      "label": "ESCUELA ALLIPEN"
    },
    {
      "value": 1372,
      "label": "ESCUELA ATENEA"
    },
    {
      "value": 1373,
      "label": "LICEO ALBERTO BLEST GANA"
    },
    {
      "value": 1374,
      "label": "LICEO POETA FEDERICO GARCIA LORCA"
    },
    {
      "value": 1375,
      "label": "ESCUELA PEDRO AGUIRRE CERDA"
    },
    {
      "value": 1376,
      "label": "LICEO ALMIRANTE GALVARINO RIVEROS"
    },
    {
      "value": 1377,
      "label": "LICEO ALMIRANTE GALVARINO RIVEROS LOCAL: 2"
    },
    {
      "value": 1378,
      "label": "ESCUELA UNESCO D-110"
    },
    {
      "value": 1379,
      "label": "ESCUELA UNESCO D-110 LOCAL: 2"
    },
    {
      "value": 1380,
      "label": "ESCUELA SOL NACIENTE"
    },
    {
      "value": 1381,
      "label": "ESCUELA VALLE DEL INCA"
    },
    {
      "value": 1382,
      "label": "LICEO POLIV. ABDON CIFUENTES"
    },
    {
      "value": 1383,
      "label": "ESCUELA DRA.ELOISA DIAZ INSUNZA"
    },
    {
      "value": 1384,
      "label": "ESCUELA DRA.ELOISA DIAZ INSUNZA LOCAL: 2"
    },
    {
      "value": 1385,
      "label": "ESCUELA BASICA VALLE DE PUANGUE"
    },
    {
      "value": 1386,
      "label": "LICEO PRESIDENTE BALMACEDA"
    },
    {
      "value": 1387,
      "label": "ESCUELA SAN JOSE OBRERO"
    },
    {
      "value": 1388,
      "label": "LICEO CHRISTA MC AULIFFE"
    },
    {
      "value": 1389,
      "label": "LICEO CHRISTA MC AULIFFE LOCAL: 2"
    },
    {
      "value": 1391,
      "label": "LICEO JUAN GOMEZ MILLAS LOCAL: 2"
    },
    {
      "value": 1392,
      "label": "COLEGIO BATALLA DE LA CONCEPCION"
    },
    {
      "value": 1393,
      "label": "ESCUELA BASICA AVIADORES"
    },
    {
      "value": 1394,
      "label": "ESCUELA CIUDAD DE LYON"
    },
    {
      "value": 1395,
      "label": "LICEO ELENA CAFFARENA MORICE"
    },
    {
      "value": 1396,
      "label": "LICEO ELENA CAFFARENA MORICE LOCAL: 2"
    },
    {
      "value": 1397,
      "label": "ESCUELA BASICA MARIO ARCE GATICA"
    },
    {
      "value": 1398,
      "label": "COMPLEJO EDUCACIONAL FELIPE HERRERA LANE"
    },
    {
      "value": 1399,
      "label": "ESCUELA BASICA PAUL HARRIS"
    },
    {
      "value": 1400,
      "label": "ESCUELA BASICA VILLA SANTA ELENA"
    },
    {
      "value": 1401,
      "label": "ESCUELA BASICA VILLA SANTA ELENA LOCAL: 2"
    },
    {
      "value": 1402,
      "label": "ESCUELA MARCIAL MARTINEZ FERRARI"
    },
    {
      "value": 1403,
      "label": "ESCUELA ALBERTO BACHELET"
    },
    {
      "value": 1404,
      "label": "ESCUELA CLAUDIO ARRAU LEON"
    },
    {
      "value": 1405,
      "label": "LICEO POLIV. LUIS HUMBERTO ACOSTA GAY"
    },
    {
      "value": 1406,
      "label": "LICEO POLIV. LUIS HUMBERTO ACOSTA GAY LOCAL: 2"
    },
    {
      "value": 1408,
      "label": "ESCUELA REPUBLICA DEL ECUADOR LOCAL: 2"
    },
    {
      "value": 1409,
      "label": "ESCUELA BASICA ARTURO ALESSANDRI PALMA"
    },
    {
      "value": 1410,
      "label": "ESCUELA BASICA ARTURO ALESSANDRI PALMA LOCAL: 2"
    },
    {
      "value": 1411,
      "label": "ESCUELA BASICA PACTO ANDINO"
    },
    {
      "value": 1412,
      "label": "ESCUELA BASICA PACTO ANDINO LOCAL: 2"
    },
    {
      "value": 1413,
      "label": "COMPLEJO EDUCACIONAL ESTACION CENTRAL"
    },
    {
      "value": 1414,
      "label": "COMPLEJO EDUCACIONAL ESTACION CENTRAL LOCAL: 2"
    },
    {
      "value": 1415,
      "label": "ESCUELA D-276 CAROLINA VERGARA AYARES"
    },
    {
      "value": 1416,
      "label": "ESCUELA D-276 CAROLINA VERGARA AYARES LOCAL: 2"
    },
    {
      "value": 1417,
      "label": "ESCUELA REPUBLICA DE PALESTINA"
    },
    {
      "value": 1418,
      "label": "ESCUELA REPUBLICA DE PALESTINA LOCAL: 2"
    },
    {
      "value": 1419,
      "label": "ESCUELA D-260 RAMON DEL RIO"
    },
    {
      "value": 1420,
      "label": "ESCUELA D-260 RAMON DEL RIO LOCAL: 2"
    },
    {
      "value": 1421,
      "label": "ESCUELA D-57 CARLOS CONDELL"
    },
    {
      "value": 1422,
      "label": "ESCUELA D-57 CARLOS CONDELL LOCAL: 2"
    },
    {
      "value": 1423,
      "label": "LICEO POLIVALENTE A N�71 GUILLERMO FELIU CRUZ"
    },
    {
      "value": 1424,
      "label": "ESCUELA ARNALDO FALABELLA"
    },
    {
      "value": 1425,
      "label": "CENTRO EDUC. DR. AMADOR NEGHME"
    },
    {
      "value": 1426,
      "label": "ESCUELA BASICA CARLOS PRATS G."
    },
    {
      "value": 1427,
      "label": "ESCUELA ADELAIDA LA FETRA"
    },
    {
      "value": 1428,
      "label": "ESCUELA E-128 SANTA VICTORIA DE HUECHURABA"
    },
    {
      "value": 1429,
      "label": "CENTRO EDUCACIONAL DE HUECHURABA"
    },
    {
      "value": 1430,
      "label": "CENTRO EDUCACIONAL DE HUECHURABA LOCAL: 2"
    },
    {
      "value": 1431,
      "label": "CENTRO EDUCACIONAL DE HUECHURABA LOCAL: 3"
    },
    {
      "value": 1432,
      "label": "COLEGIO GRACE COLLEGE"
    },
    {
      "value": 1433,
      "label": "CENTRO EDUCACIONAL ERNESTO YANEZ RIVERA"
    },
    {
      "value": 1434,
      "label": "LICEO POLIVALENTE A 80 PDTE JOSE MANUEL BALMACEDA"
    },
    {
      "value": 1435,
      "label": "LICEO POLIVALENTE A 80 PDTE JOSE MANUEL BALMACEDA LOCAL: 2"
    },
    {
      "value": 1436,
      "label": "LICEO GABRIELA MISTRAL"
    },
    {
      "value": 1437,
      "label": "LICEO GABRIELA MISTRAL LOCAL: 2"
    },
    {
      "value": 1438,
      "label": "LICEO SAN FRANCISCO DE QUITO"
    },
    {
      "value": 1439,
      "label": "LICEO SAN FRANCISCO DE QUITO LOCAL: 2"
    },
    {
      "value": 1440,
      "label": "LICEO ROSA ESTER ALESANDRI RODRIGUEZ"
    },
    {
      "value": 1441,
      "label": "LICEO ROSA ESTER ALESANDRI RODRIGUEZ LOCAL: 2"
    },
    {
      "value": 1442,
      "label": "ESCUELA BASICA EFRAIN MALDONADO TORRES"
    },
    {
      "value": 1443,
      "label": "ESCUELA BASICA EFRAIN MALDONADO TORRES LOCAL: 2"
    },
    {
      "value": 1444,
      "label": "CENTRO DE EDUCACION MARIO BERTERO CEVASCO"
    },
    {
      "value": 1445,
      "label": "LICEO ABDON CIFUENTES EX 111"
    },
    {
      "value": 1446,
      "label": "COLEGIO NACIONES UNIDAS"
    },
    {
      "value": 1447,
      "label": "LICEO POLITECNICO CIENCIA Y TECNOLOGIA EX-112"
    },
    {
      "value": 1448,
      "label": "LICEO POLITECNICO CIENCIA Y TECNOLOGIA EX-112 LOCAL: 2"
    },
    {
      "value": 1449,
      "label": "ESCUELA ESPERANZA JOVEN"
    },
    {
      "value": 1450,
      "label": "ESCUELA ESPERANZA JOVEN LOCAL: 2"
    },
    {
      "value": 1451,
      "label": "LICEO PORTAL DE LA CISTERNA"
    },
    {
      "value": 1452,
      "label": "COLEGIO PALESTINO"
    },
    {
      "value": 1453,
      "label": "ESCUELA OSCAR ENCALADA YOVANOVICH"
    },
    {
      "value": 1454,
      "label": "COLEGIO ANTU"
    },
    {
      "value": 1455,
      "label": "LICEO POLIVALENTE OLOF PALME"
    },
    {
      "value": 1456,
      "label": "COMPLEJO EDUC. MUNICIPAL CARDEN.A.SAMORE"
    },
    {
      "value": 1457,
      "label": "COLEGIO ALCANTARA DE LA CORDILLERA"
    },
    {
      "value": 1458,
      "label": "COLEGIO ALCANTARA DE LA CORDILLERA LOCAL: 2"
    },
    {
      "value": 1459,
      "label": "ESCUELA BELLAVISTA"
    },
    {
      "value": 1460,
      "label": "ESCUELA LAS ARAUCARIAS"
    },
    {
      "value": 1461,
      "label": "COLEGIO SANTA MARIA"
    },
    {
      "value": 1462,
      "label": "COLEGIO SANTA MARIA LOCAL: 2"
    },
    {
      "value": 1463,
      "label": "CHILEAN EAGLES COLLEGE"
    },
    {
      "value": 1464,
      "label": "CHILEAN EAGLES COLLEGE LOCAL: 2"
    },
    {
      "value": 1465,
      "label": "CHILEAN EAGLES COLLEGE LOCAL: 3"
    },
    {
      "value": 1466,
      "label": "COLEGIO BENJAMIN VICUNA MACKENNA"
    },
    {
      "value": 1467,
      "label": "COLEGIO BENJAMIN VICUNA MACKENNA LOCAL: 2"
    },
    {
      "value": 1468,
      "label": "COLEGIO BENJAMIN VICUNA MACKENNA LOCAL: 3"
    },
    {
      "value": 1469,
      "label": "ESTADIO BICENTENARIO"
    },
    {
      "value": 1470,
      "label": "ESTADIO BICENTENARIO LOCAL: 2"
    },
    {
      "value": 1471,
      "label": "ESTADIO BICENTENARIO LOCAL: 3"
    },
    {
      "value": 1472,
      "label": "ESTADIO BICENTENARIO LOCAL: 4"
    },
    {
      "value": 1473,
      "label": "LICEO POLIVALENTE MUNICIPAL DE LA FLORIDA"
    },
    {
      "value": 1474,
      "label": "LICEO INDIRA GANDHI"
    },
    {
      "value": 1475,
      "label": "COLEGIO ANDREW CARNEGIE COLLEGE"
    },
    {
      "value": 1476,
      "label": "COLEGIO CAPITAN PASTENE (EX-CATALUNA)"
    },
    {
      "value": 1477,
      "label": "COLEGIO CAPITAN PASTENE (EX-CATALUNA) LOCAL: 2"
    },
    {
      "value": 1478,
      "label": "ESCUELA LAS LILAS"
    },
    {
      "value": 1479,
      "label": "LICEO ALTO CORDILLERA"
    },
    {
      "value": 1480,
      "label": "LICEO ALTO CORDILLERA LOCAL: 2"
    },
    {
      "value": 1481,
      "label": "ESTADIO BICENTENARIO"
    },
    {
      "value": 1482,
      "label": "ESTADIO BICENTENARIO LOCAL: 2"
    },
    {
      "value": 1483,
      "label": "COLEGIO DIVINA PASTORA"
    },
    {
      "value": 1484,
      "label": "COLEGIO DIVINA PASTORA LOCAL: 2"
    },
    {
      "value": 1485,
      "label": "ESCUELA MARCELA PAZ"
    },
    {
      "value": 1486,
      "label": "ESCUELA MARCELA PAZ LOCAL: 2"
    },
    {
      "value": 1487,
      "label": "ESTADIO BICENTENARIO"
    },
    {
      "value": 1488,
      "label": "COLEGIO DR SOTERO DEL RIO"
    },
    {
      "value": 1489,
      "label": "COLEGIO DR SOTERO DEL RIO LOCAL: 2"
    },
    {
      "value": 1490,
      "label": "ESTADIO BICENTENARIO LOCAL: 3"
    },
    {
      "value": 1491,
      "label": "LICEO LOS ALMENDROS"
    },
    {
      "value": 1492,
      "label": "LICEO LOS ALMENDROS LOCAL: 2"
    },
    {
      "value": 1493,
      "label": "ESTADIO BICENTENARIO LOCAL: 2"
    },
    {
      "value": 1494,
      "label": "ESCUELA POETA OSCAR CASTRO ZUNIGA"
    },
    {
      "value": 1495,
      "label": "LICEO DOCTOR ALEJANDRO DEL RIO"
    },
    {
      "value": 1496,
      "label": "ESCUELA BAS. BENJAMIN SUBERCASEAUX"
    },
    {
      "value": 1497,
      "label": "ESCUELA LA ARAUCANIA"
    },
    {
      "value": 1498,
      "label": "ESCUELA BELGICA"
    },
    {
      "value": 1499,
      "label": "LICEO MUNICIPAL POETA NERUDA"
    },
    {
      "value": 1500,
      "label": "ESCUELA HEROES DE YUNGAY"
    },
    {
      "value": 1501,
      "label": "LICEO POLIVALENTE FRANCISCO FRIAS VALENZUELA"
    },
    {
      "value": 1502,
      "label": "COLEGIO MUNICIPAL PROCERES DE CHILE"
    },
    {
      "value": 1503,
      "label": "LICEO MALAQUIAS CONCHA"
    },
    {
      "value": 1504,
      "label": "ESCUELA TECNO SUR"
    },
    {
      "value": 1505,
      "label": "ESCUELA BASICA ISLAS DE CHILE"
    },
    {
      "value": 1506,
      "label": "LICEO MALAQUIAS CONCHA LOCAL: 2"
    },
    {
      "value": 1507,
      "label": "LICEO GRANJA SUR"
    },
    {
      "value": 1508,
      "label": "LICEO GRANJA SUR LOCAL: 2"
    },
    {
      "value": 1509,
      "label": "ESCUELA BASICA SANITAS"
    },
    {
      "value": 1510,
      "label": "LICEO MUNICIPAL DE BATUCO"
    },
    {
      "value": 1511,
      "label": "COMPLEJO EDUCACIONAL MANUEL PLAZA REYES"
    },
    {
      "value": 1512,
      "label": "COMPLEJO EDUCACIONAL MANUEL PLAZA REYES LOCAL: 2"
    },
    {
      "value": 1513,
      "label": "ESCUELA MANUEL SEGOVIA MONTENEGRO"
    },
    {
      "value": 1514,
      "label": "ESCUELA MANUEL SEGOVIA MONTENEGRO LOCAL: 2"
    },
    {
      "value": 1515,
      "label": "ESCUELA POLONIA GUTIERREZ"
    },
    {
      "value": 1516,
      "label": "ESCUELA POLONIA GUTIERREZ LOCAL: 2"
    },
    {
      "value": 1517,
      "label": "COLEGIO SOL DEL VALLE"
    },
    {
      "value": 1518,
      "label": "CENTRO EDUCACIONAL MARIANO LATORRE"
    },
    {
      "value": 1519,
      "label": "LICEO VICTOR JARA"
    },
    {
      "value": 1520,
      "label": "ESCUELA MUNICIPAL VIOLETA PARRA"
    },
    {
      "value": 1521,
      "label": "LICEO PABLO DE ROKHA"
    },
    {
      "value": 1522,
      "label": "LICEO PABLO DE ROKHA LOCAL: 2"
    },
    {
      "value": 1523,
      "label": "ESCUELA BASICA JUAN DE DIOS ALDEA"
    },
    {
      "value": 1524,
      "label": "LICEO SIMON BOLIVAR"
    },
    {
      "value": 1525,
      "label": "LICEO MUNICIPAL EL ROBLE"
    },
    {
      "value": 1526,
      "label": "COLEGIO CAPITAN AVALOS"
    },
    {
      "value": 1527,
      "label": "COLEGIO CAPITAN AVALOS LOCAL: 2"
    },
    {
      "value": 1529,
      "label": "ESCUELA PROF. AURELIA ROJAS BURGOS"
    },
    {
      "value": 1530,
      "label": "CENTRO EDUCACIONAL LA PINTANA"
    },
    {
      "value": 1531,
      "label": "CENTRO EDUCACIONAL LA PINTANA LOCAL: 2"
    },
    {
      "value": 1532,
      "label": "LICEO LAS AMERICAS"
    },
    {
      "value": 1533,
      "label": "COLEGIO DE LA SALLE"
    },
    {
      "value": 1535,
      "label": "ESCUELA PALESTINA LOCAL: 2"
    },
    {
      "value": 1536,
      "label": "COLEGIO CONFEDERAC SUIZA"
    },
    {
      "value": 1537,
      "label": "COLEGIO CONFEDERAC SUIZA LOCAL: 2"
    },
    {
      "value": 1538,
      "label": "COLEGIO TERESIANO ENRIQUE DE OSSO"
    },
    {
      "value": 1539,
      "label": "COLEGIO TERESIANO ENRIQUE DE OSSO LOCAL: 2"
    },
    {
      "value": 1540,
      "label": "COMPLEJO EDUCACIONAL LA REINA SECCION MEDIA"
    },
    {
      "value": 1541,
      "label": "COMPLEJO EDUCACIONAL LA REINA SECCION MEDIA LOCAL: 2"
    },
    {
      "value": 1542,
      "label": "COMPLEJO EDUCACIONAL LA REINA SECCION BASICA"
    },
    {
      "value": 1543,
      "label": "LICEO EUGENIO MARIA DE HOSTOS"
    },
    {
      "value": 1544,
      "label": "COLEGIO SAN CONSTANTINO"
    },
    {
      "value": 1545,
      "label": "COLEGIO COMPANIA DE MARIA APOQUINDO"
    },
    {
      "value": 1546,
      "label": "COLEGIO COMPANIA DE MARIA APOQUINDO LOCAL: 2"
    },
    {
      "value": 1547,
      "label": "LICEO RAFAEL SOTOMAYOR"
    },
    {
      "value": 1548,
      "label": "LICEO SANTA MARIA DE LAS CONDES"
    },
    {
      "value": 1549,
      "label": "LICEO SANTA MARIA DE LAS CONDES LOCAL: 2"
    },
    {
      "value": 1550,
      "label": "ESC. DE EDUC. DIFERENCIAL PAUL HARRIS"
    },
    {
      "value": 1551,
      "label": "COLEGIO NUESTRA. SRA. DEL ROSARIO"
    },
    {
      "value": 1552,
      "label": "COLEGIO NUESTRA. SRA. DEL ROSARIO LOCAL: 2"
    },
    {
      "value": 1553,
      "label": "COLEGIO SAN FRANCISCO DEL ALBA"
    },
    {
      "value": 1554,
      "label": "LICEO SIMON BOLIVAR"
    },
    {
      "value": 1555,
      "label": "LICEO SIMON BOLIVAR LOCAL: 2"
    },
    {
      "value": 1556,
      "label": "LICEO ALEXANDER FLEMING"
    },
    {
      "value": 1557,
      "label": "LICEO ALEXANDER FLEMING LOCAL: 2"
    },
    {
      "value": 1558,
      "label": "LICEO JUAN PABLO II DE LAS CONDES"
    },
    {
      "value": 1559,
      "label": "LICEO JUAN PABLO II DE LAS CONDES LOCAL: 2"
    },
    {
      "value": 1560,
      "label": "COLEGIO SAN FRANCISCO DEL ALBA LOCAL: 2"
    },
    {
      "value": 1561,
      "label": "COLEGIO LEONARDO DA VINCI DE LAS CONDES"
    },
    {
      "value": 1562,
      "label": "COLEGIO VILLA MARIA ACADEMY"
    },
    {
      "value": 1563,
      "label": "COLEGIO VILLA MARIA ACADEMY LOCAL: 2"
    },
    {
      "value": 1564,
      "label": "COLEGIO DEL VERBO DIVINO"
    },
    {
      "value": 1565,
      "label": "COLEGIO DEL VERBO DIVINO LOCAL: 2"
    },
    {
      "value": 1566,
      "label": "COLEGIO DEL VERBO DIVINO LOCAL: 3"
    },
    {
      "value": 1567,
      "label": "COLEGIO ALCAZAR DE LAS CONDES"
    },
    {
      "value": 1568,
      "label": "COLEGIO ALCAZAR DE LAS CONDES LOCAL: 2"
    },
    {
      "value": 1569,
      "label": "COLEGIO ALCAZAR DE LAS CONDES LOCAL: 3"
    },
    {
      "value": 1570,
      "label": "COLEGIO SEMINARIO PONTIFICIO MENOR"
    },
    {
      "value": 1571,
      "label": "COLEGIO SEMINARIO PONTIFICIO MENOR LOCAL: 2"
    },
    {
      "value": 1572,
      "label": "CENTRO CIVICO LO BARNECHEA"
    },
    {
      "value": 1573,
      "label": "CENTRO CIVICO LO BARNECHEA LOCAL: 2"
    },
    {
      "value": 1574,
      "label": "COLEGIO MAYFLOWER"
    },
    {
      "value": 1575,
      "label": "COLEGIO MAYFLOWER LOCAL: 2"
    },
    {
      "value": 1576,
      "label": "COLEGIO THE NEWLAND"
    },
    {
      "value": 1577,
      "label": "COLEGIO THE NEWLAND LOCAL: 2"
    },
    {
      "value": 1578,
      "label": "COLEGIO THE NEWLAND LOCAL: 3"
    },
    {
      "value": 1579,
      "label": "ESCUELA DE ED.DIFERENCIAL MADRE TIERRA"
    },
    {
      "value": 1580,
      "label": "LICEO TENIENTE FRANCISCO MERY AGUIRRE"
    },
    {
      "value": 1581,
      "label": "COLEGIO HERNAN OLGUIN"
    },
    {
      "value": 1582,
      "label": "ESCUELA REPUBLICA DE INDONESIA"
    },
    {
      "value": 1583,
      "label": "ESCUELA D 570 RAUL SAEZ"
    },
    {
      "value": 1584,
      "label": "ESCUELA ESPECIAL DE EDUC TAMARUGAL 355"
    },
    {
      "value": 1585,
      "label": "ESCUELA REPUBLICA DE LAS FILIPINAS"
    },
    {
      "value": 1586,
      "label": "ESCUELA ALICIA ARIZTIA"
    },
    {
      "value": 1587,
      "label": "ESCUELA ACAPULCO"
    },
    {
      "value": 1588,
      "label": "ESCUELA BLUE STAR COLLEGE"
    },
    {
      "value": 1589,
      "label": "LICEO POLIVALENTE"
    },
    {
      "value": 1590,
      "label": "ESCUELA BASICA BERNARDO O'HIGGINS"
    },
    {
      "value": 1591,
      "label": "ESCUELA BASICA CLARA ESTRELLA"
    },
    {
      "value": 1592,
      "label": "CENTRO EDUC.CARDENAL JOSE MARIA CARO"
    },
    {
      "value": 1593,
      "label": "ESCUELA D 571 SANTA ADRIANA"
    },
    {
      "value": 1594,
      "label": "COMPLEJO EDUCACIONAL PEDRO PRADO"
    },
    {
      "value": 1595,
      "label": "ANEXO INSTITUTO PEDRO PRADO"
    },
    {
      "value": 1596,
      "label": "ESCUELA SOR TERESA DE LOS ANDES"
    },
    {
      "value": 1597,
      "label": "ESCUELA JAIME GOMEZ GARCIA"
    },
    {
      "value": 1598,
      "label": "ESCUELA MARISCAL DE AYACUCHO"
    },
    {
      "value": 1599,
      "label": "ESCUELA GOLDA MEIR"
    },
    {
      "value": 1600,
      "label": "ESCUELA PROFESORA GLADYS VALENZUELA V"
    },
    {
      "value": 1601,
      "label": "ESCUELA POETA PABLO NERUDA"
    },
    {
      "value": 1602,
      "label": "ESC.DE EDUCACION DIFERENCIAL QUILLAHUE"
    },
    {
      "value": 1603,
      "label": "ESCUELA MUSTAFA KEMAL ATATURK"
    },
    {
      "value": 1604,
      "label": "ESTADIO MONUMENTAL"
    },
    {
      "value": 1605,
      "label": "ESTADIO MONUMENTAL LOCAL: 2"
    },
    {
      "value": 1606,
      "label": "ESTADIO MONUMENTAL LOCAL: 3"
    },
    {
      "value": 1607,
      "label": "LICEO MERCEDES MARIN DEL SOLAR"
    },
    {
      "value": 1608,
      "label": "LICEO MERCEDES MARIN DEL SOLAR LOCAL: 2"
    },
    {
      "value": 1609,
      "label": "LICEO VILLA MACUL ACADEMIA"
    },
    {
      "value": 1610,
      "label": "COMPLEJO EDUC. JOAQUIN EDWARDS BELLO. A-55"
    },
    {
      "value": 1611,
      "label": "ESCUELA JULIO MONTT SALAMANCA E-194"
    },
    {
      "value": 1612,
      "label": "ESCUELA JOSE BERNARDO SUAREZ"
    },
    {
      "value": 1613,
      "label": "COLEGIO PARTICULAR EL ALBA DE MACUL"
    },
    {
      "value": 1614,
      "label": "GIMNASIO MUNICIPAL"
    },
    {
      "value": 1615,
      "label": "ESCUELA VILLA MACUL D-200"
    },
    {
      "value": 1616,
      "label": "ESCUELA BASICA JULIO BARRENECHEA"
    },
    {
      "value": 1617,
      "label": "COLEGIO ALCAZAR"
    },
    {
      "value": 1618,
      "label": "COLEGIO LOS ALPES MAIPU"
    },
    {
      "value": 1619,
      "label": "COLEGIO SANTA MARIA DE MAIPU"
    },
    {
      "value": 1620,
      "label": "COLEGIO SANTA MARIA DE MAIPU LOCAL: 2"
    },
    {
      "value": 1621,
      "label": "ESCUELA REINA DE SUECIA"
    },
    {
      "value": 1622,
      "label": "ESCUELA REINA DE SUECIA LOCAL: 2"
    },
    {
      "value": 1623,
      "label": "LICEO BICENTENARIO DE NINAS DE MAIPU"
    },
    {
      "value": 1624,
      "label": "LICEO BICENTENARIO DE NINAS DE MAIPU LOCAL: 2"
    },
    {
      "value": 1625,
      "label": "COLEGIO ALICANTE DEL ROSAL"
    },
    {
      "value": 1626,
      "label": "ESCUELA GENERAL SAN MARTIN"
    },
    {
      "value": 1627,
      "label": "ESCUELA GENERAL OHIGGINS A-274"
    },
    {
      "value": 1628,
      "label": "ESCUELA GENERAL OHIGGINS A-274 LOCAL: 2"
    },
    {
      "value": 1629,
      "label": "COLEGIO DE LA PROVIDENCIA CARMELA LARRAIN DE INFANTE"
    },
    {
      "value": 1630,
      "label": "ESCUELA PRESIDENTE RIESCO ERRAZURIZ A-275"
    },
    {
      "value": 1631,
      "label": "ESCUELA PRESIDENTE RIESCO ERRAZURIZ A-275 LOCAL: 2"
    },
    {
      "value": 1632,
      "label": "LICEO INDUSTRIAL DOMINGO MATTE PEREZ"
    },
    {
      "value": 1633,
      "label": "ESCUELA VICENTE REYES PALAZUELOS"
    },
    {
      "value": 1634,
      "label": "LICEO SANTIAGO BUERAS Y AVARIA"
    },
    {
      "value": 1635,
      "label": "ESCUELA BASICA N 263 RAMON FREIRE"
    },
    {
      "value": 1636,
      "label": "ESCUELA LEON HUMBERTO VALENZUELA"
    },
    {
      "value": 1637,
      "label": "TEATRO MUNICIPAL"
    },
    {
      "value": 1638,
      "label": "SALON AUDITORIUM MUNICIPAL"
    },
    {
      "value": 1639,
      "label": "CENTRO DE EDUC. TECN.PROFESIONAL CODEDUC"
    },
    {
      "value": 1640,
      "label": "CENTRO DE EDUC. TECN.PROFESIONAL CODEDUC LOCAL: 2"
    },
    {
      "value": 1641,
      "label": "POLIDEPORTIVO OLIMPICO"
    },
    {
      "value": 1642,
      "label": "GIMNASIO MUNICIPAL"
    },
    {
      "value": 1643,
      "label": "INSTITUTO DE HUMANIDADES B. OHIGGINS"
    },
    {
      "value": 1644,
      "label": "COLEGIO REPUBLICA DE GUATEMALA"
    },
    {
      "value": 1645,
      "label": "LICEO JOSE IGNACIO ZENTENO A-75"
    },
    {
      "value": 1646,
      "label": "LICEO JOSE IGNACIO ZENTENO A-75 LOCAL: 2"
    },
    {
      "value": 1647,
      "label": "ESCUELA TOMAS VARGAS E-280"
    },
    {
      "value": 1648,
      "label": "ESCUELA LOS BOSQUINOS"
    },
    {
      "value": 1649,
      "label": "ESCUELA EL LLANO DE MAIPU"
    },
    {
      "value": 1650,
      "label": "ESCUELA TOMAS VARGAS E-280 LOCAL: 2"
    },
    {
      "value": 1651,
      "label": "COLEGIO ALBERTO PEREZ"
    },
    {
      "value": 1652,
      "label": "LICEO NACIONAL DE MAIPU"
    },
    {
      "value": 1653,
      "label": "LICEO NACIONAL DE MAIPU LOCAL: 2"
    },
    {
      "value": 1654,
      "label": "ESCUELA BASICA MUNICIPAL SAN LUIS"
    },
    {
      "value": 1655,
      "label": "COLEGIO JACQUES COUSTEAU"
    },
    {
      "value": 1656,
      "label": "LICEO NACIONAL DE MAIPU LOCAL: 3"
    },
    {
      "value": 1657,
      "label": "LICEO TECNOLOGICO ENRIQUE KIRBERG B"
    },
    {
      "value": 1658,
      "label": "LICEO TECNOLOGICO ENRIQUE KIRBERG B LOCAL: 2"
    },
    {
      "value": 1659,
      "label": "LICEO TECNOLOGICO ENRIQUE KIRBERG B LOCAL: 3"
    },
    {
      "value": 1660,
      "label": "COLEGIO JACQUES COUSTEAU LOCAL: 2"
    },
    {
      "value": 1661,
      "label": "COLEGIO SAN SEBASTIAN"
    },
    {
      "value": 1662,
      "label": "LICEO MUNICIPAL F-N�860"
    },
    {
      "value": 1663,
      "label": "LICEO POLITECNICO MUNICIPAL MELIPILLA"
    },
    {
      "value": 1664,
      "label": "LICEO POLITECNICO MUNICIPAL MELIPILLA LOCAL: 2"
    },
    {
      "value": 1665,
      "label": "LICEO POLITECNICO MUNICIPAL MELIPILLA LOCAL: 3"
    },
    {
      "value": 1666,
      "label": "LICEO POLITECNICO MUNICIPAL MELIPILLA LOCAL: 4"
    },
    {
      "value": 1667,
      "label": "LICEO POLIV. HNOS. SOTOMAYOR BAEZA"
    },
    {
      "value": 1668,
      "label": "LICEO POLIV. HNOS. SOTOMAYOR BAEZA LOCAL: 2"
    },
    {
      "value": 1669,
      "label": "COLEGIO NUESTRA SRA. DE LA PRESENTACION"
    },
    {
      "value": 1670,
      "label": "COLEGIO NUESTRA SRA. DE LA PRESENTACION LOCAL: 2"
    },
    {
      "value": 1671,
      "label": "COLEGIO SAN AGUSTIN"
    },
    {
      "value": 1672,
      "label": "COLEGIO SAN AGUSTIN LOCAL: 2"
    },
    {
      "value": 1673,
      "label": "LICEO GABRIELA MISTRAL"
    },
    {
      "value": 1674,
      "label": "ESTADIO NACIONAL LOCAL: 1"
    },
    {
      "value": 1675,
      "label": "ESTADIO NACIONAL LOCAL: 2"
    },
    {
      "value": 1676,
      "label": "ESTADIO NACIONAL LOCAL: 3"
    },
    {
      "value": 1677,
      "label": "ESTADIO NACIONAL LOCAL: 4"
    },
    {
      "value": 1678,
      "label": "ESTADIO NACIONAL LOCAL: 5"
    },
    {
      "value": 1679,
      "label": "ESTADIO NACIONAL LOCAL: 6"
    },
    {
      "value": 1680,
      "label": "ESTADIO NACIONAL LOCAL: 7"
    },
    {
      "value": 1681,
      "label": "ESTADIO NACIONAL LOCAL: 8"
    },
    {
      "value": 1682,
      "label": "ESTADIO NACIONAL LOCAL: 9"
    },
    {
      "value": 1683,
      "label": "LICEO AUGUSTO D HALMAR"
    },
    {
      "value": 1684,
      "label": "ESC.PRESIDENTE EDUARDO FREI MONTALVA"
    },
    {
      "value": 1685,
      "label": "COLEGIO SANTO TOMAS"
    },
    {
      "value": 1686,
      "label": "COLEGIO SANTO TOMAS LOCAL: 2"
    },
    {
      "value": 1687,
      "label": "ESCUELA REPUBLICA DE SIRIA"
    },
    {
      "value": 1688,
      "label": "LICEO REPUBLICA DE SIRIA"
    },
    {
      "value": 1689,
      "label": "LICEO LENKA FRANULIC"
    },
    {
      "value": 1690,
      "label": "ESCUELA ANEXO LICEO A-52"
    },
    {
      "value": 1691,
      "label": "ESCUELA REPUBLICA DE COSTA RICA"
    },
    {
      "value": 1692,
      "label": "LICEO COMERCIAL GABRIEL GONZALEZ VIDELA"
    },
    {
      "value": 1693,
      "label": "LICEO CARMELA SILVA DONOSO"
    },
    {
      "value": 1694,
      "label": "LICEO CARMELA SILVA DONOSO LOCAL: 2"
    },
    {
      "value": 1695,
      "label": "LICEO CARMELA SILVA DONOSO LOCAL: 3"
    },
    {
      "value": 1696,
      "label": "LICEO INDUSTRIAL CHILENO-ALEMAN"
    },
    {
      "value": 1697,
      "label": "LICEO INDUSTRIAL CHILENO-ALEMAN LOCAL: 2"
    },
    {
      "value": 1698,
      "label": "LICEO JOSE TORIBIO MEDINA A-52"
    },
    {
      "value": 1699,
      "label": "LICEO JOSE TORIBIO MEDINA A-52 LOCAL: 2"
    },
    {
      "value": 1700,
      "label": "LICEO CARMELA SILVA DONOSO LOCAL: 4"
    },
    {
      "value": 1701,
      "label": "COMPLEJO EDUC.PART. LUIS PASTEUR"
    },
    {
      "value": 1702,
      "label": "COMPLEJO EDUC.PART. LUIS PASTEUR LOCAL: 2"
    },
    {
      "value": 1703,
      "label": "LICEO PAUL HARRIS"
    },
    {
      "value": 1704,
      "label": "LICEO PAUL HARRIS LOCAL: 2"
    },
    {
      "value": 1705,
      "label": "ESCUELA REPUBLICA ARGENTINA"
    },
    {
      "value": 1706,
      "label": "LICEO GREGORIO MORALES MIRANDA"
    },
    {
      "value": 1707,
      "label": "LICEO MARIA CARVAJAL FUENZALIDA"
    },
    {
      "value": 1708,
      "label": "CENTRO EDUC. ENRIQUE BERNSTEIN CARABANTE"
    },
    {
      "value": 1709,
      "label": "CENTRO EDUC. ENRIQUE BERNSTEIN CARABANTE LOCAL: 2"
    },
    {
      "value": 1711,
      "label": "ESCUELA PAULA JARAQUEMADA ALQUIZAR LOCAL: 2"
    },
    {
      "value": 1712,
      "label": "ESCUELA POETAS DE CHILE"
    },
    {
      "value": 1713,
      "label": "LICEO ENRIQUE BACKAUSSE"
    },
    {
      "value": 1714,
      "label": "LICEO ENRIQUE BACKAUSSE LOCAL: 2"
    },
    {
      "value": 1715,
      "label": "ESCUELA LA VICTORIA"
    },
    {
      "value": 1716,
      "label": "ESCUELA BOROA"
    },
    {
      "value": 1717,
      "label": "ESCUELA BOROA LOCAL: 2"
    },
    {
      "value": 1718,
      "label": "ESCUELA CONSOLIDADA DAVILA"
    },
    {
      "value": 1719,
      "label": "ESCUELA CONSOLIDADA DAVILA LOCAL: 2"
    },
    {
      "value": 1720,
      "label": "ESCUELA VILLA SUR"
    },
    {
      "value": 1721,
      "label": "ESCUELA 563 LO VALLEDOR"
    },
    {
      "value": 1722,
      "label": "ESCUELA 563 LO VALLEDOR LOCAL: 2"
    },
    {
      "value": 1723,
      "label": "COLEGIO PARQUE LAS AMERICAS"
    },
    {
      "value": 1724,
      "label": "COLEGIO PARQUE LAS AMERICAS LOCAL: 2"
    },
    {
      "value": 1725,
      "label": "ESCUELA CIUDAD DE BARCELONA"
    },
    {
      "value": 1726,
      "label": "LICEO EUGENIO PEREIRA SALAS"
    },
    {
      "value": 1727,
      "label": "ESCUELA ROSALINA PESCIO VARGAS"
    },
    {
      "value": 1728,
      "label": "LICEO COMERCIAL REPUBLICA DE BRASIL"
    },
    {
      "value": 1729,
      "label": "ESCUELA 664 MALLOCO"
    },
    {
      "value": 1730,
      "label": "ESCUELA EMILIA LASCAR"
    },
    {
      "value": 1731,
      "label": "ESCUELA TERESA DE CALCUTA"
    },
    {
      "value": 1732,
      "label": "ESCUELA REPUBLICA DE ISRAEL"
    },
    {
      "value": 1733,
      "label": "ESCUELA MAND EDUARDO FREI MONTALVA"
    },
    {
      "value": 1734,
      "label": "ESCUELA JUAN BAUTISTA PASTENE"
    },
    {
      "value": 1735,
      "label": "COLEGIO ALCANTARA DE LOS ALTOS DE PENALOLEN"
    },
    {
      "value": 1736,
      "label": "COLEGIO MIRAVALLE"
    },
    {
      "value": 1737,
      "label": "COLEGIO MIRAVALLE LOCAL: 2"
    },
    {
      "value": 1738,
      "label": "ESCUELA E-180 MATILDE HUICI NAVAS"
    },
    {
      "value": 1739,
      "label": "ESCUELA UNION NACIONAL ARABE"
    },
    {
      "value": 1740,
      "label": "ESCUELA TOBALABA"
    },
    {
      "value": 1741,
      "label": "CENTRO EDUC ERASMO ESCALA"
    },
    {
      "value": 1743,
      "label": "CENTRO EDUC. VALLE HERMOSO"
    },
    {
      "value": 1744,
      "label": "ESCUELA LUIS ARRIETA CANAS"
    },
    {
      "value": 1745,
      "label": "ESCUELA CARLOS FERNANDEZ PENA"
    },
    {
      "value": 1746,
      "label": "LICEO ANTONIO HERMIDA FABRES"
    },
    {
      "value": 1747,
      "label": "LICEO ANTONIO HERMIDA FABRES LOCAL: 2"
    },
    {
      "value": 1748,
      "label": "CENTRO EDUCACION MARIANO EGANA LOCAL: 2"
    },
    {
      "value": 1749,
      "label": "COLEGIO JORGE PRIETO LETELIER"
    },
    {
      "value": 1750,
      "label": "COLEGIO JORGE PRIETO LETELIER LOCAL: 2"
    },
    {
      "value": 1751,
      "label": "ESCUELA PARTICULAR WELCOME SCHOOL"
    },
    {
      "value": 1752,
      "label": "COLEGIO SANTA MARTA DE PENALOLEN"
    },
    {
      "value": 1753,
      "label": "COLEGIO SANTA MARTA DE PENALOLEN LOCAL: 2"
    },
    {
      "value": 1754,
      "label": "LICEO EL PRINCIPAL"
    },
    {
      "value": 1755,
      "label": "LICEO EL LLANO"
    },
    {
      "value": 1756,
      "label": "LICEO EL LLANO LOCAL: 2"
    },
    {
      "value": 1757,
      "label": "LICEO JOSE VICTORINO LASTARRIA"
    },
    {
      "value": 1758,
      "label": "LICEO DE NINAS N� 7 LUISA SAAVEDRA DE GONZALEZ"
    },
    {
      "value": 1759,
      "label": "LICEO DE NINAS N� 7 LUISA SAAVEDRA DE GONZALEZ LOCAL: 2"
    },
    {
      "value": 1760,
      "label": "INSTITUTO DE HUMANIDADES LUIS CAMPINO"
    },
    {
      "value": 1761,
      "label": "INSTITUTO DE HUMANIDADES LUIS CAMPINO LOCAL: 2"
    },
    {
      "value": 1762,
      "label": "COLEGIO PROVIDENCIA"
    },
    {
      "value": 1763,
      "label": "LICEO B 42 TAJAMAR"
    },
    {
      "value": 1764,
      "label": "ESCUELA JUAN PABLO DUARTE"
    },
    {
      "value": 1765,
      "label": "ESCUELA JUAN PABLO DUARTE LOCAL: 2"
    },
    {
      "value": 1766,
      "label": "COLEGIO COMPANIA DE MARIA"
    },
    {
      "value": 1767,
      "label": "COLEGIO COMPANIA DE MARIA LOCAL: 2"
    },
    {
      "value": 1768,
      "label": "UNIVERSIDAD AUTONOMA DE CHILE"
    },
    {
      "value": 1769,
      "label": "LICEO ARTURO ALESSANDRI PALMA"
    },
    {
      "value": 1770,
      "label": "LICEO ARTURO ALESSANDRI PALMA LOCAL: 2"
    },
    {
      "value": 1771,
      "label": "CAMPUS ORIENTE U. CATOLICA"
    },
    {
      "value": 1772,
      "label": "CAMPUS ORIENTE U. CATOLICA LOCAL: 2"
    },
    {
      "value": 1773,
      "label": "CAMPUS ORIENTE U. CATOLICA LOCAL: 3"
    },
    {
      "value": 1774,
      "label": "CAMPUS ORIENTE U. CATOLICA LOCAL: 4"
    },
    {
      "value": 1775,
      "label": "LICEO CARMELA CARVAJAL DE PRAT"
    },
    {
      "value": 1776,
      "label": "LICEO CARMELA CARVAJAL DE PRAT LOCAL: 2"
    },
    {
      "value": 1777,
      "label": "LICEO CIUDAD DE BRASILIA"
    },
    {
      "value": 1778,
      "label": "ESCUELA MONSENOR CARLOS OVIEDO"
    },
    {
      "value": 1779,
      "label": "ESCUELA MONSENOR CARLOS OVIEDO LOCAL: 2"
    },
    {
      "value": 1780,
      "label": "ESCUELA ELVIRA SANTA CRUZ OSSA"
    },
    {
      "value": 1781,
      "label": "ESCUELA ELVIRA SANTA CRUZ OSSA LOCAL: 2"
    },
    {
      "value": 1782,
      "label": "ESCUELA ESTADO DE FLORIDA"
    },
    {
      "value": 1783,
      "label": "ESCUELA SAN DANIEL"
    },
    {
      "value": 1785,
      "label": "ESCUELA ALEXANDER GRAHAM BELL LOCAL: 2"
    },
    {
      "value": 1786,
      "label": "ESCUELA EL SALITRE"
    },
    {
      "value": 1787,
      "label": "ESCUELA EL SALITRE LOCAL: 2"
    },
    {
      "value": 1788,
      "label": "ESCUELA TENIENTE HERNAN MERINO CORREA"
    },
    {
      "value": 1789,
      "label": "ESCUELA TENIENTE HERNAN MERINO CORREA LOCAL: 2"
    },
    {
      "value": 1790,
      "label": "ESCUELA ESTRELLA DE CHILE"
    },
    {
      "value": 1791,
      "label": "ESCUELA ESTRELLA DE CHILE LOCAL: 2"
    },
    {
      "value": 1792,
      "label": "LICEO MONSENOR ENRIQUE ALVEAR"
    },
    {
      "value": 1793,
      "label": "ESCUELA 1546 MONSENOR ENRIQUE ALVEAR"
    },
    {
      "value": 1794,
      "label": "LICEO MONSENOR ENRIQUE ALVEAR LOCAL: 2"
    },
    {
      "value": 1795,
      "label": "ESCUELA ANTILHUE"
    },
    {
      "value": 1796,
      "label": "ESCUELA PUERTO FUTURO"
    },
    {
      "value": 1797,
      "label": "LICEO MUNICIPAL CHILOE"
    },
    {
      "value": 1798,
      "label": "ESCUELA LAS PALMAS"
    },
    {
      "value": 1799,
      "label": "ESCUELA LOS ANDES"
    },
    {
      "value": 1800,
      "label": "ESCUELA NONATO COO"
    },
    {
      "value": 1801,
      "label": "ESCUELA NONATO COO LOCAL: 2"
    },
    {
      "value": 1802,
      "label": "LICEO INDUSTRIAL DE PUENTE ALTO"
    },
    {
      "value": 1803,
      "label": "LICEO INDUSTRIAL DE PUENTE ALTO LOCAL: 2"
    },
    {
      "value": 1804,
      "label": "LICEO PUENTE ALTO"
    },
    {
      "value": 1805,
      "label": "LICEO PUENTE ALTO LOCAL: 2"
    },
    {
      "value": 1806,
      "label": "COLEGIO DOMINGO MATTE MESIAS"
    },
    {
      "value": 1807,
      "label": "COLEGIO DOMINGO MATTE MESIAS LOCAL: 2"
    },
    {
      "value": 1808,
      "label": "COLEGIO SAN JOSE DE PUENTE ALTO"
    },
    {
      "value": 1809,
      "label": "COLEGIO SAN JOSE DE PUENTE ALTO LOCAL: 2"
    },
    {
      "value": 1810,
      "label": "ESCUELA REPUBLICA DE GRECIA"
    },
    {
      "value": 1811,
      "label": "COLEGIO SAN CARLOS DE ARAGON"
    },
    {
      "value": 1812,
      "label": "COLEGIO SAN CARLOS DE ARAGON LOCAL: 2"
    },
    {
      "value": 1813,
      "label": "LICEO NUESTRA SENORA DE LAS MERCEDES"
    },
    {
      "value": 1814,
      "label": "LICEO SAN GERONIMO"
    },
    {
      "value": 1815,
      "label": "LICEO SAN GERONIMO LOCAL: 2"
    },
    {
      "value": 1816,
      "label": "COMPLEJO EDUCACIONAL CONSOLIDADA"
    },
    {
      "value": 1817,
      "label": "COMPLEJO EDUCACIONAL CONSOLIDADA LOCAL: 2"
    },
    {
      "value": 1818,
      "label": "CENTRO EDUCACIONAL NUEVA CREACION"
    },
    {
      "value": 1819,
      "label": "ESCUELA OSCAR BONILLA"
    },
    {
      "value": 1820,
      "label": "LICEO COMERCIAL DE PUENTE ALTO"
    },
    {
      "value": 1821,
      "label": "LICEO COMERCIAL DE PUENTE ALTO LOCAL: 2"
    },
    {
      "value": 1822,
      "label": "COLEGIO MAIPO"
    },
    {
      "value": 1823,
      "label": "COLEGIO MAIPO LOCAL: 2"
    },
    {
      "value": 1825,
      "label": "ESCUELA VILLA INDEPENDENCIA LOCAL: 2"
    },
    {
      "value": 1826,
      "label": "COLEGIO ALICANTE DEL SOL"
    },
    {
      "value": 1827,
      "label": "COLEGIO ALICANTE DEL SOL LOCAL: 2"
    },
    {
      "value": 1828,
      "label": "COLEGIO ALICANTE DEL SOL LOCAL: 3"
    },
    {
      "value": 1829,
      "label": "COLEGIO ALTAZOR"
    },
    {
      "value": 1830,
      "label": "LICEO POLIVALENTE MARIA REINA"
    },
    {
      "value": 1831,
      "label": "LICEO POLIVALENTE MARIA REINA LOCAL: 2"
    },
    {
      "value": 1832,
      "label": "LICEO POLIVALENTE MARIA REINA LOCAL: 3"
    },
    {
      "value": 1833,
      "label": "COLEGIO EL SEMBRADOR"
    },
    {
      "value": 1834,
      "label": "COLEGIO EL SEMBRADOR LOCAL: 2"
    },
    {
      "value": 1835,
      "label": "COLEGIO CARDENAL RAUL SILVA HENRIQUEZ"
    },
    {
      "value": 1836,
      "label": "ESCUELA SANTA JOAQUINA DE VEDRUNA"
    },
    {
      "value": 1837,
      "label": "ESCUELA SANTA JOAQUINA DE VEDRUNA LOCAL: 2"
    },
    {
      "value": 1838,
      "label": "COLEGIO ARTURO PRAT"
    },
    {
      "value": 1839,
      "label": "COLEGIO ARTURO PRAT LOCAL: 2"
    },
    {
      "value": 1840,
      "label": "ESCUELA EJERCITO LIBERTADOR"
    },
    {
      "value": 1841,
      "label": "ESCUELA VILLA PEDRO AGUIRRE CERDA"
    },
    {
      "value": 1842,
      "label": "COLEGIO LUIS MATTE LARRAIN"
    },
    {
      "value": 1843,
      "label": "COLEGIO OBISPO ALVEAR"
    },
    {
      "value": 1844,
      "label": "COLEGIO SANTA MARIA DE LA CORDILLERA"
    },
    {
      "value": 1845,
      "label": "COLEGIO SANTA MARIA DE LA CORDILLERA LOCAL: 2"
    },
    {
      "value": 1846,
      "label": "LICEO ING.MILITAR JUAN MACKENA OREALLY"
    },
    {
      "value": 1847,
      "label": "LICEO ING.MILITAR JUAN MACKENA OREALLY LOCAL: 2"
    },
    {
      "value": 1848,
      "label": "ESCUELA ANDES DEL SUR"
    },
    {
      "value": 1849,
      "label": "ESCUELA GABRIELA"
    },
    {
      "value": 1850,
      "label": "COMPLEJO EDUCACIONAL J. MIGUEL CARRERA"
    },
    {
      "value": 1851,
      "label": "ESCUELA ESTADO DE MICHIGAN"
    },
    {
      "value": 1852,
      "label": "ESCUELA MERCEDES FONTECILLA"
    },
    {
      "value": 1853,
      "label": "COLEGIO MELFORD COLLEGE"
    },
    {
      "value": 1854,
      "label": "COLEGIO SANTA BARBARA"
    },
    {
      "value": 1856,
      "label": "ESCUELA LUIS CRUZ MARTINEZ LOCAL: 2"
    },
    {
      "value": 1857,
      "label": "ESCUELA BASICA ANA FRANK"
    },
    {
      "value": 1858,
      "label": "COLEGIO PALMARES"
    },
    {
      "value": 1859,
      "label": "ESCUELA PROFESORA MARIA LUISA SEPULVEDA"
    },
    {
      "value": 1860,
      "label": "COLEGIO SAN SEBASTIAN"
    },
    {
      "value": 1861,
      "label": "COLEGIO SAN SEBASTIAN LOCAL: 2"
    },
    {
      "value": 1862,
      "label": "ESCUELA EL MANIO"
    },
    {
      "value": 1863,
      "label": "ESCUELA LUIS CRUZ MARTINEZ LOCAL: 3"
    },
    {
      "value": 1864,
      "label": "ESCUELA ANTUMALAL"
    },
    {
      "value": 1865,
      "label": "ESCUELA INGLATERRA"
    },
    {
      "value": 1866,
      "label": "ESCUELA INGLATERRA LOCAL: 2"
    },
    {
      "value": 1867,
      "label": "LICEO GUILLERMO LABARCA HUBERTSON"
    },
    {
      "value": 1868,
      "label": "LICEO GUILLERMO LABARCA HUBERTSON LOCAL: 2"
    },
    {
      "value": 1869,
      "label": "ESCUELA GRENOBLE"
    },
    {
      "value": 1870,
      "label": "ESCUELA GRENOBLE LOCAL: 2"
    },
    {
      "value": 1871,
      "label": "ESCUELA INSIGNE GABRIELA"
    },
    {
      "value": 1872,
      "label": "ESCUELA DIEGO PORTALES"
    },
    {
      "value": 1873,
      "label": "LICEO POLIVALENTE JUAN A.RIOS"
    },
    {
      "value": 1874,
      "label": "LICEO INDUSTRIAL BENJAMIN FRANKLIN"
    },
    {
      "value": 1875,
      "label": "ESCUELA SANTA TERESA DE AVILA"
    },
    {
      "value": 1876,
      "label": "ESCUELA SANTA TERESA DE AVILA LOCAL: 2"
    },
    {
      "value": 1877,
      "label": "CENTRO EDUC.ESCRITORES DE CHILE"
    },
    {
      "value": 1878,
      "label": "CENTRO EDUC.ESCRITORES DE CHILE LOCAL: 2"
    },
    {
      "value": 1879,
      "label": "ESCUELA ESPANA"
    },
    {
      "value": 1880,
      "label": "ESCUELA PUERTO RICO"
    },
    {
      "value": 1881,
      "label": "ESCUELA HERMANA MARIA GORETTI"
    },
    {
      "value": 1882,
      "label": "COMPLEJO EDUCACIONAL JUANITA FERNANDEZ SOLAR"
    },
    {
      "value": 1883,
      "label": "LICEO PAULA JARAQUEMADA"
    },
    {
      "value": 1884,
      "label": "ESCUELA REPUBLICA DEL PARAGUAY"
    },
    {
      "value": 1885,
      "label": "ESCUELA REPUBLICA DEL PARAGUAY LOCAL: 2"
    },
    {
      "value": 1886,
      "label": "LICEO DE ADULTOS JORGE ALESSANDRI R."
    },
    {
      "value": 1887,
      "label": "LICEO DE ADULTOS JORGE ALESSANDRI R. LOCAL: 2"
    },
    {
      "value": 1888,
      "label": "ESCUELA JUAN VERDAGUER PLANAS"
    },
    {
      "value": 1889,
      "label": "LICEO HEROE ARTURO PEREZ CANTO"
    },
    {
      "value": 1890,
      "label": "LICEO HEROE ARTURO PEREZ CANTO LOCAL: 2"
    },
    {
      "value": 1891,
      "label": "CENTRO EDUCACIONAL JOSE MIGUEL CARRERA B-36"
    },
    {
      "value": 1892,
      "label": "CENTRO EDUCACIONAL JOSE MIGUEL CARRERA B-36 LOCAL: 2"
    },
    {
      "value": 1893,
      "label": "ESTADIO RECOLETA"
    },
    {
      "value": 1894,
      "label": "ESCUELA REBECA MATTE BELLO"
    },
    {
      "value": 1895,
      "label": "ESCUELA GENERAL ALEJANDRO GOROSTIAGA O"
    },
    {
      "value": 1896,
      "label": "ESCUELA BASICA LO VELASQUEZ"
    },
    {
      "value": 1897,
      "label": "ESCUELA GUSTAVO LE PAIGE"
    },
    {
      "value": 1898,
      "label": "ESCUELA GUSTAVO LE PAIGE LOCAL: 2"
    },
    {
      "value": 1899,
      "label": "ESCUELA BASICA ISABEL LE BRUN"
    },
    {
      "value": 1900,
      "label": "ESCUELA THOMAS ALVA EDISON N� 317"
    },
    {
      "value": 1901,
      "label": "ESCUELA MONSERRAT ROBERT DE GARCIA"
    },
    {
      "value": 1902,
      "label": "ESCUELA DOMINGO SANTA MARIA GONZALEZ"
    },
    {
      "value": 1903,
      "label": "INSTITUTO CUMBRE DE CONDORES ORIENTE"
    },
    {
      "value": 1904,
      "label": "INSTITUTO CUMBRE DE CONDORES ORIENTE LOCAL: 2"
    },
    {
      "value": 1905,
      "label": "ESCUELA CAPITAN JOSE LUIS ARANEDA"
    },
    {
      "value": 1906,
      "label": "INSTITUTO CUMBRES DE CONDORES PONIENTE"
    },
    {
      "value": 1907,
      "label": "INSTITUTO CUMBRES DE CONDORES PONIENTE LOCAL: 2"
    },
    {
      "value": 1908,
      "label": "LICEO COMERCIAL"
    },
    {
      "value": 1909,
      "label": "LICEO COMERCIAL LOCAL: 2"
    },
    {
      "value": 1910,
      "label": "LICEO A-127 FIDEL PINOCHET LE-BRUN"
    },
    {
      "value": 1911,
      "label": "LICEO A-127 FIDEL PINOCHET LE-BRUN LOCAL: 2"
    },
    {
      "value": 1912,
      "label": "LICEO ELVIRA BRADY MALDONADO"
    },
    {
      "value": 1913,
      "label": "LICEO ELVIRA BRADY MALDONADO LOCAL: 2"
    },
    {
      "value": 1914,
      "label": "ESCUELA REPUBLICA DEL BRASIL"
    },
    {
      "value": 1915,
      "label": "ESCUELA REPUBLICA DEL PERU"
    },
    {
      "value": 1916,
      "label": "ESCUELA PILAR MOLINER DE NUEZ"
    },
    {
      "value": 1917,
      "label": "ESCUELA BASICA ANTUPILLAN"
    },
    {
      "value": 1918,
      "label": "ESCUELA ESCRITORA MARCELA PAZ"
    },
    {
      "value": 1919,
      "label": "ESCUELA ISABEL RIQUELME"
    },
    {
      "value": 1920,
      "label": "ESCUELA ISABEL RIQUELME LOCAL: 2"
    },
    {
      "value": 1921,
      "label": "ESCUELA L.GRAL. BERNARDO OHIGGINS"
    },
    {
      "value": 1922,
      "label": "ESCUELA L.GRAL. BERNARDO OHIGGINS LOCAL: 2"
    },
    {
      "value": 1923,
      "label": "LICEO IND. MIGUEL AYLWIN GAJARDO"
    },
    {
      "value": 1924,
      "label": "LICEO IND. MIGUEL AYLWIN GAJARDO LOCAL: 2"
    },
    {
      "value": 1925,
      "label": "ESCUELA MANUEL MAGALLANES MOURE"
    },
    {
      "value": 1926,
      "label": "ESCUELA HERNAN MERINO CORREA"
    },
    {
      "value": 1927,
      "label": "LICEO POLIV. LUCILA GODOY ALCAYAGA"
    },
    {
      "value": 1928,
      "label": "ESCUELA ALEMANIA D-774"
    },
    {
      "value": 1929,
      "label": "ESCUELA ALEMANIA D-774 LOCAL: 2"
    },
    {
      "value": 1930,
      "label": "ESCUELA ESCRITORA MARCELA PAZ LOCAL: 2"
    },
    {
      "value": 1931,
      "label": "ESCUELA DIEGO PORTALES E-753"
    },
    {
      "value": 1932,
      "label": "ESCUELA DIEGO PORTALES E-753 LOCAL: 2"
    },
    {
      "value": 1933,
      "label": "CENTRO EDUCACIONAL BALDOMERO LILLO"
    },
    {
      "value": 1934,
      "label": "CENTRO EDUCACIONAL BALDOMERO LILLO LOCAL: 2"
    },
    {
      "value": 1935,
      "label": "LICEO BICENTENARIO"
    },
    {
      "value": 1936,
      "label": "COLEGIO SEBASTIAN ELCANO"
    },
    {
      "value": 1937,
      "label": "LICEO MUNICIPAL SAN JOAQUIN"
    },
    {
      "value": 1938,
      "label": "CENTRO EDUC. PROVINCIA DE NUBLE"
    },
    {
      "value": 1939,
      "label": "GIMNASIO MUNICIPAL"
    },
    {
      "value": 1940,
      "label": "ESCUELA BASICA FRAY CAMILO HENRIQUEZ"
    },
    {
      "value": 1941,
      "label": "COLEGIO CIUDAD DE FRANKFORT"
    },
    {
      "value": 1942,
      "label": "LICEO PARTICULAR ESPIRITU SANTO"
    },
    {
      "value": 1943,
      "label": "LICEO INDUSTRIAL AGUSTIN EDWARDS ROSS"
    },
    {
      "value": 1944,
      "label": "CENTRO EDUCACIONAL HORACIO ARAVENA A."
    },
    {
      "value": 1945,
      "label": "COLEGIO ADVENTISTA SANTIAGO SUR"
    },
    {
      "value": 1946,
      "label": "ESCUELA BASICA POETA NERUDA (EX-483)"
    },
    {
      "value": 1947,
      "label": "COLEGIO INFOCAP"
    },
    {
      "value": 1948,
      "label": "ESCUELA POETA VICTOR DOMINGO SILVA"
    },
    {
      "value": 1949,
      "label": "ESCUELA JULIETA BECERRA ALVAREZ"
    },
    {
      "value": 1950,
      "label": "LICEO POLIVALENTE SAN JOSE DE MAIPO"
    },
    {
      "value": 1951,
      "label": "LICEO ANDRES BELLO"
    },
    {
      "value": 1952,
      "label": "LICEO ANDRES BELLO LOCAL: 2"
    },
    {
      "value": 1953,
      "label": "COLEGIO PARROQUIAL SAN MIGUEL"
    },
    {
      "value": 1954,
      "label": "INSTITUTO SUPERIOR DE COMERCIO DE CHILE (EX A99)"
    },
    {
      "value": 1955,
      "label": "INSTITUTO MIGUEL LEON PRADO"
    },
    {
      "value": 1956,
      "label": "ESCUELA LLANO SUBERCASEAUX"
    },
    {
      "value": 1957,
      "label": "COLEGIO SANTO CURA DE ARS"
    },
    {
      "value": 1958,
      "label": "ESCUELA BASICA MUNICIPAL PABLO NERUDA"
    },
    {
      "value": 1959,
      "label": "ESCUELA DE LA INDUSTRIA GRAFICA HECTOR GOMEZ"
    },
    {
      "value": 1960,
      "label": "LICEO BETSABE HORMAZABAL DE ALARCON"
    },
    {
      "value": 1961,
      "label": "LICEO BETSABE HORMAZABAL DE ALARCON LOCAL: 2"
    },
    {
      "value": 1962,
      "label": "LICEO MUNICIPAL DE SAN PEDRO"
    },
    {
      "value": 1963,
      "label": "LICEO MUNICIPALIZADO ARAUCANIA"
    },
    {
      "value": 1964,
      "label": "CENTRO EDUCACIONAL SAN RAMON"
    },
    {
      "value": 1965,
      "label": "ESCUELA TUPAHUE"
    },
    {
      "value": 1966,
      "label": "ESCUELA ARTURO MATTE LARRAIN"
    },
    {
      "value": 1967,
      "label": "ESCUELA BASICA ALIVEN"
    },
    {
      "value": 1968,
      "label": "CENTRO EDUCACIONAL MIRADOR"
    },
    {
      "value": 1969,
      "label": "ESCUELA VILLA LA CULTURA"
    },
    {
      "value": 1970,
      "label": "LICEO MUNICIPAL PURKUYEN"
    },
    {
      "value": 1971,
      "label": "ESCUELA EDUCADORES DE CHILE"
    },
    {
      "value": 1972,
      "label": "LICEO COMERCIAL VATE VICENTE HUIDOBRO"
    },
    {
      "value": 1973,
      "label": "ESCUELA NANIHUE"
    },
    {
      "value": 1974,
      "label": "ESCUELA KARELMAPU"
    },
    {
      "value": 1975,
      "label": "ESCUELA SENDERO DEL SABER"
    },
    {
      "value": 1976,
      "label": "LICEO JAVIERA CARRERA"
    },
    {
      "value": 1977,
      "label": "ESCUELA DR LUIS CALVO MACKENNA"
    },
    {
      "value": 1978,
      "label": "ESCUELA DR LUIS CALVO MACKENNA LOCAL: 2"
    },
    {
      "value": 1979,
      "label": "LICEO MIGUEL LUIS AMUNATEGUI"
    },
    {
      "value": 1980,
      "label": "LICEO MIGUEL LUIS AMUNATEGUI LOCAL: 2"
    },
    {
      "value": 1981,
      "label": "LICEO DE APLICACION 29"
    },
    {
      "value": 1982,
      "label": "LICEO DE APLICACION 29 LOCAL: 2"
    },
    {
      "value": 1983,
      "label": "INST.SUP.DE COMERCIO EDUARDO FREI M."
    },
    {
      "value": 1984,
      "label": "ESCUELA LIBERTADORES DE CHILE"
    },
    {
      "value": 1985,
      "label": "LICEO ISAURA DINATOR DE GUZMAN"
    },
    {
      "value": 1986,
      "label": "LICEO ISAURA DINATOR DE GUZMAN LOCAL: 2"
    },
    {
      "value": 1987,
      "label": "LICEO DE APLICACION 21"
    },
    {
      "value": 1988,
      "label": "ESCUELA REPUBLICA DE ALEMANIA"
    },
    {
      "value": 1989,
      "label": "LICEO INDUSTRIAL ELIODORO GARCIA ZEGERS"
    },
    {
      "value": 1990,
      "label": "LICEO INDUSTRIAL ELIODORO GARCIA ZEGERS LOCAL: 2"
    },
    {
      "value": 1991,
      "label": "ESCUELA REPUBLICA DE PANAMA"
    },
    {
      "value": 1992,
      "label": "ESCUELA SALVADOR SANFUENTES"
    },
    {
      "value": 1993,
      "label": "ESCUELA SALVADOR SANFUENTES LOCAL: 2"
    },
    {
      "value": 1994,
      "label": "LICEO MIGUEL DE CERVANTES Y SAAVEDRA ANEXO BASICA"
    },
    {
      "value": 1995,
      "label": "LICEO MIGUEL DE CERVANTES Y SAAVEDRA ANEXO BASICA LOCAL: 2"
    },
    {
      "value": 1996,
      "label": "LICEO PDTE. GABRIEL GONZALEZ VIDELA"
    },
    {
      "value": 1997,
      "label": "LICEO LIB. GRAL JOSE DE SAN MARTIN"
    },
    {
      "value": 1998,
      "label": "LICEO DARIO SALAS"
    },
    {
      "value": 1999,
      "label": "ESCUELA CADETE ARTURO PRAT CHACON"
    },
    {
      "value": 2000,
      "label": "LICEO CONFEDERACION SUIZA"
    },
    {
      "value": 2001,
      "label": "ESCUELA FERNANDO ALESSANDRI RODRIGUEZ"
    },
    {
      "value": 2002,
      "label": "ESCUELA REPUBLICA DE COLOMBIA"
    },
    {
      "value": 2003,
      "label": "ESC. REPUBLICA ORIENTAL DE URUGUAY"
    },
    {
      "value": 2004,
      "label": "ESCUELA BENJAMIN VICUNA MACKENNA"
    },
    {
      "value": 2005,
      "label": "LICEO TERESA PRATS DE SARRATEA"
    },
    {
      "value": 2006,
      "label": "LICEO REPUBLICA DE BRASIL"
    },
    {
      "value": 2007,
      "label": "LICEO METROPOLITANO DE ADULTOS"
    },
    {
      "value": 2008,
      "label": "ESCUELA IRENE FREI DE CID"
    },
    {
      "value": 2009,
      "label": "ESCUELA PROVINCIA DE CHILOE"
    },
    {
      "value": 2010,
      "label": "ESCUELA PILOTO PARDO"
    },
    {
      "value": 2011,
      "label": "LICEO MANUEL BARROS BORGONO"
    },
    {
      "value": 2012,
      "label": "LICEO MANUEL BARROS BORGONO LOCAL: 2"
    },
    {
      "value": 2013,
      "label": "ESCUELA REPUBLICA DE GRECIA"
    },
    {
      "value": 2014,
      "label": "ESCUELA REPUBLICA DE GRECIA LOCAL: 2"
    },
    {
      "value": 2015,
      "label": "LICEO POLITECNICO C-120"
    },
    {
      "value": 2016,
      "label": "LICEO POLIVALENTE A-119 MEDIA"
    },
    {
      "value": 2017,
      "label": "LICEO POLIVALENTE A-119 MEDIA LOCAL: 2"
    },
    {
      "value": 2018,
      "label": "LICEO A-119 BASICO"
    },
    {
      "value": 2019,
      "label": "LICEO POLITECNICO C-120 LOCAL: 2"
    },
    {
      "value": 2020,
      "label": "LICEO HUERTOS FAMILIARES"
    },
    {
      "value": 2021,
      "label": "LICEO POLIVALENTE MANUEL RODRIGUEZ C-82"
    },
    {
      "value": 2022,
      "label": "LICEO POLIVALENTE MANUEL RODRIGUEZ C-82 LOCAL: 2"
    },
    {
      "value": 2023,
      "label": "LICEO AMANDA LABARCA B-66"
    },
    {
      "value": 2024,
      "label": "COLEGIO TABANCURA"
    },
    {
      "value": 2025,
      "label": "ALIANZA FRANCESA"
    },
    {
      "value": 2026,
      "label": "COLEGIO ANTARTICA CHILENA"
    },
    {
      "value": 2027,
      "label": "COLEGIO SAN PEDRO NOLASCO"
    },
    {
      "value": 2028,
      "label": "COLEGIO SANTA URSULA"
    },
    {
      "value": 2030,
      "label": "COLEGIO DE LOS SAGRADOS CORAZONES LOCAL: 2"
    },
    {
      "value": 2031,
      "label": "COLEGIO SAINT GEORGES"
    },
    {
      "value": 2032,
      "label": "COLEGIO SAINT GEORGES LOCAL: 2"
    }
  ];
export default locales;