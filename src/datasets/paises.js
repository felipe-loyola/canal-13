var paises = [
    {
        "value": 8001,
        "label": "ALEMANIA"
    },
    {
        "value": 8002,
        "label": "ARGENTINA"
    },
    {
        "value": 8003,
        "label": "AUSTRALIA"
    },
    {
        "value": 8004,
        "label": "AUSTRIA"
    },
    {
        "value": 8005,
        "label": "BELGICA"
    },
    {
        "value": 8006,
        "label": "BOLIVIA"
    },
    {
        "value": 8007,
        "label": "BRASIL"
    },
    {
        "value": 8008,
        "label": "CANADA"
    },
    {
        "value": 8009,
        "label": "COLOMBIA"
    },
    {
        "value": 8010,
        "label": "COREA"
    },
    {
        "value": 8011,
        "label": "COSTA RICA"
    },
    {
        "value": 8012,
        "label": "CROACIA"
    },
    {
        "value": 8013,
        "label": "CUBA"
    },
    {
        "value": 8014,
        "label": "DINAMARCA"
    },
    {
        "value": 8015,
        "label": "ECUADOR"
    },
    {
        "value": 8016,
        "label": "EGIPTO"
    },
    {
        "value": 8017,
        "label": "EL LIBANO"
    },
    {
        "value": 8018,
        "label": "EL SALVADOR"
    },
    {
        "value": 8019,
        "label": "EMIRATOS ARABES UNvalueOS"
    },
    {
        "value": 8020,
        "label": "ESPANA"
    },
    {
        "value": 8021,
        "label": "ESTADOS UNvalueOS"
    },
    {
        "value": 8022,
        "label": "FEDERACION RUSA"
    },
    {
        "value": 8023,
        "label": "FILIPINAS"
    },
    {
        "value": 8024,
        "label": "FINLANDIA"
    },
    {
        "value": 8025,
        "label": "FRANCIA"
    },
    {
        "value": 8026,
        "label": "GRECIA"
    },
    {
        "value": 8027,
        "label": "GUATEMALA"
    },
    {
        "value": 8028,
        "label": "HAITI"
    },
    {
        "value": 8029,
        "label": "HONDURAS"
    },
    {
        "value": 8030,
        "label": "HUNGRIA"
    },
    {
        "value": 8031,
        "label": "INDIA"
    },
    {
        "value": 8032,
        "label": "INDONESIA"
    },
    {
        "value": 8033,
        "label": "IRLANDA"
    },
    {
        "value": 8034,
        "label": "ISRAEL"
    },
    {
        "value": 8035,
        "label": "ITALIA"
    },
    {
        "value": 8036,
        "label": "JAPON"
    },
    {
        "value": 8037,
        "label": "JORDANIA"
    },
    {
        "value": 8038,
        "label": "KENIA"
    },
    {
        "value": 8039,
        "label": "MALASIA"
    },
    {
        "value": 8040,
        "label": "MEXICO"
    },
    {
        "value": 8041,
        "label": "NICARAGUA"
    },
    {
        "value": 8042,
        "label": "NORUEGA"
    },
    {
        "value": 8043,
        "label": "NUEVA ZELANDA"
    },
    {
        "value": 8044,
        "label": "labelES BAJOS"
    },
    {
        "value": 8045,
        "label": "PANAMA"
    },
    {
        "value": 8046,
        "label": "PARAGUAY"
    },
    {
        "value": 8047,
        "label": "PERU"
    },
    {
        "value": 8048,
        "label": "POLONIA"
    },
    {
        "value": 8049,
        "label": "PORTUGAL"
    },
    {
        "value": 8050,
        "label": "REINO UNvalueO"
    },
    {
        "value": 8051,
        "label": "REPUBLICA CHECA"
    },
    {
        "value": 8052,
        "label": "REPUBLICA DOMINICANA"
    },
    {
        "value": 8053,
        "label": "REPUBLICA POPULAR CHINA"
    },
    {
        "value": 8054,
        "label": "RUMANIA"
    },
    {
        "value": 8055,
        "label": "SINGAPUR"
    },
    {
        "value": 8056,
        "label": "CHILE"
    },
    {
        "value": 8057,
        "label": "SUDAFRICA"
    },
    {
        "value": 8058,
        "label": "SUECIA"
    },
    {
        "value": 8059,
        "label": "SUIZA"
    },
    {
        "value": 8060,
        "label": "TAILANDIA"
    },
    {
        "value": 8061,
        "label": "TURQUIA"
    },
    {
        "value": 8062,
        "label": "URUGUAY"
    },
    {
        "value": 8063,
        "label": "VENEZUELA"
    }
]

export default paises;