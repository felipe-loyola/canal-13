var usuarios = 
{
    "dominique.riveracastillo@gmail.com": {
      "nombre": "Dominique Rivera",
      "coordinador": "Felipe",
      "comuna": "PROVIDENCIA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "sofiacubillosm@gmail.com": {
      "nombre": "Sofia Cubillos Moruardo",
      "coordinador": "Felipe",
      "comuna": "PROVIDENCIA",
      "perfil": "Auditor Periodista"
    },
    "francisca.baeza@ug.uchile.cl": {
      "nombre": "Francisca Baeza Castro",
      "coordinador": "Diego",
      "comuna": "MoVIL VOLANTE",
      "perfil": "Auditor Movil"
    },
    "Jbarahonal_23@hotmail.com": {
      "nombre": "Juan Ricardo Barahona Lagos",
      "coordinador": "Diego",
      "comuna": "MoVIL",
      "perfil": "Supervisor Movil"
    },
    "brandonulloa@live.com": {
      "nombre": "Brandon Marcelo Ulloa Mateluna",
      "coordinador": "Felipe",
      "comuna": "HUECHURABA",
      "perfil": "Auditor de Punto Fijo"
    },
    "cristobal.rodriguez@mail.udp.cl": {
      "nombre": "Cristobal Rodriguez Costabal",
      "coordinador": "Felipe",
      "comuna": "HUECHURABA",
      "perfil": "Supervisor"
    },
    "matiasmendezb@hotmail.com": {
      "nombre": "Matias Mendez Bustos",
      "coordinador": "Felipe",
      "comuna": "HUECHURABA",
      "perfil": "Auditor de Punto Fijo"
    },
    "thaby070899@gmail.com":{
      "nombre": "Thabata Aálenco Diaz",
      "coordinador": "Diego",
      "comuna": "Independencia",
      "perfil": "Auditor de Punto Fijo"
    },
    "thomas.dowding@hotmail.com": {
      "nombre": "Thomas Philip Dowding Sancha",
      "coordinador": "Felipe",
      "comuna": "HUECHURABA",
      "perfil": "Auditor Periodista"
    },
    "lorena.r_93@hotmail.com": {
      "nombre": "Lorena Rojas Vivar",
      "coordinador": "Sofia",
      "comuna": "LA PINTANA",
      "perfil": "Supervisor y Auditor Periodista"
    },
    "nasha015@gmail.com":{
      "nombre":"Gabriela Moya",
      "coordinador":"Diego",
      "comuna":"MAIPU",
      "perfil":"Auditor Periodista"
    },
    "godoy_xy@hotmail.com":{
      "nombre" : "Rodrigo Godoy",
      "coordinador": "Felipe",
      "comuna": "HUECHURABA",
      "perfil": "Auditor de Punto Fijo"
    },
    "camilasalgador.cs@gmail.com": {
      "nombre": "Camila Andrea Salgado Ramirez",
      "coordinador": "Felipe",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "cata.moreno.a@gmail.com": {
      "nombre": "Catalina Camila Moreno Araya",
      "coordinador": "Felipe",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "aravenarojas.carol1981@gmail.com": {
      "nombre": "Carol Aravena Rojas",
      "coordinador": "Felipe",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "vanessa.huala@gmail.com": {
      "nombre": "Vanessa Huala Guajardo",
      "coordinador": "Felipe",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor de Punto Fijo"
    },
    "constanza.gutierrezt@gmail.com": {
      "nombre": "Constanza Gutierrez Torres",
      "coordinador": "Felipe",
      "comuna": "CONCEPCION",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "carlos.contreras.olguin@gmail.com": {
      "nombre": "Carlos Alfredo Contreras Olguin",
      "coordinador": "Sofia",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "vale.hinojosa.hernandez@gmail.com": {
      "nombre": "Valentina Hinojosa Hernandez",
      "coordinador": "Sofia",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor Periodista"
    },
    "alvarezamigo.fernanda@gmail.com": {
      "nombre": "Fernanda Nicole Alvarez Amigo",
      "coordinador": "Felipe",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "nicole.rojas.duran@gmail.com": {
      "nombre": "Nicole Rojas Duran",
      "coordinador": "Felipe",
      "comuna": "PENALOLEN",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "ger.aranedaa@gmail.com": {
      "nombre": "Geraldine Caroline Araneda Palma",
      "coordinador": "Sofia",
      "comuna": "PUENTE ALTO",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "mc.floresaguilera@gmail.com": {
      "nombre": "Matilde Carmen Flores Aguilera",
      "coordinador": "Sofia",
      "comuna": "PUENTE ALTO",
      "perfil": "Auditor Periodista"
    },
    "123pabloo@gmail.com": {
      "nombre": "Pablo Castro Molina",
      "coordinador": "Felipe",
      "comuna": "SAN MIGUEL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "c.andrescantero@gmail.com": {
      "nombre": "Carlos Lara Cantero",
      "coordinador": "Felipe",
      "comuna": "MACUL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "michaelvalenzuela.e@hotmail.com": {
      "nombre": "Michael Valenzuela Estay",
      "coordinador": "Felipe",
      "comuna": "QUILICURA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "cest2707@gmail.com": {
      "nombre": "Celeste Soto Torres",
      "coordinador": "Pedro",
      "comuna": "ESTACION CENTRAL",
      "perfil": "Auditor Periodista"
    },
    "maria.alejandrasalari@gmail.com": {
      "nombre": "Maria Salazar Riquelme",
      "coordinador": "Pedro",
      "comuna": "ESTACION CENTRAL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "piacatalinaru@gmail.com": {
      "nombre": "Pia Rubio Ureta",
      "coordinador": "Pedro",
      "comuna": "SAN MIGUEL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "kammybarru@hotmail.com": {
      "nombre": "Kamila Barrueto Toledo",
      "coordinador": "Sofia",
      "comuna": "ESTACION CENTRAL",
      "perfil": "Auditor de Punto Fijo"
    },
    "karlh.cs@gmail.com": {
      "nombre": "Karl Carsens Silva",
      "coordinador": "Sofia",
      "comuna": "ESTACION CENTRAL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "cinthiarojas1d@gmail.com": {
      "nombre": "Cinthia Rojas Copia",
      "coordinador": "Sofia",
      "comuna": "CERRILLOS",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "claudioc_9@hotmail.com": {
      "nombre": "Claudio Coronado Alarcon",
      "coordinador": "Sofia",
      "comuna": "ESTACION CENTRAL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "aylinrena@gmail.com": {
      "nombre": "Aylin Negrete",
      "coordinador": "Sofia",
      "comuna": "QUINTA NORMAL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "slquezadac@gmail.com": {
      "nombre": "Sandra Lorena Quezada Carrasco",
      "coordinador": "Sofia",
      "comuna": "PENALOLEN",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "alanisbelen55@gmail.com": {
      "nombre": "Alanis Ortiz Morales",
      "coordinador": "Sofia",
      "comuna": "PUDAHUEL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "uka.blah@gmail.com": {
      "nombre": "Lucas Alejandro Calderon Rebolledo",
      "coordinador": "Sofia",
      "comuna": "MAIPU",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "estudiante_ek@hotmail.com": {
      "nombre": "Rodrigo Galaz Orrego",
      "coordinador": "Sofia",
      "comuna": "QUINTA NORMAL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "durley.armijos@grupotawa.com": {
      "nombre": "Durley Armijos",
      "coordinador": "Felipe",
      "comuna": "PUENTE ALTO",
      "perfil": "Auditor Periodista"
    },
    "vania.montecino@gmail.com": {
      "nombre": "Vania Ivette Montecino Carvajal",
      "coordinador": "Felipe",
      "comuna": "PUENTE ALTO",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "cami.riverasantoro@gmail.com": {
      "nombre": "Camila Rivera Santoro",
      "coordinador": "Felipe",
      "comuna": "PENALOLEN",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "claudya.alejandra123@gmail.com": {
      "nombre": "Claudia Hernandez Lara",
      "coordinador": "Pedro",
      "comuna": "MAIPU",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "c.francisca.d@gmail.com": {
      "nombre": "Francisca Delfina CachaNa Ramirez",
      "coordinador": "Pedro",
      "comuna": "MAIPU",
      "perfil": "Auditor de Punto Fijo"
    },
    "Ignacio.hernandez.lara@gmail.com": {
      "nombre": "Ignacio Hernandez Lara",
      "coordinador": "Pedro",
      "comuna": "MAIPU",
      "perfil": "Auditor Periodista"
    },
    "rinvernizzi@alumnos.uai.cl": {
      "nombre": "Renzo invernizzi Bobadilla",
      "coordinador": "Felipe",
      "comuna": "SANTIAGO",
      "perfil": "Supervisor y Auditor Periodista"
    },
    "danielvalencia830@gmail.com": {
      "nombre": "Daniel Valencia Mosqueira",
      "coordinador": "Felipe",
      "comuna": "RECOLETA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "dalegria14@gmail.com": {
      "nombre": "Daniela Alegria Munita",
      "coordinador": "Diego",
      "comuna": "RECOLETA",
      "perfil": "Auditor Periodista"
    },
    "rayenagz@outlook.com": {
      "nombre": "Rayen Alen Gonzalez Zamorano",
      "coordinador": "Diego",
      "comuna": "RECOLETA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "daniella.soriano@gmail.com": {
      "nombre": "Daniella Soriano Sagredo",
      "coordinador": "Sofia",
      "comuna": "CONCHALI",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "maildealexandra@gmail.com": {
      "nombre": "Alexandra CabaNa Echeverria",
      "coordinador": "Sofia",
      "comuna": "PENALOLEN",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "fernandaangelica0401@gmail.com": {
      "nombre": "Fernanda MuNoz Soto",
      "coordinador": "Pedro",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor"
    },
    "natalin1092@hotmail.com": {
      "nombre": "Natalia Gonzalez Aravena",
      "coordinador": "Pedro",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor de Punto Fijo"
    },
    "larrainthomas@hotmail.com": {
      "nombre": "Thomas larrain Carrasco",
      "coordinador": "Pedro",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor Periodista"
    },
    "carlosprieto.h@hotmail.com": {
      "nombre": "Carlos Prieto hormazabal",
      "coordinador": "Pedro",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor Periodista"
    },
    "ferradamaxi@gmail.com": {
      "nombre": "Cristian Ferrada Vasquez",
      "coordinador": "Pedro",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor de Punto Fijo"
    },
    "javieresc77@gmail.com": {
      "nombre": "Javier Eduardo Soto Catripan",
      "coordinador": "Pedro",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor"
    },
    "tec.danilo.moya@protonmail.com": {
      "nombre": "Danilo Moya",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Supervisor"
    },
    "darling.darling@gmail.com": {
      "nombre": "Darling NuNez Cartagena",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Auditor de Punto Fijo"
    },
    "milena.s.moreno@gmail.com": {
      "nombre": "Milena Salome Moreno Urra",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Auditor Periodista"
    },
    "tracypalmaurra@gmail.com": {
      "nombre": "Tracy Palma",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Auditor Periodista"
    },
    "howard.kenneth77@gmail.com": {
      "nombre": "Kenneth Howard Lashibat",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Auditor de Punto Fijo"
    },
    "marisolcabello2@gmail.com": {
      "nombre": "Marisol Cabello Ahumada",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Supervisor"
    },
    "nathaly.montecinosr@gmail.com": {
      "nombre": "Nathaly Montecinos Robles",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Auditor de Punto Fijo"
    },
    "paulina.jara2014@umce.cl": {
      "nombre": "Paulina Jara",
      "coordinador": "Pedro",
      "comuna": "NUÑOA",
      "perfil": "Auditor Periodista"
    },
    "jenifervcm@gmail.com": {
      "nombre": "Jenifer Castillo Molina",
      "coordinador": "Sofia",
      "comuna": "SAN MIGUEL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "lorenasuvayke@gmail.com": {
      "nombre": "Lorena Suvayke Contreras",
      "coordinador": "Sofia",
      "comuna": "SANTIAGO",
      "perfil": "Supervisor y Auditor Periodista"
    },
    "cristobal.carmona@ug.uchile.cl": {
      "nombre": "Cristobal Carmona Fontencilla",
      "coordinador": "Sofia",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor de Punto Fijo"
    },
    "javierarayen.huerta@gmail.com": {
      "nombre": "Javiera Rayen Huerta Ubeda",
      "coordinador": "Sofia",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "francisca.villalobos.espinoza@gmail.com": {
      "nombre": "Francisca Villalobos Espinoza",
      "coordinador": "Sofia",
      "comuna": "PENALOLEN",
      "perfil": "Auditor Periodista"
    },
    "negro_marlon@hotmail.com": {
      "nombre": "Marlon Farias AcuNa",
      "coordinador": "Sofia",
      "comuna": "PENALOLEN",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "diegoroa0@gmail.com": {
      "nombre": "Diego Roa Bahamondes",
      "coordinador": "Pedro",
      "comuna": "PROVIDENCIA",
      "perfil": "Supervisor y Auditor Periodista"
    },
    "margothsotocatripan@gmail.com": {
      "nombre": "Margoth Soto Catripan",
      "coordinador": "Diego",
      "comuna": "EL BOSQUE",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "mignacia.sv@gmail.com": {
      "nombre": "Maria Ignacia Sierra",
      "coordinador": "Diego",
      "comuna": "EL BOSQUE",
      "perfil": "Auditor de Punto Fijo"
    },
    "federico.aguilera@ug.uchile.cl": {
      "nombre": "Federico Aguilera Barrios",
      "coordinador": "Diego",
      "comuna": "SANTIAGO",
      "perfil": "Auditor de Punto Fijo"
    },
    "maca.fran1718@gmail.com": {
      "nombre": "Macarena Mayol Gonzalez",
      "coordinador": "Diego",
      "comuna": "SANTIAGO",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "camilapaz.godoym@gmail.com": {
      "nombre": "Camila Godoy MuNoz",
      "coordinador": "Pedro",
      "comuna": "SAN BERNARDO",
      "perfil": "Auditor Periodista"
    },
    "sleiva.obreque@gmail.com": {
      "nombre": "Sara Leiva Obreque",
      "coordinador": "Pedro",
      "comuna": "SAN BERNARDO",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "cristopher.acu70@gmail.com": {
      "nombre": "Cristopher AcuNa Palma",
      "coordinador": "Felipe",
      "comuna": "CONCEPCION",
      "perfil": "Auditor Periodista"
    },
    "kamoreno_a@hotmail.com": {
      "nombre": "Karen Gisella Moreno Alvarez",
      "coordinador": "Pedro",
      "comuna": "CURICo",
      "perfil": "Supervisor y Auditor Periodista"
    },
    "diazleonmatias@gmail.com": {
      "nombre": "Matias Cristian Diaz Leon",
      "coordinador": "Sofia",
      "comuna": "INDEPENDENCIA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "evelyngrimanesa@gmail.com": {
      "nombre": "Evelyn Grimanesa Rojas Deliz",
      "coordinador": "Diego",
      "comuna": "LA FLORIDA",
      "perfil": "Auditor de Punto Fijo"
    },
    "jrojasdeliz98@gmail.com": {
      "nombre": "Jocelyn Estefania Rojas Deliz",
      "coordinador": "Diego",
      "comuna": "LA FLORIDA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "rojasnayareth@gmail.com": {
      "nombre": "Nayareth Rojas IbaNez",
      "coordinador": "Diego",
      "comuna": "MACUL",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "karlamujica5@gmail.com": {
      "nombre": "Karla Mujica Alvarez",
      "coordinador": "Diego",
      "comuna": "SANTIAGO",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "ingeniero.hurtado@gmail.com": {
      "nombre": "Felipe Hurtado Bernejo",
      "coordinador": "Diego",
      "comuna": "MAIPU",
      "perfil": "Supervisor"
    },
    "masha015@gmail.com": {
      "nombre": "Gabriela Moya",
      "coordinador": "Diego",
      "comuna": "MAIPU",
      "perfil": "Auditor Periodista"
    },
    "luisfranciscosalinas@gmail.com": {
      "nombre": "Luis Franco Salinas PiNa",
      "coordinador": "Diego",
      "comuna": "MAIPU",
      "perfil": "Auditor de Punto Fijo"
    },
    "yaritzadelpilar@gmail.com": {
      "nombre": "Yaritza del Pilar Diaz Hurtado",
      "coordinador": "Diego",
      "comuna": "MAIPU",
      "perfil": "Auditor de Punto Fijo"
    },
    "karina.saldano.28@gmail.com": {
      "nombre": "Karina Paola SaldaNa Godoy",
      "coordinador": "Diego",
      "comuna": "LA CISTERNA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "javier.sebastian.soto.s@gmail.com": {
      "nombre": "Javier Soto Salamanca",
      "coordinador": "Diego",
      "comuna": "INDEPENDENCIA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "thaby070899@hotmail.com": {
      "nombre": "Thabata Aalenco Diaz",
      "coordinador": "Diego",
      "comuna": "INDEPENDENCIA",
      "perfil": "Auditor de Punto Fijo"
    },
    "sfuentesu@gmail.com": {
      "nombre": "Sandra Fuentes uribe",
      "coordinador": "Diego",
      "comuna": "INDEPENDENCIA",
      "perfil": "Auditor de Punto Fijo"
    },
    "rastaman_kastor@hotmail.com": {
      "nombre": "Sebastian Requena",
      "coordinador": "Diego",
      "comuna": "INDEPENDENCIA",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "karenloreto.13@hotmail.cl": {
      "nombre": "Karen Loreto Gonzalez Garcia",
      "coordinador": "Diego",
      "comuna": "Viña del Mar",
      "perfil": "Supervisor y Auditor de Punto Fijo"
    },
    "oulamen@gmail.com": {
      "nombre": "Leonardo Enrique Ibacache Araya",
      "coordinador": "Diego",
      "comuna": "VALPARAiSO",
      "perfil": "Auditor Periodista"
    },
    "normagonzalez._@hotmail.com": {
      "nombre": "Norma Gonzalez Garcia",
      "coordinador": "Diego",
      "comuna": "Viña del mar",
      "perfil": "Auditor Periodista"
    }
  }

export default usuarios;