var locales = {
    1: {
        "value": 1,
        "label": " COLEGIO SAN ANTONIO DE MATILLA"
    },
    2: {
        "value": 2,
        "label": " ANEXO DE COLEGIO SAN ANTONIO DE MATILLA"
    },
    3: {
        "value": 3,
        "label": " LICEO S.S JUAN PABLO SEGUNDO"
    },
    4: {
        "value": 4,
        "label": " LICEO LOS CONDORES"
    },
    5: {
        "value": 5,
        "label": " COLEGIO MARISTA HERMANO FERNANDO"
    },
    6: {
        "value": 6,
        "label": " COLEGIO SIMON BOLIVAR"
    },
    7: {
        "value": 7,
        "label": " COLEGIO NAZARET"
    },
    8: {
        "value": 8,
        "label": " LICEO PABLO NERUDA"
    },
    9: {
        "value": 9,
        "label": " ESCUELA CAMINA"
    },
    10: {
        "value": 10,
        "label": " LICEO TECNICO PROFESIONAL COLCHANE"
    },
    11: {
        "value": 11,
        "label": " ESCUELA CHIAPA"
    },
    12: {
        "value": 12,
        "label": " LICEO DE HUARA"
    },
    13: {
        "value": 13,
        "label": " GIMNASIO MUNICIPAL DE HUARA"
    },
    14: {
        "value": 14,
        "label": " LICEO LUIS CRUZ MARTINEZ"
    },
    15: {
        "value": 15,
        "label": " UNIVERSvalueAD ARTURO PRAT"
    },
    16: {
        "value": 16,
        "label": " ESC.EDUC.ESPECIAL FLOR DEL INCA"
    },
    17: {
        "value": 17,
        "label": " ESCUELA PROFESOR MANUEL CASTRO RAMOS"
    },
    18: {
        "value": 18,
        "label": " ESCUELA PLACvalueO VILLARROEL"
    },
    19: {
        "value": 19,
        "label": " ESCUELA PAULA JARAQUEMADA ALQUIZAR"
    },
    20: {
        "value": 20,
        "label": " COLEGIO REPUBLICA DE ITALIA"
    },
    21: {
        "value": 21,
        "label": " ESCUELA ALMIRANTE PATRICIO LYNCH"
    },
    22: {
        "value": 22,
        "label": " ESCUELA EDUARDO LLANOS"
    },
    23: {
        "value": 23,
        "label": " COLEGIO DIOCESANO OBISPO LABBE"
    },
    24: {
        "value": 24,
        "label": " LICEO BICENTENARIO DOMINGO SANTA MARIA DE IQQ."
    },
    25: {
        "value": 25,
        "label": " LICEO POLITEC. JOSE GUTIERREZ DE LA FUEN"
    },
    26: {
        "value": 26,
        "label": " LICEO LIBERT. GRAL.BERNARDO O'HIGGINS"
    },
    27: {
        "value": 27,
        "label": " INST. COM. DE IQUIQUE BALDOMERO WOLNITZKY"
    },
    28: {
        "value": 28,
        "label": " LICEO ELENA DUVAUCHELLE CABEZON"
    },
    29: {
        "value": 29,
        "label": " ESCUELA CHIPANA"
    },
    30: {
        "value": 30,
        "label": " ESCUELA GABRIELA MISTRAL"
    },
    31: {
        "value": 31,
        "label": " ESCUELA CENTENARIO"
    },
    32: {
        "value": 32,
        "label": " ESCUELA THILDA PORTILLO OLIVARES"
    },
    33: {
        "value": 33,
        "label": " COLEGIO REPUBLICA DE CROACIA"
    },
    34: {
        "value": 34,
        "label": " LICEO COLEGIO DEPORTIVO"
    },
    35: {
        "value": 35,
        "label": " COLEGIO ESPANA"
    },
    36: {
        "value": 36,
        "label": " LICEO ACADEMIA IQUIQUE"
    },
    37: {
        "value": 37,
        "label": " ESCUELA SAN ANDRES DE PICA"
    },
    38: {
        "value": 38,
        "label": " ESCUELA BASICA MAMINA"
    },
    39: {
        "value": 39,
        "label": " ESCUELA BASICA POZO ALMONTE"
    },
    40: {
        "value": 40,
        "label": " LIC. ALCDE.SERGIO GONZALEZ GUTIERREZ"
    },
    41: {
        "value": 41,
        "label": " ESCUELA ESPANA"
    },
    42: {
        "value": 42,
        "label": " LICEO LA PORTADA"
    },
    43: {
        "value": 43,
        "label": " ESCUELA PADRE GUSTAVO LE PAIGE WALQUE"
    },
    44: {
        "value": 44,
        "label": " ESCUELA JOSE PAPIC RADNIC"
    },
    45: {
        "value": 45,
        "label": " ESCUELA GONZALEZ ECHEGOYEN"
    },
    46: {
        "value": 46,
        "label": " ESCUELA LAS AMERICAS"
    },
    47: {
        "value": 47,
        "label": " ESCUELA LAS ROCAS"
    },
    48: {
        "value": 48,
        "label": " LICEO MAYOR GRAL.(E) OSCAR BONILLA"
    },
    49: {
        "value": 49,
        "label": " ESCUELA REPUBLICA ARGENTINA"
    },
    50: {
        "value": 50,
        "label": " ESCUELA ARTURO PRAT"
    },
    51: {
        "value": 51,
        "label": " ESCUELA JAPON"
    },
    52: {
        "value": 52,
        "label": " ESCUELA ECOLOGICA PADRE ALBERTO HURTADO"
    },
    53: {
        "value": 53,
        "label": " ESCUELA JUAN PABLO II"
    },
    54: {
        "value": 54,
        "label": " REVERENDO PADRE PATRICIO CARIOLA"
    },
    55: {
        "value": 55,
        "label": " ESCUELA ELMO FUNEZ CARRIZO"
    },
    56: {
        "value": 56,
        "label": " ESCUELA HEROES DE LA CONCEPCION"
    },
    57: {
        "value": 57,
        "label": " LICEO CIENTIFICO HUMANISTA LA CHIMBA"
    },
    58: {
        "value": 58,
        "label": " LICEO MARTA NAREA DIAZ"
    },
    59: {
        "value": 59,
        "label": " ESCUELA ARMANDO CARRERA GONZALEZ"
    },
    60: {
        "value": 60,
        "label": " ESCUELA LIBERTADORES DE CHILE"
    },
    61: {
        "value": 61,
        "label": " ESCUELA ALCALDE MAXIMILIANO POBLETE"
    },
    62: {
        "value": 62,
        "label": " C.E.I.A. ANTONIO RENDIC."
    },
    63: {
        "value": 63,
        "label": " ESCUELA REPUBLICA DEL ECUADOR"
    },
    64: {
        "value": 64,
        "label": " LICEO TECNICO DE ANTOFAGASTA"
    },
    65: {
        "value": 65,
        "label": " LICEO COMERCIAL JERARDO MUNOZ CAMPO"
    },
    66: {
        "value": 66,
        "label": " ESCUELA DARIO SALAS DIAZ"
    },
    67: {
        "value": 67,
        "label": " ESCUELA REPUBLICA DE LOS ESTADOS UNvalueOS"
    },
    68: {
        "value": 68,
        "label": " LICEO MARIO BAHAMONDE SILVA"
    },
    69: {
        "value": 69,
        "label": " ESCUELA PROFESORA LJUBICA DOMIC WUTH"
    },
    70: {
        "value": 70,
        "label": " ESCUELA CLAUDIO MATTE PEREZ"
    },
    71: {
        "value": 71,
        "label": " LICEO ANDRES SABELLA"
    },
    72: {
        "value": 72,
        "label": " COLEGIO INGLES SAN JOSE"
    },
    73: {
        "value": 73,
        "label": " LICEO DOMINGO HERRERA RIVERA"
    },
    74: {
        "value": 74,
        "label": " ESCUELA BASICA VADO DE TOPATER"
    },
    75: {
        "value": 75,
        "label": " ESCUELA BASICA 21 DE MAYO"
    },
    76: {
        "value": 76,
        "label": " ESCUELA BASICA PRESvalueENTE BALMACEDA"
    },
    77: {
        "value": 77,
        "label": " LICEO POLITECNICO CESAREO AGUIRRE G."
    },
    78: {
        "value": 78,
        "label": " ESCUELA BASICA REPUBLICA DE FRANCIA"
    },
    79: {
        "value": 79,
        "label": " ESCUELA BASICA EMILIO SOTOMAYOR"
    },
    80: {
        "value": 80,
        "label": " ESCUELA DIFERENCIAL LOA"
    },
    81: {
        "value": 81,
        "label": " COLEGIO CALAMA"
    },
    82: {
        "value": 82,
        "label": " LICEO LUIS CRUZ MARTINEZ"
    },
    83: {
        "value": 83,
        "label": " LICEO ELEUTERIO RAMIREZ MOLINA"
    },
    84: {
        "value": 84,
        "label": " ESCUELA CLAUDIO ARRAU"
    },
    85: {
        "value": 85,
        "label": " ESCUELA GRECIA"
    },
    86: {
        "value": 86,
        "label": " ESCUELA REPUBLICA DE BOLIVIA"
    },
    87: {
        "value": 87,
        "label": " ESCUELA BASICA JOHN FITZGERALD KENNEDY"
    },
    88: {
        "value": 88,
        "label": " LICEO FRANCISCO DE AGUIRRE"
    },
    89: {
        "value": 89,
        "label": " INSTITUTO OBISPO SILVA LEZAETA"
    },
    90: {
        "value": 90,
        "label": " LICEO MINERO AMERICA"
    },
    91: {
        "value": 91,
        "label": " LICEO JORGE ALESSANDRI RODRIGUEZ"
    },
    92: {
        "value": 92,
        "label": " ESCUELA BASICA SAN FRANCISCO"
    },
    93: {
        "value": 93,
        "label": " COLEGIO CHUQUICAMATA"
    },
    94: {
        "value": 94,
        "label": " ESCUELA ARTURO PEREZ CANTO"
    },
    95: {
        "value": 95,
        "label": " ESCUELA ARTURO PEREZ CANTO"
    },
    96: {
        "value": 96,
        "label": " ESCUELA IGNACIO CARRERA PINTO"
    },
    97: {
        "value": 97,
        "label": " COMPLEJO EDUCATIVO JUAN JOSE LATORRE BENAVENT"
    },
    98: {
        "value": 98,
        "label": " ESCUELA BASICA JULIA HERRERA VARAS"
    },
    99: {
        "value": 99,
        "label": " ESC. BASICA RURAL SAN ANTONIO DE PADUA"
    },
    100: {
        "value": 100,
        "label": " ESCUELA BASICA SAN PEDRO DE ATACAMA"
    },
    101: {
        "value": 101,
        "label": " ESCUELA ESTACION BAQUEDANO"
    },
    102: {
        "value": 102,
        "label": " ESCUELA CARACOLES"
    },
    103: {
        "value": 103,
        "label": " ESCUELA VICTOR CARVAJAL MEZA"
    },
    104: {
        "value": 104,
        "label": " ESCUELA HOGAR VICTORIANO QUINTEROS SOTO"
    },
    105: {
        "value": 105,
        "label": " ESCUELA PABLO NERUDA"
    },
    106: {
        "value": 106,
        "label": " ESCUELA ARTURO PRAT CHACON"
    },
    107: {
        "value": 107,
        "label": " ESCUELA RICARDO CAMPILLAY CONTRERAS"
    },
    108: {
        "value": 108,
        "label": " EDUCADOR ARTURO ALVEAR RAMOS"
    },
    109: {
        "value": 109,
        "label": " ESCUELA FRONTERIZA"
    },
    110: {
        "value": 110,
        "label": " ESCUELA MANUEL ORELLA ECHANEZ"
    },
    111: {
        "value": 111,
        "label": " LICEO FEDERICO VARELA"
    },
    112: {
        "value": 112,
        "label": " LICEO BICENTENARIO MERCEDES FRITIS MACKENNEY"
    },
    113: {
        "value": 113,
        "label": " LICEO TECNOLOGICO DE COPIAPO"
    },
    114: {
        "value": 114,
        "label": " ESCUELA BRUNO ZAVALA FREDES"
    },
    115: {
        "value": 115,
        "label": " ESCUELA EL PALOMAR"
    },
    116: {
        "value": 116,
        "label": " LICEO JOSE ANTONIO CARVAJAL"
    },
    117: {
        "value": 117,
        "label": " LICEO DE MUSICA"
    },
    118: {
        "value": 118,
        "label": " ESCUELA BERNARDO O HIGGINS"
    },
    119: {
        "value": 119,
        "label": " CENTRO DE EDUCACION INTEGRADA DE ADULTOS"
    },
    120: {
        "value": 120,
        "label": " INSTITUTO COMERCIAL ALEJANDRO RIVERA DIAZ"
    },
    121: {
        "value": 121,
        "label": " ESCUELA BASICA SARA CORTES CORTES"
    },
    122: {
        "value": 122,
        "label": " LICEO DIEGO DE ALMEYDA"
    },
    123: {
        "value": 123,
        "label": " LICEO DIEGO DE ALMEYDA"
    },
    124: {
        "value": 124,
        "label": " ESCUELA BASICA ALEJANDRO NOEMI HUERTA"
    },
    125: {
        "value": 125,
        "label": " ESCUELA BASICA JOSE MIGUEL CARRERA"
    },
    126: {
        "value": 126,
        "label": " ESCUELA VICTOR MANUEL SANCHEZ CABANAS"
    },
    127: {
        "value": 127,
        "label": " ESCUELA BASICA ROBERTO CUADRA ALQUINTA"
    },
    128: {
        "value": 128,
        "label": " LICEO PEDRO TRONCOSO MACHUCA"
    },
    129: {
        "value": 129,
        "label": " LICEO JOSE SANTOS OSSA"
    },
    130: {
        "value": 130,
        "label": " ESCUELA BASICA GUALBERTO KONG FERNANDEZ"
    },
    131: {
        "value": 131,
        "label": " ESCUELA IGNACIO CARRERA PINTO"
    },
    132: {
        "value": 132,
        "label": " LICEO PEDRO REGALADO S. VvalueELA O."
    },
    133: {
        "value": 133,
        "label": " ESCUELA LUIS CRUZ MARTINEZ"
    },
    134: {
        "value": 134,
        "label": " ESCUELA MUNICIPAL CANELA BAJA"
    },
    135: {
        "value": 135,
        "label": " ESCUELA BASICA MINCHA NORTE"
    },
    136: {
        "value": 136,
        "label": " ESCUELA BASICA AMERICA"
    },
    137: {
        "value": 137,
        "label": " LICEO SAMUEL ROMAN ROJAS"
    },
    138: {
        "value": 138,
        "label": " LICEO DIEGO PORTALES"
    },
    139: {
        "value": 139,
        "label": " LICEO INDUSTRIAL JOSE TOMAS DE URMENETA"
    },
    140: {
        "value": 140,
        "label": " ESCUELA COQUIMBO"
    },
    141: {
        "value": 141,
        "label": " COLEGIO BERNARDO O'HIGGINS"
    },
    142: {
        "value": 142,
        "label": " COLEGIO PARTICULAR GABRIELA MISTRAL"
    },
    143: {
        "value": 143,
        "label": " INSTITUTO SUPERIOR DE COMERCIO DE COQUIMBO"
    },
    144: {
        "value": 144,
        "label": " ESCUELA PRESvalueENTE ANIBAL PINTO"
    },
    145: {
        "value": 145,
        "label": " COLEGIO DE ARTES CLAUDIO ARRAU"
    },
    146: {
        "value": 146,
        "label": " ESCUELA CARDENAL JOSE MARIA CARO"
    },
    147: {
        "value": 147,
        "label": " ESTADIO TECHADO MUNICIPAL"
    },
    148: {
        "value": 148,
        "label": " COLEGIO DISCOVERY SCHOOL"
    },
    149: {
        "value": 149,
        "label": " INSTITUTO DE ADM. Y COMERCIO ESTADO DE ISRAEL"
    },
    150: {
        "value": 150,
        "label": " LICEO FERNANDO BINVIGNAT MARIN"
    },
    151: {
        "value": 151,
        "label": " ESCUELA JOSE ALFARO ALFARO"
    },
    152: {
        "value": 152,
        "label": " ESCUELA SANTO TOMAS DE AQUINO"
    },
    153: {
        "value": 153,
        "label": " LICEO CARMEN AURORA RODRIGUEZ HENRIQUEZ"
    },
    154: {
        "value": 154,
        "label": " LICEO DOMINGO ORTIZ DE ROZAS"
    },
    155: {
        "value": 155,
        "label": " COLEGIO SANTA TERESA"
    },
    156: {
        "value": 156,
        "label": " ESCUELA BASICA VALLE DEL CHOAPA"
    },
    157: {
        "value": 157,
        "label": " LICEO POLITECNICO PABLO RODRIGUEZ CAVIEDES"
    },
    158: {
        "value": 158,
        "label": " ESCUELA BASICA PEDRO PABLO MUNOZ"
    },
    159: {
        "value": 159,
        "label": " ESCUELA ALONSO DE ERCILLA"
    },
    160: {
        "value": 160,
        "label": " COLEGIO CARLOS CONDELL DE LA HAZA"
    },
    161: {
        "value": 161,
        "label": " LICEO JORGE ALESSANDRI RODRIGUEZ"
    },
    162: {
        "value": 162,
        "label": " COLEGIO JAVIERA CARRERA"
    },
    163: {
        "value": 163,
        "label": " COLEGIO HEROES DE LA CONCEPCION"
    },
    164: {
        "value": 164,
        "label": " LICEO TECNICO MARTA BRUNET"
    },
    165: {
        "value": 165,
        "label": " COLEGIO ANDRES BELLO"
    },
    166: {
        "value": 166,
        "label": " COLEGIO GABRIEL GONZALEZ VvalueELA"
    },
    167: {
        "value": 167,
        "label": " LICEO GREGORIO CORDOVEZ"
    },
    168: {
        "value": 168,
        "label": " LICEO GABRIELA MISTRAL DE LA SERENA"
    },
    169: {
        "value": 169,
        "label": " LICEO IGNACIO CARRERA PINTO"
    },
    170: {
        "value": 170,
        "label": " COLEGIO JAPON"
    },
    171: {
        "value": 171,
        "label": " ESCUELA GERMAN RIESCO"
    },
    172: {
        "value": 172,
        "label": " COLEGIO GERONIMO RENDIC"
    },
    173: {
        "value": 173,
        "label": " COLEGIO PARTICULAR SAN JOSE"
    },
    174: {
        "value": 174,
        "label": " ESCUELA CLARA VIAL ORREGO"
    },
    175: {
        "value": 175,
        "label": " COLEGIO DIEGO DE ALMAGRO"
    },
    176: {
        "value": 176,
        "label": " LICEO NICOLAS FEDERICO LOHSE VARGAS"
    },
    177: {
        "value": 177,
        "label": " ESCUELA BASICA TERESA CANNON DE B."
    },
    178: {
        "value": 178,
        "label": " COLEGIO RIO GRANDE"
    },
    179: {
        "value": 179,
        "label": " ESCUELA ALEJANDRO CHELEN ROJAS"
    },
    180: {
        "value": 180,
        "label": " ESCUELA EL PALQUI"
    },
    181: {
        "value": 181,
        "label": " COLEGIO CERRO GUAYAQUIL"
    },
    182: {
        "value": 182,
        "label": " COLEGIO REPUBLICA DE CHILE"
    },
    183: {
        "value": 183,
        "label": " ESCUELA BASICA WENCESLAO VARGAS"
    },
    184: {
        "value": 184,
        "label": " ESC.BASICA SAN ANTONIO DE LA VILLA"
    },
    185: {
        "value": 185,
        "label": " ESCUELA ARTURO VILLALON SIEULANNE"
    },
    186: {
        "value": 186,
        "label": " ESCUELA BASICA ARTURO ALESSANDRI PALMA"
    },
    187: {
        "value": 187,
        "label": " ESCUELA BASICA HELENE LANG"
    },
    188: {
        "value": 188,
        "label": " ESCUELA GUARDIAMARINA ERNESTO RIQUELME V"
    },
    189: {
        "value": 189,
        "label": " COLEGIO FRAY JORGE"
    },
    190: {
        "value": 190,
        "label": " ESCUELA DE ARTES Y MUSICA"
    },
    191: {
        "value": 191,
        "label": " LICEO POLITECNICO DE OVALLE"
    },
    192: {
        "value": 192,
        "label": " ESCUELA BASICA JOSE TOMAS OVALLE B."
    },
    193: {
        "value": 193,
        "label": " ESCUELA BASICA OSCAR ARAYA MOLINA"
    },
    194: {
        "value": 194,
        "label": " LICEO ESTELA AVILA DE PERRY"
    },
    195: {
        "value": 195,
        "label": " LICEO ALEJANDRO ALVAREZ JOFRE"
    },
    196: {
        "value": 196,
        "label": " LICEO MISTRALIANO"
    },
    197: {
        "value": 197,
        "label": " ESCUELA BELGICA"
    },
    198: {
        "value": 198,
        "label": " LICEO ALBERTO GALLARDO LORCA"
    },
    199: {
        "value": 199,
        "label": " ESCUELA BASICA AMANECER"
    },
    200: {
        "value": 200,
        "label": " ESCUELA SAMO ALTO"
    },
    201: {
        "value": 201,
        "label": " ESCUELA BASICA CHILLEPIN"
    },
    202: {
        "value": 202,
        "label": " ESCUELA MATILDE SALAMANCA"
    },
    203: {
        "value": 203,
        "label": " LICEO DE SALAMANCA"
    },
    204: {
        "value": 204,
        "label": " LICEO CARLOS ROBERTO MONDACA CORTES"
    },
    205: {
        "value": 205,
        "label": " ESCUELA BASICA LUCILA GODOY ALCAYAGA"
    },
    206: {
        "value": 206,
        "label": " LICEO TECNICO CARLOS ALESSANDRI ALTAMIRANO"
    },
    207: {
        "value": 207,
        "label": " COLEGIO BASICO CARLOS ALESSANDRI ALTAMIRANO"
    },
    208: {
        "value": 208,
        "label": " LICEO POLIVALENTE A-2 DE CABILDO"
    },
    209: {
        "value": 209,
        "label": " ESCUELA BASICA MUNICIPAL ARAUCARIA"
    },
    210: {
        "value": 210,
        "label": " LICEO PEDRO DE VALDIVIA B-17"
    },
    211: {
        "value": 211,
        "label": " ESCUELA BASICA PALESTINA"
    },
    212: {
        "value": 212,
        "label": " ESCUELA BASICA JOSEFINA HUICI"
    },
    213: {
        "value": 213,
        "label": " ESCUELA BASICA IRMA SAPIAIN "
    },
    214: {
        "value": 214,
        "label": " LICEO PARTICULAR SAN JOSE"
    },
    215: {
        "value": 215,
        "label": " COLEGIO PARTICULAR SAN JOSE"
    },
    216: {
        "value": 216,
        "label": " ESCUELA BASICA LA PAMPILLA"
    },
    217: {
        "value": 217,
        "label": " COLEGIO PRESvalueENTE AGUIRRE CERDA G-496"
    },
    218: {
        "value": 218,
        "label": " LICEO A-44 POETA VICENTE HUvalueOBRO"
    },
    219: {
        "value": 219,
        "label": " LICEO MANUEL DE SALAS"
    },
    220: {
        "value": 220,
        "label": " ESCUELA MANUEL BRAVO REYES"
    },
    221: {
        "value": 221,
        "label": " LICEO POLIVALENTE FERNANDO SILVA CASTELLON"
    },
    222: {
        "value": 222,
        "label": " ESCUELA E-88 MARIA TERESA DEL CANTO MOLINA"
    },
    223: {
        "value": 223,
        "label": " LICEO POLITECNICO DE CONCON"
    },
    224: {
        "value": 224,
        "label": " FUNDACION EDUCACIONAL COLEGIO PARROQUIAL SANTA MARIA GORETTI"
    },
    225: {
        "value": 225,
        "label": " ESCUELA BASICA ORO NEGRO D-367"
    },
    226: {
        "value": 226,
        "label": " COLEGIO RAYEN CAVEN"
    },
    227: {
        "value": 227,
        "label": " COLEGIO EL QUISCO"
    },
    228: {
        "value": 228,
        "label": " COMPLEJO EDUCACIONAL CLARA SOLOVERA"
    },
    229: {
        "value": 229,
        "label": " COLEGIO EL TABO"
    },
    230: {
        "value": 230,
        "label": " LICEO MUNICIPAL LUIS LABORDA"
    },
    231: {
        "value": 231,
        "label": " COLEGIO PARROQUIAL SAN NICOLAS"
    },
    232: {
        "value": 232,
        "label": " COLEGIO LORENZO BAEZA VEGA"
    },
    233: {
        "value": 233,
        "label": " COLEGIO INSULAR ROBINSON CRUSOE G-261"
    },
    234: {
        "value": 234,
        "label": " ESCUELA MARIA ALONSO CHACON"
    },
    235: {
        "value": 235,
        "label": " COLEGIO LEONARDO DA VINCI"
    },
    236: {
        "value": 236,
        "label": " LICEO PULMAHUE DE LA LIGUA"
    },
    237: {
        "value": 237,
        "label": " ESCUELA BASICA GABRIELA MISTRAL E-10"
    },
    238: {
        "value": 238,
        "label": " ESCUELA BASICA BRASILIA"
    },
    239: {
        "value": 239,
        "label": " ESCUELA COEDUCACIONAL TENIENTE HERNAN MERINO CORREA"
    },
    240: {
        "value": 240,
        "label": " ESCUELA BASICA SUPERIOR MIXTA N 88"
    },
    241: {
        "value": 241,
        "label": " LICEO DE LIMACHE"
    },
    242: {
        "value": 242,
        "label": " LICEO POLITECNICO LLAY LLAY B-15"
    },
    243: {
        "value": 243,
        "label": " ESCUELA BASICA AGUSTIN EDWARDS "
    },
    244: {
        "value": 244,
        "label": " ESCUELA BASICA RIO BLANCO"
    },
    245: {
        "value": 245,
        "label": " LICEO TECNICO AMANCAY C-9"
    },
    246: {
        "value": 246,
        "label": " LICEO REPUBLICA ARGENTINA"
    },
    247: {
        "value": 247,
        "label": " LICEO MAXIMILIANO SALAS MARCHAN"
    },
    248: {
        "value": 248,
        "label": " LICEO COMERCIAL DE LOS ANDES"
    },
    249: {
        "value": 249,
        "label": " ESCUELA BASICA ESPANA"
    },
    250: {
        "value": 250,
        "label": " LICEO PARTICULAR FELIPE CORTES"
    },
    251: {
        "value": 251,
        "label": " ESCUELA BASICA ULDA ARACENA GONZALEZ F-203"
    },
    252: {
        "value": 252,
        "label": " LICEO MUNICIPAL JUAN RUSQUE PORTAL"
    },
    253: {
        "value": 253,
        "label": " LICEO DE OLMUE"
    },
    254: {
        "value": 254,
        "label": " ESCUELA ATENAS"
    },
    255: {
        "value": 255,
        "label": " COLEGIO PARTICULAR IGNACIO CARRERA PINTO"
    },
    256: {
        "value": 256,
        "label": " COLEGIO PANQUEHUE"
    },
    257: {
        "value": 257,
        "label": " ESCUELA BASICA PAPUDO"
    },
    258: {
        "value": 258,
        "label": " LICEO CORDILLERA"
    },
    259: {
        "value": 259,
        "label": " LICEO JOSE MANUEL BORGONO NUNEZ"
    },
    260: {
        "value": 260,
        "label": " COLEGIO GENERAL JOSE VELASQUEZ BORQUEZ (2)"
    },
    261: {
        "value": 261,
        "label": " COLEGIO GENERAL JOSE VELASQUEZ BORQUEZ (1)"
    },
    262: {
        "value": 262,
        "label": " ESCUELA MARIE POUSSEPIN"
    },
    263: {
        "value": 263,
        "label": " LICEO MANUEL MARIN FRITIS"
    },
    264: {
        "value": 264,
        "label": " COLEGIO ROBERTO MATTA "
    },
    265: {
        "value": 265,
        "label": " ESCUELA BASICA NUESTRO MUNDO"
    },
    266: {
        "value": 266,
        "label": " LICEO SANTIAGO ESCUTI ORREGO"
    },
    267: {
        "value": 267,
        "label": " ESCUELA BASICA DE NINAS CANADA"
    },
    268: {
        "value": 268,
        "label": " COLEGIO VALLE DE QUILLOTA "
    },
    269: {
        "value": 269,
        "label": " COLEGIO REPUBLICA DE MEXICO "
    },
    270: {
        "value": 270,
        "label": " ESCUELA SUPERIOR N°1"
    },
    271: {
        "value": 271,
        "label": " LICEO COMERCIAL DE QUILLOTA C-27"
    },
    272: {
        "value": 272,
        "label": " COLEGIO ARAUCO "
    },
    273: {
        "value": 273,
        "label": " ESTADIO MUNICIPAL LUCIO FARINA FERNANDEZ"
    },
    274: {
        "value": 274,
        "label": " ESCUELA BASICA E-163 ABEL GUERRERO AGUIRRE"
    },
    275: {
        "value": 275,
        "label": " COLEGIO D-417 GUARDIAMARINA GUILLERMO ZANARTU I."
    },
    276: {
        "value": 276,
        "label": " LICEO JUAN XXIII SEDE EL BELLOTO"
    },
    277: {
        "value": 277,
        "label": " LICEO GASTRONOMIA Y TURISMO"
    },
    278: {
        "value": 278,
        "label": " ESCUELA G-419 LOS MOLLES "
    },
    279: {
        "value": 279,
        "label": " ESCUELA D-430 FERNANDO DURAN VILLARREAL"
    },
    280: {
        "value": 280,
        "label": " COLEGIO JOSE MIGUEL INFANTE D-437"
    },
    281: {
        "value": 281,
        "label": " LICEO A-39 GUILLERMO GRONEMEYER ZAMORANO"
    },
    282: {
        "value": 282,
        "label": " ESCUELA BASICA DARIO SALAS E-423"
    },
    283: {
        "value": 283,
        "label": " COLEGIO PASIONISTAS DE QUILPUE"
    },
    284: {
        "value": 284,
        "label": " ESCUELA D-416 MANUEL BULNES PRIETO"
    },
    285: {
        "value": 285,
        "label": " COLEGIO FUNDADORES DE QUILPUE"
    },
    286: {
        "value": 286,
        "label": " COLEGIO ESPERANZA"
    },
    287: {
        "value": 287,
        "label": " LICEO COMERCIAL A-40 ALEJANDRO LUBET VERGARA"
    },
    288: {
        "value": 288,
        "label": " ESCUELA BASICA D-426 COMANDANTE ELEUTERIO RAMIREZ MOLINA"
    },
    289: {
        "value": 289,
        "label": " COLEGIO CAPITAN IGNACIO CARRERA PINTO"
    },
    290: {
        "value": 290,
        "label": " COLEGIO DON ORIONE"
    },
    291: {
        "value": 291,
        "label": " ESCUELA BASICA REPUBLICA DE FRANCIA"
    },
    292: {
        "value": 292,
        "label": " LICEO POLITECNICO QUINTERO"
    },
    293: {
        "value": 293,
        "label": " COLEGIO ARTISTICO COSTA MAUCO "
    },
    294: {
        "value": 294,
        "label": " ESCUELA PERFECTO DE LA FUENTE DEL VILLAR"
    },
    295: {
        "value": 295,
        "label": " GIMNASIO MUNICIPAL"
    },
    296: {
        "value": 296,
        "label": " ESCUELA G-474 CUNCUMEN"
    },
    297: {
        "value": 297,
        "label": " LICEO JUAN DANTE PARRAGUEZ ARELLANO"
    },
    298: {
        "value": 298,
        "label": " GRUPO ESCOLAR PRESvalueENTE PEDRO AGUIRRE CERDA "
    },
    299: {
        "value": 299,
        "label": " INSTITUTO COMERCIAL MARITIMO PACIFICO SUR"
    },
    300: {
        "value": 300,
        "label": " INSTITUTO BICENTENARIO J.MIGUEL CARRERA"
    },
    301: {
        "value": 301,
        "label": " ESCUELA MOVILIZADORES PORTUARIOS"
    },
    302: {
        "value": 302,
        "label": " ESCUELA VILLA LAS DUNAS"
    },
    303: {
        "value": 303,
        "label": " COLEGIO ESPANA"
    },
    304: {
        "value": 304,
        "label": " ESCUELA INDUSTRIAL SAN ANTONIO"
    },
    305: {
        "value": 305,
        "label": " ESCUELA PABLO NERUDA "
    },
    306: {
        "value": 306,
        "label": " LICEO SAN ESTEBAN"
    },
    307: {
        "value": 307,
        "label": " LICEO DE NINAS CORINA URBINA VILLANUEVA B-5"
    },
    308: {
        "value": 308,
        "label": " COLEGIO PORTALIANO"
    },
    309: {
        "value": 309,
        "label": " ESCUELA JOSE DE SAN MARTIN"
    },
    310: {
        "value": 310,
        "label": " LICEO DOCTOR ROBERTO HUMERES O."
    },
    311: {
        "value": 311,
        "label": " ESCUELA BASICA GENERAL BERNARDO O HIGGINS RIQUELME (E-58)"
    },
    312: {
        "value": 312,
        "label": " ESCUELA MANUEL RODRIGUEZ ERDOIZA"
    },
    313: {
        "value": 313,
        "label": " LICEO DARIO SALAS (2) "
    },
    314: {
        "value": 314,
        "label": " LICEO DARIO SALAS (1)"
    },
    315: {
        "value": 315,
        "label": " COLEGIO PEOPLE HELP PEOPLE"
    },
    316: {
        "value": 316,
        "label": " LICEO B-30 DE NINAS MARIA FRANCK DE MACDOUGALL"
    },
    317: {
        "value": 317,
        "label": " INSTITUTO DE MATEMATICAS PUCV MALAQUIAS MORALES MUNOZ"
    },
    318: {
        "value": 318,
        "label": " LICEO COEDUCACIONAL LA IGUALDAD"
    },
    319: {
        "value": 319,
        "label": " ESCUELA D-255 ALEMANIA"
    },
    320: {
        "value": 320,
        "label": " ESCUELA A-19 HERNAN OLGUIN"
    },
    321: {
        "value": 321,
        "label": " ESCUELA REPUBLICA DEL PARAGUAY"
    },
    322: {
        "value": 322,
        "label": " LICEO JUANA ROSS DE EDWARDS"
    },
    323: {
        "value": 323,
        "label": " INSTITUTO SUPERIOR DE COMERCIO FRANCISCO ARAYA BENNET"
    },
    324: {
        "value": 324,
        "label": " ESCUELA D-272 JUAN JOSE LATORRE"
    },
    325: {
        "value": 325,
        "label": " LICEO TECNICO PROFESIONAL BARON"
    },
    326: {
        "value": 326,
        "label": " COLEGIO LEONARDO MURIALDO"
    },
    327: {
        "value": 327,
        "label": " ESCUELA D-314 JOAQUIN EDWARDS BELLO"
    },
    328: {
        "value": 328,
        "label": " ESCUELA JUAN DE SAAVEDRA"
    },
    329: {
        "value": 329,
        "label": " ESCUELA D-250 GASPAR CABRALES"
    },
    330: {
        "value": 330,
        "label": " ESCUELA ERNESTO QUIROS WEBER"
    },
    331: {
        "value": 331,
        "label": " ESCUELA D-256 REPUBLICA DEL URUGUAY"
    },
    332: {
        "value": 332,
        "label": " COLEGIO SALESIANOS VALPARAISO"
    },
    333: {
        "value": 333,
        "label": " COLEGIO ARTURO EDWARDS (SEDE CARRERA)"
    },
    334: {
        "value": 334,
        "label": " ESCUELA BASICA RAMON BARROS LUCO D-270"
    },
    335: {
        "value": 335,
        "label": " LICEO MIXTO POLIVALENTE MATILDE BRANDAU DE ROSS"
    },
    336: {
        "value": 336,
        "label": " ESCUELA GRECIA (D-251)"
    },
    337: {
        "value": 337,
        "label": " LICEO TECNICO DE VALPARAISO"
    },
    338: {
        "value": 338,
        "label": " COLEGIO ARTURO EDWARDS (SEDE COLON)"
    },
    339: {
        "value": 339,
        "label": " SEMINARIO SAN RAFAEL"
    },
    340: {
        "value": 340,
        "label": " LICEO BICENTENARIO VALPARAISO (B-29)"
    },
    341: {
        "value": 341,
        "label": " COLEGIO SAN PEDRO NOLASCO"
    },
    342: {
        "value": 342,
        "label": " LICEO EDUARDO DE LA BARRA"
    },
    343: {
        "value": 343,
        "label": " SCUOLA ITALIANA ARTURO DELL'ORO"
    },
    344: {
        "value": 344,
        "label": " COLEGIO CARLOS COUSINO"
    },
    345: {
        "value": 345,
        "label": " LICEO PEDRO MONTT C-100"
    },
    346: {
        "value": 346,
        "label": " GIMNASIO PUCV CASA CENTRAL"
    },
    347: {
        "value": 347,
        "label": " COLEGIO INMACULADA CONCEPCION DE NUESTRA SENORA DE LOURDES"
    },
    348: {
        "value": 348,
        "label": " ESCUELA DE TRIPULANTES Y PORTUARIA DE VALPARAISO"
    },
    349: {
        "value": 349,
        "label": " COLEGIO AGUSTIN EDWARDS"
    },
    350: {
        "value": 350,
        "label": " UNIVERSvalueAD DE PLAYA ANCHA FACULTAD DE CIENCIAS DE LA SALUD"
    },
    351: {
        "value": 351,
        "label": " ESCUELA BLAS CUEVAS RAMON ALLENDE"
    },
    352: {
        "value": 352,
        "label": " ESCUELA BASICA N°150 LAGUNA VERDE G-289"
    },
    353: {
        "value": 353,
        "label": " COLEGIO NUEVA ERA SIGLO XXI"
    },
    354: {
        "value": 354,
        "label": " COLEGIO E-268 REPUBLICA DE MEXICO"
    },
    355: {
        "value": 355,
        "label": " LICEO B-26 MARIA LUISA BOMBAL"
    },
    356: {
        "value": 356,
        "label": " ESCUELA E-266 CARABINERO PEDRO A. CARIAGA M."
    },
    357: {
        "value": 357,
        "label": " COLEGIO PATRICIO LYNCH"
    },
    358: {
        "value": 358,
        "label": " ESCUELA REPUBLICA ARABE SIRIA D-246"
    },
    359: {
        "value": 359,
        "label": " CENTRO EDUCATIVO REINO DE SUECIA"
    },
    360: {
        "value": 360,
        "label": " ESCUELA DIEGO PORTALES PALAZUELOS"
    },
    361: {
        "value": 361,
        "label": " COLEGIO ALBERTO HURTADO SEGUNDO"
    },
    362: {
        "value": 362,
        "label": " LICEO TECNOLOGICO ALFREDO NAZAR FERES"
    },
    363: {
        "value": 363,
        "label": " ESCUELA E-312 ESTADO DE ISRAEL"
    },
    364: {
        "value": 364,
        "label": " ESCUELA D-245 NACIONES UNvalueAS"
    },
    365: {
        "value": 365,
        "label": " INSTITUTO TECNICO PROFESIONAL MARITIMO DE VALPARAISO (2)"
    },
    366: {
        "value": 366,
        "label": " COLEGIO MARIA AUXILIADORA"
    },
    367: {
        "value": 367,
        "label": " INSTITUTO TECNICO PROFESIONAL MARITIMO DE VALPARAISO (1)"
    },
    368: {
        "value": 368,
        "label": " COLEGIO GUARDIAMARINA RIQUELME"
    },
    369: {
        "value": 369,
        "label": " LICEO TECNOLOGICO VILLA ALEMANA"
    },
    370: {
        "value": 370,
        "label": " ESCUELA LATINA INES GALLARDO ORELLANA"
    },
    371: {
        "value": 371,
        "label": " LICEO BICENTENARIO MARY GRAHAM (BASICA)"
    },
    372: {
        "value": 372,
        "label": " COLEGIO NUEVO MILENIO"
    },
    373: {
        "value": 373,
        "label": " COLEGIO MANUEL MONTT"
    },
    374: {
        "value": 374,
        "label": " SCUOLA ITALIANA GIROLAMO LONGHI VILLA ALEMANA"
    },
    375: {
        "value": 375,
        "label": " LICEO BICENTENARIO TECNICO PROFESIONAL MARY GRAHAM (MEDIA)"
    },
    376: {
        "value": 376,
        "label": " COLEGIO NACIONAL"
    },
    377: {
        "value": 377,
        "label": " COLEGIO HISPANO (SEDE MEDIA)"
    },
    378: {
        "value": 378,
        "label": " COLEGIO CHARLES DARWIN"
    },
    379: {
        "value": 379,
        "label": " COLEGIO ALEXANDER FLEMING"
    },
    380: {
        "value": 380,
        "label": " COLEGIO WINTERHILL VINA DEL MAR"
    },
    381: {
        "value": 381,
        "label": " ESCUELA BASICA D-316 REPUBLICA DEL ECUADOR"
    },
    382: {
        "value": 382,
        "label": " COLEGIO FRIENDLY HIGH SCHOOL"
    },
    383: {
        "value": 383,
        "label": " ESCUELA PRESvalueENTE JOSE MANUEL BALMACEDA"
    },
    384: {
        "value": 384,
        "label": " COLEGIO INMACULADA DE LOURDES"
    },
    385: {
        "value": 385,
        "label": " ESCUELA D-346 TEODORO LOWEY"
    },
    386: {
        "value": 386,
        "label": " LICEO BENJAMIN VICUNA MACKENNA"
    },
    387: {
        "value": 387,
        "label": " ESCUELA LORD COCHRANE"
    },
    388: {
        "value": 388,
        "label": " ESCUELA BASICA CHORRILLOS E-323"
    },
    389: {
        "value": 389,
        "label": " LICEO A-33 GUILLERMO RIVERA COTAPOS"
    },
    390: {
        "value": 390,
        "label": " ESCUELA LIBERTADOR BERNARDO OHIGGINS"
    },
    391: {
        "value": 391,
        "label": " ESCUELA ESPECIAL JUANITA DE AGUIRRE CERDA"
    },
    392: {
        "value": 392,
        "label": " SEMINARIO SAN RAFAEL"
    },
    393: {
        "value": 393,
        "label": " LICEO SANTA TERESA DE LOS ANDES"
    },
    394: {
        "value": 394,
        "label": " COLEGIO JUANITA FERNANDEZ"
    },
    395: {
        "value": 395,
        "label": " LICEO INDUSTRIAL DE VINA DEL MAR"
    },
    396: {
        "value": 396,
        "label": " PAN AMERICAN COLLEGE VINA DEL MAR"
    },
    397: {
        "value": 397,
        "label": " ESCUELA ARTURO PRAT CHACON "
    },
    398: {
        "value": 398,
        "label": " COLEGIO CASTELIANO"
    },
    399: {
        "value": 399,
        "label": " ESCUELA PATRICIO LYNCH ZALDIVAR"
    },
    400: {
        "value": 400,
        "label": " SCUOLA ITALIANA ARTURO DELL' ORO VINA DEL MAR"
    },
    401: {
        "value": 401,
        "label": " COLEGIO ANA MARIA JANER"
    },
    402: {
        "value": 402,
        "label": " COLEGIO MIRAFLORES D-329"
    },
    403: {
        "value": 403,
        "label": " ESCUELA BASICA SANTA JULIA D-334"
    },
    404: {
        "value": 404,
        "label": " COLEGIO SAN IGNACIO (SEDE LUSITANIA)"
    },
    405: {
        "value": 405,
        "label": " ESTADIO MUNICIPAL SAUSALITO"
    },
    406: {
        "value": 406,
        "label": " CORPORACION DOCENTE SAINT DOMINIC"
    },
    407: {
        "value": 407,
        "label": " ESCUELA BASICA HUMBERTO VILCHES ALZAMORA"
    },
    408: {
        "value": 408,
        "label": " LICEO POLITECNICO JOSE FRANCISCO VERGARA ETCHEVERS"
    },
    409: {
        "value": 409,
        "label": " ESCUELA BASICA D-318 UNESCO"
    },
    410: {
        "value": 410,
        "label": " FACULTAD DE FILOSOFIA Y EDUCACION PUCV CAMPUS SAUSALITO"
    },
    411: {
        "value": 411,
        "label": " COLEGIO RUBEN CASTRO"
    },
    412: {
        "value": 412,
        "label": " LICEO A-36 BICENTENARIO DE VINA DEL MAR"
    },
    413: {
        "value": 413,
        "label": " COLEGIO REPUBLICA DE COLOMBIA"
    },
    414: {
        "value": 414,
        "label": " GIMNASIO POLvalueEPORTIVO REGIONAL"
    },
    415: {
        "value": 415,
        "label": " COLEGIO MARIA AUXILIADORA"
    },
    416: {
        "value": 416,
        "label": " ESCUELA PRESvalueENTE PEDRO AGUIRRE CERDA"
    },
    417: {
        "value": 417,
        "label": " ESCUELA BASICA MUNICIPAL MERCEDES MATURANA GALLARDO"
    },
    418: {
        "value": 418,
        "label": " LICEO ZAPALLAR"
    },
    419: {
        "value": 419,
        "label": " LICEO FERMIN DEL REAL CASTILLO"
    },
    420: {
        "value": 420,
        "label": " COLEGIO BASICO DE CHEPICA"
    },
    421: {
        "value": 421,
        "label": " COMPLEJO EDUCACIONAL CHIMBARONGO"
    },
    422: {
        "value": 422,
        "label": " ESCUELA MUNICIPAL E-456"
    },
    423: {
        "value": 423,
        "label": " ESCUELA FERNANDO ARENAS ALMARZA"
    },
    424: {
        "value": 424,
        "label": " COLEGIO JESUS ANDINO"
    },
    425: {
        "value": 425,
        "label": " LICEO MUNICIPAL DE CODEGUA"
    },
    426: {
        "value": 426,
        "label": " COLEGIO MUNICIPAL HUALLILEN"
    },
    427: {
        "value": 427,
        "label": " LICEO MUNICIPAL LUIS GREGORIO VALENZUELA LAVIN"
    },
    428: {
        "value": 428,
        "label": " LICEO BERTA ZAMORANO"
    },
    429: {
        "value": 429,
        "label": " ESCUELA MUNICIPAL OSVALDO RUIZ GARCIA"
    },
    430: {
        "value": 430,
        "label": " ESCUELA LAURA MATUS MELENDEZ"
    },
    431: {
        "value": 431,
        "label": " LICEO CLAUDIO ARRAU"
    },
    432: {
        "value": 432,
        "label": " ESCUELA LO MIRANDA"
    },
    433: {
        "value": 433,
        "label": " COLEGIO REPUBLICA DE CHILE"
    },
    434: {
        "value": 434,
        "label": " LICEO MUNICIPAL MISAEL LOBOS MONROY"
    },
    435: {
        "value": 435,
        "label": " ESCUELA SIXTO MENDEZ PARADA"
    },
    436: {
        "value": 436,
        "label": " ESCUELA HERNAN OLGUIN MAYBE"
    },
    437: {
        "value": 437,
        "label": " ESCUELA PROFESORA MONICA SILVA GOMEZ INTERNADO"
    },
    438: {
        "value": 438,
        "label": " ESCUELA PROFESORA MONICA SILVA GOMEZ"
    },
    439: {
        "value": 439,
        "label": " ESCUELA CONTRAMAESTRE CONSTANTINO MICALVI"
    },
    440: {
        "value": 440,
        "label": " LICEO FRANCISCO ANTONIO ENCINA"
    },
    441: {
        "value": 441,
        "label": " ESCUELA ROMILIO ARELLANO TRONCOSO"
    },
    442: {
        "value": 442,
        "label": " LICEO EL ROSARIO DE LITUECHE ANEXO"
    },
    443: {
        "value": 443,
        "label": " LICEO EL ROSARIO DE LITUECHE"
    },
    444: {
        "value": 444,
        "label": " LICEO MUNICIPAL DE LOLOL"
    },
    445: {
        "value": 445,
        "label": " ESCUELA GALVARINO VALENZUELA MORAGA"
    },
    446: {
        "value": 446,
        "label": " COLEGIO BELLAVISTA"
    },
    447: {
        "value": 447,
        "label": " COLEGIO GABRIELA MISTRAL"
    },
    448: {
        "value": 448,
        "label": " COLEGIO LOS LLANOS"
    },
    449: {
        "value": 449,
        "label": " LICEO MACHALI"
    },
    450: {
        "value": 450,
        "label": " COLEGIO SANTA TERESA DE LOS ANDES"
    },
    451: {
        "value": 451,
        "label": " ESCUELA ALCvalueES REYES FRIAS"
    },
    452: {
        "value": 452,
        "label": " ESCUELA ALCvalueES REYES FRIAS ANEXO"
    },
    453: {
        "value": 453,
        "label": " ESCUELA MUNICIPAL DE PELEQUEN"
    },
    454: {
        "value": 454,
        "label": " LICEO MUNICIPAL INSTITUTO CARDENAL CARO"
    },
    455: {
        "value": 455,
        "label": " ESCUELA MUNICIPAL AMERICA"
    },
    456: {
        "value": 456,
        "label": " LICEO ELVIRA SANCHEZ DE GARCES"
    },
    457: {
        "value": 457,
        "label": " ESCUELA GABRIELA MISTRAL"
    },
    458: {
        "value": 458,
        "label": " LICEO ALBERTO HURTADO"
    },
    459: {
        "value": 459,
        "label": " COLEGIO BASICO CONSOLvalueADO"
    },
    460: {
        "value": 460,
        "label": " LICEO MUNICIPAL JUAN PABLO II"
    },
    461: {
        "value": 461,
        "label": " GIMNASIO MUNICIPAL DE NAVvalueAD"
    },
    462: {
        "value": 462,
        "label": " COLEGIO MUNICIPAL DIVINA GABRIELA"
    },
    463: {
        "value": 463,
        "label": " ESCUELA MUNICIPAL DE GULTRO"
    },
    464: {
        "value": 464,
        "label": " COLEGIO MARIA VILLALOBOS ARTEAGA"
    },
    465: {
        "value": 465,
        "label": " LICEO TECNICO MUNICIPAL"
    },
    466: {
        "value": 466,
        "label": " ESCUELA MUNICIPAL DE PALMILLA"
    },
    467: {
        "value": 467,
        "label": " ESCUELA MUNICIPAL DE PALMILLA GIMNASIO"
    },
    468: {
        "value": 468,
        "label": " ESCUELA BASICA UNION DE MUJERES AMERICANAS"
    },
    469: {
        "value": 469,
        "label": " LICEO MIREYA CATALAN URZUA"
    },
    470: {
        "value": 470,
        "label": " ESCUELA BASICA MERCEDES URZUA DIAZ"
    },
    471: {
        "value": 471,
        "label": " ESCUELA MUNICIPAL SAN PEDRO DE ALCANTARA"
    },
    472: {
        "value": 472,
        "label": " COLEGIO BASICO VIOLETA PARRA"
    },
    473: {
        "value": 473,
        "label": " LICEO VICTOR JARA"
    },
    474: {
        "value": 474,
        "label": " ESCUELA ANTONIO DE ZUNIGA ANEXO"
    },
    475: {
        "value": 475,
        "label": " ESCUELA ANTONIO DE ZUNIGA"
    },
    476: {
        "value": 476,
        "label": " LICEO LATINOAMERICANO"
    },
    477: {
        "value": 477,
        "label": " ESCUELA ENRIQUE SERRANO"
    },
    478: {
        "value": 478,
        "label": " LICEO AGUSTIN ROSS EDWARDS"
    },
    479: {
        "value": 479,
        "label": " ESCUELA DIGNA CAMILO AGUILAR"
    },
    480: {
        "value": 480,
        "label": " LICEO SAN FRANCISCO DE PLACILLA"
    },
    481: {
        "value": 481,
        "label": " ESCUELA MUNICIPAL DE PUMANQUE"
    },
    482: {
        "value": 482,
        "label": " LICEO REPUBLICA DE ITALIA"
    },
    483: {
        "value": 483,
        "label": " ESCUELA BASICA REPUBLICA DE ITALIA"
    },
    484: {
        "value": 484,
        "label": " LICEO DE NINAS MARIA LUISA BOMBAL"
    },
    485: {
        "value": 485,
        "label": " ESCUELA MUNICIPAL MARCELA PAZ"
    },
    486: {
        "value": 486,
        "label": " ESCUELA REPUBLICA ARGENTINA Y MOISES MUSSA"
    },
    487: {
        "value": 487,
        "label": " LICEO COMERCIAL JORGE ALESSANDRI RODRIGUEZ"
    },
    488: {
        "value": 488,
        "label": " COLEGIO MUNICIPAL EDUARDO DE GEYTER"
    },
    489: {
        "value": 489,
        "label": " ESCUELA MUNICIPAL EL COBRE"
    },
    490: {
        "value": 490,
        "label": " LICEO TECNICO DE RANCAGUA"
    },
    491: {
        "value": 491,
        "label": " COLEGIO JOSE ANTONIO MANSO DE VELASCO"
    },
    492: {
        "value": 492,
        "label": " LICEO OSCAR CASTRO ZUNIGA"
    },
    493: {
        "value": 493,
        "label": " LICEO COMERCIAL DIEGO PORTALES"
    },
    494: {
        "value": 494,
        "label": " LICEO MUNICIPAL JOSE VICTORINO LASTARRIA"
    },
    495: {
        "value": 495,
        "label": " LICEO INDUSTRIAL ERNESTO PINTO LAGARRIGUE"
    },
    496: {
        "value": 496,
        "label": " LICEO INDUSTRIAL PRESvalueENTE PEDRO AGUIRRE CERDA"
    },
    497: {
        "value": 497,
        "label": " COLEGIO LIBERTADOR SIMON BOLIVAR"
    },
    498: {
        "value": 498,
        "label": " CENTRO DE EDUCACION DE ADULTOS FRANCISCO TELLO"
    },
    499: {
        "value": 499,
        "label": " COLEGIO ESPANA"
    },
    500: {
        "value": 500,
        "label": " ESCUELA MUNICIPAL ISABEL RIQUELME"
    },
    501: {
        "value": 501,
        "label": " COLEGIO AURORA DE CHILE"
    },
    502: {
        "value": 502,
        "label": " ESCUELA BERNARDO OHIGGINS"
    },
    503: {
        "value": 503,
        "label": " COLEGIO MINERAL EL TENIENTE"
    },
    504: {
        "value": 504,
        "label": " COLEGIO MANUEL RODRIGUEZ"
    },
    505: {
        "value": 505,
        "label": " COLEGIO SANTA FILOMENA"
    },
    506: {
        "value": 506,
        "label": " ESCUELA MUNICIPAL PATRICIO MEKIS"
    },
    507: {
        "value": 507,
        "label": " ESCUELA COLONIA ESMERALDA"
    },
    508: {
        "value": 508,
        "label": " COLEGIO LA PAZ"
    },
    509: {
        "value": 509,
        "label": " COLEGIO LUIS GALDAMES"
    },
    510: {
        "value": 510,
        "label": " LICEO INDUSTRIAL DE RENGO"
    },
    511: {
        "value": 511,
        "label": " LICEO LUIS URBINA FLORES"
    },
    512: {
        "value": 512,
        "label": " LICEO POLITECNICO TOMAS MARIN DE POVEDA"
    },
    513: {
        "value": 513,
        "label": " LICEO ORIENTE"
    },
    514: {
        "value": 514,
        "label": " ESCUELA FERNANDA AEDO FAUNDEZ"
    },
    515: {
        "value": 515,
        "label": " COLEGIO MANUEL FRANCISCO CORREA"
    },
    516: {
        "value": 516,
        "label": " ESCUELA REPUBLICA DE FRANCIA"
    },
    517: {
        "value": 517,
        "label": " ESCUELA BERTA SAAVEDRA SEGURA"
    },
    518: {
        "value": 518,
        "label": " LICEO REQUINOA"
    },
    519: {
        "value": 519,
        "label": " ESCUELA CONCENTRAC. RURAL SERGIO VERDUGO HERRERA"
    },
    520: {
        "value": 520,
        "label": " INSTITUTO COMERCIAL ALBERTO VALENZUELA LLANOS"
    },
    521: {
        "value": 521,
        "label": " LICEO DE NINAS EDUARDO CHARME"
    },
    522: {
        "value": 522,
        "label": " ESCUELA OLEGARIO LAZO BAEZA"
    },
    523: {
        "value": 523,
        "label": " LICEO HERIBERTO SOTO SOTO"
    },
    524: {
        "value": 524,
        "label": " ESCUELA BASICA ISABEL LA CATOLICA"
    },
    525: {
        "value": 525,
        "label": " LICEO DE HOMBRES NEANDRO SCHILLING"
    },
    526: {
        "value": 526,
        "label": " ESCUELA JORGE MUNOZ SILVA"
    },
    527: {
        "value": 527,
        "label": " ESCUELA HOGAR MARIA LUISA BOUCHON"
    },
    528: {
        "value": 528,
        "label": " LICEO MUNICIPAL DE SANTA CRUZ"
    },
    529: {
        "value": 529,
        "label": " ESCUELA MARIA ARAYA VALDES"
    },
    530: {
        "value": 530,
        "label": " INSTITUTO POLITECNICO"
    },
    531: {
        "value": 531,
        "label": " ESCUELA MUNICIPAL DE SANTA CRUZ"
    },
    532: {
        "value": 532,
        "label": " ESCUELA CARMEN GALLEGOS DE ROBLES"
    },
    533: {
        "value": 533,
        "label": " INSTITUTO SAN VICENTE DE TAGUA TAGUA"
    },
    534: {
        "value": 534,
        "label": " ESCUELA ESPECIAL PAULA JARAQUEMADA"
    },
    535: {
        "value": 535,
        "label": " LICEO IGNACIO CARRERA PINTO"
    },
    536: {
        "value": 536,
        "label": " ESCUELA MUNICIPAL ZUNIGA"
    },
    537: {
        "value": 537,
        "label": " LICEO CLAUDINA URRUTIA DE LAVIN"
    },
    538: {
        "value": 538,
        "label": " ESCUELA INDEPENDENCIA"
    },
    539: {
        "value": 539,
        "label": " LICEO ANTONIO VARAS"
    },
    540: {
        "value": 540,
        "label": " ESCUELA PURISIMA CONCEPCION DE POCILLAS"
    },
    541: {
        "value": 541,
        "label": " ESCUELA OCTAVIO PALMA PEREZ"
    },
    542: {
        "value": 542,
        "label": " ESCUELA LOS HEROES"
    },
    543: {
        "value": 543,
        "label": " LICEO FEDERICO ALBERT FAUPP"
    },
    544: {
        "value": 544,
        "label": " ESCUELA BASICA DE COLBUN"
    },
    545: {
        "value": 545,
        "label": " LICEO CAPITAN IGNACIO CARRERA PINTO"
    },
    546: {
        "value": 546,
        "label": " ESCUELA ENRIQUE DONN MULLER"
    },
    547: {
        "value": 547,
        "label": " INSTITUTO POLITECNICO SUPERIOR EGvalueIO ROZ"
    },
    548: {
        "value": 548,
        "label": " COLEGIO SANTIAGO ONEDERRA"
    },
    549: {
        "value": 549,
        "label": " LICEO CONSTITUCION"
    },
    550: {
        "value": 550,
        "label": " COLEGIO SAN ALBERTO HURTADO"
    },
    551: {
        "value": 551,
        "label": " LICEO RURAL PUTU"
    },
    552: {
        "value": 552,
        "label": " LICEO RURAL ENRIQUE MAC IVER"
    },
    553: {
        "value": 553,
        "label": " LICEO LUIS EDMUNDO CORREA ROJAS"
    },
    554: {
        "value": 554,
        "label": " GIMNASIO MUNICIPAL"
    },
    555: {
        "value": 555,
        "label": " ESCUELA FRAY PEDRO ARMENGOL VALENZUELA"
    },
    556: {
        "value": 556,
        "label": " ESCUELA ELENA ARMIJO MORALES"
    },
    557: {
        "value": 557,
        "label": " ESCUELA PALESTINA"
    },
    558: {
        "value": 558,
        "label": " LICEO FERNANDO LAZCANO"
    },
    559: {
        "value": 559,
        "label": " LICEO LUIS CRUZ MARTINEZ DE CURICO"
    },
    560: {
        "value": 560,
        "label": " GIMNASIO MUNICIPAL"
    },
    561: {
        "value": 561,
        "label": " INSTITUTO POLITECNICO SUPERIOR JUAN TERRI"
    },
    562: {
        "value": 562,
        "label": " ESCUELA ERNESTO CASTRO ARELLANO"
    },
    563: {
        "value": 563,
        "label": " ESCUELA REPUBLICA DE BRASIL"
    },
    564: {
        "value": 564,
        "label": " ESCUELA ESPANA"
    },
    565: {
        "value": 565,
        "label": " ESCUELA CATALUNA"
    },
    566: {
        "value": 566,
        "label": " LICEO SAN IGNACIO"
    },
    567: {
        "value": 567,
        "label": " ESC. MONSENOR MANUEL LARRAIN ERRAZURIZ"
    },
    568: {
        "value": 568,
        "label": " LICEO HUALANE"
    },
    569: {
        "value": 569,
        "label": " ESCUELA LA HUERTA DE MATAQUITO"
    },
    570: {
        "value": 570,
        "label": " COLEGIO DR. MANUEL AVILES INOSTROZA"
    },
    571: {
        "value": 571,
        "label": " LICEO AUGUSTO SANTELICES VALENZUELA"
    },
    572: {
        "value": 572,
        "label": " INSTITUTO COMERCIAL"
    },
    573: {
        "value": 573,
        "label": " LICEO POLITECNICO DE LINARES"
    },
    574: {
        "value": 574,
        "label": " ESCUELA RAMON BELMAR SALDIAS"
    },
    575: {
        "value": 575,
        "label": " LICEO VALENTIN LETELIER"
    },
    576: {
        "value": 576,
        "label": " LICEO TECNICO PROFESIONAL DIEGO PORTALES"
    },
    577: {
        "value": 577,
        "label": " ESCUELA REPUBLICA DE FRANCIA"
    },
    578: {
        "value": 578,
        "label": " ESCUELA UNO ISABEL RIQUELME"
    },
    579: {
        "value": 579,
        "label": " ESCUELA BASICA ESPANA"
    },
    580: {
        "value": 580,
        "label": " ESCUELA JUAN DE LA CRUZ DOMINGUEZ G."
    },
    581: {
        "value": 581,
        "label": " LICEO ARTURO ALESSANDRI PALMA"
    },
    582: {
        "value": 582,
        "label": " ESCUELA COLIN"
    },
    583: {
        "value": 583,
        "label": " ESCUELA RURAL DUAO"
    },
    584: {
        "value": 584,
        "label": " GIMNASIO MUNICIPAL"
    },
    585: {
        "value": 585,
        "label": " ESCUELA MAULE"
    },
    586: {
        "value": 586,
        "label": " LICEO JUAN AGUSTIN MORALES GONZALEZ"
    },
    587: {
        "value": 587,
        "label": " LICEO POLIVALENTE DE MOLINA"
    },
    588: {
        "value": 588,
        "label": " GIMNASIO MUNICIPAL"
    },
    589: {
        "value": 589,
        "label": " ESCUELA REINO DE DINAMARCA"
    },
    590: {
        "value": 590,
        "label": " ESC.INTERN.LAS PALMAS TRES ESQUINAS"
    },
    591: {
        "value": 591,
        "label": " ESCUELA ARRAU MENDEZ"
    },
    592: {
        "value": 592,
        "label": " ESCUELA JOSE MATTA CALLEJON"
    },
    593: {
        "value": 593,
        "label": " COLEGIO ALBERTO MOLINA CASTILLO"
    },
    594: {
        "value": 594,
        "label": " LICEO FEDERICO HEISE MARTI"
    },
    595: {
        "value": 595,
        "label": " ESCUELA LUIS ARMANDO GOMEZ MUNOZ"
    },
    596: {
        "value": 596,
        "label": " COLEGIO PABLO NERUDA"
    },
    597: {
        "value": 597,
        "label": " CENTRO EDUCACIONAL INTEGRADO DE ADULTOS "
    },
    598: {
        "value": 598,
        "label": " LICEO DE PELARCO"
    },
    599: {
        "value": 599,
        "label": " LICEO PELLUHUE"
    },
    600: {
        "value": 600,
        "label": " ESCUELA CORINTO"
    },
    601: {
        "value": 601,
        "label": " COMPLEJO EDUCACIONAL PENCAHUE"
    },
    602: {
        "value": 602,
        "label": " ESCUELA PADRE LUIS OLIVA NAVARRETE"
    },
    603: {
        "value": 603,
        "label": " ESCUELA RIVERA DEL MAULE"
    },
    604: {
        "value": 604,
        "label": " ESCUELA RAUCO"
    },
    605: {
        "value": 605,
        "label": " GIMNASIO MUNICIPAL"
    },
    606: {
        "value": 606,
        "label": " ESCUELA MANUEL MONTT"
    },
    607: {
        "value": 607,
        "label": " GIMNASIO MUNICIPAL"
    },
    608: {
        "value": 608,
        "label": " LICEO GUILLERMO MARIN LARRAIN"
    },
    609: {
        "value": 609,
        "label": " LICEO AGROINDUSTRIAL RIO CLARO"
    },
    610: {
        "value": 610,
        "label": " LICEO ARTURO ALESSANDRI PALMA"
    },
    611: {
        "value": 611,
        "label": " ESC.DE EDUC.GRAL. BASICA MONSENOR ENRIQUE COR"
    },
    612: {
        "value": 612,
        "label": " ESCUELA VILLA PRAT"
    },
    613: {
        "value": 613,
        "label": " LICEO SAN CLEMENTE ENTRE RIOS"
    },
    614: {
        "value": 614,
        "label": " ESCUELA ARTURO PRAT CHACON"
    },
    615: {
        "value": 615,
        "label": " ESCUELA SAN CLEMENTE"
    },
    616: {
        "value": 616,
        "label": " ESC. PASO INTERNACIONAL PEHUENCHE"
    },
    617: {
        "value": 617,
        "label": " ESCUELA LOS MONTES"
    },
    618: {
        "value": 618,
        "label": " ESCUELA MARIANO LATORRE"
    },
    619: {
        "value": 619,
        "label": " ESCUELA JULIO MONTT SALAMANCA"
    },
    620: {
        "value": 620,
        "label": " ESCUELA JORGE GONZALEZ BASTIAS"
    },
    621: {
        "value": 621,
        "label": " ESCUELA JUSTA NARVAEZ"
    },
    622: {
        "value": 622,
        "label": " ESCUELA JOSE MANUEL BALMACEDA"
    },
    623: {
        "value": 623,
        "label": " ESCUELA MANUEL DE SALAS"
    },
    624: {
        "value": 624,
        "label": " LICEO MANUEL MONTT"
    },
    625: {
        "value": 625,
        "label": " ESCUELA SAN RAFAEL"
    },
    626: {
        "value": 626,
        "label": " ESCUELA PROSPERvalueAD"
    },
    627: {
        "value": 627,
        "label": " LICEO MARTA DONOSO ESPEJO"
    },
    628: {
        "value": 628,
        "label": " LICEO DIEGO PORTALES"
    },
    629: {
        "value": 629,
        "label": " ESCUELA CARLOS SPANO"
    },
    630: {
        "value": 630,
        "label": " ESCUELA LA FLORvalueA"
    },
    631: {
        "value": 631,
        "label": " ESCUELA BASICA"
    },
    632: {
        "value": 632,
        "label": " ESCUELA JUAN LUIS SANFUENTES"
    },
    633: {
        "value": 633,
        "label": " LICEO ABATE MOLINA"
    },
    634: {
        "value": 634,
        "label": " LICEO INDUSTRIAL SUPERIOR"
    },
    635: {
        "value": 635,
        "label": " ESCUELA UNO SAN AGUSTIN"
    },
    636: {
        "value": 636,
        "label": " LICEO TECNICO AMELIA COURBIS"
    },
    637: {
        "value": 637,
        "label": " LICEO HECTOR PEREZ BIOTT"
    },
    638: {
        "value": 638,
        "label": " ESCUELA COSTANERA"
    },
    639: {
        "value": 639,
        "label": " ESCUELA LAS ARAUCARIAS"
    },
    640: {
        "value": 640,
        "label": " ESCUELA ESPERANZA"
    },
    641: {
        "value": 641,
        "label": " LICEO DE CULTURA Y DIFUSION ARTISTICA"
    },
    642: {
        "value": 642,
        "label": " LICEO CARLOS CONDELL"
    },
    643: {
        "value": 643,
        "label": " ESCUELA JOSE ABELARDO NUNEZ"
    },
    644: {
        "value": 644,
        "label": " INST.SUP.DE COMERCIO ENRIQUE MALDONADO S"
    },
    645: {
        "value": 645,
        "label": " ESCUELA VILLA CULENAR"
    },
    646: {
        "value": 646,
        "label": " ESCUELA EL EDEN"
    },
    647: {
        "value": 647,
        "label": " ESCUELA BRILLA EL SOL"
    },
    648: {
        "value": 648,
        "label": " ESCUELA TENO"
    },
    649: {
        "value": 649,
        "label": " LICEO TENO"
    },
    650: {
        "value": 650,
        "label": " ESCUELA ENTRE AGUAS DE LLICO"
    },
    651: {
        "value": 651,
        "label": " LICEO NUEVO HORIZONTE"
    },
    652: {
        "value": 652,
        "label": " ESCUELA TIMOTEO ARAYA ALEGRIA"
    },
    653: {
        "value": 653,
        "label": " GIMNASIO MUNICIPAL"
    },
    654: {
        "value": 654,
        "label": " LICEO JUAN GOMEZ MILLAS"
    },
    655: {
        "value": 655,
        "label": " LICEO JUAN DE DIOS PUGA"
    },
    656: {
        "value": 656,
        "label": " ANEXO LICEO JUAN DE DIOS PUGA"
    },
    657: {
        "value": 657,
        "label": " ESCUELA DE CONCENTRACION FRONTERIZA RALCO ALTO BIOBIO"
    },
    658: {
        "value": 658,
        "label": " LICEO DOCTOR VICTOR RIOS RUIZ"
    },
    659: {
        "value": 659,
        "label": " ESCUELA BASICA MARE NOSTRUM"
    },
    660: {
        "value": 660,
        "label": " ESCUELA BASICA VICENTE MILLAN IRIARTE"
    },
    661: {
        "value": 661,
        "label": " LICEO SAN FELIPE DE ARAUCO"
    },
    662: {
        "value": 662,
        "label": " ESCUELA EDELMIRA VERGARA QUINONES"
    },
    663: {
        "value": 663,
        "label": " LICEO POLITECNICO CARAMPANGUE"
    },
    664: {
        "value": 664,
        "label": " ESCUELA BASICA SAN PEDRO DE LARAQUETE"
    },
    665: {
        "value": 665,
        "label": " LICEO TECNICO FILvalueOR GAETE MONSALVEZ"
    },
    666: {
        "value": 666,
        "label": " ESCUELA BASICA BRISAS DEL MAR"
    },
    667: {
        "value": 667,
        "label": " LICEO SANTA CRUZ DE LARQUI"
    },
    668: {
        "value": 668,
        "label": " LICEO MANUEL BULNES"
    },
    669: {
        "value": 669,
        "label": " ESCUELA EL PALPAL DE SANTA CLARA"
    },
    670: {
        "value": 670,
        "label": " ANEXO ENRIQUE ZANARTU PRIETO"
    },
    671: {
        "value": 671,
        "label": " ESCUELA ALTO CABRERO"
    },
    672: {
        "value": 672,
        "label": " ESCUELA BASICA ENRIQUE ZANARTU PRIETO"
    },
    673: {
        "value": 673,
        "label": " ESCUELA ORLANDO VERA VILLARROEL"
    },
    674: {
        "value": 674,
        "label": " ESCUELA ARTURO PRAT CHACON"
    },
    675: {
        "value": 675,
        "label": " ESCUELA LEONCIO ARANEDA FIGUEROA"
    },
    676: {
        "value": 676,
        "label": " LICEO HUMANISTA CIENTIFICO JOSE DE LA CRUZ MIRANDA CORREA"
    },
    677: {
        "value": 677,
        "label": " ESCUELA BASICA RENE ANDRADES TOLEDO"
    },
    678: {
        "value": 678,
        "label": " ESCUELA BASICA JOHN F. KENNEDY"
    },
    679: {
        "value": 679,
        "label": " LICEO CHIGUAYANTE"
    },
    680: {
        "value": 680,
        "label": " ESCUELA BASICA MANQUIMAVvalueA"
    },
    681: {
        "value": 681,
        "label": " ESCUELA JOSE HIPOLITO SALAS Y TORO"
    },
    682: {
        "value": 682,
        "label": " ESCUELA BELGICA"
    },
    683: {
        "value": 683,
        "label": " ESCUELA REPUBLICA FEDERAL ALEMANA"
    },
    684: {
        "value": 684,
        "label": " ESCUELA BASICA REPUBLICA DE GRECIA"
    },
    685: {
        "value": 685,
        "label": " ESCUELA BALMACEDA SAAVEDRA L."
    },
    686: {
        "value": 686,
        "label": " COLEGIO AURORA DE CHILE CHIGUAYANTE"
    },
    687: {
        "value": 687,
        "label": " ESCUELA BASICA LOS HEROES"
    },
    688: {
        "value": 688,
        "label": " COLEGIO SAN VICENTE DE PAUL"
    },
    689: {
        "value": 689,
        "label": " COLEGIO SAN BUENAVENTURA"
    },
    690: {
        "value": 690,
        "label": " COLEGIO DE LA PURISIMA CONCEPCION"
    },
    691: {
        "value": 691,
        "label": " ESCUELA REPUBLICA DE MEXICO"
    },
    692: {
        "value": 692,
        "label": " INSTITUTO SANTA MARIA"
    },
    693: {
        "value": 693,
        "label": " ESCUELA BASICA REYES DE ESPANA"
    },
    694: {
        "value": 694,
        "label": " ESCUELA ARTURO MERINO BENITEZ"
    },
    695: {
        "value": 695,
        "label": " INSTITUTO SUPERIOR DE COMERCIO PROFESOR FERNANDO PEREZ BECERRA"
    },
    696: {
        "value": 696,
        "label": " INSTITUTO INDUSTRIAL SUPERIOR DE CHILLAN"
    },
    697: {
        "value": 697,
        "label": " LICEO MARTIN RUIZ DE GAMBOA"
    },
    698: {
        "value": 698,
        "label": " COLEGIO POLIVALENTE PADRE ALBERTO HURTADO"
    },
    699: {
        "value": 699,
        "label": " ESCUELA BASICA JUAN MADRvalue AZOLA"
    },
    700: {
        "value": 700,
        "label": " ESCUELA BASICA ROSITA O'HIGGINS RIQUELME"
    },
    701: {
        "value": 701,
        "label": " LICEO COEDUCACIONAL NARCISO TONDREAU"
    },
    702: {
        "value": 702,
        "label": " ESCUELA BASICA JOSE MARIA CARO RODRIGUEZ"
    },
    703: {
        "value": 703,
        "label": " LICEO BICENTENARIO MARTA BRUNET CARAVES"
    },
    704: {
        "value": 704,
        "label": " COLEGIO CREACION CHILLAN"
    },
    705: {
        "value": 705,
        "label": " ESCUELA BASICA QUINCHAMALI"
    },
    706: {
        "value": 706,
        "label": " LICEO POLITECNICO JUAN PACHECO ALTAMIRANO"
    },
    707: {
        "value": 707,
        "label": " COLEGIO DARIO SALAS"
    },
    708: {
        "value": 708,
        "label": " ESCUELA TOMAS LAGO"
    },
    709: {
        "value": 709,
        "label": " ESCUELA BASICA RUCAPEQUEN"
    },
    710: {
        "value": 710,
        "label": " ESCUELA REINO DE SUECIA"
    },
    711: {
        "value": 711,
        "label": " LICEO POLIVALENTE DIEGO MISSENE BURGOS"
    },
    712: {
        "value": 712,
        "label": " ESCUELA MARIA TERESA MARCHANT CONTRERAS"
    },
    713: {
        "value": 713,
        "label": " ESCUELA VILLA JESUS DE COELEMU"
    },
    714: {
        "value": 714,
        "label": " ESCUELA GUARILIHUE ALTO"
    },
    715: {
        "value": 715,
        "label": " ESCUELA VEGAS DE ITATA"
    },
    716: {
        "value": 716,
        "label": " ESCUELA HEROES DE IQUIQUE"
    },
    717: {
        "value": 717,
        "label": " LICEO CLAUDIO ARRAU LEON"
    },
    718: {
        "value": 718,
        "label": " ESCUELA MARTA COLVIN ANDRADE"
    },
    719: {
        "value": 719,
        "label": " ESCUELA BRITANICA GUILLERMINA DRAKE WOOD"
    },
    720: {
        "value": 720,
        "label": " COLEGIO SANTA LUISA DE CONCEPCION"
    },
    721: {
        "value": 721,
        "label": " COLEGIO MARINA DE CHILE"
    },
    722: {
        "value": 722,
        "label": " ESCUELA ESTHER HUNNEUS DE CLARO"
    },
    723: {
        "value": 723,
        "label": " LICEO REBECA MATTE BELLO"
    },
    724: {
        "value": 724,
        "label": " COLEGIO CAMILO HENRIQUEZ"
    },
    725: {
        "value": 725,
        "label": " ESCUELA DIEGO PORTALES PALAZUELOS"
    },
    726: {
        "value": 726,
        "label": " COLEGIO SANTA EUFRASIA"
    },
    727: {
        "value": 727,
        "label": " LICEO DE ADULTOS JOSE MANUEL BALMACEDA FERNANDEZ"
    },
    728: {
        "value": 728,
        "label": " COLEGIO TECNICO PROFESIONAL LOS ACACIOS"
    },
    729: {
        "value": 729,
        "label": " LICEO DE NINAS"
    },
    730: {
        "value": 730,
        "label": " LICEO TECNICO FEMENINO A-29 DE CONCEPCION"
    },
    731: {
        "value": 731,
        "label": " LICEO JUAN GREGORIO LAS HERAS"
    },
    732: {
        "value": 732,
        "label": " ESCUELA BASICA RENE LOUVEL BERT"
    },
    733: {
        "value": 733,
        "label": " COLEGIO BICENTENARIO REPUBLICA DEL BRASIL"
    },
    734: {
        "value": 734,
        "label": " LICEO JUAN MARTINEZ DE ROZAS"
    },
    735: {
        "value": 735,
        "label": " CENTRO INTEGRADO DE EDUCACION ESPECIAL"
    },
    736: {
        "value": 736,
        "label": " LICEO EXPERIMENTAL LUCILA GODOY ALCAYAGA"
    },
    737: {
        "value": 737,
        "label": " LICEO ESPANA"
    },
    738: {
        "value": 738,
        "label": " LICEO ENRIQUE MOLINA GARMENDIA"
    },
    739: {
        "value": 739,
        "label": " LICEO COMERCIAL ENRIQUE OYARZUN MONDACA"
    },
    740: {
        "value": 740,
        "label": " LICEO ANDALIEN"
    },
    741: {
        "value": 741,
        "label": " COLEGIO SALESIANO "
    },
    742: {
        "value": 742,
        "label": " LICEO COMERCIAL FEMENINO DE CONCEPCION"
    },
    743: {
        "value": 743,
        "label": " COLEGIO BIOBIO"
    },
    744: {
        "value": 744,
        "label": " LICEO REPUBLICA DEL ECUADOR"
    },
    745: {
        "value": 745,
        "label": " LICEO DOMINGO SANTA MARIA"
    },
    746: {
        "value": 746,
        "label": " LICEO REPUBLICA DE ISRAEL"
    },
    747: {
        "value": 747,
        "label": " LICEO INDUSTRIAL DE CONCEPCION"
    },
    748: {
        "value": 748,
        "label": " COLEGIO SAN PEDRO NOLASCO"
    },
    749: {
        "value": 749,
        "label": " GIMNASIO MUNICIPAL"
    },
    750: {
        "value": 750,
        "label": " INSTITUTO DE HUMANvalueADES ALFREDO SILVA S"
    },
    751: {
        "value": 751,
        "label": " COLEGIO CREACION CONCEPCION"
    },
    752: {
        "value": 752,
        "label": " LICEO POLIVALENTE NAHUELBUTA"
    },
    753: {
        "value": 753,
        "label": " ESCUELA SAN LUIS DE CONTULMO"
    },
    754: {
        "value": 754,
        "label": " LICEO YOBILO DE CORONEL"
    },
    755: {
        "value": 755,
        "label": " ESCUELA RAFAEL SOTOMAYOR BAEZA"
    },
    756: {
        "value": 756,
        "label": " ESCUELA REMIGIO CASTRO ABURTO"
    },
    757: {
        "value": 757,
        "label": " ESCUELA BASICA RAMON FREIRE SERRANO"
    },
    758: {
        "value": 758,
        "label": " LICEO ANTONIO SALAMANCA MORALES"
    },
    759: {
        "value": 759,
        "label": " ESCUELA OCTAVIO SALINAS CARIAGA"
    },
    760: {
        "value": 760,
        "label": " ESCUELA ARTURO HUGHES CERNA"
    },
    761: {
        "value": 761,
        "label": " ESCUELA BASICA JAVIERA CARRERA"
    },
    762: {
        "value": 762,
        "label": " LICEO COMERCIAL ANDRES BELLO LOPEZ"
    },
    763: {
        "value": 763,
        "label": " ESCUELA ROSITA RENARD"
    },
    764: {
        "value": 764,
        "label": " ESCUELA FRANCISCO COLOANE"
    },
    765: {
        "value": 765,
        "label": " INSTITUTO DE HUMANvalueADES ENRIQUE CURTI CANOBBIO"
    },
    766: {
        "value": 766,
        "label": " ESCUELA PABLO NERUDA"
    },
    767: {
        "value": 767,
        "label": " LICEO POLIVALENTE MARIANO LATORRE"
    },
    768: {
        "value": 768,
        "label": " ESCUELA RAMIRO ROA GONZALEZ"
    },
    769: {
        "value": 769,
        "label": " ESCUELA COLICO SUR"
    },
    770: {
        "value": 770,
        "label": " ESCUELA VIRGEN DEL CARMEN"
    },
    771: {
        "value": 771,
        "label": " ESCUELA ELISE MOTTART"
    },
    772: {
        "value": 772,
        "label": " LICEO COPIULEMU"
    },
    773: {
        "value": 773,
        "label": " LICEO LUIS DE ALAVA"
    },
    774: {
        "value": 774,
        "label": " ESCUELA BASICA FLORvalueA"
    },
    775: {
        "value": 775,
        "label": " COLEGIO BASICO VILLA ACERO"
    },
    776: {
        "value": 776,
        "label": " LICEO SIMON BOLIVAR"
    },
    777: {
        "value": 777,
        "label": " ESCUELA BASICA CRISTOBAL COLON"
    },
    778: {
        "value": 778,
        "label": " LICEO INDUSTRIAL DE LA CONSTRUCCION HERNAN VALENZUELA"
    },
    779: {
        "value": 779,
        "label": " LICEO TECNICO PROFESIONAL PEDRO DEL RIO ZANARTU"
    },
    780: {
        "value": 780,
        "label": " COLEGIO DE LOS SAGRADOS CORAZONES"
    },
    781: {
        "value": 781,
        "label": " ESCUELA HELEN KELLER ADAMS"
    },
    782: {
        "value": 782,
        "label": " ESCUELA THOMAS JEFFERSON D 465"
    },
    783: {
        "value": 783,
        "label": " ESCUELA BLANCA ESTELA PRAT CARVAJAL"
    },
    784: {
        "value": 784,
        "label": " LICEO TECNICO PROFESIONAL LUCILA GODOY ALCAYAGA"
    },
    785: {
        "value": 785,
        "label": " ESCUELA REPUBLICA DEL PERU"
    },
    786: {
        "value": 786,
        "label": " COLEGIO ALONKURA"
    },
    787: {
        "value": 787,
        "label": " ESCUELA NUEVA REPUBLICA"
    },
    788: {
        "value": 788,
        "label": " ESCUELA MANUEL AMAT Y JUNIET"
    },
    789: {
        "value": 789,
        "label": " ESCUELA CARLOS ALBERTO FERNANDEZ CASTILLO"
    },
    790: {
        "value": 790,
        "label": " ESCUELA ANDRES ALCAZAR"
    },
    791: {
        "value": 791,
        "label": " LICEO POLITECNICO HEROES DE LA CONCEPCION"
    },
    792: {
        "value": 792,
        "label": " ESCUELA JOSE ABELARDO NUNEZ"
    },
    793: {
        "value": 793,
        "label": " ESCUELA ARMANDO ARANCIBIA OLIVOS"
    },
    794: {
        "value": 794,
        "label": " ESCUELA FRESIA GRACIELA MULLER RUIZ"
    },
    795: {
        "value": 795,
        "label": " LICEO ISvalueORA RAMOS DE GAJARDO"
    },
    796: {
        "value": 796,
        "label": " LICEO TECNICO PROFESIONAL DOCTOR RIGOBERTO IGLESIAS BASTIAS DE LEBU"
    },
    797: {
        "value": 797,
        "label": " ESCUELA PEHUEN"
    },
    798: {
        "value": 798,
        "label": " ESCUELA GUILLERMO RODRIGUEZ RIOBO"
    },
    799: {
        "value": 799,
        "label": " ESCUELA ORLANDO DELGADO ZUNIGA"
    },
    800: {
        "value": 800,
        "label": " LICEO POLITECNICO CAUPOLICAN"
    },
    801: {
        "value": 801,
        "label": " ESCUELA FELIX EYHERAMENDY"
    },
    802: {
        "value": 802,
        "label": " LICEO COEDUCACIONAL SANTA MARIA DE LOS ANGELES"
    },
    803: {
        "value": 803,
        "label": " ESCUELA HOGAR NIEVES VASQUEZ PALACIOS"
    },
    804: {
        "value": 804,
        "label": " ESCUELA ESPANA"
    },
    805: {
        "value": 805,
        "label": " ESCUELA GENERAL JOSE DE SAN MARTIN"
    },
    806: {
        "value": 806,
        "label": " ESCUELA COLONIA ARABE"
    },
    807: {
        "value": 807,
        "label": " LICEO TECNICO JUANITA FERNANDEZ SOLAR"
    },
    808: {
        "value": 808,
        "label": " ESCUELA BLANCO ENCALADA"
    },
    809: {
        "value": 809,
        "label": " ESCUELA ISLA DE LAJA"
    },
    810: {
        "value": 810,
        "label": " LICEO LOS ANGELES A-59"
    },
    811: {
        "value": 811,
        "label": " LICEO COMERCIAL DIEGO PORTALES PALAZUELO"
    },
    812: {
        "value": 812,
        "label": " ESCUELA JOSE MANSO DE VELASCO"
    },
    813: {
        "value": 813,
        "label": " LICEO INDUSTRIAL SAMUEL VIVANCO PARADA"
    },
    814: {
        "value": 814,
        "label": " ESCUELA BASICA REPUBLICA DE ISRAEL"
    },
    815: {
        "value": 815,
        "label": " LICEO LOS ANGELES A-59 LOCAL: 2"
    },
    816: {
        "value": 816,
        "label": " ESCUELA THOMAS JEFFERSON"
    },
    817: {
        "value": 817,
        "label": " ESCUELA BASICA 11 DE SEPTIEMBRE"
    },
    818: {
        "value": 818,
        "label": " LICEO COMERCIAL CAMILO HENRIQUEZ"
    },
    819: {
        "value": 819,
        "label": " COLEGIO INGLES WOODLAND"
    },
    820: {
        "value": 820,
        "label": " COLEGIO DON ORIONE"
    },
    821: {
        "value": 821,
        "label": " COLEGIO SAN GABRIEL ARCANGEL"
    },
    822: {
        "value": 822,
        "label": " ESCUELA BASICA ITILHUE"
    },
    823: {
        "value": 823,
        "label": " LICEO SANTA FE"
    },
    824: {
        "value": 824,
        "label": " ESCUELA BASICA SANTA MARIA DE GUADALUPE"
    },
    825: {
        "value": 825,
        "label": " LICEO COMERCIAL PRESvalueENTE FREI MONTALVA"
    },
    826: {
        "value": 826,
        "label": " COLEGIO ANGEL DE PEREDO"
    },
    827: {
        "value": 827,
        "label": " ESCUELA BASICA KONRAD ADENAUER"
    },
    828: {
        "value": 828,
        "label": " ESCUELA BASICA BELLO HORIZONTE"
    },
    829: {
        "value": 829,
        "label": " ESCUELA BASICA THOMPSON MATTHEWS"
    },
    830: {
        "value": 830,
        "label": " LICEO CARLOS COUSINO GOYENECHEA"
    },
    831: {
        "value": 831,
        "label": " ESCUELA BALDOMERO LILLO FIGUEROA"
    },
    832: {
        "value": 832,
        "label": " ESCUELA BASICA MULCHEN"
    },
    833: {
        "value": 833,
        "label": " LICEO NUEVO MUNDO"
    },
    834: {
        "value": 834,
        "label": " LICEO MIGUEL ANGEL CERDA LEIVA"
    },
    835: {
        "value": 835,
        "label": " ESCUELA VILLA LA GRANJA"
    },
    836: {
        "value": 836,
        "label": " LICEO MUNICIPAL DE NACIMIENTO"
    },
    837: {
        "value": 837,
        "label": " ESCUELA EL SABER"
    },
    838: {
        "value": 838,
        "label": " ESCUELA OSCAR GUERRERO QUINSAC"
    },
    839: {
        "value": 839,
        "label": " LICEO POLIVALENTE LA FRONTERA"
    },
    840: {
        "value": 840,
        "label": " ESCUELA GLORIAS NAVALES"
    },
    841: {
        "value": 841,
        "label": " LICEO POLIVALENTE SAN GREGORIO"
    },
    842: {
        "value": 842,
        "label": " ESCUELA BASICA GENERAL CRUZ G-382"
    },
    843: {
        "value": 843,
        "label": " ESCUELA DAFNE ELVIRA ZAPATA ROZAS"
    },
    844: {
        "value": 844,
        "label": " LICEO POLIVALENTE TOMAS ARNALDO HERRERA VEGA"
    },
    845: {
        "value": 845,
        "label": " ESCUELA ALMIRANTE PATRICIO LYNCH"
    },
    846: {
        "value": 846,
        "label": " ESCUELA ISLA DE PASCUA"
    },
    847: {
        "value": 847,
        "label": " ESCUELA PENCO"
    },
    848: {
        "value": 848,
        "label": " LICEO PENCOPOLITANO"
    },
    849: {
        "value": 849,
        "label": " ESCUELA REPUBLICA DE ITALIA"
    },
    850: {
        "value": 850,
        "label": " ESCUELA BASICA LOS CONQUISTADORES"
    },
    851: {
        "value": 851,
        "label": " LICEO POLITECNICO JOSE MANUEL PINTO ARIAS"
    },
    852: {
        "value": 852,
        "label": " COLEGIO FRANCISCO DE ASIS"
    },
    853: {
        "value": 853,
        "label": " ESCUELA JOSE TOHA SOLDEVILLA"
    },
    854: {
        "value": 854,
        "label": " LICEO NIBALDO SEPULVEDA FERNANDEZ"
    },
    855: {
        "value": 855,
        "label": " INSTITUTO VALLE DEL SOL QUILACO"
    },
    856: {
        "value": 856,
        "label": " LICEO ISABEL RIQUELME"
    },
    857: {
        "value": 857,
        "label": " LICEO FRANCISCO BASCUNAN GUERRERO"
    },
    858: {
        "value": 858,
        "label": " ESCUELA BASICA EL CASINO"
    },
    859: {
        "value": 859,
        "label": " ESCUELA BASICA HEROES DEL ITATA"
    },
    860: {
        "value": 860,
        "label": " LICEO LUIS CRUZ MARTINEZ"
    },
    861: {
        "value": 861,
        "label": " ESCUELA NUEVA AMERICA"
    },
    862: {
        "value": 862,
        "label": " LICEO POLITECNICO CARLOS MONTANE CASTRO"
    },
    863: {
        "value": 863,
        "label": " ESCUELA BASICA NIPAS"
    },
    864: {
        "value": 864,
        "label": " ESCUELA CACHAPOAL"
    },
    865: {
        "value": 865,
        "label": " LICEO POLITECNICO CAP. IGNACIO CARRERA P"
    },
    866: {
        "value": 866,
        "label": " COLEGIO SAGRADO CORAZON DE JESUS"
    },
    867: {
        "value": 867,
        "label": " ESCUELA GENERAL JOSE MIGUEL CARRERA VERDUGO"
    },
    868: {
        "value": 868,
        "label": " LICEO DIEGO PORTALES PALAZUELOS"
    },
    869: {
        "value": 869,
        "label": " ESCUELA GENERAL SOFANOR PARRA HERMOSILLA"
    },
    870: {
        "value": 870,
        "label": " ESCUELA JOAQUIN DEL PINO ROZAS Y NEGRETE"
    },
    871: {
        "value": 871,
        "label": " LICEO JORGE ALESSANDRI RODRIGUEZ"
    },
    872: {
        "value": 872,
        "label": " LICEO PUEBLO SECO"
    },
    873: {
        "value": 873,
        "label": " LICEO MANUEL JESUS ORTIZ"
    },
    874: {
        "value": 874,
        "label": " LICEO POLITECNICO MARIA WARD"
    },
    875: {
        "value": 875,
        "label": " LICEO TECNICO PUENTE NUBLE"
    },
    876: {
        "value": 876,
        "label": " ESCUELA SERGIO MARTIN ALAMOS"
    },
    877: {
        "value": 877,
        "label": " COLEGIO GALVARINO "
    },
    878: {
        "value": 878,
        "label": " COLEGIO FRATERNvalueAD"
    },
    879: {
        "value": 879,
        "label": " LICEO SAN PEDRO"
    },
    880: {
        "value": 880,
        "label": " ESCUELA ENRIQUE SORO BARRIGA"
    },
    881: {
        "value": 881,
        "label": " COLEGIO INSTITUTO SAN PEDRO"
    },
    882: {
        "value": 882,
        "label": " LICEO MAURICIO HOCHSCHILD"
    },
    883: {
        "value": 883,
        "label": " ESCUELA MIGUEL JOSE ZANARTU SANTA MARIA"
    },
    884: {
        "value": 884,
        "label": " COLEGIO SAN IGNACIO"
    },
    885: {
        "value": 885,
        "label": " ESCUELA SARGENTO CANDELARIA PEREZ"
    },
    886: {
        "value": 886,
        "label": " COLEGIO NUEVOS HORIZONTES"
    },
    887: {
        "value": 887,
        "label": " ESCUELA DARIO SALAS MARCHANT"
    },
    888: {
        "value": 888,
        "label": " COLEGIO CONCEPCION SAN PEDRO"
    },
    889: {
        "value": 889,
        "label": " LICEO ISvalueORA AGUIRRE TUPPER"
    },
    890: {
        "value": 890,
        "label": " ESCUELA BASICA CACIQUE LEVIAN"
    },
    891: {
        "value": 891,
        "label": " ESCUELA BASICA ENRIQUE BERNSTEIN CARABANTES"
    },
    892: {
        "value": 892,
        "label": " ESCUELA RECAREDO VIGUERAS ARANEDA"
    },
    893: {
        "value": 893,
        "label": " LICEO NUEVA ZELANDIA"
    },
    894: {
        "value": 894,
        "label": " COLEGIO BASICO NUEVA LOS LOBOS"
    },
    895: {
        "value": 895,
        "label": " ESCUELA BASICA CERRO SAN FRANCISCO"
    },
    896: {
        "value": 896,
        "label": " ESCUELA VILLA CENTINELA SUR"
    },
    897: {
        "value": 897,
        "label": " ESCUELA SANTA LEONOR"
    },
    898: {
        "value": 898,
        "label": " ESCUELA DIEGO PORTALES D N° 475"
    },
    899: {
        "value": 899,
        "label": " ESCUELA ARTURO PRAT CHACON"
    },
    900: {
        "value": 900,
        "label": " LICEO ALMIRANTE PEDRO ESPINA RITCHIE"
    },
    901: {
        "value": 901,
        "label": " ESCUELA MEXICO ESTADO DE GUERRERO"
    },
    902: {
        "value": 902,
        "label": " ESCUELA BASICA LIBERTAD"
    },
    903: {
        "value": 903,
        "label": " ESCUELA BASICA LA DAMA BLANCA"
    },
    904: {
        "value": 904,
        "label": " LICEO ANITA SERRANO SEPULVEDA"
    },
    905: {
        "value": 905,
        "label": " LICEO LA ASUNCION"
    },
    906: {
        "value": 906,
        "label": " COLEGIO LOS CONDORES"
    },
    907: {
        "value": 907,
        "label": " COLEGIO REMODELACION SIMONS"
    },
    908: {
        "value": 908,
        "label": " LICEO TECNICO"
    },
    909: {
        "value": 909,
        "label": " LICEO INDUSTRIAL JUAN ANTONIO RIOS"
    },
    910: {
        "value": 910,
        "label": " LICEO COMERCIAL PROFESOR SERGIO MORAGA ARCIL"
    },
    911: {
        "value": 911,
        "label": " LICEO POLIVALENTE LAS SALINAS"
    },
    912: {
        "value": 912,
        "label": " ESCUELA BASICA LAS HIGUERAS"
    },
    913: {
        "value": 913,
        "label": " COLEGIO HUACHIPATO"
    },
    914: {
        "value": 914,
        "label": " COLEGIO BASICO SAN VICENTE"
    },
    915: {
        "value": 915,
        "label": " ESCUELA VILLA INDEPENDENCIA"
    },
    916: {
        "value": 916,
        "label": " ESCUELA BASICA CRUZ DEL SUR"
    },
    917: {
        "value": 917,
        "label": " ESCUELA ADULTOS LAS AMERICAS"
    },
    918: {
        "value": 918,
        "label": " LICEO TRAPAQUEANTE"
    },
    919: {
        "value": 919,
        "label": " ESCUELA ELOISA GONZALEZ"
    },
    920: {
        "value": 920,
        "label": " ESCUELA BASICA DICHATO"
    },
    921: {
        "value": 921,
        "label": " ESCUELA BASICA RAFAEL"
    },
    922: {
        "value": 922,
        "label": " LICEO VICENTE ALBERTO PALACIOS VALDES"
    },
    923: {
        "value": 923,
        "label": " LICEO COMERCIAL DE TOME"
    },
    924: {
        "value": 924,
        "label": " ESCUELA PARTICULAR MARGARITA NASEAU"
    },
    925: {
        "value": 925,
        "label": " ESCUELA BASICA BELLAVISTA"
    },
    926: {
        "value": 926,
        "label": " LICEO POLIVALENTE TOME - ALTO"
    },
    927: {
        "value": 927,
        "label": " ESCUELA BASICA REPUBLICA DE ECUADOR"
    },
    928: {
        "value": 928,
        "label": " ESCUELA BASICA VALLE LONQUEN"
    },
    929: {
        "value": 929,
        "label": " ESCUELA LOS AVELLANOS"
    },
    930: {
        "value": 930,
        "label": " ESCUELA ALEJANDRO PEREZ URBANO"
    },
    931: {
        "value": 931,
        "label": " ESCUELA BASICA TUCAPEL"
    },
    932: {
        "value": 932,
        "label": " ESCUELA LUIS MARTINEZ GONZALEZ"
    },
    933: {
        "value": 933,
        "label": " LICEO DE HUEPIL"
    },
    934: {
        "value": 934,
        "label": " ESCUELA MUNICIPAL RERE"
    },
    935: {
        "value": 935,
        "label": " LICEO MUNICIPAL RIO CLARO"
    },
    936: {
        "value": 936,
        "label": " ESCUELA PADRE PEDRO CAMPOS MENCHACA"
    },
    937: {
        "value": 937,
        "label": " LICEO PADRE LUIS ALBERTO SALDES IRARRAZABAL"
    },
    938: {
        "value": 938,
        "label": " ESCUELA HEROES DE CHILE"
    },
    939: {
        "value": 939,
        "label": " LICEO TECNICO PROFESIONAL GONZALO GUGLIELMI MONTIEL"
    },
    940: {
        "value": 940,
        "label": " LICEO CAMPANARIO"
    },
    941: {
        "value": 941,
        "label": " LICEO DE YUNGAY"
    },
    942: {
        "value": 942,
        "label": " ESCUELA FERNANDO BAQUEDANO"
    },
    943: {
        "value": 943,
        "label": " COLEGIO BASICO ARAGON"
    },
    944: {
        "value": 944,
        "label": " ESCUELA BASICA NAHUELBUTA"
    },
    945: {
        "value": 945,
        "label": " ESCUELA JOSE ELIAS BOLIVAR HERRERA"
    },
    946: {
        "value": 946,
        "label": " LICEO ENRIQUE BALLACEY COTTEREAU"
    },
    947: {
        "value": 947,
        "label": " LICEO TECNICO JUANITA FERNANDEZ SOLAR"
    },
    948: {
        "value": 948,
        "label": " ESCUELA HERMANOS CARRERA"
    },
    949: {
        "value": 949,
        "label": " EX. LICEO MERCEDES MANOSALVA AREVALO"
    },
    950: {
        "value": 950,
        "label": " ESCUELA VILLA HUEQUEN"
    },
    951: {
        "value": 951,
        "label": " ESCUELA DARIO SALAS DIAZ"
    },
    952: {
        "value": 952,
        "label": " COMPLEJO EDUCACIONAL CLAUDIO ARRAU LEON"
    },
    953: {
        "value": 953,
        "label": " ESCUELA BASICA KIM RUKA"
    },
    954: {
        "value": 954,
        "label": " ESCUELA BASICA NEHUENTUE"
    },
    955: {
        "value": 955,
        "label": " LICEO MUNICIPAL TROVOLHUE"
    },
    956: {
        "value": 956,
        "label": " COLEGIO SAN FRANCISCO DE ASIS"
    },
    957: {
        "value": 957,
        "label": " LICEO JAMES MUNDELL"
    },
    958: {
        "value": 958,
        "label": " ESCUELA BENJAMIN FRANKLIN"
    },
    959: {
        "value": 959,
        "label": " ESCUELA BASICA VICTOR DURAN PEREZ"
    },
    960: {
        "value": 960,
        "label": " ESCUELA THOMAS ALVA EDISON"
    },
    961: {
        "value": 961,
        "label": " COMPLEJO EDUCACIONAL COLLIPULLI"
    },
    962: {
        "value": 962,
        "label": " ESCUELA F-90 RAUL CASTRO MARQUEZ"
    },
    963: {
        "value": 963,
        "label": " ESCUELA LEOVIGILDO KLEY"
    },
    964: {
        "value": 964,
        "label": " LICEO MUNICIPAL ATENEA"
    },
    965: {
        "value": 965,
        "label": " COMPLEJO EDUCACIONAL JUAN BOSCO"
    },
    966: {
        "value": 966,
        "label": " LICEO ARTURO VALENZUELA"
    },
    967: {
        "value": 967,
        "label": " LICEO LAS ARAUCARIAS BASICA"
    },
    968: {
        "value": 968,
        "label": " LICEO LAS ARAUCARIAS MEDIA"
    },
    969: {
        "value": 969,
        "label": " ESCUELA PATRICIO CHAVEZ SOTO"
    },
    970: {
        "value": 970,
        "label": " ESCUELA RAMON EDUARDO RAMIREZ HENRIQUEZ"
    },
    971: {
        "value": 971,
        "label": " COMPLEJO EDUCACIONAL MONSENOR FCO. VALDES S"
    },
    972: {
        "value": 972,
        "label": " ESC. SAN FRANCISCO DE ASIS DE ERCILLA"
    },
    973: {
        "value": 973,
        "label": " ESCUELA BASICA SALVADOR ALLENDE"
    },
    974: {
        "value": 974,
        "label": " ESCUELA JUAN SEGUEL"
    },
    975: {
        "value": 975,
        "label": " COLEGIO SANTA CRUZ DE FREIRE"
    },
    976: {
        "value": 976,
        "label": " LICEO JUAN SCHLEYER"
    },
    977: {
        "value": 977,
        "label": " GIMNASIO MUNICIPAL"
    },
    978: {
        "value": 978,
        "label": " ESCUELA RAIMAPU"
    },
    979: {
        "value": 979,
        "label": " ESCUELA LA ESPERANZA"
    },
    980: {
        "value": 980,
        "label": " ESCUELA MUNICIPAL GABRIELA MISTRAL"
    },
    981: {
        "value": 981,
        "label": " ESCUELA MUNICIPAL RIO QUILLEM"
    },
    982: {
        "value": 982,
        "label": " ESCUELA BASICA CINCO"
    },
    983: {
        "value": 983,
        "label": " COMPLEJO EDUCACIONAL ANDRES A. GORBEA"
    },
    984: {
        "value": 984,
        "label": " LICEO JOSE VICTORINO LASTARRIA"
    },
    985: {
        "value": 985,
        "label": " ESCUELA BASICA LICARAYEN"
    },
    986: {
        "value": 986,
        "label": " ESCUELA BASICA N°1 LOS CACHORROS"
    },
    987: {
        "value": 987,
        "label": " ESCUELA LOS CARRERA"
    },
    988: {
        "value": 988,
        "label": " ESC. BASICA AMELIA GODOY"
    },
    989: {
        "value": 989,
        "label": " LICEO H-C JORGE TEILLIER SANDOVAL"
    },
    990: {
        "value": 990,
        "label": " ESC. BASICA NUMERO SEIS"
    },
    991: {
        "value": 991,
        "label": " LICEO LOS CASTANOS"
    },
    992: {
        "value": 992,
        "label": " COMPLEJO EDUCACIONAL JUAN XXIII"
    },
    993: {
        "value": 993,
        "label": " LICEO BICENTENARIO PADRE ALBERTO HURTADO CRUCHAGA"
    },
    994: {
        "value": 994,
        "label": " ESCUELA ALBORADA"
    },
    995: {
        "value": 995,
        "label": " ESCUELA ALBORADA LOCAL: 2"
    },
    996: {
        "value": 996,
        "label": " ESCUELA BASICA LAFQUEN ICALMA"
    },
    997: {
        "value": 997,
        "label": " ESCUELA DE LIUCURA"
    },
    998: {
        "value": 998,
        "label": " LICEO BRIGADIER CARLOS SCHALCHLI V."
    },
    999: {
        "value": 999,
        "label": " ESCUELA PARTICULAR N° 2 LONQUIMAY"
    },
    1000: {
        "value": 1000,
        "label": " ESCUELA GUSTAVO VASQUEZ DIAZ"
    },
    1001: {
        "value": 1001,
        "label": " ESCUELA BASICA REPUBLICA DE ITALIA"
    },
    1002: {
        "value": 1002,
        "label": " ESCUELA F-174 VALLE DE LUMACO"
    },
    1003: {
        "value": 1003,
        "label": " ESC. MUNICIPAL PICHIPELLAHUEN"
    },
    1004: {
        "value": 1004,
        "label": " LICEO LOS ANDES"
    },
    1005: {
        "value": 1005,
        "label": " ESCUELA BASICA VOLCAN LLAIMA"
    },
    1006: {
        "value": 1006,
        "label": " ESCUELA ALEJANDRO GOROSTIAGA"
    },
    1007: {
        "value": 1007,
        "label": " ESCUELA ESPECIAL AVANCEMOS JUNTOS"
    },
    1008: {
        "value": 1008,
        "label": " LICEO LUIS GONZALEZ VASQUEZ"
    },
    1009: {
        "value": 1009,
        "label": " HOGAR MASCULINO LUIS GONZALEZ"
    },
    1010: {
        "value": 1010,
        "label": " HOGAR FEMENINO LUIS GONZALEZ"
    },
    1011: {
        "value": 1011,
        "label": " ESCUELA MONSENOR GUvalueO DE RAMBERGA"
    },
    1012: {
        "value": 1012,
        "label": " COMPLEJO EDUCACIONAL PADRE OSCAR MOSER"
    },
    1013: {
        "value": 1013,
        "label": " ESCUELA PARTICULAR NTRA.SRA.DEL CARMEN"
    },
    1014: {
        "value": 1014,
        "label": " ESCUELA PARTICULAR SAN RAFAEL ARCANGEL"
    },
    1015: {
        "value": 1015,
        "label": " ESCUELA PARTICULAR SAN BERNARDO"
    },
    1016: {
        "value": 1016,
        "label": " ESCUELA BASICA SAN SEBASTIAN DE MAQUEHUE"
    },
    1017: {
        "value": 1017,
        "label": " COMPLEJO EDUCACIONAL PADRE LAS CASAS"
    },
    1018: {
        "value": 1018,
        "label": " ESCUELA DARIO SALAS"
    },
    1019: {
        "value": 1019,
        "label": " ESCUELA PARTICULAR HABIT-ART"
    },
    1020: {
        "value": 1020,
        "label": " ESCUELA EDUARDO FREI MONTALVA"
    },
    1021: {
        "value": 1021,
        "label": " LICEO ISABEL POBLETE V."
    },
    1022: {
        "value": 1022,
        "label": " LICEO MUNICIPAL LA FRONTERA"
    },
    1023: {
        "value": 1023,
        "label": " ESCUELA UNION LATINOAMERICANA"
    },
    1024: {
        "value": 1024,
        "label": " ESCUELA LAS AMERICAS"
    },
    1025: {
        "value": 1025,
        "label": " LICEO DE CIENCIAS Y HUMANvalueADES"
    },
    1026: {
        "value": 1026,
        "label": " ESCUELA CARLOS HOLZAPFEL"
    },
    1027: {
        "value": 1027,
        "label": " COMPLEJO EDUCACIONAL PABLO SEXTO"
    },
    1028: {
        "value": 1028,
        "label": " LICEO DE HOTELERIA Y TURISMO"
    },
    1029: {
        "value": 1029,
        "label": " ESCUELA PARTICULAR 167 RAMON GUINEZ"
    },
    1030: {
        "value": 1030,
        "label": " LICEO MARIA AURORA GUINEZ RAMIREZ"
    },
    1031: {
        "value": 1031,
        "label": " ESCUELA PEDRO DE ONA"
    },
    1032: {
        "value": 1032,
        "label": " ESCUELA LA NOBEL GABRIELA"
    },
    1033: {
        "value": 1033,
        "label": " LICEO POLITECNICO DOMINGO STA MARIA"
    },
    1034: {
        "value": 1034,
        "label": " ESCUELA BASICA LOS NOGALES"
    },
    1035: {
        "value": 1035,
        "label": " ESCUELA PARTICULAR PADRE JUAN WEVERING"
    },
    1036: {
        "value": 1036,
        "label": " ESCUELA PARTICULAR SAN SEBASTIAN"
    },
    1037: {
        "value": 1037,
        "label": " LICEO REINO DE SUECIA"
    },
    1038: {
        "value": 1038,
        "label": " ESCUELA MILLARAY"
    },
    1039: {
        "value": 1039,
        "label": " ESCUELA LLAIMA"
    },
    1040: {
        "value": 1040,
        "label": " LICEO BICENTENARIO DE TEMUCO"
    },
    1041: {
        "value": 1041,
        "label": " COLEGIO SCOLE CREARE"
    },
    1042: {
        "value": 1042,
        "label": " COLEGIO SAN FRANCISCO"
    },
    1043: {
        "value": 1043,
        "label": " ESCUELA LOS TRIGALES"
    },
    1044: {
        "value": 1044,
        "label": " COLEGIO CENTENARIO"
    },
    1045: {
        "value": 1045,
        "label": " LICEO COMERCIAL TIBURCIO SAAVEDRA"
    },
    1046: {
        "value": 1046,
        "label": " LICEO PABLO NERUDA"
    },
    1047: {
        "value": 1047,
        "label": " COLEGIO MUNDO MAGICO"
    },
    1048: {
        "value": 1048,
        "label": " INTERNADO LICEO PABLO NERUDA"
    },
    1049: {
        "value": 1049,
        "label": " LICEO TECNICO DE TEMUCO"
    },
    1050: {
        "value": 1050,
        "label": " COLEGIO ADVENTISTA"
    },
    1051: {
        "value": 1051,
        "label": " ESCUELA PARTICULAR SIGLO XXI"
    },
    1052: {
        "value": 1052,
        "label": " ESCUELA CAMPOS DEPORTIVOS"
    },
    1053: {
        "value": 1053,
        "label": " COLEGIO SAINT PATRICK SCHOOL"
    },
    1054: {
        "value": 1054,
        "label": " LICEO CAMILO HENRIQUEZ DE TEMUCO"
    },
    1055: {
        "value": 1055,
        "label": " ESCUELA LABRANZA"
    },
    1056: {
        "value": 1056,
        "label": " ESCUELA PEDRO DE VALDIVIA"
    },
    1057: {
        "value": 1057,
        "label": " ESCUELA VILLA ALEGRE"
    },
    1058: {
        "value": 1058,
        "label": " ESCUELA VILLA CAROLINA"
    },
    1059: {
        "value": 1059,
        "label": " LICEO G. MISTRAL Y EX LOCAL MARCELA PAZ "
    },
    1060: {
        "value": 1060,
        "label": " COLEGIO MONTESSORI"
    },
    1061: {
        "value": 1061,
        "label": " ESCUELA ANDRES BELLO"
    },
    1062: {
        "value": 1062,
        "label": " INSTITUTO CLARET"
    },
    1063: {
        "value": 1063,
        "label": " ESCUELA PARTICULAR FRANCIA"
    },
    1064: {
        "value": 1064,
        "label": " LICEO POLITECNICO PUEBLO NUEVO"
    },
    1065: {
        "value": 1065,
        "label": " ESCUELA STANDARD"
    },
    1066: {
        "value": 1066,
        "label": " COLEGIO METODISTA"
    },
    1067: {
        "value": 1067,
        "label": " ESCUELA ALONSO DE ERCILLA"
    },
    1068: {
        "value": 1068,
        "label": " COLEGIO PROVvalueENCIA DE TEMUCO"
    },
    1069: {
        "value": 1069,
        "label": " ESCUELA ARTURO PRAT"
    },
    1070: {
        "value": 1070,
        "label": " COLEGIO SANTA CRUZ"
    },
    1071: {
        "value": 1071,
        "label": " LICEO MUNICIPAL BARROS ARANA"
    },
    1072: {
        "value": 1072,
        "label": " ESCUELA HORIZONTES"
    },
    1073: {
        "value": 1073,
        "label": " COMPLEJO EDUCACIONAL NUEVA ALBORADA"
    },
    1074: {
        "value": 1074,
        "label": " ESCUELA PARTICULAR PADRE ALEJANDRO ORTEGA"
    },
    1075: {
        "value": 1075,
        "label": " ESCUELA PARTICULAR SUBVENCIONADA PADRE ISvalueORO"
    },
    1076: {
        "value": 1076,
        "label": " ESCUELA AGUAS Y GAVIOTAS"
    },
    1077: {
        "value": 1077,
        "label": " COMPLEJO EDUCACIONAL MARTIN KLEINKNECHT"
    },
    1078: {
        "value": 1078,
        "label": " ESCUELA REPUBLICA DE ISRAEL"
    },
    1079: {
        "value": 1079,
        "label": " COLEGIO LOUIS PASTEUR"
    },
    1080: {
        "value": 1080,
        "label": " LICEO LUCILA GODOY ALCAYAGA"
    },
    1081: {
        "value": 1081,
        "label": " COMPLEJO EDUCACIONAL LUIS DURAND DURAND"
    },
    1082: {
        "value": 1082,
        "label": " ESCUELA SELVA OSCURA"
    },
    1083: {
        "value": 1083,
        "label": " LICEO JORGE ALESSANDRI RODRIGUEZ"
    },
    1084: {
        "value": 1084,
        "label": " COLEGIO IGNACIO CARRERA PINTO"
    },
    1085: {
        "value": 1085,
        "label": " ESCUELA HEROES DE IQUIQUE"
    },
    1086: {
        "value": 1086,
        "label": " LICEO POLITECNICO MANUEL MONTT"
    },
    1087: {
        "value": 1087,
        "label": " COMPLEJO EDUCACIONAL LA GRANJA"
    },
    1088: {
        "value": 1088,
        "label": " ESCUELA JAPON"
    },
    1089: {
        "value": 1089,
        "label": " ESCUELA PARTICULAR N°8 VILCUN"
    },
    1090: {
        "value": 1090,
        "label": " COLEGIO AMERICA"
    },
    1091: {
        "value": 1091,
        "label": " COMPLEJO EDUCACIONAL PADRE NICOLAS"
    },
    1092: {
        "value": 1092,
        "label": " ESCUELA EPU KLEI"
    },
    1093: {
        "value": 1093,
        "label": " ESCUELA VOIPIR DE NANCUL"
    },
    1094: {
        "value": 1094,
        "label": " ESCUELA PARTICULAR SAGRADA FAMILIA"
    },
    1095: {
        "value": 1095,
        "label": " ESCUELA ALEXANDER GRAHAM BELL"
    },
    1096: {
        "value": 1096,
        "label": " LICEO BICENTENARIO ARAUCANIA"
    },
    1097: {
        "value": 1097,
        "label": " ESCUELA JOSE ABELARDO NUNEZ"
    },
    1098: {
        "value": 1098,
        "label": " ESCUELA VALENTIN LETELIER"
    },
    1099: {
        "value": 1099,
        "label": " ESCUELA MARIANO LATORRE"
    },
    1100: {
        "value": 1100,
        "label": " ESCUELA INDUSTRIAL SAN JOSE"
    },
    1101: {
        "value": 1101,
        "label": " LICEO BICENTENARIO DE ANCUD"
    },
    1102: {
        "value": 1102,
        "label": " GIMNASIO EXTERIOR LICEO DOMINGO ESPINEIRA RIESCO"
    },
    1103: {
        "value": 1103,
        "label": " LICEO DOMINGO ESPINEIRA RIESCO Y GIMNASIO"
    },
    1104: {
        "value": 1104,
        "label": " LICEO AGRICOLA"
    },
    1105: {
        "value": 1105,
        "label": " ESCUELA RURAL VILLA CHACAO"
    },
    1106: {
        "value": 1106,
        "label": " LICEO POLITECNICO"
    },
    1107: {
        "value": 1107,
        "label": " ESCUELA EULOGIO GOYCOLEA GARAY Y GIMNASIO"
    },
    1108: {
        "value": 1108,
        "label": " ESCUELA JOSE MANUEL BALMACEDA"
    },
    1109: {
        "value": 1109,
        "label": " COLEGIO SAN MIGUEL"
    },
    1110: {
        "value": 1110,
        "label": " ESCUELA RURAL CARMEN MIRANDA NAVARRO"
    },
    1111: {
        "value": 1111,
        "label": " ESCUELA UNIFICADA ISLA GUAR"
    },
    1112: {
        "value": 1112,
        "label": " ESCUELA RURAL CHAUQUEAR"
    },
    1113: {
        "value": 1113,
        "label": " ESCUELA RURAL PARGUA"
    },
    1114: {
        "value": 1114,
        "label": " ESCUELA LUIS URIBE DIAZ Y GIMNASIO"
    },
    1115: {
        "value": 1115,
        "label": " LICEO GALVARINO RIVEROS CARDENAS"
    },
    1116: {
        "value": 1116,
        "label": " LICEO POLITECNICO DE CASTRO"
    },
    1117: {
        "value": 1117,
        "label": " LICEO FRANCISCO COLOANE"
    },
    1118: {
        "value": 1118,
        "label": " ESCUELA INES MUNOZ DE GARCIA"
    },
    1119: {
        "value": 1119,
        "label": " ESCUELA RURAL LOS ANGELES"
    },
    1120: {
        "value": 1120,
        "label": " ESCUELA RURAL MIRTA OYARZO VERA"
    },
    1121: {
        "value": 1121,
        "label": " ESCUELA RURAL AYACARA"
    },
    1122: {
        "value": 1122,
        "label": " GIMNASIO ESCUELA BASICA ALMIRANTE JUAN JOSE LATORRE"
    },
    1123: {
        "value": 1123,
        "label": " ESCUELA BASICA ALMIRANTE JUAN JOSE LATORRE"
    },
    1124: {
        "value": 1124,
        "label": " ESCUELA RURAL VALLE EL FRIO"
    },
    1125: {
        "value": 1125,
        "label": " LICEO MANUEL JESUS ANDRADE BORQUEZ"
    },
    1126: {
        "value": 1126,
        "label": " ESCUELA BASICA SAN CARLOS"
    },
    1127: {
        "value": 1127,
        "label": " ESCUELA BASICA FRONTERIZA JUAN SOLER MANFREDINI"
    },
    1128: {
        "value": 1128,
        "label": " ESCUELA RURAL CAPITÀN DE BANDADA CARLOS RODRIGUEZ"
    },
    1129: {
        "value": 1129,
        "label": " ESCUELA RURAL RIO PUELO"
    },
    1130: {
        "value": 1130,
        "label": " LICEO ALFREDO BARRIA OYARZUN"
    },
    1131: {
        "value": 1131,
        "label": " ESCUELA BASICA DALCAHUE"
    },
    1132: {
        "value": 1132,
        "label": " LICEO POLIVALENTE DALCAHUE"
    },
    1133: {
        "value": 1133,
        "label": " ESCUELA RURAL JUAN VICTORINO TANGOL"
    },
    1134: {
        "value": 1134,
        "label": " ESCUELA BASICA FRESIA"
    },
    1135: {
        "value": 1135,
        "label": " LICEO CARLOS IBANEZ DEL CAMPO"
    },
    1136: {
        "value": 1136,
        "label": " ESCUELA RURAL SAN ANDRES"
    },
    1137: {
        "value": 1137,
        "label": " ESCUELA RURAL LOS LINARES DE CASMA"
    },
    1138: {
        "value": 1138,
        "label": " LICEO IGNACIO CARRERA PINTO"
    },
    1139: {
        "value": 1139,
        "label": " ESCUELA ARTURO ALESSANDRI PALMA"
    },
    1140: {
        "value": 1140,
        "label": " ESCUELA FUTALEUFU"
    },
    1141: {
        "value": 1141,
        "label": " COLEGIO MAURICIO HITCHCOCK"
    },
    1142: {
        "value": 1142,
        "label": " LICEO HORNOPIREN"
    },
    1143: {
        "value": 1143,
        "label": " ESCUELA RURAL CATARATAS DEL ALERCE"
    },
    1144: {
        "value": 1144,
        "label": " ESCUELA RURAL 745 SEMILLERO"
    },
    1145: {
        "value": 1145,
        "label": " LICEO POLITECNICO HOLANDA"
    },
    1146: {
        "value": 1146,
        "label": " ESCUELA BASICA GABRIELA MISTRAL"
    },
    1147: {
        "value": 1147,
        "label": " ESCUELA RURAL CANITAS"
    },
    1148: {
        "value": 1148,
        "label": " LICEO PUNTA DE RIELES"
    },
    1149: {
        "value": 1149,
        "label": " COLEGIO DE DIFUSION ARTISTICA LOS ULMOS"
    },
    1150: {
        "value": 1150,
        "label": " GIMNASIO MUNICIPAL"
    },
    1151: {
        "value": 1151,
        "label": " ESCUELA NUEVA PENSYLVANIA"
    },
    1152: {
        "value": 1152,
        "label": " LICEO CAPITAN DE FRAGATA FRANCISCO VvalueAL GORMAZ"
    },
    1153: {
        "value": 1153,
        "label": " ESCUELA ENCARNACION OLIVARES"
    },
    1154: {
        "value": 1154,
        "label": " ESCUELA RURAL HILDA HUNQUEN"
    },
    1155: {
        "value": 1155,
        "label": " ESCUELA MONSENOR FRANCISCO VALDES SUBERCASEAUX"
    },
    1156: {
        "value": 1156,
        "label": " ESCUELA LEONILA FOLCH LOPEZ"
    },
    1157: {
        "value": 1157,
        "label": " ESCUELA EFRAIN CAMPANA SILVA"
    },
    1158: {
        "value": 1158,
        "label": " LICEO CARMELA CARVAJAL DE PRAT"
    },
    1159: {
        "value": 1159,
        "label": " LICEO ELEUTERIO RAMIREZ"
    },
    1160: {
        "value": 1160,
        "label": " ESCUELA SUIZA"
    },
    1161: {
        "value": 1161,
        "label": " ESCUELA LAGO RUPANCO"
    },
    1162: {
        "value": 1162,
        "label": " ESCUELA DE ARTES Y CULTURA OSORNO Y GIMNASIO"
    },
    1163: {
        "value": 1163,
        "label": " ESCUELA MEXICO DE MICHOACAN"
    },
    1164: {
        "value": 1164,
        "label": " INSTITUTO POLITECNICO"
    },
    1165: {
        "value": 1165,
        "label": " ESCUELA ESPANA"
    },
    1166: {
        "value": 1166,
        "label": " LICEO INDUSTRIAL OSORNO"
    },
    1167: {
        "value": 1167,
        "label": " ESCUELA JUAN RICARDO SANCHEZ ASCENCIO"
    },
    1168: {
        "value": 1168,
        "label": " GIMNASIO E INTERNADO LICEO INDUSTRIAL OSORNO"
    },
    1169: {
        "value": 1169,
        "label": " GIMNASIO INSTITUTO COMERCIAL"
    },
    1170: {
        "value": 1170,
        "label": " ESCUELA ESPECIAL `ANA AICHELE CARRASCO`"
    },
    1171: {
        "value": 1171,
        "label": " ESCUELA N. 46 ITALIA"
    },
    1172: {
        "value": 1172,
        "label": " INSTITUTO COMERCIAL"
    },
    1173: {
        "value": 1173,
        "label": " ESCUELA N.88 CANADA"
    },
    1174: {
        "value": 1174,
        "label": " ESCUELA SOCIEDAD DE SOCORRO DE SENORAS"
    },
    1175: {
        "value": 1175,
        "label": " ESCUELA FUNDACION PAUL HARRIS"
    },
    1176: {
        "value": 1176,
        "label": " LICEO RAHUE"
    },
    1177: {
        "value": 1177,
        "label": " ESCUELA ROBERTO WHITE GESELL"
    },
    1178: {
        "value": 1178,
        "label": " ESCUELA BASICA KIMUN LAWAL"
    },
    1179: {
        "value": 1179,
        "label": " ESCUELA ALERCE HISTORICO"
    },
    1180: {
        "value": 1180,
        "label": " INSTITUTO TÉCNICO FORJADORES DE ALERCE"
    },
    1181: {
        "value": 1181,
        "label": " ESCUELA RURAL LA CHAMIZA"
    },
    1182: {
        "value": 1182,
        "label": " ESCUELA RURAL HUELMO"
    },
    1183: {
        "value": 1183,
        "label": " ESCUELA RURAL MAILLEN ESTERO"
    },
    1184: {
        "value": 1184,
        "label": " LICEO RURAL LAS QUEMAS"
    },
    1185: {
        "value": 1185,
        "label": " ESCUELA RURAL LA VARA"
    },
    1186: {
        "value": 1186,
        "label": " ESCUELA RURAL LENCA"
    },
    1187: {
        "value": 1187,
        "label": " LICEO ISvalueORA ZEGERS DE HUNEEUS"
    },
    1188: {
        "value": 1188,
        "label": " LICEO COMERCIAL MIRAMAR"
    },
    1189: {
        "value": 1189,
        "label": " ESCUELA REPUBLICA ARGENTINA"
    },
    1190: {
        "value": 1190,
        "label": " ESCUELA ESPANA"
    },
    1191: {
        "value": 1191,
        "label": " LICEO DE HOMBRES MANUEL MONTT"
    },
    1192: {
        "value": 1192,
        "label": " LICEO COMERCIAL "
    },
    1193: {
        "value": 1193,
        "label": " ESCUELA DE CULTURA Y DIFUSION ARTISTICA"
    },
    1194: {
        "value": 1194,
        "label": " ESCUELA DIFERENCIAL `LOS EUCALIPTOS`"
    },
    1195: {
        "value": 1195,
        "label": " ESCUELA CAPITAN ARTURO PRAT CHACON"
    },
    1196: {
        "value": 1196,
        "label": " LICEO BENJAMIN VICUNA MACKENNA"
    },
    1197: {
        "value": 1197,
        "label": " ESCUELA BASICA LAS CAMELIAS"
    },
    1198: {
        "value": 1198,
        "label": " ESCUELA PADRE ALBERTO HURTADO"
    },
    1199: {
        "value": 1199,
        "label": " LICEO POLITECNICO MIRASOL"
    },
    1200: {
        "value": 1200,
        "label": " ESCUELA MIRASOL"
    },
    1201: {
        "value": 1201,
        "label": " ESCUELA PABLO NERUDA"
    },
    1202: {
        "value": 1202,
        "label": " ESCUELA N 7 ARABE-SIRIA"
    },
    1203: {
        "value": 1203,
        "label": " ESCUELA N 10 ANGELMO"
    },
    1204: {
        "value": 1204,
        "label": " ESCUELA MELIPULLI"
    },
    1205: {
        "value": 1205,
        "label": " ESCUELA CHILOE"
    },
    1206: {
        "value": 1206,
        "label": " LICEO ANDRES BELLO"
    },
    1207: {
        "value": 1207,
        "label": " ESCUELA LIBERTAD"
    },
    1208: {
        "value": 1208,
        "label": " ESCUELA BASICA LICARAYEN"
    },
    1209: {
        "value": 1209,
        "label": " ESCUELA RURAL LAS CASCADAS"
    },
    1210: {
        "value": 1210,
        "label": " ESCUELA ALBERTO HURTADO"
    },
    1211: {
        "value": 1211,
        "label": " LICEO BENJAMIN MUNOZ GAMERO"
    },
    1212: {
        "value": 1212,
        "label": " ESCUELA RURAL EPSON"
    },
    1213: {
        "value": 1213,
        "label": " COLEGIO NUEVA BRAUNAU"
    },
    1214: {
        "value": 1214,
        "label": " ESCUELA RURAL ELLA MINTE DE ROTH"
    },
    1215: {
        "value": 1215,
        "label": " LICEO PEDRO AGUIRRE CERDA"
    },
    1216: {
        "value": 1216,
        "label": " COLEGIO MIRADOR DEL LAGO"
    },
    1217: {
        "value": 1217,
        "label": " ESCUELA GRUPO ESCOLAR"
    },
    1218: {
        "value": 1218,
        "label": " COLEGIO ROSITA NOVARO"
    },
    1219: {
        "value": 1219,
        "label": " ESCUELA RURAL CRISTO REY"
    },
    1220: {
        "value": 1220,
        "label": " ESCUELA PUQUELDON"
    },
    1221: {
        "value": 1221,
        "label": " ESCUELA BERTOLDO HOFMANN KAHLER"
    },
    1222: {
        "value": 1222,
        "label": " ESCUELA RURAL NUEVA ISRAEL"
    },
    1223: {
        "value": 1223,
        "label": " LICEO TOMAS BURGOS"
    },
    1224: {
        "value": 1224,
        "label": " COLEGIO CRECER"
    },
    1225: {
        "value": 1225,
        "label": " ESCUELA VILLA LO BURGOS"
    },
    1226: {
        "value": 1226,
        "label": " INSTITUTO ALEMAN PURRANQUE"
    },
    1227: {
        "value": 1227,
        "label": " GIMNASIO MUNICIPAL"
    },
    1228: {
        "value": 1228,
        "label": " ESCUELA ENTRE LAGOS"
    },
    1229: {
        "value": 1229,
        "label": " ESCUELA CHILHUE"
    },
    1230: {
        "value": 1230,
        "label": " LICEO RAYEN MAPU"
    },
    1231: {
        "value": 1231,
        "label": " ESCUELA EDUCADORA EULOGIA BORQUEZ PEREZ"
    },
    1232: {
        "value": 1232,
        "label": " ESCUELA BASICA SECTOR ORIENTE"
    },
    1233: {
        "value": 1233,
        "label": " ESCUELA RURAL LLIUCO"
    },
    1234: {
        "value": 1234,
        "label": " ESCUELA RURAL BORDEMAR"
    },
    1235: {
        "value": 1235,
        "label": " ESCUELA RURAL LINDA VISTA"
    },
    1236: {
        "value": 1236,
        "label": " ESCUELA RURAL MONTEMAR"
    },
    1237: {
        "value": 1237,
        "label": " ESCUELA BASICA MIL PAISAJES"
    },
    1238: {
        "value": 1238,
        "label": " LICEO POLIVALENTE"
    },
    1239: {
        "value": 1239,
        "label": " ESCUELA RURAL LA VILLA"
    },
    1240: {
        "value": 1240,
        "label": " ESCUELA RURAL AMANECER 2000"
    },
    1241: {
        "value": 1241,
        "label": " LICEO INSULAR ENSENANZA BASICA"
    },
    1242: {
        "value": 1242,
        "label": " LICEO INSULAR ENSENANZA MEDIA"
    },
    1243: {
        "value": 1243,
        "label": " COLEGIO RIACHUELO"
    },
    1244: {
        "value": 1244,
        "label": " LICEO JOSE TORIBIO MEDINA"
    },
    1245: {
        "value": 1245,
        "label": " ESCUELA ANDREW JACKSON"
    },
    1246: {
        "value": 1246,
        "label": " ESCUELA RIO NEGRO"
    },
    1247: {
        "value": 1247,
        "label": " ESCUELA RURAL BAHIA MANSA"
    },
    1248: {
        "value": 1248,
        "label": " COLEGIO TECNICO PROFESIONAL MISION SAN JUAN DE LA COSTA"
    },
    1249: {
        "value": 1249,
        "label": " GIMNASIO MUNICIPAL"
    },
    1250: {
        "value": 1250,
        "label": " LICEO POLITECNICO ANTULAFKEN"
    },
    1251: {
        "value": 1251,
        "label": " COLEGIO FORESTAL DE QUILACAHUIN"
    },
    1252: {
        "value": 1252,
        "label": " ESCUELA BASICA SAN PABLO"
    },
    1253: {
        "value": 1253,
        "label": " LICEO FRAY PABLO DE ROYO"
    },
    1254: {
        "value": 1254,
        "label": " ESCUELA AYSEN"
    },
    1255: {
        "value": 1255,
        "label": " LICEO POLITECNICO DE AYSEN"
    },
    1256: {
        "value": 1256,
        "label": " ESCUELA BASICA E-7"
    },
    1257: {
        "value": 1257,
        "label": " ESCUELA MANIHUALES"
    },
    1258: {
        "value": 1258,
        "label": " ESCUELA PEDRO AGUIRRE CERDA"
    },
    1259: {
        "value": 1259,
        "label": " ESCUELA ALMIRANTE SIMPSON"
    },
    1260: {
        "value": 1260,
        "label": " ESCUELA BASICA DE CHILE CHICO"
    },
    1261: {
        "value": 1261,
        "label": " ESC. LIBERTADOR BERNARDO O'HIGGINS RIQUELME"
    },
    1262: {
        "value": 1262,
        "label": " ESCUELA GUvalueO GOMEZ MUNOZ"
    },
    1263: {
        "value": 1263,
        "label": " ESCUELA AMANDA LABARCA HUBERSTONE"
    },
    1264: {
        "value": 1264,
        "label": " ESCUELA EUSEBIO IBAR SCHEPELER"
    },
    1265: {
        "value": 1265,
        "label": " ESCUELA HAMBURGO"
    },
    1266: {
        "value": 1266,
        "label": " ESC. BASICA HERNAN MERINO CORREA"
    },
    1267: {
        "value": 1267,
        "label": " ESCUELA JOSE ANTOLIN SILVA ORMENO"
    },
    1268: {
        "value": 1268,
        "label": " ESCUELA PEDRO QUINTANA MANSILA"
    },
    1269: {
        "value": 1269,
        "label": " ESCUELA PARTICULAR SAN JOSE OBRERO"
    },
    1270: {
        "value": 1270,
        "label": " ESCUELA BAQUEDANO"
    },
    1271: {
        "value": 1271,
        "label": " LICEO JOSEFINA AGUIRRE MONTENEGRO"
    },
    1272: {
        "value": 1272,
        "label": " LICEO REPUBLICA ARGENTINA"
    },
    1273: {
        "value": 1273,
        "label": " LICEO SAN FELIPE BENICIO DE COYHAIQUE"
    },
    1274: {
        "value": 1274,
        "label": " COLEGIO ANTOINE DE SAINT EXUPERY"
    },
    1275: {
        "value": 1275,
        "label": " ESCUELA RURAL VALLE DE LA LUNA"
    },
    1276: {
        "value": 1276,
        "label": " ESCUELA RURAL VALLE SIMPSON"
    },
    1277: {
        "value": 1277,
        "label": " ESCUELA DE MELINKA"
    },
    1278: {
        "value": 1278,
        "label": " ESC.CON INTERNADO JOSE MIGUEL CARRERA"
    },
    1279: {
        "value": 1279,
        "label": " ESCUELA RURAL CON INTERNADO LA TAPERA"
    },
    1280: {
        "value": 1280,
        "label": " ESCUELA PIONEROS DEL SUR"
    },
    1281: {
        "value": 1281,
        "label": " ESCUELA RURAL CARRETERA AUSTRAL"
    },
    1282: {
        "value": 1282,
        "label": " ESCUELA AONIKENK"
    },
    1283: {
        "value": 1283,
        "label": " ESCUELA GABRIELA MISTRAL"
    },
    1284: {
        "value": 1284,
        "label": " ESCUELA RURAL CERRO CASTILLO"
    },
    1285: {
        "value": 1285,
        "label": " ESCUELA MUNICIPAL CDTE LUIS BRAVO BRAVO"
    },
    1286: {
        "value": 1286,
        "label": " ESCUELA BASICA VILLA LAS ESTRELLAS"
    },
    1287: {
        "value": 1287,
        "label": " LICEO DONALD MC-INTYRE"
    },
    1288: {
        "value": 1288,
        "label": " ESCUELA DIEGO PORTALES"
    },
    1289: {
        "value": 1289,
        "label": " ESCUELA BERNARDO O'HIGGINS"
    },
    1290: {
        "value": 1290,
        "label": " ESCUELA SANTIAGO BUERAS"
    },
    1291: {
        "value": 1291,
        "label": " LICEO LUIS CRUZ MARTINEZ"
    },
    1292: {
        "value": 1292,
        "label": " LICEO GABRIELA MISTRAL"
    },
    1293: {
        "value": 1293,
        "label": " ESCUELA MIGUEL MONTECINOS"
    },
    1294: {
        "value": 1294,
        "label": " LICEO HERNANDO DE MAGALLANES"
    },
    1295: {
        "value": 1295,
        "label": " ESCUELA CERRO SOMBRERO"
    },
    1296: {
        "value": 1296,
        "label": " LICEO SARA BRAUN"
    },
    1297: {
        "value": 1297,
        "label": " INSTITUTO SUPERIOR DE COMERCIO"
    },
    1298: {
        "value": 1298,
        "label": " LICEO LUIS ALBERTO BARRERA"
    },
    1299: {
        "value": 1299,
        "label": " LICEO POLITECNICO RAUL SILVA HENRIQUEZ"
    },
    1300: {
        "value": 1300,
        "label": " LICEO POLIVALENTE MARIA BEHETY"
    },
    1301: {
        "value": 1301,
        "label": " LICEO JUAN BAUTISTA CONTARDI"
    },
    1302: {
        "value": 1302,
        "label": " ESCUELA PEDRO PABLO LEMAITRE"
    },
    1303: {
        "value": 1303,
        "label": " ESCUELA PAUL HARRIS"
    },
    1304: {
        "value": 1304,
        "label": " ESCUELA CROACIA"
    },
    1305: {
        "value": 1305,
        "label": " ESCUELA VILLA LAS NIEVES"
    },
    1306: {
        "value": 1306,
        "label": " ESCUELA ESPANA"
    },
    1307: {
        "value": 1307,
        "label": " ESCUELA BERNARDO O'HIGGINS"
    },
    1308: {
        "value": 1308,
        "label": " ESCUELA JUAN BAUTISTA CONTARDI"
    },
    1309: {
        "value": 1309,
        "label": " ESCUELA ARTURO PRAT CHACON"
    },
    1310: {
        "value": 1310,
        "label": " ESCUELA HERNANDO DE MAGALLANES"
    },
    1311: {
        "value": 1311,
        "label": " ESCUELA PORTUGAL"
    },
    1312: {
        "value": 1312,
        "label": " ESCUELA JUAN WILLIAMS"
    },
    1313: {
        "value": 1313,
        "label": " LICEO INDUSTRIAL ARMANDO QUEZADA ACHARAN"
    },
    1314: {
        "value": 1314,
        "label": " ESCUELA DIECIOCHO DE SEPTIEMBRE"
    },
    1315: {
        "value": 1315,
        "label": " ESCUELA REPUBLICA ARGENTINA"
    },
    1316: {
        "value": 1316,
        "label": " ESCUELA PATAGONIA"
    },
    1317: {
        "value": 1317,
        "label": " ESCUELA PADRE ALBERTO HURTADO"
    },
    1318: {
        "value": 1318,
        "label": " ESCUELA GENERAL MANUEL BULNES P."
    },
    1319: {
        "value": 1319,
        "label": " SALA DE USO MULTIPLE"
    },
    1320: {
        "value": 1320,
        "label": " ESCUELA PUNTA DELGADA"
    },
    1321: {
        "value": 1321,
        "label": " ESCUELA IGNACIO CARRERA PINTO"
    },
    1322: {
        "value": 1322,
        "label": " ESCUELA RAMON SERRANO "
    },
    1323: {
        "value": 1323,
        "label": " LICEO MUNICIPAL SARA TRONCOSO TRONCOSO"
    },
    1324: {
        "value": 1324,
        "label": " LICEO A-131 MEDIA HAYDEE AZOCAR MANSILLA "
    },
    1325: {
        "value": 1325,
        "label": " LICEO A-131 MEDIA HAYDEE AZOCAR MANSILLA LOCAL: 2"
    },
    1326: {
        "value": 1326,
        "label": " LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA"
    },
    1327: {
        "value": 1327,
        "label": " LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 2"
    },
    1328: {
        "value": 1328,
        "label": " LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 3"
    },
    1329: {
        "value": 1329,
        "label": " LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 4"
    },
    1330: {
        "value": 1330,
        "label": " LICEO A-131 BASICA HAYDEE AZOCAR MANSILLA LOCAL: 5"
    },
    1331: {
        "value": 1331,
        "label": " GIMNASIO MUNICIPAL"
    },
    1332: {
        "value": 1332,
        "label": " LICEO POLIVALENTE MODERNO CARDENAL CARO BASICA"
    },
    1333: {
        "value": 1333,
        "label": " LICEO POETISA GABRIELA MISTRAL ENSENANZA BASICA "
    },
    1334: {
        "value": 1334,
        "label": " LICEO POETISA GABRIELA MISTRAL ENSENANZA BASICA LOCAL: 2"
    },
    1335: {
        "value": 1335,
        "label": " LICEO POETISA GABRIELA MISTRAL ENSENANZA BASICA LOCAL: 3"
    },
    1336: {
        "value": 1336,
        "label": " ESCUELA JOAQUIN PRIETO VIAL"
    },
    1337: {
        "value": 1337,
        "label": " ESCUELA CERRILLOS"
    },
    1338: {
        "value": 1338,
        "label": " LICEO POLIVALENTE DR. LUIS VARGAS SALCEDO"
    },
    1339: {
        "value": 1339,
        "label": " ESCUELA ESTRELLA REINA DE CHILE"
    },
    1340: {
        "value": 1340,
        "label": " ESCUELA LOTHAR KOMMER BRUGER"
    },
    1341: {
        "value": 1341,
        "label": " ESCUELA BASICA D-259 CONDORES DE PLATA"
    },
    1342: {
        "value": 1342,
        "label": " ESCUELA BASICA PEDRO AGUIRRE CERDA"
    },
    1343: {
        "value": 1343,
        "label": " ESCUELA EJERCITO LIBERTADOR "
    },
    1344: {
        "value": 1344,
        "label": " ESCUELA 407 SARGENTO CANDELARIA"
    },
    1345: {
        "value": 1345,
        "label": " ESCUELA FEDERICO ACEVEDO SALAZAR"
    },
    1346: {
        "value": 1346,
        "label": " ESCUELA 380 DR. TREVISO GIRARDI TONELLI"
    },
    1347: {
        "value": 1347,
        "label": " ESCUELA PROVINCIA DE ARAUCO"
    },
    1348: {
        "value": 1348,
        "label": " ESCUELA MARIA LUISA BOMBAL"
    },
    1349: {
        "value": 1349,
        "label": " ESCUELA MILLAHUE"
    },
    1350: {
        "value": 1350,
        "label": " ESCUELA 1193 CARLOS BERRIOS VALDES "
    },
    1351: {
        "value": 1351,
        "label": " ESCUELA 1193 CARLOS BERRIOS VALDES LOCAL: 2"
    },
    1352: {
        "value": 1352,
        "label": " ESCUELA 377 CIUDAD SANTO DOMINGO DE GUZMAN"
    },
    1353: {
        "value": 1353,
        "label": " LICEO LOS HEROES DE LA CONCEPCION"
    },
    1354: {
        "value": 1354,
        "label": " ESCUELA ESPECIAL SGTO.CANDELARIA"
    },
    1355: {
        "value": 1355,
        "label": " ESCUELA GENERAL RENE ESCAURIAZA"
    },
    1356: {
        "value": 1356,
        "label": " ESCUELA PROF. MANUEL GUERRERO CEBALLOS"
    },
    1357: {
        "value": 1357,
        "label": " ESCUELA REPUBLICA DE CROACIA"
    },
    1358: {
        "value": 1358,
        "label": " ESCUELA HERMINDA DE LA VICTORIA"
    },
    1359: {
        "value": 1359,
        "label": " ESCUELA BASICA PAULO FREIRE"
    },
    1360: {
        "value": 1360,
        "label": " LICEO BICENTENARIO DE EXCELENCIA "
    },
    1361: {
        "value": 1361,
        "label": " ESCUELA BASICA ALGARROBAL"
    },
    1362: {
        "value": 1362,
        "label": " ESCUELA BASICA MARCOS GOOYCOLEA CORTES"
    },
    1363: {
        "value": 1363,
        "label": " ESCUELA BASICA SANTA TERESA DEL CARMELO"
    },
    1364: {
        "value": 1364,
        "label": " INSTITUTO CHACABUCO"
    },
    1365: {
        "value": 1365,
        "label": " LICEO POLIVALENTE RIGOBERTO FONTT I."
    },
    1366: {
        "value": 1366,
        "label": " ESCUELA PREMIO NOBEL PABLO NERUDA"
    },
    1367: {
        "value": 1367,
        "label": " LICEO ESMERALDA"
    },
    1368: {
        "value": 1368,
        "label": " ESCUELA ANDALIEN "
    },
    1369: {
        "value": 1369,
        "label": " INSTITUTO CHACABUCO LOCAL: 2"
    },
    1370: {
        "value": 1370,
        "label": " ESCUELA E-114 GENERAL BERNALES"
    },
    1371: {
        "value": 1371,
        "label": " ESCUELA ALLIPEN"
    },
    1372: {
        "value": 1372,
        "label": " ESCUELA ATENEA"
    },
    1373: {
        "value": 1373,
        "label": " LICEO ALBERTO BLEST GANA"
    },
    1374: {
        "value": 1374,
        "label": " LICEO POETA FEDERICO GARCIA LORCA"
    },
    1375: {
        "value": 1375,
        "label": " ESCUELA PEDRO AGUIRRE CERDA"
    },
    1376: {
        "value": 1376,
        "label": " LICEO ALMIRANTE GALVARINO RIVEROS "
    },
    1377: {
        "value": 1377,
        "label": " LICEO ALMIRANTE GALVARINO RIVEROS LOCAL: 2"
    },
    1378: {
        "value": 1378,
        "label": " ESCUELA UNESCO D-110 "
    },
    1379: {
        "value": 1379,
        "label": " ESCUELA UNESCO D-110 LOCAL: 2"
    },
    1380: {
        "value": 1380,
        "label": " ESCUELA SOL NACIENTE"
    },
    1381: {
        "value": 1381,
        "label": " ESCUELA VALLE DEL INCA"
    },
    1382: {
        "value": 1382,
        "label": " LICEO POLIV. ABDON CIFUENTES"
    },
    1383: {
        "value": 1383,
        "label": " ESCUELA DRA.ELOISA DIAZ INSUNZA "
    },
    1384: {
        "value": 1384,
        "label": " ESCUELA DRA.ELOISA DIAZ INSUNZA LOCAL: 2"
    },
    1385: {
        "value": 1385,
        "label": " ESCUELA BASICA VALLE DE PUANGUE"
    },
    1386: {
        "value": 1386,
        "label": " LICEO PRESvalueENTE BALMACEDA"
    },
    1387: {
        "value": 1387,
        "label": " ESCUELA SAN JOSE OBRERO "
    },
    1388: {
        "value": 1388,
        "label": " LICEO CHRISTA MC AULIFFE"
    },
    1389: {
        "value": 1389,
        "label": " LICEO CHRISTA MC AULIFFE LOCAL: 2"
    },
    1390: {
        "value": 1390,
        "label": " LICEO JUAN GOMEZ MILLAS "
    },
    1391: {
        "value": 1391,
        "label": " LICEO JUAN GOMEZ MILLAS LOCAL: 2"
    },
    1392: {
        "value": 1392,
        "label": " COLEGIO BATALLA DE LA CONCEPCION"
    },
    1393: {
        "value": 1393,
        "label": " ESCUELA BASICA AVIADORES"
    },
    1394: {
        "value": 1394,
        "label": " ESCUELA CIUDAD DE LYON"
    },
    1395: {
        "value": 1395,
        "label": " LICEO ELENA CAFFARENA MORICE "
    },
    1396: {
        "value": 1396,
        "label": " LICEO ELENA CAFFARENA MORICE LOCAL: 2"
    },
    1397: {
        "value": 1397,
        "label": " ESCUELA BASICA MARIO ARCE GATICA"
    },
    1398: {
        "value": 1398,
        "label": " COMPLEJO EDUCACIONAL FELIPE HERRERA LANE"
    },
    1399: {
        "value": 1399,
        "label": " ESCUELA BASICA PAUL HARRIS"
    },
    1400: {
        "value": 1400,
        "label": " ESCUELA BASICA VILLA SANTA ELENA "
    },
    1401: {
        "value": 1401,
        "label": " ESCUELA BASICA VILLA SANTA ELENA LOCAL: 2"
    },
    1402: {
        "value": 1402,
        "label": " ESCUELA MARCIAL MARTINEZ FERRARI"
    },
    1403: {
        "value": 1403,
        "label": " ESCUELA ALBERTO BACHELET "
    },
    1404: {
        "value": 1404,
        "label": " ESCUELA CLAUDIO ARRAU LEON"
    },
    1405: {
        "value": 1405,
        "label": " LICEO POLIV. LUIS HUMBERTO ACOSTA GAY "
    },
    1406: {
        "value": 1406,
        "label": " LICEO POLIV. LUIS HUMBERTO ACOSTA GAY LOCAL: 2"
    },
    1407: {
        "value": 1407,
        "label": " ESCUELA REPUBLICA DEL ECUADOR "
    },
    1408: {
        "value": 1408,
        "label": " ESCUELA REPUBLICA DEL ECUADOR LOCAL: 2"
    },
    1409: {
        "value": 1409,
        "label": " ESCUELA BASICA ARTURO ALESSANDRI PALMA "
    },
    1410: {
        "value": 1410,
        "label": " ESCUELA BASICA ARTURO ALESSANDRI PALMA LOCAL: 2"
    },
    1411: {
        "value": 1411,
        "label": " ESCUELA BASICA PACTO ANDINO "
    },
    1412: {
        "value": 1412,
        "label": " ESCUELA BASICA PACTO ANDINO LOCAL: 2"
    },
    1413: {
        "value": 1413,
        "label": " COMPLEJO EDUCACIONAL ESTACION CENTRAL "
    },
    1414: {
        "value": 1414,
        "label": " COMPLEJO EDUCACIONAL ESTACION CENTRAL LOCAL: 2"
    },
    1415: {
        "value": 1415,
        "label": " ESCUELA D-276 CAROLINA VERGARA AYARES "
    },
    1416: {
        "value": 1416,
        "label": " ESCUELA D-276 CAROLINA VERGARA AYARES LOCAL: 2"
    },
    1417: {
        "value": 1417,
        "label": " ESCUELA REPUBLICA DE PALESTINA"
    },
    1418: {
        "value": 1418,
        "label": " ESCUELA REPUBLICA DE PALESTINA LOCAL: 2"
    },
    1419: {
        "value": 1419,
        "label": " ESCUELA D-260 RAMON DEL RIO "
    },
    1420: {
        "value": 1420,
        "label": " ESCUELA D-260 RAMON DEL RIO LOCAL: 2"
    },
    1421: {
        "value": 1421,
        "label": " ESCUELA D-57 CARLOS CONDELL "
    },
    1422: {
        "value": 1422,
        "label": " ESCUELA D-57 CARLOS CONDELL LOCAL: 2"
    },
    1423: {
        "value": 1423,
        "label": " LICEO POLIVALENTE A N°71 GUILLERMO FELIU CRUZ"
    },
    1424: {
        "value": 1424,
        "label": " ESCUELA ARNALDO FALABELLA"
    },
    1425: {
        "value": 1425,
        "label": " CENTRO EDUC. DR. AMADOR NEGHME "
    },
    1426: {
        "value": 1426,
        "label": " ESCUELA BASICA CARLOS PRATS G."
    },
    1427: {
        "value": 1427,
        "label": " ESCUELA ADELAvalueA LA FETRA"
    },
    1428: {
        "value": 1428,
        "label": " ESCUELA E-128 SANTA VICTORIA DE HUECHURABA"
    },
    1429: {
        "value": 1429,
        "label": " CENTRO EDUCACIONAL DE HUECHURABA "
    },
    1430: {
        "value": 1430,
        "label": " CENTRO EDUCACIONAL DE HUECHURABA LOCAL: 2"
    },
    1431: {
        "value": 1431,
        "label": " CENTRO EDUCACIONAL DE HUECHURABA LOCAL: 3"
    },
    1432: {
        "value": 1432,
        "label": " COLEGIO GRACE COLLEGE"
    },
    1433: {
        "value": 1433,
        "label": " CENTRO EDUCACIONAL ERNESTO YANEZ RIVERA"
    },
    1434: {
        "value": 1434,
        "label": " LICEO POLIVALENTE A 80 PDTE JOSE MANUEL BALMACEDA "
    },
    1435: {
        "value": 1435,
        "label": " LICEO POLIVALENTE A 80 PDTE JOSE MANUEL BALMACEDA LOCAL: 2"
    },
    1436: {
        "value": 1436,
        "label": " LICEO GABRIELA MISTRAL "
    },
    1437: {
        "value": 1437,
        "label": " LICEO GABRIELA MISTRAL LOCAL: 2"
    },
    1438: {
        "value": 1438,
        "label": " LICEO SAN FRANCISCO DE QUITO "
    },
    1439: {
        "value": 1439,
        "label": " LICEO SAN FRANCISCO DE QUITO LOCAL: 2"
    },
    1440: {
        "value": 1440,
        "label": " LICEO ROSA ESTER ALESANDRI RODRIGUEZ "
    },
    1441: {
        "value": 1441,
        "label": " LICEO ROSA ESTER ALESANDRI RODRIGUEZ LOCAL: 2"
    },
    1442: {
        "value": 1442,
        "label": " ESCUELA BASICA EFRAIN MALDONADO TORRES "
    },
    1443: {
        "value": 1443,
        "label": " ESCUELA BASICA EFRAIN MALDONADO TORRES LOCAL: 2"
    },
    1444: {
        "value": 1444,
        "label": " CENTRO DE EDUCACION MARIO BERTERO CEVASCO"
    },
    1445: {
        "value": 1445,
        "label": " LICEO ABDON CIFUENTES EX 111"
    },
    1446: {
        "value": 1446,
        "label": " COLEGIO NACIONES UNvalueAS"
    },
    1447: {
        "value": 1447,
        "label": " LICEO POLITECNICO CIENCIA Y TECNOLOGIA EX-112 "
    },
    1448: {
        "value": 1448,
        "label": " LICEO POLITECNICO CIENCIA Y TECNOLOGIA EX-112 LOCAL: 2"
    },
    1449: {
        "value": 1449,
        "label": " ESCUELA ESPERANZA JOVEN "
    },
    1450: {
        "value": 1450,
        "label": " ESCUELA ESPERANZA JOVEN LOCAL: 2"
    },
    1451: {
        "value": 1451,
        "label": " LICEO PORTAL DE LA CISTERNA"
    },
    1452: {
        "value": 1452,
        "label": " COLEGIO PALESTINO"
    },
    1453: {
        "value": 1453,
        "label": " ESCUELA OSCAR ENCALADA YOVANOVICH"
    },
    1454: {
        "value": 1454,
        "label": " COLEGIO ANTU"
    },
    1455: {
        "value": 1455,
        "label": " LICEO POLIVALENTE OLOF PALME"
    },
    1456: {
        "value": 1456,
        "label": " COMPLEJO EDUC. MUNICIPAL CARDEN.A.SAMORE"
    },
    1457: {
        "value": 1457,
        "label": " COLEGIO ALCANTARA DE LA CORDILLERA"
    },
    1458: {
        "value": 1458,
        "label": " COLEGIO ALCANTARA DE LA CORDILLERA LOCAL: 2"
    },
    1459: {
        "value": 1459,
        "label": " ESCUELA BELLAVISTA"
    },
    1460: {
        "value": 1460,
        "label": " ESCUELA LAS ARAUCARIAS"
    },
    1461: {
        "value": 1461,
        "label": " COLEGIO SANTA MARIA "
    },
    1462: {
        "value": 1462,
        "label": " COLEGIO SANTA MARIA LOCAL: 2"
    },
    1463: {
        "value": 1463,
        "label": " CHILEAN EAGLES COLLEGE "
    },
    1464: {
        "value": 1464,
        "label": " CHILEAN EAGLES COLLEGE LOCAL: 2"
    },
    1465: {
        "value": 1465,
        "label": " CHILEAN EAGLES COLLEGE LOCAL: 3"
    },
    1466: {
        "value": 1466,
        "label": " COLEGIO BENJAMIN VICUNA MACKENNA "
    },
    1467: {
        "value": 1467,
        "label": " COLEGIO BENJAMIN VICUNA MACKENNA LOCAL: 2"
    },
    1468: {
        "value": 1468,
        "label": " COLEGIO BENJAMIN VICUNA MACKENNA LOCAL: 3"
    },
    1469: {
        "value": 1469,
        "label": "EST. BICENTE. L1 BELLA"
    },
    1470: {
        "value": 1470,
        "label": "EST. BICENTE. L2 BELLA"
    },
    1471: {
        "value": 1471,
        "label": "EST. BICENTE. L3 BELLA"
    },
    1472: {
        "value": 1472,
        "label": "EST. BICENTE. L4 BELLA"
    },
    1473: {
        "value": 1473,
        "label": " LICEO POLIVALENTE MUNICIPAL DE LA FLORvalueA"
    },
    1474: {
        "value": 1474,
        "label": " LICEO INDIRA GANDHI"
    },
    1475: {
        "value": 1475,
        "label": " COLEGIO ANDREW CARNEGIE COLLEGE"
    },
    1476: {
        "value": 1476,
        "label": " COLEGIO CAPITAN PASTENE (EX-CATALUNA) "
    },
    1477: {
        "value": 1477,
        "label": " COLEGIO CAPITAN PASTENE (EX-CATALUNA) LOCAL: 2"
    },
    1478: {
        "value": 1478,
        "label": " ESCUELA LAS LILAS"
    },
    1479: {
        "value": 1479,
        "label": " LICEO ALTO CORDILLERA "
    },
    1480: {
        "value": 1480,
        "label": " LICEO ALTO CORDILLERA LOCAL: 2"
    },
    1481: {
        "value": 1481,
        "label": "EST. BICENTE. L1 SANJOSE"
    },
    1482: {
        "value": 1482,
        "label": "EST. BICENTE. L2 SANJOSE"
    },
    1483: {
        "value": 1483,
        "label": " COLEGIO DIVINA PASTORA "
    },
    1484: {
        "value": 1484,
        "label": " COLEGIO DIVINA PASTORA LOCAL: 2"
    },
    1485: {
        "value": 1485,
        "label": " ESCUELA MARCELA PAZ "
    },
    1486: {
        "value": 1486,
        "label": " ESCUELA MARCELA PAZ LOCAL: 2"
    },
    1487: {
        "value": 1487,
        "label": "EST. BICENTE. L1 TRINI"
    },
    1488: {
        "value": 1488,
        "label": " COLEGIO DR SOTERO DEL RIO "
    },
    1489: {
        "value": 1489,
        "label": " COLEGIO DR SOTERO DEL RIO LOCAL: 2"
    },
    1490: {
        "value": 1490,
        "label": "EST. BICENTE. L3 TRINI"
    },
    1491: {
        "value": 1491,
        "label": " LICEO LOS ALMENDROS "
    },
    1492: {
        "value": 1492,
        "label": " LICEO LOS ALMENDROS LOCAL: 2"
    },
    1493: {
        "value": 1493,
        "label": "EST. BICENTE. L2 TRINI"
    },
    1494: {
        "value": 1494,
        "label": " ESCUELA POETA OSCAR CASTRO ZUNIGA"
    },
    1495: {
        "value": 1495,
        "label": " LICEO DOCTOR ALEJANDRO DEL RIO"
    },
    1496: {
        "value": 1496,
        "label": " ESCUELA BAS. BENJAMIN SUBERCASEAUX"
    },
    1497: {
        "value": 1497,
        "label": " ESCUELA LA ARAUCANIA"
    },
    1498: {
        "value": 1498,
        "label": " ESCUELA BELGICA"
    },
    1499: {
        "value": 1499,
        "label": " LICEO MUNICIPAL POETA NERUDA"
    },
    1500: {
        "value": 1500,
        "label": " ESCUELA HEROES DE YUNGAY"
    },
    1501: {
        "value": 1501,
        "label": " LICEO POLIVALENTE FRANCISCO FRIAS VALENZUELA"
    },
    1502: {
        "value": 1502,
        "label": " COLEGIO MUNICIPAL PROCERES DE CHILE"
    },
    1503: {
        "value": 1503,
        "label": " LICEO MALAQUIAS CONCHA "
    },
    1504: {
        "value": 1504,
        "label": " ESCUELA TECNO SUR"
    },
    1505: {
        "value": 1505,
        "label": " ESCUELA BASICA ISLAS DE CHILE"
    },
    1506: {
        "value": 1506,
        "label": " LICEO MALAQUIAS CONCHA LOCAL: 2"
    },
    1507: {
        "value": 1507,
        "label": " LICEO GRANJA SUR"
    },
    1508: {
        "value": 1508,
        "label": " LICEO GRANJA SUR LOCAL: 2"
    },
    1509: {
        "value": 1509,
        "label": " ESCUELA BASICA SANITAS"
    },
    1510: {
        "value": 1510,
        "label": " LICEO MUNICIPAL DE BATUCO"
    },
    1511: {
        "value": 1511,
        "label": " COMPLEJO EDUCACIONAL MANUEL PLAZA REYES "
    },
    1512: {
        "value": 1512,
        "label": " COMPLEJO EDUCACIONAL MANUEL PLAZA REYES LOCAL: 2"
    },
    1513: {
        "value": 1513,
        "label": " ESCUELA MANUEL SEGOVIA MONTENEGRO "
    },
    1514: {
        "value": 1514,
        "label": " ESCUELA MANUEL SEGOVIA MONTENEGRO LOCAL: 2"
    },
    1515: {
        "value": 1515,
        "label": " ESCUELA POLONIA GUTIERREZ "
    },
    1516: {
        "value": 1516,
        "label": " ESCUELA POLONIA GUTIERREZ LOCAL: 2"
    },
    1517: {
        "value": 1517,
        "label": " COLEGIO SOL DEL VALLE"
    },
    1518: {
        "value": 1518,
        "label": " CENTRO EDUCACIONAL MARIANO LATORRE"
    },
    1519: {
        "value": 1519,
        "label": " LICEO VICTOR JARA"
    },
    1520: {
        "value": 1520,
        "label": " ESCUELA MUNICIPAL VIOLETA PARRA"
    },
    1521: {
        "value": 1521,
        "label": " LICEO PABLO DE ROKHA "
    },
    1522: {
        "value": 1522,
        "label": " LICEO PABLO DE ROKHA LOCAL: 2"
    },
    1523: {
        "value": 1523,
        "label": " ESCUELA BASICA JUAN DE DIOS ALDEA"
    },
    1524: {
        "value": 1524,
        "label": " LICEO SIMON BOLIVAR"
    },
    1525: {
        "value": 1525,
        "label": " LICEO MUNICIPAL EL ROBLE"
    },
    1526: {
        "value": 1526,
        "label": " COLEGIO CAPITAN AVALOS "
    },
    1527: {
        "value": 1527,
        "label": " COLEGIO CAPITAN AVALOS LOCAL: 2"
    },
    1528: {
        "value": 1528,
        "label": " COLEGIO PABLO NERUDA "
    },
    1529: {
        "value": 1529,
        "label": " ESCUELA PROF. AURELIA ROJAS BURGOS"
    },
    1530: {
        "value": 1530,
        "label": " CENTRO EDUCACIONAL LA PINTANA"
    },
    1531: {
        "value": 1531,
        "label": " CENTRO EDUCACIONAL LA PINTANA LOCAL: 2"
    },
    1532: {
        "value": 1532,
        "label": " LICEO LAS AMERICAS "
    },
    1533: {
        "value": 1533,
        "label": " COLEGIO DE LA SALLE"
    },
    1534: {
        "value": 1534,
        "label": " ESCUELA PALESTINA "
    },
    1535: {
        "value": 1535,
        "label": " ESCUELA PALESTINA LOCAL: 2"
    },
    1536: {
        "value": 1536,
        "label": " COLEGIO CONFEDERAC SUIZA "
    },
    1537: {
        "value": 1537,
        "label": " COLEGIO CONFEDERAC SUIZA LOCAL: 2"
    },
    1538: {
        "value": 1538,
        "label": " COLEGIO TERESIANO ENRIQUE DE OSSO "
    },
    1539: {
        "value": 1539,
        "label": " COLEGIO TERESIANO ENRIQUE DE OSSO LOCAL: 2"
    },
    1540: {
        "value": 1540,
        "label": " COMPLEJO EDUCACIONAL LA REINA SECCION MEDIA "
    },
    1541: {
        "value": 1541,
        "label": " COMPLEJO EDUCACIONAL LA REINA SECCION MEDIA LOCAL: 2"
    },
    1542: {
        "value": 1542,
        "label": " COMPLEJO EDUCACIONAL LA REINA SECCION BASICA"
    },
    1543: {
        "value": 1543,
        "label": " LICEO EUGENIO MARIA DE HOSTOS"
    },
    1544: {
        "value": 1544,
        "label": " COLEGIO SAN CONSTANTINO"
    },
    1545: {
        "value": 1545,
        "label": " COLEGIO COMPANIA DE MARIA APOQUINDO "
    },
    1546: {
        "value": 1546,
        "label": " COLEGIO COMPANIA DE MARIA APOQUINDO LOCAL: 2"
    },
    1547: {
        "value": 1547,
        "label": " LICEO RAFAEL SOTOMAYOR"
    },
    1548: {
        "value": 1548,
        "label": " LICEO SANTA MARIA DE LAS CONDES "
    },
    1549: {
        "value": 1549,
        "label": " LICEO SANTA MARIA DE LAS CONDES LOCAL: 2"
    },
    1550: {
        "value": 1550,
        "label": " ESC. DE EDUC. DIFERENCIAL PAUL HARRIS"
    },
    1551: {
        "value": 1551,
        "label": " COLEGIO NUESTRA. SRA. DEL ROSARIO "
    },
    1552: {
        "value": 1552,
        "label": " COLEGIO NUESTRA. SRA. DEL ROSARIO LOCAL: 2"
    },
    1553: {
        "value": 1553,
        "label": " COLEGIO SAN FRANCISCO DEL ALBA "
    },
    1554: {
        "value": 1554,
        "label": " LICEO SIMON BOLIVAR"
    },
    1555: {
        "value": 1555,
        "label": " LICEO SIMON BOLIVAR LOCAL: 2"
    },
    1556: {
        "value": 1556,
        "label": " LICEO ALEXANDER FLEMING"
    },
    1557: {
        "value": 1557,
        "label": " LICEO ALEXANDER FLEMING LOCAL: 2"
    },
    1558: {
        "value": 1558,
        "label": " LICEO JUAN PABLO II DE LAS CONDES "
    },
    1559: {
        "value": 1559,
        "label": " LICEO JUAN PABLO II DE LAS CONDES LOCAL: 2"
    },
    1560: {
        "value": 1560,
        "label": " COLEGIO SAN FRANCISCO DEL ALBA LOCAL: 2"
    },
    1561: {
        "value": 1561,
        "label": " COLEGIO LEONARDO DA VINCI DE LAS CONDES"
    },
    1562: {
        "value": 1562,
        "label": " COLEGIO VILLA MARIA ACADEMY "
    },
    1563: {
        "value": 1563,
        "label": " COLEGIO VILLA MARIA ACADEMY LOCAL: 2"
    },
    1564: {
        "value": 1564,
        "label": " COLEGIO DEL VERBO DIVINO "
    },
    1565: {
        "value": 1565,
        "label": " COLEGIO DEL VERBO DIVINO LOCAL: 2"
    },
    1566: {
        "value": 1566,
        "label": " COLEGIO DEL VERBO DIVINO LOCAL: 3"
    },
    1567: {
        "value": 1567,
        "label": " COLEGIO ALCAZAR DE LAS CONDES "
    },
    1568: {
        "value": 1568,
        "label": " COLEGIO ALCAZAR DE LAS CONDES LOCAL: 2"
    },
    1569: {
        "value": 1569,
        "label": " COLEGIO ALCAZAR DE LAS CONDES LOCAL: 3"
    },
    1570: {
        "value": 1570,
        "label": " COLEGIO SEMINARIO PONTIFICIO MENOR"
    },
    1571: {
        "value": 1571,
        "label": " COLEGIO SEMINARIO PONTIFICIO MENOR LOCAL: 2"
    },
    1572: {
        "value": 1572,
        "label": " CENTRO CIVICO LO BARNECHEA"
    },
    1573: {
        "value": 1573,
        "label": " CENTRO CIVICO LO BARNECHEA LOCAL: 2"
    },
    1574: {
        "value": 1574,
        "label": " COLEGIO MAYFLOWER"
    },
    1575: {
        "value": 1575,
        "label": " COLEGIO MAYFLOWER LOCAL: 2"
    },
    1576: {
        "value": 1576,
        "label": " COLEGIO THE NEWLAND"
    },
    1577: {
        "value": 1577,
        "label": " COLEGIO THE NEWLAND LOCAL: 2"
    },
    1578: {
        "value": 1578,
        "label": " COLEGIO THE NEWLAND LOCAL: 3"
    },
    1579: {
        "value": 1579,
        "label": " ESCUELA DE ED.DIFERENCIAL MADRE TIERRA "
    },
    1580: {
        "value": 1580,
        "label": " LICEO TENIENTE FRANCISCO MERY AGUIRRE"
    },
    1581: {
        "value": 1581,
        "label": " COLEGIO HERNAN OLGUIN"
    },
    1582: {
        "value": 1582,
        "label": " ESCUELA REPUBLICA DE INDONESIA"
    },
    1583: {
        "value": 1583,
        "label": " ESCUELA D 570 RAUL SAEZ"
    },
    1584: {
        "value": 1584,
        "label": " ESCUELA ESPECIAL DE EDUC TAMARUGAL 355"
    },
    1585: {
        "value": 1585,
        "label": " ESCUELA REPUBLICA DE LAS FILIPINAS"
    },
    1586: {
        "value": 1586,
        "label": " ESCUELA ALICIA ARIZTIA"
    },
    1587: {
        "value": 1587,
        "label": " ESCUELA ACAPULCO"
    },
    1588: {
        "value": 1588,
        "label": " ESCUELA BLUE STAR COLLEGE"
    },
    1589: {
        "value": 1589,
        "label": " LICEO POLIVALENTE"
    },
    1590: {
        "value": 1590,
        "label": " ESCUELA BASICA BERNARDO O'HIGGINS"
    },
    1591: {
        "value": 1591,
        "label": " ESCUELA BASICA CLARA ESTRELLA"
    },
    1592: {
        "value": 1592,
        "label": " CENTRO EDUC.CARDENAL JOSE MARIA CARO"
    },
    1593: {
        "value": 1593,
        "label": " ESCUELA D 571 SANTA ADRIANA"
    },
    1594: {
        "value": 1594,
        "label": " COMPLEJO EDUCACIONAL PEDRO PRADO"
    },
    1595: {
        "value": 1595,
        "label": " ANEXO INSTITUTO PEDRO PRADO"
    },
    1596: {
        "value": 1596,
        "label": " ESCUELA SOR TERESA DE LOS ANDES"
    },
    1597: {
        "value": 1597,
        "label": " ESCUELA JAIME GOMEZ GARCIA"
    },
    1598: {
        "value": 1598,
        "label": " ESCUELA MARISCAL DE AYACUCHO"
    },
    1599: {
        "value": 1599,
        "label": " ESCUELA GOLDA MEIR"
    },
    1600: {
        "value": 1600,
        "label": " ESCUELA PROFESORA GLADYS VALENZUELA V"
    },
    1601: {
        "value": 1601,
        "label": " ESCUELA POETA PABLO NERUDA"
    },
    1602: {
        "value": 1602,
        "label": " ESC.DE EDUCACION DIFERENCIAL QUILLAHUE"
    },
    1603: {
        "value": 1603,
        "label": " ESCUELA MUSTAFA KEMAL ATATURK"
    },
    1604: {
        "value": 1604,
        "label": " ESTADIO MONUMENTAL"
    },
    1605: {
        "value": 1605,
        "label": " ESTADIO MONUMENTAL LOCAL: 2"
    },
    1606: {
        "value": 1606,
        "label": " ESTADIO MONUMENTAL LOCAL: 3"
    },
    1607: {
        "value": 1607,
        "label": " LICEO MERCEDES MARIN DEL SOLAR "
    },
    1608: {
        "value": 1608,
        "label": " LICEO MERCEDES MARIN DEL SOLAR LOCAL: 2"
    },
    1609: {
        "value": 1609,
        "label": " LICEO VILLA MACUL ACADEMIA "
    },
    1610: {
        "value": 1610,
        "label": " COMPLEJO EDUC. JOAQUIN EDWARDS BELLO. A-55 "
    },
    1611: {
        "value": 1611,
        "label": " ESCUELA JULIO MONTT SALAMANCA E-194 "
    },
    1612: {
        "value": 1612,
        "label": " ESCUELA JOSE BERNARDO SUAREZ"
    },
    1613: {
        "value": 1613,
        "label": " COLEGIO PARTICULAR EL ALBA DE MACUL"
    },
    1614: {
        "value": 1614,
        "label": " GIMNASIO MUNICIPAL"
    },
    1615: {
        "value": 1615,
        "label": " ESCUELA VILLA MACUL D-200 "
    },
    1616: {
        "value": 1616,
        "label": " ESCUELA BASICA JULIO BARRENECHEA"
    },
    1617: {
        "value": 1617,
        "label": " COLEGIO ALCAZAR"
    },
    1618: {
        "value": 1618,
        "label": " COLEGIO LOS ALPES MAIPU"
    },
    1619: {
        "value": 1619,
        "label": " COLEGIO SANTA MARIA DE MAIPU "
    },
    1620: {
        "value": 1620,
        "label": " COLEGIO SANTA MARIA DE MAIPU LOCAL: 2"
    },
    1621: {
        "value": 1621,
        "label": " ESCUELA REINA DE SUECIA "
    },
    1622: {
        "value": 1622,
        "label": " ESCUELA REINA DE SUECIA LOCAL: 2"
    },
    1623: {
        "value": 1623,
        "label": " LICEO BICENTENARIO DE NINAS DE MAIPU "
    },
    1624: {
        "value": 1624,
        "label": " LICEO BICENTENARIO DE NINAS DE MAIPU LOCAL: 2"
    },
    1625: {
        "value": 1625,
        "label": " COLEGIO ALICANTE DEL ROSAL"
    },
    1626: {
        "value": 1626,
        "label": " ESCUELA GENERAL SAN MARTIN"
    },
    1627: {
        "value": 1627,
        "label": " ESCUELA GENERAL OHIGGINS A-274 "
    },
    1628: {
        "value": 1628,
        "label": " ESCUELA GENERAL OHIGGINS A-274 LOCAL: 2"
    },
    1629: {
        "value": 1629,
        "label": " COLEGIO DE LA PROVvalueENCIA CARMELA LARRAIN DE INFANTE"
    },
    1630: {
        "value": 1630,
        "label": " ESCUELA PRESvalueENTE RIESCO ERRAZURIZ A-275 "
    },
    1631: {
        "value": 1631,
        "label": " ESCUELA PRESvalueENTE RIESCO ERRAZURIZ A-275 LOCAL: 2"
    },
    1632: {
        "value": 1632,
        "label": " LICEO INDUSTRIAL DOMINGO MATTE PEREZ"
    },
    1633: {
        "value": 1633,
        "label": " ESCUELA VICENTE REYES PALAZUELOS"
    },
    1634: {
        "value": 1634,
        "label": " LICEO SANTIAGO BUERAS Y AVARIA"
    },
    1635: {
        "value": 1635,
        "label": " ESCUELA BASICA N 263 RAMON FREIRE"
    },
    1636: {
        "value": 1636,
        "label": " ESCUELA LEON HUMBERTO VALENZUELA"
    },
    1637: {
        "value": 1637,
        "label": " TEATRO MUNICIPAL"
    },
    1638: {
        "value": 1638,
        "label": " SALON AUDITORIUM MUNICIPAL"
    },
    1639: {
        "value": 1639,
        "label": " CENTRO DE EDUC. TECN.PROFESIONAL CODEDUC "
    },
    1640: {
        "value": 1640,
        "label": " CENTRO DE EDUC. TECN.PROFESIONAL CODEDUC LOCAL: 2"
    },
    1641: {
        "value": 1641,
        "label": " POLvalueEPORTIVO OLIMPICO"
    },
    1642: {
        "value": 1642,
        "label": " GIMNASIO MUNICIPAL"
    },
    1643: {
        "value": 1643,
        "label": " INSTITUTO DE HUMANvalueADES B. OHIGGINS "
    },
    1644: {
        "value": 1644,
        "label": " COLEGIO REPUBLICA DE GUATEMALA"
    },
    1645: {
        "value": 1645,
        "label": " LICEO JOSE IGNACIO ZENTENO A-75"
    },
    1646: {
        "value": 1646,
        "label": " LICEO JOSE IGNACIO ZENTENO A-75 LOCAL: 2"
    },
    1647: {
        "value": 1647,
        "label": " ESCUELA TOMAS VARGAS E-280 "
    },
    1648: {
        "value": 1648,
        "label": " ESCUELA LOS BOSQUINOS"
    },
    1649: {
        "value": 1649,
        "label": " ESCUELA EL LLANO DE MAIPU"
    },
    1650: {
        "value": 1650,
        "label": " ESCUELA TOMAS VARGAS E-280 LOCAL: 2"
    },
    1651: {
        "value": 1651,
        "label": " COLEGIO ALBERTO PEREZ"
    },
    1652: {
        "value": 1652,
        "label": " LICEO NACIONAL DE MAIPU "
    },
    1653: {
        "value": 1653,
        "label": " LICEO NACIONAL DE MAIPU LOCAL: 2"
    },
    1654: {
        "value": 1654,
        "label": " ESCUELA BASICA MUNICIPAL SAN LUIS"
    },
    1655: {
        "value": 1655,
        "label": " COLEGIO JACQUES COUSTEAU"
    },
    1656: {
        "value": 1656,
        "label": " LICEO NACIONAL DE MAIPU LOCAL: 3"
    },
    1657: {
        "value": 1657,
        "label": " LICEO TECNOLOGICO ENRIQUE KIRBERG B "
    },
    1658: {
        "value": 1658,
        "label": " LICEO TECNOLOGICO ENRIQUE KIRBERG B LOCAL: 2"
    },
    1659: {
        "value": 1659,
        "label": " LICEO TECNOLOGICO ENRIQUE KIRBERG B LOCAL: 3"
    },
    1660: {
        "value": 1660,
        "label": " COLEGIO JACQUES COUSTEAU LOCAL: 2"
    },
    1661: {
        "value": 1661,
        "label": " COLEGIO SAN SEBASTIAN "
    },
    1662: {
        "value": 1662,
        "label": " LICEO MUNICIPAL F-N°860"
    },
    1663: {
        "value": 1663,
        "label": " LICEO POLITECNICO MUNICIPAL MELIPILLA "
    },
    1664: {
        "value": 1664,
        "label": " LICEO POLITECNICO MUNICIPAL MELIPILLA LOCAL: 2"
    },
    1665: {
        "value": 1665,
        "label": " LICEO POLITECNICO MUNICIPAL MELIPILLA LOCAL: 3"
    },
    1666: {
        "value": 1666,
        "label": " LICEO POLITECNICO MUNICIPAL MELIPILLA LOCAL: 4"
    },
    1667: {
        "value": 1667,
        "label": " LICEO POLIV. HNOS. SOTOMAYOR BAEZA "
    },
    1668: {
        "value": 1668,
        "label": " LICEO POLIV. HNOS. SOTOMAYOR BAEZA LOCAL: 2"
    },
    1669: {
        "value": 1669,
        "label": " COLEGIO NUESTRA SRA. DE LA PRESENTACION "
    },
    1670: {
        "value": 1670,
        "label": " COLEGIO NUESTRA SRA. DE LA PRESENTACION LOCAL: 2"
    },
    1671: {
        "value": 1671,
        "label": " COLEGIO SAN AGUSTIN "
    },
    1672: {
        "value": 1672,
        "label": " COLEGIO SAN AGUSTIN LOCAL: 2"
    },
    1673: {
        "value": 1673,
        "label": " LICEO GABRIELA MISTRAL "
    },
    1674: {
        "value": 1674,
        "label": " ESTADIO NACIONAL LOCAL: 1"
    },
    1675: {
        "value": 1675,
        "label": " ESTADIO NACIONAL LOCAL: 2"
    },
    1676: {
        "value": 1676,
        "label": " ESTADIO NACIONAL LOCAL: 3"
    },
    1677: {
        "value": 1677,
        "label": " ESTADIO NACIONAL LOCAL: 4"
    },
    1678: {
        "value": 1678,
        "label": " ESTADIO NACIONAL LOCAL: 5"
    },
    1679: {
        "value": 1679,
        "label": " ESTADIO NACIONAL LOCAL: 6"
    },
    1680: {
        "value": 1680,
        "label": " ESTADIO NACIONAL LOCAL: 7"
    },
    1681: {
        "value": 1681,
        "label": " ESTADIO NACIONAL LOCAL: 8"
    },
    1682: {
        "value": 1682,
        "label": " ESTADIO NACIONAL LOCAL: 9"
    },
    1683: {
        "value": 1683,
        "label": " LICEO AUGUSTO D HALMAR"
    },
    1684: {
        "value": 1684,
        "label": " ESC.PRESvalueENTE EDUARDO FREI MONTALVA"
    },
    1685: {
        "value": 1685,
        "label": " COLEGIO SANTO TOMAS "
    },
    1686: {
        "value": 1686,
        "label": " COLEGIO SANTO TOMAS LOCAL: 2"
    },
    1687: {
        "value": 1687,
        "label": " ESCUELA REPUBLICA DE SIRIA"
    },
    1688: {
        "value": 1688,
        "label": " LICEO REPUBLICA DE SIRIA"
    },
    1689: {
        "value": 1689,
        "label": " LICEO LENKA FRANULIC"
    },
    1690: {
        "value": 1690,
        "label": " ESCUELA ANEXO LICEO A-52"
    },
    1691: {
        "value": 1691,
        "label": " ESCUELA REPUBLICA DE COSTA RICA"
    },
    1692: {
        "value": 1692,
        "label": " LICEO COMERCIAL GABRIEL GONZALEZ VvalueELA"
    },
    1693: {
        "value": 1693,
        "label": " LICEO CARMELA SILVA DONOSO"
    },
    1694: {
        "value": 1694,
        "label": " LICEO CARMELA SILVA DONOSO LOCAL: 2"
    },
    1695: {
        "value": 1695,
        "label": " LICEO CARMELA SILVA DONOSO LOCAL: 3"
    },
    1696: {
        "value": 1696,
        "label": " LICEO INDUSTRIAL CHILENO-ALEMAN "
    },
    1697: {
        "value": 1697,
        "label": " LICEO INDUSTRIAL CHILENO-ALEMAN LOCAL: 2"
    },
    1698: {
        "value": 1698,
        "label": " LICEO JOSE TORIBIO MEDINA A-52 "
    },
    1699: {
        "value": 1699,
        "label": " LICEO JOSE TORIBIO MEDINA A-52 LOCAL: 2"
    },
    1700: {
        "value": 1700,
        "label": " LICEO CARMELA SILVA DONOSO LOCAL: 4"
    },
    1701: {
        "value": 1701,
        "label": " COMPLEJO EDUC.PART. LUIS PASTEUR "
    },
    1702: {
        "value": 1702,
        "label": " COMPLEJO EDUC.PART. LUIS PASTEUR LOCAL: 2"
    },
    1703: {
        "value": 1703,
        "label": " LICEO PAUL HARRIS "
    },
    1704: {
        "value": 1704,
        "label": " LICEO PAUL HARRIS LOCAL: 2"
    },
    1705: {
        "value": 1705,
        "label": " ESCUELA REPUBLICA ARGENTINA"
    },
    1706: {
        "value": 1706,
        "label": " LICEO GREGORIO MORALES MIRANDA"
    },
    1707: {
        "value": 1707,
        "label": " LICEO MARIA CARVAJAL FUENZALvalueA"
    },
    1708: {
        "value": 1708,
        "label": " CENTRO EDUC. ENRIQUE BERNSTEIN CARABANTE "
    },
    1709: {
        "value": 1709,
        "label": " CENTRO EDUC. ENRIQUE BERNSTEIN CARABANTE LOCAL: 2"
    },
    1710: {
        "value": 1710,
        "label": " ESCUELA PAULA JARAQUEMADA ALQUIZAR "
    },
    1711: {
        "value": 1711,
        "label": " ESCUELA PAULA JARAQUEMADA ALQUIZAR LOCAL: 2"
    },
    1712: {
        "value": 1712,
        "label": " ESCUELA POETAS DE CHILE"
    },
    1713: {
        "value": 1713,
        "label": " LICEO ENRIQUE BACKAUSSE "
    },
    1714: {
        "value": 1714,
        "label": " LICEO ENRIQUE BACKAUSSE LOCAL: 2"
    },
    1715: {
        "value": 1715,
        "label": " ESCUELA LA VICTORIA"
    },
    1716: {
        "value": 1716,
        "label": " ESCUELA BOROA"
    },
    1717: {
        "value": 1717,
        "label": " ESCUELA BOROA LOCAL: 2"
    },
    1718: {
        "value": 1718,
        "label": " ESCUELA CONSOLvalueADA DAVILA "
    },
    1719: {
        "value": 1719,
        "label": " ESCUELA CONSOLvalueADA DAVILA LOCAL: 2"
    },
    1720: {
        "value": 1720,
        "label": " ESCUELA VILLA SUR"
    },
    1721: {
        "value": 1721,
        "label": " ESCUELA 563 LO VALLEDOR "
    },
    1722: {
        "value": 1722,
        "label": " ESCUELA 563 LO VALLEDOR LOCAL: 2"
    },
    1723: {
        "value": 1723,
        "label": " COLEGIO PARQUE LAS AMERICAS "
    },
    1724: {
        "value": 1724,
        "label": " COLEGIO PARQUE LAS AMERICAS LOCAL: 2"
    },
    1725: {
        "value": 1725,
        "label": " ESCUELA CIUDAD DE BARCELONA"
    },
    1726: {
        "value": 1726,
        "label": " LICEO EUGENIO PEREIRA SALAS"
    },
    1727: {
        "value": 1727,
        "label": " ESCUELA ROSALINA PESCIO VARGAS"
    },
    1728: {
        "value": 1728,
        "label": " LICEO COMERCIAL REPUBLICA DE BRASIL"
    },
    1729: {
        "value": 1729,
        "label": " ESCUELA 664 MALLOCO"
    },
    1730: {
        "value": 1730,
        "label": " ESCUELA EMILIA LASCAR"
    },
    1731: {
        "value": 1731,
        "label": " ESCUELA TERESA DE CALCUTA"
    },
    1732: {
        "value": 1732,
        "label": " ESCUELA REPUBLICA DE ISRAEL"
    },
    1733: {
        "value": 1733,
        "label": " ESCUELA MAND EDUARDO FREI MONTALVA"
    },
    1734: {
        "value": 1734,
        "label": " ESCUELA JUAN BAUTISTA PASTENE"
    },
    1735: {
        "value": 1735,
        "label": " COLEGIO ALCANTARA DE LOS ALTOS DE PENALOLEN"
    },
    1736: {
        "value": 1736,
        "label": " COLEGIO MIRAVALLE"
    },
    1737: {
        "value": 1737,
        "label": " COLEGIO MIRAVALLE LOCAL: 2"
    },
    1738: {
        "value": 1738,
        "label": " ESCUELA E-180 MATILDE HUICI NAVAS"
    },
    1739: {
        "value": 1739,
        "label": " ESCUELA UNION NACIONAL ARABE"
    },
    1740: {
        "value": 1740,
        "label": " ESCUELA TOBALABA"
    },
    1741: {
        "value": 1741,
        "label": " CENTRO EDUC ERASMO ESCALA "
    },
    1742: {
        "value": 1742,
        "label": " CENTRO EDUCACION MARIANO EGANA "
    },
    1743: {
        "value": 1743,
        "label": " CENTRO EDUC. VALLE HERMOSO"
    },
    1744: {
        "value": 1744,
        "label": " ESCUELA LUIS ARRIETA CANAS"
    },
    1745: {
        "value": 1745,
        "label": " ESCUELA CARLOS FERNANDEZ PENA"
    },
    1746: {
        "value": 1746,
        "label": " LICEO ANTONIO HERMvalueA FABRES "
    },
    1747: {
        "value": 1747,
        "label": " LICEO ANTONIO HERMvalueA FABRES LOCAL: 2"
    },
    1748: {
        "value": 1748,
        "label": " CENTRO EDUCACION MARIANO EGANA LOCAL: 2"
    },
    1749: {
        "value": 1749,
        "label": " COLEGIO JORGE PRIETO LETELIER "
    },
    1750: {
        "value": 1750,
        "label": " COLEGIO JORGE PRIETO LETELIER LOCAL: 2"
    },
    1751: {
        "value": 1751,
        "label": " ESCUELA PARTICULAR WELCOME SCHOOL"
    },
    1752: {
        "value": 1752,
        "label": " COLEGIO SANTA MARTA DE PENALOLEN"
    },
    1753: {
        "value": 1753,
        "label": " COLEGIO SANTA MARTA DE PENALOLEN LOCAL: 2"
    },
    1754: {
        "value": 1754,
        "label": " LICEO EL PRINCIPAL"
    },
    1755: {
        "value": 1755,
        "label": " LICEO EL LLANO "
    },
    1756: {
        "value": 1756,
        "label": " LICEO EL LLANO LOCAL: 2"
    },
    1757: {
        "value": 1757,
        "label": " LICEO JOSE VICTORINO LASTARRIA"
    },
    1758: {
        "value": 1758,
        "label": " LICEO DE NINAS N° 7 LUISA SAAVEDRA DE GONZALEZ "
    },
    1759: {
        "value": 1759,
        "label": " LICEO DE NINAS N° 7 LUISA SAAVEDRA DE GONZALEZ LOCAL: 2"
    },
    1760: {
        "value": 1760,
        "label": " INSTITUTO DE HUMANvalueADES LUIS CAMPINO "
    },
    1761: {
        "value": 1761,
        "label": " INSTITUTO DE HUMANvalueADES LUIS CAMPINO LOCAL: 2"
    },
    1762: {
        "value": 1762,
        "label": " COLEGIO PROVvalueENCIA"
    },
    1763: {
        "value": 1763,
        "label": " LICEO B 42 TAJAMAR"
    },
    1764: {
        "value": 1764,
        "label": " ESCUELA JUAN PABLO DUARTE "
    },
    1765: {
        "value": 1765,
        "label": " ESCUELA JUAN PABLO DUARTE LOCAL: 2"
    },
    1766: {
        "value": 1766,
        "label": " COLEGIO COMPANIA DE MARIA "
    },
    1767: {
        "value": 1767,
        "label": " COLEGIO COMPANIA DE MARIA LOCAL: 2"
    },
    1768: {
        "value": 1768,
        "label": " UNIVERSvalueAD AUTONOMA DE CHILE"
    },
    1769: {
        "value": 1769,
        "label": " LICEO ARTURO ALESSANDRI PALMA"
    },
    1770: {
        "value": 1770,
        "label": " LICEO ARTURO ALESSANDRI PALMA LOCAL: 2"
    },
    1771: {
        "value": 1771,
        "label": " CAMPUS ORIENTE U. CATOLICA"
    },
    1772: {
        "value": 1772,
        "label": " CAMPUS ORIENTE U. CATOLICA LOCAL: 2"
    },
    1773: {
        "value": 1773,
        "label": " CAMPUS ORIENTE U. CATOLICA LOCAL: 3"
    },
    1774: {
        "value": 1774,
        "label": " CAMPUS ORIENTE U. CATOLICA LOCAL: 4"
    },
    1775: {
        "value": 1775,
        "label": " LICEO CARMELA CARVAJAL DE PRAT "
    },
    1776: {
        "value": 1776,
        "label": " LICEO CARMELA CARVAJAL DE PRAT LOCAL: 2"
    },
    1777: {
        "value": 1777,
        "label": " LICEO CIUDAD DE BRASILIA"
    },
    1778: {
        "value": 1778,
        "label": " ESCUELA MONSENOR CARLOS OVIEDO "
    },
    1779: {
        "value": 1779,
        "label": " ESCUELA MONSENOR CARLOS OVIEDO LOCAL: 2"
    },
    1780: {
        "value": 1780,
        "label": " ESCUELA ELVIRA SANTA CRUZ OSSA "
    },
    1781: {
        "value": 1781,
        "label": " ESCUELA ELVIRA SANTA CRUZ OSSA LOCAL: 2"
    },
    1782: {
        "value": 1782,
        "label": " ESCUELA ESTADO DE FLORvalueA"
    },
    1783: {
        "value": 1783,
        "label": " ESCUELA SAN DANIEL"
    },
    1784: {
        "value": 1784,
        "label": " ESCUELA ALEXANDER GRAHAM BELL "
    },
    1785: {
        "value": 1785,
        "label": " ESCUELA ALEXANDER GRAHAM BELL LOCAL: 2"
    },
    1786: {
        "value": 1786,
        "label": " ESCUELA EL SALITRE "
    },
    1787: {
        "value": 1787,
        "label": " ESCUELA EL SALITRE LOCAL: 2"
    },
    1788: {
        "value": 1788,
        "label": " ESCUELA TENIENTE HERNAN MERINO CORREA "
    },
    1789: {
        "value": 1789,
        "label": " ESCUELA TENIENTE HERNAN MERINO CORREA LOCAL: 2"
    },
    1790: {
        "value": 1790,
        "label": " ESCUELA ESTRELLA DE CHILE "
    },
    1791: {
        "value": 1791,
        "label": " ESCUELA ESTRELLA DE CHILE LOCAL: 2"
    },
    1792: {
        "value": 1792,
        "label": " LICEO MONSENOR ENRIQUE ALVEAR"
    },
    1793: {
        "value": 1793,
        "label": " ESCUELA 1546 MONSENOR ENRIQUE ALVEAR"
    },
    1794: {
        "value": 1794,
        "label": " LICEO MONSENOR ENRIQUE ALVEAR LOCAL: 2"
    },
    1795: {
        "value": 1795,
        "label": " ESCUELA ANTILHUE "
    },
    1796: {
        "value": 1796,
        "label": " ESCUELA PUERTO FUTURO"
    },
    1797: {
        "value": 1797,
        "label": " LICEO MUNICIPAL CHILOE"
    },
    1798: {
        "value": 1798,
        "label": " ESCUELA LAS PALMAS"
    },
    1799: {
        "value": 1799,
        "label": " ESCUELA LOS ANDES"
    },
    1800: {
        "value": 1800,
        "label": " ESCUELA NONATO COO "
    },
    1801: {
        "value": 1801,
        "label": " ESCUELA NONATO COO LOCAL: 2"
    },
    1802: {
        "value": 1802,
        "label": " LICEO INDUSTRIAL DE PUENTE ALTO"
    },
    1803: {
        "value": 1803,
        "label": " LICEO INDUSTRIAL DE PUENTE ALTO LOCAL: 2"
    },
    1804: {
        "value": 1804,
        "label": " LICEO PUENTE ALTO "
    },
    1805: {
        "value": 1805,
        "label": " LICEO PUENTE ALTO LOCAL: 2"
    },
    1806: {
        "value": 1806,
        "label": " COLEGIO DOMINGO MATTE MESIAS "
    },
    1807: {
        "value": 1807,
        "label": " COLEGIO DOMINGO MATTE MESIAS LOCAL: 2"
    },
    1808: {
        "value": 1808,
        "label": " COLEGIO SAN JOSE DE PUENTE ALTO "
    },
    1809: {
        "value": 1809,
        "label": " COLEGIO SAN JOSE DE PUENTE ALTO LOCAL: 2"
    },
    1810: {
        "value": 1810,
        "label": " ESCUELA REPUBLICA DE GRECIA"
    },
    1811: {
        "value": 1811,
        "label": " COLEGIO SAN CARLOS DE ARAGON "
    },
    1812: {
        "value": 1812,
        "label": " COLEGIO SAN CARLOS DE ARAGON LOCAL: 2"
    },
    1813: {
        "value": 1813,
        "label": " LICEO NUESTRA SENORA DE LAS MERCEDES"
    },
    1814: {
        "value": 1814,
        "label": " LICEO SAN GERONIMO "
    },
    1815: {
        "value": 1815,
        "label": " LICEO SAN GERONIMO LOCAL: 2"
    },
    1816: {
        "value": 1816,
        "label": " COMPLEJO EDUCACIONAL CONSOLvalueADA "
    },
    1817: {
        "value": 1817,
        "label": " COMPLEJO EDUCACIONAL CONSOLvalueADA LOCAL: 2"
    },
    1818: {
        "value": 1818,
        "label": " CENTRO EDUCACIONAL NUEVA CREACION"
    },
    1819: {
        "value": 1819,
        "label": " ESCUELA OSCAR BONILLA"
    },
    1820: {
        "value": 1820,
        "label": " LICEO COMERCIAL DE PUENTE ALTO "
    },
    1821: {
        "value": 1821,
        "label": " LICEO COMERCIAL DE PUENTE ALTO LOCAL: 2"
    },
    1822: {
        "value": 1822,
        "label": " COLEGIO MAIPO "
    },
    1823: {
        "value": 1823,
        "label": " COLEGIO MAIPO LOCAL: 2"
    },
    1824: {
        "value": 1824,
        "label": " ESCUELA VILLA INDEPENDENCIA "
    },
    1825: {
        "value": 1825,
        "label": " ESCUELA VILLA INDEPENDENCIA LOCAL: 2"
    },
    1826: {
        "value": 1826,
        "label": " COLEGIO ALICANTE DEL SOL "
    },
    1827: {
        "value": 1827,
        "label": " COLEGIO ALICANTE DEL SOL LOCAL: 2"
    },
    1828: {
        "value": 1828,
        "label": " COLEGIO ALICANTE DEL SOL LOCAL: 3"
    },
    1829: {
        "value": 1829,
        "label": " COLEGIO ALTAZOR"
    },
    1830: {
        "value": 1830,
        "label": " LICEO POLIVALENTE MARIA REINA "
    },
    1831: {
        "value": 1831,
        "label": " LICEO POLIVALENTE MARIA REINA LOCAL: 2"
    },
    1832: {
        "value": 1832,
        "label": " LICEO POLIVALENTE MARIA REINA LOCAL: 3"
    },
    1833: {
        "value": 1833,
        "label": " COLEGIO EL SEMBRADOR "
    },
    1834: {
        "value": 1834,
        "label": " COLEGIO EL SEMBRADOR LOCAL: 2"
    },
    1835: {
        "value": 1835,
        "label": " COLEGIO CARDENAL RAUL SILVA HENRIQUEZ"
    },
    1836: {
        "value": 1836,
        "label": " ESCUELA SANTA JOAQUINA DE VEDRUNA"
    },
    1837: {
        "value": 1837,
        "label": " ESCUELA SANTA JOAQUINA DE VEDRUNA LOCAL: 2"
    },
    1838: {
        "value": 1838,
        "label": " COLEGIO ARTURO PRAT"
    },
    1839: {
        "value": 1839,
        "label": " COLEGIO ARTURO PRAT LOCAL: 2"
    },
    1840: {
        "value": 1840,
        "label": " ESCUELA EJERCITO LIBERTADOR"
    },
    1841: {
        "value": 1841,
        "label": " ESCUELA VILLA PEDRO AGUIRRE CERDA"
    },
    1842: {
        "value": 1842,
        "label": " COLEGIO LUIS MATTE LARRAIN"
    },
    1843: {
        "value": 1843,
        "label": " COLEGIO OBISPO ALVEAR"
    },
    1844: {
        "value": 1844,
        "label": " COLEGIO SANTA MARIA DE LA CORDILLERA "
    },
    1845: {
        "value": 1845,
        "label": " COLEGIO SANTA MARIA DE LA CORDILLERA LOCAL: 2"
    },
    1846: {
        "value": 1846,
        "label": " LICEO ING.MILITAR JUAN MACKENA OREALLY "
    },
    1847: {
        "value": 1847,
        "label": " LICEO ING.MILITAR JUAN MACKENA OREALLY LOCAL: 2"
    },
    1848: {
        "value": 1848,
        "label": " ESCUELA ANDES DEL SUR"
    },
    1849: {
        "value": 1849,
        "label": " ESCUELA GABRIELA"
    },
    1850: {
        "value": 1850,
        "label": " COMPLEJO EDUCACIONAL J. MIGUEL CARRERA"
    },
    1851: {
        "value": 1851,
        "label": " ESCUELA ESTADO DE MICHIGAN"
    },
    1852: {
        "value": 1852,
        "label": " ESCUELA MERCEDES FONTECILLA"
    },
    1853: {
        "value": 1853,
        "label": " COLEGIO MELFORD COLLEGE"
    },
    1854: {
        "value": 1854,
        "label": " COLEGIO SANTA BARBARA "
    },
    1855: {
        "value": 1855,
        "label": " ESCUELA LUIS CRUZ MARTINEZ"
    },
    1856: {
        "value": 1856,
        "label": " ESCUELA LUIS CRUZ MARTINEZ LOCAL: 2"
    },
    1857: {
        "value": 1857,
        "label": " ESCUELA BASICA ANA FRANK"
    },
    1858: {
        "value": 1858,
        "label": " COLEGIO PALMARES"
    },
    1859: {
        "value": 1859,
        "label": " ESCUELA PROFESORA MARIA LUISA SEPULVEDA"
    },
    1860: {
        "value": 1860,
        "label": " COLEGIO SAN SEBASTIAN "
    },
    1861: {
        "value": 1861,
        "label": " COLEGIO SAN SEBASTIAN LOCAL: 2"
    },
    1862: {
        "value": 1862,
        "label": " ESCUELA EL MANIO"
    },
    1863: {
        "value": 1863,
        "label": " ESCUELA LUIS CRUZ MARTINEZ LOCAL: 3"
    },
    1864: {
        "value": 1864,
        "label": " ESCUELA ANTUMALAL"
    },
    1865: {
        "value": 1865,
        "label": " ESCUELA INGLATERRA"
    },
    1866: {
        "value": 1866,
        "label": " ESCUELA INGLATERRA LOCAL: 2"
    },
    1867: {
        "value": 1867,
        "label": " LICEO GUILLERMO LABARCA HUBERTSON "
    },
    1868: {
        "value": 1868,
        "label": " LICEO GUILLERMO LABARCA HUBERTSON LOCAL: 2"
    },
    1869: {
        "value": 1869,
        "label": " ESCUELA GRENOBLE"
    },
    1870: {
        "value": 1870,
        "label": " ESCUELA GRENOBLE LOCAL: 2"
    },
    1871: {
        "value": 1871,
        "label": " ESCUELA INSIGNE GABRIELA"
    },
    1872: {
        "value": 1872,
        "label": " ESCUELA DIEGO PORTALES"
    },
    1873: {
        "value": 1873,
        "label": " LICEO POLIVALENTE JUAN A.RIOS"
    },
    1874: {
        "value": 1874,
        "label": " LICEO INDUSTRIAL BENJAMIN FRANKLIN"
    },
    1875: {
        "value": 1875,
        "label": " ESCUELA SANTA TERESA DE AVILA "
    },
    1876: {
        "value": 1876,
        "label": " ESCUELA SANTA TERESA DE AVILA LOCAL: 2"
    },
    1877: {
        "value": 1877,
        "label": " CENTRO EDUC.ESCRITORES DE CHILE"
    },
    1878: {
        "value": 1878,
        "label": " CENTRO EDUC.ESCRITORES DE CHILE LOCAL: 2"
    },
    1879: {
        "value": 1879,
        "label": " ESCUELA ESPANA"
    },
    1880: {
        "value": 1880,
        "label": " ESCUELA PUERTO RICO"
    },
    1881: {
        "value": 1881,
        "label": " ESCUELA HERMANA MARIA GORETTI"
    },
    1882: {
        "value": 1882,
        "label": " COMPLEJO EDUCACIONAL JUANITA FERNANDEZ SOLAR"
    },
    1883: {
        "value": 1883,
        "label": " LICEO PAULA JARAQUEMADA"
    },
    1884: {
        "value": 1884,
        "label": " ESCUELA REPUBLICA DEL PARAGUAY "
    },
    1885: {
        "value": 1885,
        "label": " ESCUELA REPUBLICA DEL PARAGUAY LOCAL: 2"
    },
    1886: {
        "value": 1886,
        "label": " LICEO DE ADULTOS JORGE ALESSANDRI R."
    },
    1887: {
        "value": 1887,
        "label": " LICEO DE ADULTOS JORGE ALESSANDRI R. LOCAL: 2"
    },
    1888: {
        "value": 1888,
        "label": " ESCUELA JUAN VERDAGUER PLANAS"
    },
    1889: {
        "value": 1889,
        "label": " LICEO HEROE ARTURO PEREZ CANTO "
    },
    1890: {
        "value": 1890,
        "label": " LICEO HEROE ARTURO PEREZ CANTO LOCAL: 2"
    },
    1891: {
        "value": 1891,
        "label": " CENTRO EDUCACIONAL JOSE MIGUEL CARRERA B-36 "
    },
    1892: {
        "value": 1892,
        "label": " CENTRO EDUCACIONAL JOSE MIGUEL CARRERA B-36 LOCAL: 2"
    },
    1893: {
        "value": 1893,
        "label": " ESTADIO RECOLETA"
    },
    1894: {
        "value": 1894,
        "label": " ESCUELA REBECA MATTE BELLO"
    },
    1895: {
        "value": 1895,
        "label": " ESCUELA GENERAL ALEJANDRO GOROSTIAGA O"
    },
    1896: {
        "value": 1896,
        "label": " ESCUELA BASICA LO VELASQUEZ"
    },
    1897: {
        "value": 1897,
        "label": " ESCUELA GUSTAVO LE PAIGE "
    },
    1898: {
        "value": 1898,
        "label": " ESCUELA GUSTAVO LE PAIGE LOCAL: 2"
    },
    1899: {
        "value": 1899,
        "label": " ESCUELA BASICA ISABEL LE BRUN"
    },
    1900: {
        "value": 1900,
        "label": " ESCUELA THOMAS ALVA EDISON N° 317"
    },
    1901: {
        "value": 1901,
        "label": " ESCUELA MONSERRAT ROBERT DE GARCIA"
    },
    1902: {
        "value": 1902,
        "label": " ESCUELA DOMINGO SANTA MARIA GONZALEZ"
    },
    1903: {
        "value": 1903,
        "label": " INSTITUTO CUMBRE DE CONDORES ORIENTE "
    },
    1904: {
        "value": 1904,
        "label": " INSTITUTO CUMBRE DE CONDORES ORIENTE LOCAL: 2"
    },
    1905: {
        "value": 1905,
        "label": " ESCUELA CAPITAN JOSE LUIS ARANEDA"
    },
    1906: {
        "value": 1906,
        "label": " INSTITUTO CUMBRES DE CONDORES PONIENTE "
    },
    1907: {
        "value": 1907,
        "label": " INSTITUTO CUMBRES DE CONDORES PONIENTE LOCAL: 2"
    },
    1908: {
        "value": 1908,
        "label": " LICEO COMERCIAL "
    },
    1909: {
        "value": 1909,
        "label": " LICEO COMERCIAL LOCAL: 2"
    },
    1910: {
        "value": 1910,
        "label": " LICEO A-127 FvalueEL PINOCHET LE-BRUN "
    },
    1911: {
        "value": 1911,
        "label": " LICEO A-127 FvalueEL PINOCHET LE-BRUN LOCAL: 2"
    },
    1912: {
        "value": 1912,
        "label": " LICEO ELVIRA BRADY MALDONADO "
    },
    1913: {
        "value": 1913,
        "label": " LICEO ELVIRA BRADY MALDONADO LOCAL: 2"
    },
    1914: {
        "value": 1914,
        "label": " ESCUELA REPUBLICA DEL BRASIL"
    },
    1915: {
        "value": 1915,
        "label": " ESCUELA REPUBLICA DEL PERU"
    },
    1916: {
        "value": 1916,
        "label": " ESCUELA PILAR MOLINER DE NUEZ"
    },
    1917: {
        "value": 1917,
        "label": " ESCUELA BASICA ANTUPILLAN"
    },
    1918: {
        "value": 1918,
        "label": " ESCUELA ESCRITORA MARCELA PAZ"
    },
    1919: {
        "value": 1919,
        "label": " ESCUELA ISABEL RIQUELME"
    },
    1920: {
        "value": 1920,
        "label": " ESCUELA ISABEL RIQUELME LOCAL: 2"
    },
    1921: {
        "value": 1921,
        "label": " ESCUELA L.GRAL. BERNARDO OHIGGINS "
    },
    1922: {
        "value": 1922,
        "label": " ESCUELA L.GRAL. BERNARDO OHIGGINS LOCAL: 2"
    },
    1923: {
        "value": 1923,
        "label": " LICEO IND. MIGUEL AYLWIN GAJARDO"
    },
    1924: {
        "value": 1924,
        "label": " LICEO IND. MIGUEL AYLWIN GAJARDO LOCAL: 2"
    },
    1925: {
        "value": 1925,
        "label": " ESCUELA MANUEL MAGALLANES MOURE"
    },
    1926: {
        "value": 1926,
        "label": " ESCUELA HERNAN MERINO CORREA"
    },
    1927: {
        "value": 1927,
        "label": " LICEO POLIV. LUCILA GODOY ALCAYAGA"
    },
    1928: {
        "value": 1928,
        "label": " ESCUELA ALEMANIA D-774 "
    },
    1929: {
        "value": 1929,
        "label": " ESCUELA ALEMANIA D-774 LOCAL: 2"
    },
    1930: {
        "value": 1930,
        "label": " ESCUELA ESCRITORA MARCELA PAZ LOCAL: 2"
    },
    1931: {
        "value": 1931,
        "label": " ESCUELA DIEGO PORTALES E-753 "
    },
    1932: {
        "value": 1932,
        "label": " ESCUELA DIEGO PORTALES E-753 LOCAL: 2"
    },
    1933: {
        "value": 1933,
        "label": " CENTRO EDUCACIONAL BALDOMERO LILLO "
    },
    1934: {
        "value": 1934,
        "label": " CENTRO EDUCACIONAL BALDOMERO LILLO LOCAL: 2"
    },
    1935: {
        "value": 1935,
        "label": " LICEO BICENTENARIO"
    },
    1936: {
        "value": 1936,
        "label": " COLEGIO SEBASTIAN ELCANO "
    },
    1937: {
        "value": 1937,
        "label": " LICEO MUNICIPAL SAN JOAQUIN"
    },
    1938: {
        "value": 1938,
        "label": " CENTRO EDUC. PROVINCIA DE NUBLE"
    },
    1939: {
        "value": 1939,
        "label": " GIMNASIO MUNICIPAL"
    },
    1940: {
        "value": 1940,
        "label": " ESCUELA BASICA FRAY CAMILO HENRIQUEZ"
    },
    1941: {
        "value": 1941,
        "label": " COLEGIO CIUDAD DE FRANKFORT"
    },
    1942: {
        "value": 1942,
        "label": " LICEO PARTICULAR ESPIRITU SANTO"
    },
    1943: {
        "value": 1943,
        "label": " LICEO INDUSTRIAL AGUSTIN EDWARDS ROSS"
    },
    1944: {
        "value": 1944,
        "label": " CENTRO EDUCACIONAL HORACIO ARAVENA A."
    },
    1945: {
        "value": 1945,
        "label": " COLEGIO ADVENTISTA SANTIAGO SUR"
    },
    1946: {
        "value": 1946,
        "label": " ESCUELA BASICA POETA NERUDA (EX-483)"
    },
    1947: {
        "value": 1947,
        "label": " COLEGIO INFOCAP"
    },
    1948: {
        "value": 1948,
        "label": " ESCUELA POETA VICTOR DOMINGO SILVA"
    },
    1949: {
        "value": 1949,
        "label": " ESCUELA JULIETA BECERRA ALVAREZ"
    },
    1950: {
        "value": 1950,
        "label": " LICEO POLIVALENTE SAN JOSE DE MAIPO"
    },
    1951: {
        "value": 1951,
        "label": " LICEO ANDRES BELLO"
    },
    1952: {
        "value": 1952,
        "label": " LICEO ANDRES BELLO LOCAL: 2"
    },
    1953: {
        "value": 1953,
        "label": " COLEGIO PARROQUIAL SAN MIGUEL"
    },
    1954: {
        "value": 1954,
        "label": " INSTITUTO SUPERIOR DE COMERCIO DE CHILE (EX A99)"
    },
    1955: {
        "value": 1955,
        "label": " INSTITUTO MIGUEL LEON PRADO"
    },
    1956: {
        "value": 1956,
        "label": " ESCUELA LLANO SUBERCASEAUX"
    },
    1957: {
        "value": 1957,
        "label": " COLEGIO SANTO CURA DE ARS"
    },
    1958: {
        "value": 1958,
        "label": " ESCUELA BASICA MUNICIPAL PABLO NERUDA"
    },
    1959: {
        "value": 1959,
        "label": " ESCUELA DE LA INDUSTRIA GRAFICA HECTOR GOMEZ"
    },
    1960: {
        "value": 1960,
        "label": " LICEO BETSABE HORMAZABAL DE ALARCON "
    },
    1961: {
        "value": 1961,
        "label": " LICEO BETSABE HORMAZABAL DE ALARCON LOCAL: 2"
    },
    1962: {
        "value": 1962,
        "label": " LICEO MUNICIPAL DE SAN PEDRO"
    },
    1963: {
        "value": 1963,
        "label": " LICEO MUNICIPALIZADO ARAUCANIA"
    },
    1964: {
        "value": 1964,
        "label": " CENTRO EDUCACIONAL SAN RAMON"
    },
    1965: {
        "value": 1965,
        "label": " ESCUELA TUPAHUE"
    },
    1966: {
        "value": 1966,
        "label": " ESCUELA ARTURO MATTE LARRAIN"
    },
    1967: {
        "value": 1967,
        "label": " ESCUELA BASICA ALIVEN"
    },
    1968: {
        "value": 1968,
        "label": " CENTRO EDUCACIONAL MIRADOR"
    },
    1969: {
        "value": 1969,
        "label": " ESCUELA VILLA LA CULTURA"
    },
    1970: {
        "value": 1970,
        "label": " LICEO MUNICIPAL PURKUYEN"
    },
    1971: {
        "value": 1971,
        "label": " ESCUELA EDUCADORES DE CHILE"
    },
    1972: {
        "value": 1972,
        "label": " LICEO COMERCIAL VATE VICENTE HUvalueOBRO"
    },
    1973: {
        "value": 1973,
        "label": " ESCUELA NANIHUE"
    },
    1974: {
        "value": 1974,
        "label": " ESCUELA KARELMAPU"
    },
    1975: {
        "value": 1975,
        "label": " ESCUELA SENDERO DEL SABER"
    },
    1976: {
        "value": 1976,
        "label": " LICEO JAVIERA CARRERA"
    },
    1977: {
        "value": 1977,
        "label": " ESCUELA DR LUIS CALVO MACKENNA "
    },
    1978: {
        "value": 1978,
        "label": " ESCUELA DR LUIS CALVO MACKENNA LOCAL: 2"
    },
    1979: {
        "value": 1979,
        "label": " LICEO MIGUEL LUIS AMUNATEGUI "
    },
    1980: {
        "value": 1980,
        "label": " LICEO MIGUEL LUIS AMUNATEGUI LOCAL: 2"
    },
    1981: {
        "value": 1981,
        "label": " LICEO DE APLICACION 29 "
    },
    1982: {
        "value": 1982,
        "label": " LICEO DE APLICACION 29 LOCAL: 2"
    },
    1983: {
        "value": 1983,
        "label": " INST.SUP.DE COMERCIO EDUARDO FREI M."
    },
    1984: {
        "value": 1984,
        "label": " ESCUELA LIBERTADORES DE CHILE"
    },
    1985: {
        "value": 1985,
        "label": " LICEO ISAURA DINATOR DE GUZMAN"
    },
    1986: {
        "value": 1986,
        "label": " LICEO ISAURA DINATOR DE GUZMAN LOCAL: 2"
    },
    1987: {
        "value": 1987,
        "label": " LICEO DE APLICACION 21"
    },
    1988: {
        "value": 1988,
        "label": " ESCUELA REPUBLICA DE ALEMANIA"
    },
    1989: {
        "value": 1989,
        "label": " LICEO INDUSTRIAL ELIODORO GARCIA ZEGERS "
    },
    1990: {
        "value": 1990,
        "label": " LICEO INDUSTRIAL ELIODORO GARCIA ZEGERS LOCAL: 2"
    },
    1991: {
        "value": 1991,
        "label": " ESCUELA REPUBLICA DE PANAMA"
    },
    1992: {
        "value": 1992,
        "label": " ESCUELA SALVADOR SANFUENTES "
    },
    1993: {
        "value": 1993,
        "label": " ESCUELA SALVADOR SANFUENTES LOCAL: 2"
    },
    1994: {
        "value": 1994,
        "label": " LICEO MIGUEL DE CERVANTES Y SAAVEDRA ANEXO BASICA"
    },
    1995: {
        "value": 1995,
        "label": " LICEO MIGUEL DE CERVANTES Y SAAVEDRA ANEXO BASICA LOCAL: 2"
    },
    1996: {
        "value": 1996,
        "label": " LICEO PDTE. GABRIEL GONZALEZ VvalueELA"
    },
    1997: {
        "value": 1997,
        "label": " LICEO LIB. GRAL JOSE DE SAN MARTIN"
    },
    1998: {
        "value": 1998,
        "label": " LICEO DARIO SALAS"
    },
    1999: {
        "value": 1999,
        "label": " ESCUELA CADETE ARTURO PRAT CHACON"
    },
    2000: {
        "value": 2000,
        "label": " LICEO CONFEDERACION SUIZA"
    },
    2001: {
        "value": 2001,
        "label": " ESCUELA FERNANDO ALESSANDRI RODRIGUEZ"
    },
    2002: {
        "value": 2002,
        "label": " ESCUELA REPUBLICA DE COLOMBIA"
    },
    2003: {
        "value": 2003,
        "label": " ESC. REPUBLICA ORIENTAL DE URUGUAY"
    },
    2004: {
        "value": 2004,
        "label": " ESCUELA BENJAMIN VICUNA MACKENNA"
    },
    2005: {
        "value": 2005,
        "label": " LICEO TERESA PRATS DE SARRATEA"
    },
    2006: {
        "value": 2006,
        "label": " LICEO REPUBLICA DE BRASIL"
    },
    2007: {
        "value": 2007,
        "label": " LICEO METROPOLITANO DE ADULTOS"
    },
    2008: {
        "value": 2008,
        "label": " ESCUELA IRENE FREI DE Cvalue"
    },
    2009: {
        "value": 2009,
        "label": " ESCUELA PROVINCIA DE CHILOE"
    },
    2010: {
        "value": 2010,
        "label": " ESCUELA PILOTO PARDO"
    },
    2011: {
        "value": 2011,
        "label": " LICEO MANUEL BARROS BORGONO"
    },
    2012: {
        "value": 2012,
        "label": " LICEO MANUEL BARROS BORGONO LOCAL: 2"
    },
    2013: {
        "value": 2013,
        "label": " ESCUELA REPUBLICA DE GRECIA"
    },
    2014: {
        "value": 2014,
        "label": " ESCUELA REPUBLICA DE GRECIA LOCAL: 2"
    },
    2015: {
        "value": 2015,
        "label": " LICEO POLITECNICO C-120 "
    },
    2016: {
        "value": 2016,
        "label": " LICEO POLIVALENTE A-119 MEDIA"
    },
    2017: {
        "value": 2017,
        "label": " LICEO POLIVALENTE A-119 MEDIA LOCAL: 2"
    },
    2018: {
        "value": 2018,
        "label": " LICEO A-119 BASICO"
    },
    2019: {
        "value": 2019,
        "label": " LICEO POLITECNICO C-120 LOCAL: 2"
    },
    2020: {
        "value": 2020,
        "label": " LICEO HUERTOS FAMILIARES"
    },
    2021: {
        "value": 2021,
        "label": " LICEO POLIVALENTE MANUEL RODRIGUEZ C-82"
    },
    2022: {
        "value": 2022,
        "label": " LICEO POLIVALENTE MANUEL RODRIGUEZ C-82 LOCAL: 2"
    },
    2023: {
        "value": 2023,
        "label": " LICEO AMANDA LABARCA B-66 "
    },
    2024: {
        "value": 2024,
        "label": " COLEGIO TABANCURA"
    },
    2025: {
        "value": 2025,
        "label": " ALIANZA FRANCESA "
    },
    2026: {
        "value": 2026,
        "label": " COLEGIO ANTARTICA CHILENA"
    },
    2027: {
        "value": 2027,
        "label": " COLEGIO SAN PEDRO NOLASCO "
    },
    2028: {
        "value": 2028,
        "label": " COLEGIO SANTA URSULA"
    },
    2029: {
        "value": 2029,
        "label": " COLEGIO DE LOS SAGRADOS CORAZONES "
    },
    2030: {
        "value": 2030,
        "label": " COLEGIO DE LOS SAGRADOS CORAZONES LOCAL: 2"
    },
    2031: {
        "value": 2031,
        "label": " COLEGIO SAINT GEORGES"
    },
    2032: {
        "value": 2032,
        "label": " COLEGIO SAINT GEORGES LOCAL: 2"
    },
    2033: {
        "value": 2033,
        "label": " ESCUELA BASICA DE CORRAL"
    },
    2034: {
        "value": 2034,
        "label": " ESCUELA RURAL CARBONEROS"
    },
    2035: {
        "value": 2035,
        "label": " ESCUELA RURAL CURRINE"
    },
    2036: {
        "value": 2036,
        "label": " LICEO SAN CONRADO DE FUTRONO"
    },
    2037: {
        "value": 2037,
        "label": " ESCUELA BASICA JOSE M. BALMACEDA F."
    },
    2038: {
        "value": 2038,
        "label": " ESCUELA FRONTERIZA DE LLIFEN"
    },
    2039: {
        "value": 2039,
        "label": " ESCUELA RURAL NONTUELA"
    },
    2040: {
        "value": 2040,
        "label": " LICEO ANTONIO VARAS LOCAL: 2"
    },
    2041: {
        "value": 2041,
        "label": " LICEO ANTONIO VARAS"
    },
    2042: {
        "value": 2042,
        "label": " ESCUELA RURAL RININAHUE"
    },
    2043: {
        "value": 2043,
        "label": " LICEO POLIVALENTE CAMILO HENRIQUEZ G."
    },
    2044: {
        "value": 2044,
        "label": " ESCUELA PARTICULAR SAN FRANCISCO"
    },
    2045: {
        "value": 2045,
        "label": " ESCUELA ALBERTO CORDOVA LATORRE"
    },
    2046: {
        "value": 2046,
        "label": " LICEO REPUBLICA DEL BRASIL"
    },
    2047: {
        "value": 2047,
        "label": " ESCUELA LA UNION"
    },
    2048: {
        "value": 2048,
        "label": " ESCUELA RADIMADI"
    },
    2049: {
        "value": 2049,
        "label": " ESCUELA PRESvalueENTE JORGE ALESSANDRI R."
    },
    2050: {
        "value": 2050,
        "label": " ESCUELA EL MAITEN"
    },
    2051: {
        "value": 2051,
        "label": " LICEO RECTOR ABDON ANDRADE COLOMA"
    },
    2052: {
        "value": 2052,
        "label": " COLEGIO DE CULTURA Y DIFUSION ARTISTICA"
    },
    2053: {
        "value": 2053,
        "label": " ESC.DIFERENC.VILLA SAN JOSE DE LA UNION"
    },
    2054: {
        "value": 2054,
        "label": " COLEGIO TECNICO PROFESIONAL N°1 HONORIO OJEDA VALDERAS"
    },
    2055: {
        "value": 2055,
        "label": " ESCUELA RURAL PUERTO NUEVO"
    },
    2056: {
        "value": 2056,
        "label": " ESCUELA RURAL ANTILHUE"
    },
    2057: {
        "value": 2057,
        "label": " ESCUELA RURAL FOLILCO"
    },
    2058: {
        "value": 2058,
        "label": " ESCUELA FRANCIA"
    },
    2059: {
        "value": 2059,
        "label": " ESCUELA NUEVA COLLILELFU"
    },
    2060: {
        "value": 2060,
        "label": " ESCUELA FRANCIA LOCAL: 2"
    },
    2061: {
        "value": 2061,
        "label": " GIMNASIO MUNICIPAL"
    },
    2062: {
        "value": 2062,
        "label": " ESCUELA RURAL ENRIQUE HEVIA LABBE"
    },
    2063: {
        "value": 2063,
        "label": " LICEO SANTO CURA DE ARS"
    },
    2064: {
        "value": 2064,
        "label": " LICEO GABRIELA MISTRAL"
    },
    2065: {
        "value": 2065,
        "label": " ESCUELA FRAY BERNABE DE LUCERNA"
    },
    2066: {
        "value": 2066,
        "label": " LICEO SAN LUIS DE ALBA"
    },
    2067: {
        "value": 2067,
        "label": " COLEGIO SAN JOSE"
    },
    2068: {
        "value": 2068,
        "label": " ESCUELA VALLE DE MARIQUINA"
    },
    2069: {
        "value": 2069,
        "label": " LICEO POLITECNICO PESQUERO"
    },
    2070: {
        "value": 2070,
        "label": " ESCUELA RURAL JOSE ARNOLDO BILBAO"
    },
    2071: {
        "value": 2071,
        "label": " LICEO RODULFO AMANDO PHILIPPI"
    },
    2072: {
        "value": 2072,
        "label": " COLEGIO CARDENAL RAUL SILVA HENRIQUEZ"
    },
    2073: {
        "value": 2073,
        "label": " ESCUELA OLEGARIO MORALES OLIVA"
    },
    2074: {
        "value": 2074,
        "label": " ESCUELA PROYECTO DE FUTURO"
    },
    2075: {
        "value": 2075,
        "label": " ESCUELA NUEVA AURORA"
    },
    2076: {
        "value": 2076,
        "label": " ESCUELA ROBERTO OJEDA TORRES"
    },
    2077: {
        "value": 2077,
        "label": " ESCUELA RURAL LA RINCONADA"
    },
    2078: {
        "value": 2078,
        "label": " COL. DE ARTES CIENCIA Y TEC. DA VINCI"
    },
    2079: {
        "value": 2079,
        "label": " ESCUELA PART. PADRE ENRIQUE ROMER"
    },
    2080: {
        "value": 2080,
        "label": " ESCUELA PARTICULAR NUEVA LIQUINE"
    },
    2081: {
        "value": 2081,
        "label": " COMPLEJO EDUCACIONAL TIERRA DE ESPERANZA"
    },
    2082: {
        "value": 2082,
        "label": " CENTRO EDUCATIVO FERNANDO SANTIVAN"
    },
    2083: {
        "value": 2083,
        "label": " LICEO PADRE SIGISFREDO"
    },
    2084: {
        "value": 2084,
        "label": " ESCUELA MARIA ALVARADO GARAY"
    },
    2085: {
        "value": 2085,
        "label": " CENTRO EDUCACIONAL SAN SEBASTIAN"
    },
    2086: {
        "value": 2086,
        "label": " ESCUELA RURAL COLONIA DIUMEN"
    },
    2087: {
        "value": 2087,
        "label": " ESCUELA RURAL CRUCERO"
    },
    2088: {
        "value": 2088,
        "label": " ESCUELA RURAL VALLE MANTILHUE BAJO"
    },
    2089: {
        "value": 2089,
        "label": " LICEO TCO. PROF. DE RIO BUENO"
    },
    2090: {
        "value": 2090,
        "label": " ESCUELA PAMPA RIOS"
    },
    2091: {
        "value": 2091,
        "label": " ESCUELA BASICA RIO BUENO"
    },
    2092: {
        "value": 2092,
        "label": " ESCUELA BASICA PATRICIO LYNCH"
    },
    2093: {
        "value": 2093,
        "label": " LICEO VICENTE PEREZ ROSALES"
    },
    2094: {
        "value": 2094,
        "label": " ESCUELA LAS ANIMAS"
    },
    2095: {
        "value": 2095,
        "label": " ESCUELA DE NIEBLA JUAN BOSCH"
    },
    2096: {
        "value": 2096,
        "label": " LICEO BICENTENARIO CIUDAD DE LOS RIOS VALDIVI"
    },
    2097: {
        "value": 2097,
        "label": " LICEO SANTA MARIA LA BLANCA"
    },
    2098: {
        "value": 2098,
        "label": " LICEO SANTA MARIA LA BLANCA LOCAL: 2"
    },
    2099: {
        "value": 2099,
        "label": " ESCUELA CHILE"
    },
    2100: {
        "value": 2100,
        "label": " INSTITUTO COMERCIAL DE VALDIVIA"
    },
    2101: {
        "value": 2101,
        "label": " ESCUELA EL LAUREL"
    },
    2102: {
        "value": 2102,
        "label": " ESCUELA MEXICO"
    },
    2103: {
        "value": 2103,
        "label": " LICEO POLITECNICO BENJAMIN VICUNA MACKENNA"
    },
    2104: {
        "value": 2104,
        "label": " LICEO TECNICO VALDIVIA"
    },
    2105: {
        "value": 2105,
        "label": " ESCUELA EL BOSQUE"
    },
    2106: {
        "value": 2106,
        "label": " COLEGIO TENIENTE HERNAN MERINO CORREA"
    },
    2107: {
        "value": 2107,
        "label": " COLEGIO TENIENTE HERNAN MERINO CORREA LOCAL: 2"
    },
    2108: {
        "value": 2108,
        "label": " LICEO ARMANDO ROBLES RIVERA"
    },
    2109: {
        "value": 2109,
        "label": " LICEO INDUSTRIAL VALDIVIA LOCAL: 2"
    },
    2110: {
        "value": 2110,
        "label": " LICEO INDUSTRIAL VALDIVIA"
    },
    2111: {
        "value": 2111,
        "label": " INSTITUTO ITALIA DE VALDIVIA LOCAL: 2"
    },
    2112: {
        "value": 2112,
        "label": " INSTITUTO ITALIA DE VALDIVIA"
    },
    2113: {
        "value": 2113,
        "label": " ESCUELA ESPANA"
    },
    2114: {
        "value": 2114,
        "label": " ESCUELA FRANCIA"
    },
    2115: {
        "value": 2115,
        "label": " ESCUELA LEONARDO DA VINCI"
    },
    2116: {
        "value": 2116,
        "label": " LICEO INDUSTRIAL VALDIVIA LOCAL: 3"
    },
    2117: {
        "value": 2117,
        "label": " ESCUELA ESPANA LOCAL: 2"
    },
    2118: {
        "value": 2118,
        "label": " INST. SUPERIOR DE ADMINISTRACION Y TURISMO"
    },
    2119: {
        "value": 2119,
        "label": " ESCUELA FERNANDO SANTIVAN"
    },
    2120: {
        "value": 2120,
        "label": " COLEGIO INTEGRADO EDUARDO FREI MONTALVA"
    },
    2121: {
        "value": 2121,
        "label": " COLEGIO JUAN PABLO II"
    },
    2122: {
        "value": 2122,
        "label": " ESCUELA D-10 GRAL JOSE MIGUEL CARRERA"
    },
    2123: {
        "value": 2123,
        "label": " LICEO A- 5 JOVINA NARANJO FERNANDEZ"
    },
    2124: {
        "value": 2124,
        "label": " ESCUELA D-14 REGIMIENTO RANCAGUA"
    },
    2125: {
        "value": 2125,
        "label": " ESCUELA E-15 RICARDO SILVA ARRIAGADA"
    },
    2126: {
        "value": 2126,
        "label": " ESCUELA D-6 REPUBLICA DE FRANCIA"
    },
    2127: {
        "value": 2127,
        "label": " ESCUELA E-5 ESMERALDA"
    },
    2128: {
        "value": 2128,
        "label": " LICEO A-2 POLITECNICO ARICA"
    },
    2129: {
        "value": 2129,
        "label": " COLEGIO SAUCACHE"
    },
    2130: {
        "value": 2130,
        "label": " COLEGIO MIRAMAR"
    },
    2131: {
        "value": 2131,
        "label": " GIMNASIO AUGUSTO ZUBIRI (EX EPICENTRO I)"
    },
    2132: {
        "value": 2132,
        "label": " ESCUELA D-7 GRAL. PEDRO LAGOS MARCHANT"
    },
    2133: {
        "value": 2133,
        "label": " ESCUELA D-4 REPUBLICA DE ISRAEL"
    },
    2134: {
        "value": 2134,
        "label": " ESCUELA D-23 CARLOS GUIRAO MASSIF (EX-LINCOYAN)"
    },
    2135: {
        "value": 2135,
        "label": " UNIVERSvalueAD DE TARAPACA CAMPUS VELASQUEZ"
    },
    2136: {
        "value": 2136,
        "label": " ESCUELA JORGE ALESSANDRI RODRIGUEZ"
    },
    2137: {
        "value": 2137,
        "label": " COLEGIO D-91 CENTENARIO"
    },
    2138: {
        "value": 2138,
        "label": " LICEO PABLO NERUDA"
    },
    2139: {
        "value": 2139,
        "label": " LICEO INSTITUTO COMERCIAL DE ARICA"
    },
    2140: {
        "value": 2140,
        "label": " ESCUELA D-24 GABRIELA MISTRAL"
    },
    2141: {
        "value": 2141,
        "label": " LICEO AGRICOLA JOSE ABELARDO NUNEZ M."
    },
    2142: {
        "value": 2142,
        "label": " LICEO DOMINGO SANTA MARIA"
    },
    2143: {
        "value": 2143,
        "label": " ESC. D-18 HUMBERTO VALENZUELA GARCIA"
    },
    2144: {
        "value": 2144,
        "label": " ESCUELA D-21 TUCAPEL"
    },
    2145: {
        "value": 2145,
        "label": " LICEO POLI. B-4 ANTONIO VARAS DE LA BARRA"
    },
    2146: {
        "value": 2146,
        "label": " ESCUELA D-17 COMTE. JUAN JOSE SAN MARTIN"
    },
    2147: {
        "value": 2147,
        "label": " ESCUELA D-12 ROMULO J. PENA MATURANA"
    },
    2148: {
        "value": 2148,
        "label": " ESCUELA E-26 AMERICA"
    },
    2149: {
        "value": 2149,
        "label": " ESCUELA F-22 DR. RICARDO OLEA GUERRA"
    },
    2150: {
        "value": 2150,
        "label": " ESCUELA D-16 SUBTTE. LUIS CRUZ MARTINEZ"
    },
    2151: {
        "value": 2151,
        "label": " COLEGIO ALTA CORDILLERA"
    },
    2152: {
        "value": 2152,
        "label": " ESCUELA VALLE DE CUYA"
    },
    2153: {
        "value": 2153,
        "label": " LICEO VALLE DE CODPA"
    },
    2154: {
        "value": 2154,
        "label": " GIMNASIO TECHADO VISVIRI"
    },
    2155: {
        "value": 2155,
        "label": " ESCUELA SAN SANTIAGO DE BELEN"
    },
    2156: {
        "value": 2156,
        "label": " LICEO GRANADEROS"
    },
    2157: {
        "value": 2157,
        "label": " CONSULADO"
    },
    2158: {
        "value": 2158,
        "label": " CONSULADO"
    },
    2159: {
        "value": 2159,
        "label": " CONSULADO"
    },
    2160: {
        "value": 2160,
        "label": " CONSULADO"
    },
    2162: {
        "value": 2162,
        "label": " CONSULADO"
    },
    2164: {
        "value": 2164,
        "label": " CONSULADO HONORARIO DE CHILE EN ESQUEL"
    },
    2165: {
        "value": 2165,
        "label": " DIRECCION NACIONAL DE MIGRACIONES PABELLON 6"
    },
    2169: {
        "value": 2169,
        "label": " ASOC. CIVIL CENTRO DE RESvalueENTES CHILENOS DE COMODORO RIVADAVIA"
    },
    2170: {
        "value": 2170,
        "label": " CENTRO DE RESvalueENTES CHILENOS VALLE DEL CHUBUT"
    },
    2171: {
        "value": 2171,
        "label": " CONSULADO"
    },
    2172: {
        "value": 2172,
        "label": " ESCUELA DE EDUCACION PRIMARIA N1"
    },
    2173: {
        "value": 2173,
        "label": " ESCUELA PRESvalueENTE QUINTANA"
    },
    2176: {
        "value": 2176,
        "label": " ESCUELA SUPERIOR SARMIENTO"
    },
    2177: {
        "value": 2177,
        "label": " CENTRO CULTURAL MUNICIPAL JOSÉ LA VÍA EX ESTACION DE TRENES DE SAN LUIS"
    },
    2178: {
        "value": 2178,
        "label": " CONSULADO"
    },
    2182: {
        "value": 2182,
        "label": " CONSULADO"
    },
    2183: {
        "value": 2183,
        "label": " CONSULADO"
    },
    2184: {
        "value": 2184,
        "label": " COLEGIO SALESIANO SAN JOSÉ"
    },
    2185: {
        "value": 2185,
        "label": " CONSULADO"
    },
    2186: {
        "value": 2186,
        "label": " CONSULADO"
    },
    2187: {
        "value": 2187,
        "label": " CONSULADO"
    },
    2188: {
        "value": 2188,
        "label": " CENTRO BOLIVIANO AMERICANO DE SANTA CRUZ"
    },
    2189: {
        "value": 2189,
        "label": " CONSULADO"
    },
    2190: {
        "value": 2190,
        "label": " CONSULADO"
    },
    2191: {
        "value": 2191,
        "label": " CONSULADO"
    },
    2192: {
        "value": 2192,
        "label": " CONSULADO"
    },
    2195: {
        "value": 2195,
        "label": " CONSULADO"
    },
    2198: {
        "value": 2198,
        "label": " CONSULADO"
    },
    2199: {
        "value": 2199,
        "label": " 2 BLOOR STREET WEST 1ST FLOOR"
    },
    2201: {
        "value": 2201,
        "label": " HOLvalueAY INN HOTEL"
    },
    2202: {
        "value": 2202,
        "label": " COAST PLAZA HOTEL AND CONFERENCE CENTRE"
    },
    2203: {
        "value": 2203,
        "label": " HOLvalueAY INN HOTEL"
    },
    2204: {
        "value": 2204,
        "label": " CONSULADO"
    },
    2205: {
        "value": 2205,
        "label": " CONSULADO"
    },
    2206: {
        "value": 2206,
        "label": " CONSULADO"
    },
    2207: {
        "value": 2207,
        "label": " UNIVERSvalueAD CASA GRANDE"
    },
    2208: {
        "value": 2208,
        "label": " CONSULADO"
    },
    2209: {
        "value": 2209,
        "label": " CONSULADO"
    },
    2210: {
        "value": 2210,
        "label": " ST. AUGUSTINE COLLEGUE"
    },
    2212: {
        "value": 2212,
        "label": " CONSULADO"
    },
    2213: {
        "value": 2213,
        "label": " CONSULADO"
    },
    2215: {
        "value": 2215,
        "label": " JORDAN HIGH SCHOOL"
    },
    2216: {
        "value": 2216,
        "label": " CONSULADO"
    },
    2220: {
        "value": 2220,
        "label": " CATHEDRAL HIGH SCHOOL"
    },
    2224: {
        "value": 2224,
        "label": " SAN FRANCISCO CITY COLLEGE"
    },
    2226: {
        "value": 2226,
        "label": " CENTRO DE LA RAZA 2524"
    },
    2227: {
        "value": 2227,
        "label": " CONSULADO"
    },
    2229: {
        "value": 2229,
        "label": " CONSULADO"
    },
    2230: {
        "value": 2230,
        "label": " CONSULADO"
    },
    2231: {
        "value": 2231,
        "label": " EMBAJADA DE CHILE EN TEGUCIGALPA"
    },
    2232: {
        "value": 2232,
        "label": " CONSULADO"
    },
    2234: {
        "value": 2234,
        "label": " CONSULADO"
    },
    2235: {
        "value": 2235,
        "label": " CONSULADO"
    },
    2236: {
        "value": 2236,
        "label": " CONSULADO"
    },
    2237: {
        "value": 2237,
        "label": " CONSULADO"
    },
    2239: {
        "value": 2239,
        "label": " CONSULADO"
    },
    2240: {
        "value": 2240,
        "label": " CONSULADO"
    },
    2241: {
        "value": 2241,
        "label": " CONSULADO"
    },
    2242: {
        "value": 2242,
        "label": " CONSULADO"
    },
    2244: {
        "value": 2244,
        "label": " CONSULADO"
    },
    2245: {
        "value": 2245,
        "label": " CONSULADO"
    },
    2246: {
        "value": 2246,
        "label": " CONSULADO"
    },
    2247: {
        "value": 2247,
        "label": " CONSULADO"
    },
    2248: {
        "value": 2248,
        "label": " CONSULADO"
    },
    2249: {
        "value": 2249,
        "label": " CONSULADO"
    },
    2250: {
        "value": 2250,
        "label": " CONSULADO"
    },
    2251: {
        "value": 2251,
        "label": " CONSULADO"
    },
    2252: {
        "value": 2252,
        "label": " CONSULADO"
    },
    2253: {
        "value": 2253,
        "label": " CONSULADO"
    },
    2254: {
        "value": 2254,
        "label": " CONSULADO"
    },
    2255: {
        "value": 2255,
        "label": " CONSULADO"
    },
    2256: {
        "value": 2256,
        "label": " CONSULADO"
    },
    2257: {
        "value": 2257,
        "label": " CONSULADO"
    },
    2258: {
        "value": 2258,
        "label": " CONSULADO"
    },
    2259: {
        "value": 2259,
        "label": " CONSULADO"
    },
    2260: {
        "value": 2260,
        "label": " CONSULADO"
    },
    2261: {
        "value": 2261,
        "label": " CONSULADO"
    },
    2263: {
        "value": 2263,
        "label": " CONSULADO"
    },
    2264: {
        "value": 2264,
        "label": " CONSULADO"
    },
    2265: {
        "value": 2265,
        "label": " CONSULADO"
    },
    2266: {
        "value": 2266,
        "label": " CONSULADO"
    },
    2267: {
        "value": 2267,
        "label": " CONSULADO"
    },
    2269: {
        "value": 2269,
        "label": " CONSULADO"
    },
    2270: {
        "value": 2270,
        "label": " CONSULADO"
    },
    2271: {
        "value": 2271,
        "label": " ESCUELA DE ADMINISTRACION PUBLICA DE CATALUNA"
    },
    2277: {
        "value": 2277,
        "label": " CONSULADO"
    },
    2281: {
        "value": 2281,
        "label": " CONSULADO"
    },
    2282: {
        "value": 2282,
        "label": " CONSULADO"
    },
    2283: {
        "value": 2283,
        "label": " CONSULADO"
    },
    2287: {
        "value": 2287,
        "label": " CONSULADO"
    },
    2288: {
        "value": 2288,
        "label": " CONSULADO"
    },
    2289: {
        "value": 2289,
        "label": " CONSULADO"
    },
    2290: {
        "value": 2290,
        "label": " CONSULADO"
    },
    2291: {
        "value": 2291,
        "label": " CONSULADO"
    },
    2292: {
        "value": 2292,
        "label": " CONSULADO"
    },
    2293: {
        "value": 2293,
        "label": " CASA MIGRANTE"
    },
    2295: {
        "value": 2295,
        "label": " CONSULADO"
    },
    2296: {
        "value": 2296,
        "label": " FACULTADE DE CIENCIAS SOCIAIS E HUMANAS UNIVERSvalueADE NOVA DE LISBOA"
    },
    2297: {
        "value": 2297,
        "label": " CONSULADO"
    },
    2300: {
        "value": 2300,
        "label": " CONSULADO"
    },
    2301: {
        "value": 2301,
        "label": " CONSULADO"
    },
    2302: {
        "value": 2302,
        "label": " CLARION HOTEL STOCKHOLM"
    },
    2305: {
        "value": 2305,
        "label": " CONSULADO"
    },
    2307: {
        "value": 2307,
        "label": " CONSULADO"
    },
    2309: {
        "value": 2309,
        "label": " CONSULADO"
    },
    2310: {
        "value": 2310,
        "label": " CONSULADO"
    },
    2311: {
        "value": 2311,
        "label": " BAYVIEW HOTEL ON THE PARK 52"
    },
    2313: {
        "value": 2313,
        "label": " BANKS RESERVE PAVILLION"
    },
    2314: {
        "value": 2314,
        "label": " CONSULADO"
    },
    2317: {
        "value": 2317,
        "label": " CONSULADO"
    },
}

export default locales;