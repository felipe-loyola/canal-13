var comunas = [
    {
        "value": 10301,
        "label": "IQUIQUE"
      },
      {
        "value": 10302,
        "label": "ALTO HOSPICIO"
      },
      {
        "value": 12001,
        "label": "BERLIN"
      },
      {
        "value": 12002,
        "label": "FRANKFURT"
      },
      {
        "value": 12003,
        "label": "HAMBURGO"
      },
      {
        "value": 12004,
        "label": "MUNICH"
      },
      {
        "value": 12006,
        "label": "BAHIA BLANCA"
      },
      {
        "value": 12007,
        "label": "BARILOCHE"
      },
      {
        "value": 12008,
        "label": "BUENOS AIRES"
      },
      {
        "value": 12009,
        "label": "COMODORO RIVADAVIA"
      },
      {
        "value": 12010,
        "label": "CORDOBA"
      },
      {
        "value": 12011,
        "label": "ESQUEL"
      },
      {
        "value": 12012,
        "label": "MAR DEL PLATA"
      },
      {
        "value": 12013,
        "label": "MENDOZA"
      },
      {
        "value": 12014,
        "label": "NEUQUEN"
      },
      {
        "value": 12015,
        "label": "RIO GALLEGOS"
      },
      {
        "value": 12016,
        "label": "RIO GRANDE"
      },
      {
        "value": 12017,
        "label": "ROSARIO"
      },
      {
        "value": 12018,
        "label": "SALTA"
      },
      {
        "value": 12019,
        "label": "SAN JUAN"
      },
      {
        "value": 12020,
        "label": "SAN LUIS"
      },
      {
        "value": 12021,
        "label": "TRELEW"
      },
      {
        "value": 12022,
        "label": "USHUAIA"
      },
      {
        "value": 12023,
        "label": "CANBERRA"
      },
      {
        "value": 12024,
        "label": "MELBOURNE"
      },
      {
        "value": 12025,
        "label": "PERTH"
      },
      {
        "value": 12026,
        "label": "SYDNEY"
      },
      {
        "value": 12027,
        "label": "VIENA"
      },
      {
        "value": 12028,
        "label": "BRUSELAS"
      },
      {
        "value": 12029,
        "label": "LA PAZ"
      },
      {
        "value": 12030,
        "label": "SANTA CRUZ"
      },
      {
        "value": 12031,
        "label": "BRASILIA"
      },
      {
        "value": 12032,
        "label": "PORTO ALEGRE"
      },
      {
        "value": 12033,
        "label": "RIO DE JANEIRO"
      },
      {
        "value": 12034,
        "label": "SAO PAULO"
      },
      {
        "value": 12035,
        "label": "BUCAREST"
      },
      {
        "value": 12036,
        "label": "CALGARY"
      },
      {
        "value": 12037,
        "label": "MONTREAL"
      },
      {
        "value": 12038,
        "label": "OTTAWA"
      },
      {
        "value": 12039,
        "label": "TORONTO"
      },
      {
        "value": 12040,
        "label": "VANCOUVER"
      },
      {
        "value": 12041,
        "label": "WINNIPEG"
      },
      {
        "value": 12042,
        "label": "BEIJING"
      },
      {
        "value": 12043,
        "label": "GUANGZHOU"
      },
      {
        "value": 12044,
        "label": "HONG KONG"
      },
      {
        "value": 12045,
        "label": "SHANGHAI"
      },
      {
        "value": 12046,
        "label": "BOGOTA"
      },
      {
        "value": 12047,
        "label": "SAN JOSE"
      },
      {
        "value": 12048,
        "label": "ZAGREB"
      },
      {
        "value": 12049,
        "label": "LA HABANA"
      },
      {
        "value": 12050,
        "label": "COPENHAGUE"
      },
      {
        "value": 12051,
        "label": "GUAYAQUIL"
      },
      {
        "value": 12052,
        "label": "QUITO"
      },
      {
        "value": 12053,
        "label": "EL CAIRO"
      },
      {
        "value": 12054,
        "label": "SAN SALVADOR"
      },
      {
        "value": 12055,
        "label": "ABU DHABI"
      },
      {
        "value": 12056,
        "label": "BARCELONA"
      },
      {
        "value": 12057,
        "label": "MADRvalue"
      },
      {
        "value": 12058,
        "label": "CHICAGO"
      },
      {
        "value": 12059,
        "label": "HOUSTON"
      },
      {
        "value": 12060,
        "label": "LOS ANGELES"
      },
      {
        "value": 12061,
        "label": "MIAMI"
      },
      {
        "value": 12062,
        "label": "NUEVA YORK"
      },
      {
        "value": 12063,
        "label": "SALT LAKE CITY"
      },
      {
        "value": 12064,
        "label": "SAN FRANCISCO"
      },
      {
        "value": 12065,
        "label": "SEATTLE"
      },
      {
        "value": 12066,
        "label": "WASHINGTON"
      },
      {
        "value": 12068,
        "label": "MANILA"
      },
      {
        "value": 12069,
        "label": "HELSINKI"
      },
      {
        "value": 12070,
        "label": "PARIS"
      },
      {
        "value": 12072,
        "label": "ATENAS"
      },
      {
        "value": 12073,
        "label": "GUATEMALA"
      },
      {
        "value": 12075,
        "label": "PUERTO PRINCIPE"
      },
      {
        "value": 12076,
        "label": "TEGUCIGALPA"
      },
      {
        "value": 12077,
        "label": "BUDAPEST"
      },
      {
        "value": 12078,
        "label": "NUEVA DELHI"
      },
      {
        "value": 12079,
        "label": "JAKARTA"
      },
      {
        "value": 12081,
        "label": "DUBLIN"
      },
      {
        "value": 12082,
        "label": "TEL AVIV"
      },
      {
        "value": 12083,
        "label": "MILAN"
      },
      {
        "value": 12084,
        "label": "ROMA"
      },
      {
        "value": 12086,
        "label": "TOKIO"
      },
      {
        "value": 12087,
        "label": "AMMAN"
      },
      {
        "value": 12088,
        "label": "NAIROBI"
      },
      {
        "value": 12089,
        "label": "BEIRUT"
      },
      {
        "value": 12090,
        "label": "KUALA LUMPUR"
      },
      {
        "value": 12092,
        "label": "CIUDAD DE MEXICO"
      },
      {
        "value": 12093,
        "label": "MANAGUA"
      },
      {
        "value": 12094,
        "label": "OSLO"
      },
      {
        "value": 12095,
        "label": "WELLINGTON"
      },
      {
        "value": 12096,
        "label": "AMSTERDAM"
      },
      {
        "value": 12098,
        "label": "CIUDAD DE PANAMA"
      },
      {
        "value": 12099,
        "label": "ASUNCION"
      },
      {
        "value": 12100,
        "label": "LIMA"
      },
      {
        "value": 12101,
        "label": "TACNA"
      },
      {
        "value": 12102,
        "label": "VARSOVIA"
      },
      {
        "value": 12103,
        "label": "LISBOA"
      },
      {
        "value": 12104,
        "label": "LONDRES"
      },
      {
        "value": 12105,
        "label": "PRAGA"
      },
      {
        "value": 12106,
        "label": "SEUL"
      },
      {
        "value": 12107,
        "label": "SANTO DOMINGO"
      },
      {
        "value": 12108,
        "label": "MOSCU"
      },
      {
        "value": 12109,
        "label": "SINGAPUR"
      },
      {
        "value": 12111,
        "label": "PRETORIA"
      },
      {
        "value": 12112,
        "label": "ESTOCOLMO"
      },
      {
        "value": 12113,
        "label": "GOTEMBURGO"
      },
      {
        "value": 12114,
        "label": "BERNA"
      },
      {
        "value": 12115,
        "label": "BANGKOK"
      },
      {
        "value": 12117,
        "label": "ANKARA"
      },
      {
        "value": 12118,
        "label": "MONTEVvalueEO"
      },
      {
        "value": 12119,
        "label": "CARACAS"
      },
      {
        "value": 12120,
        "label": "PUERTO ORDAZ"
      },
      {
        "value": 15201,
        "label": "HUARA"
      },
      {
        "value": 15202,
        "label": "CAMINA"
      },
      {
        "value": 15203,
        "label": "COLCHANE"
      },
      {
        "value": 15204,
        "label": "PICA"
      },
      {
        "value": 15205,
        "label": "POZO ALMONTE"
      },
      {
        "value": 20401,
        "label": "TOCOPILLA"
      },
      {
        "value": 20402,
        "label": "MARIA ELENA"
      },
      {
        "value": 20501,
        "label": "CALAMA"
      },
      {
        "value": 20502,
        "label": "OLLAGUE"
      },
      {
        "value": 20503,
        "label": "SAN PEDRO DE ATACAMA"
      },
      {
        "value": 20601,
        "label": "ANTOFAGASTA"
      },
      {
        "value": 20602,
        "label": "MEJILLONES"
      },
      {
        "value": 20603,
        "label": "SIERRA GORDA"
      },
      {
        "value": 20604,
        "label": "TALTAL"
      },
      {
        "value": 30701,
        "label": "CHANARAL"
      },
      {
        "value": 30702,
        "label": "DIEGO DE ALMAGRO"
      },
      {
        "value": 30801,
        "label": "COPIAPO"
      },
      {
        "value": 30802,
        "label": "CALDERA"
      },
      {
        "value": 30803,
        "label": "TIERRA AMARILLA"
      },
      {
        "value": 30901,
        "label": "VALLENAR"
      },
      {
        "value": 30902,
        "label": "FREIRINA"
      },
      {
        "value": 30903,
        "label": "HUASCO"
      },
      {
        "value": 30904,
        "label": "ALTO DEL CARMEN"
      },
      {
        "value": 41001,
        "label": "LA SERENA"
      },
      {
        "value": 41002,
        "label": "LA HIGUERA"
      },
      {
        "value": 41003,
        "label": "COQUIMBO"
      },
      {
        "value": 41004,
        "label": "ANDACOLLO"
      },
      {
        "value": 41005,
        "label": "VICUNA"
      },
      {
        "value": 41006,
        "label": "PAIHUANO"
      },
      {
        "value": 41101,
        "label": "OVALLE"
      },
      {
        "value": 41102,
        "label": "RIO HURTADO"
      },
      {
        "value": 41103,
        "label": "MONTE PATRIA"
      },
      {
        "value": 41104,
        "label": "COMBARBALA"
      },
      {
        "value": 41105,
        "label": "PUNITAQUI"
      },
      {
        "value": 41201,
        "label": "ILLAPEL"
      },
      {
        "value": 41202,
        "label": "SALAMANCA"
      },
      {
        "value": 41203,
        "label": "LOS VILOS"
      },
      {
        "value": 41204,
        "label": "CANELA"
      },
      {
        "value": 51301,
        "label": "LA LIGUA"
      },
      {
        "value": 51302,
        "label": "PETORCA"
      },
      {
        "value": 51303,
        "label": "CABILDO"
      },
      {
        "value": 51304,
        "label": "ZAPALLAR"
      },
      {
        "value": 51305,
        "label": "PAPUDO"
      },
      {
        "value": 51401,
        "label": "LOS ANDES"
      },
      {
        "value": 51402,
        "label": "SAN ESTEBAN"
      },
      {
        "value": 51403,
        "label": "CALLE LARGA"
      },
      {
        "value": 51404,
        "label": "RINCONADA"
      },
      {
        "value": 51501,
        "label": "SAN FELIPE"
      },
      {
        "value": 51502,
        "label": "PUTAENDO"
      },
      {
        "value": 51503,
        "label": "SANTA MARIA"
      },
      {
        "value": 51504,
        "label": "PANQUEHUE"
      },
      {
        "value": 51505,
        "label": "LLAY-LLAY"
      },
      {
        "value": 51506,
        "label": "CATEMU"
      },
      {
        "value": 51601,
        "label": "QUILLOTA"
      },
      {
        "value": 51602,
        "label": "LA CRUZ"
      },
      {
        "value": 51603,
        "label": "CALERA"
      },
      {
        "value": 51604,
        "label": "NOGALES"
      },
      {
        "value": 51605,
        "label": "HIJUELAS"
      },
      {
        "value": 51701,
        "label": "VALPARAISO"
      },
      {
        "value": 51702,
        "label": "VINA DEL MAR"
      },
      {
        "value": 51703,
        "label": "QUINTERO"
      },
      {
        "value": 51704,
        "label": "PUCHUNCAVI"
      },
      {
        "value": 51707,
        "label": "CASABLANCA"
      },
      {
        "value": 51708,
        "label": "JUAN FERNANDEZ"
      },
      {
        "value": 51709,
        "label": "CONCON"
      },
      {
        "value": 51801,
        "label": "SAN ANTONIO"
      },
      {
        "value": 51802,
        "label": "CARTAGENA"
      },
      {
        "value": 51803,
        "label": "EL TABO"
      },
      {
        "value": 51804,
        "label": "EL QUISCO"
      },
      {
        "value": 51805,
        "label": "ALGARROBO"
      },
      {
        "value": 51806,
        "label": "SANTO DOMINGO"
      },
      {
        "value": 51901,
        "label": "ISLA DE PASCUA"
      },
      {
        "value": 55401,
        "label": "QUILPUE"
      },
      {
        "value": 55402,
        "label": "VILLA ALEMANA"
      },
      {
        "value": 55403,
        "label": "LIMACHE"
      },
      {
        "value": 55404,
        "label": "OLMUE"
      },
      {
        "value": 62601,
        "label": "RANCAGUA"
      },
      {
        "value": 62602,
        "label": "GRANEROS"
      },
      {
        "value": 62603,
        "label": "MOSTAZAL"
      },
      {
        "value": 62604,
        "label": "CODEGUA"
      },
      {
        "value": 62605,
        "label": "MACHALI"
      },
      {
        "value": 62606,
        "label": "OLIVAR"
      },
      {
        "value": 62607,
        "label": "REQUINOA"
      },
      {
        "value": 62608,
        "label": "RENGO"
      },
      {
        "value": 62609,
        "label": "MALLOA"
      },
      {
        "value": 62610,
        "label": "QUINTA DE TILCOCO"
      },
      {
        "value": 62611,
        "label": "SAN VICENTE"
      },
      {
        "value": 62612,
        "label": "PICHvalueEGUA"
      },
      {
        "value": 62613,
        "label": "PEUMO"
      },
      {
        "value": 62614,
        "label": "COLTAUCO"
      },
      {
        "value": 62615,
        "label": "COINCO"
      },
      {
        "value": 62616,
        "label": "DONIHUE"
      },
      {
        "value": 62617,
        "label": "LAS CABRAS"
      },
      {
        "value": 62701,
        "label": "SAN FERNANDO"
      },
      {
        "value": 62702,
        "label": "CHIMBARONGO"
      },
      {
        "value": 62703,
        "label": "PLACILLA"
      },
      {
        "value": 62704,
        "label": "NANCAGUA"
      },
      {
        "value": 62705,
        "label": "CHEPICA"
      },
      {
        "value": 62706,
        "label": "SANTA CRUZ"
      },
      {
        "value": 62707,
        "label": "LOLOL"
      },
      {
        "value": 62708,
        "label": "PUMANQUE"
      },
      {
        "value": 62709,
        "label": "PALMILLA"
      },
      {
        "value": 62710,
        "label": "PERALILLO"
      },
      {
        "value": 62801,
        "label": "PICHILEMU"
      },
      {
        "value": 62802,
        "label": "NAVvalueAD"
      },
      {
        "value": 62803,
        "label": "LITUECHE"
      },
      {
        "value": 62804,
        "label": "LA ESTRELLA"
      },
      {
        "value": 62805,
        "label": "MARCHIGUE"
      },
      {
        "value": 62806,
        "label": "PAREDONES"
      },
      {
        "value": 72901,
        "label": "CURICO"
      },
      {
        "value": 72902,
        "label": "TENO"
      },
      {
        "value": 72903,
        "label": "ROMERAL"
      },
      {
        "value": 72904,
        "label": "MOLINA"
      },
      {
        "value": 72905,
        "label": "SAGRADA FAMILIA"
      },
      {
        "value": 72906,
        "label": "HUALANE"
      },
      {
        "value": 72907,
        "label": "LICANTEN"
      },
      {
        "value": 72908,
        "label": "VICHUQUEN"
      },
      {
        "value": 72909,
        "label": "RAUCO"
      },
      {
        "value": 73001,
        "label": "TALCA"
      },
      {
        "value": 73002,
        "label": "PELARCO"
      },
      {
        "value": 73003,
        "label": "RIO CLARO"
      },
      {
        "value": 73004,
        "label": "SAN CLEMENTE"
      },
      {
        "value": 73005,
        "label": "MAULE"
      },
      {
        "value": 73006,
        "label": "EMPEDRADO"
      },
      {
        "value": 73007,
        "label": "PENCAHUE"
      },
      {
        "value": 73008,
        "label": "CONSTITUCION"
      },
      {
        "value": 73009,
        "label": "CUREPTO"
      },
      {
        "value": 73010,
        "label": "SAN RAFAEL"
      },
      {
        "value": 73101,
        "label": "LINARES"
      },
      {
        "value": 73102,
        "label": "YERBAS BUENAS"
      },
      {
        "value": 73103,
        "label": "COLBUN"
      },
      {
        "value": 73104,
        "label": "LONGAVI"
      },
      {
        "value": 73105,
        "label": "PARRAL"
      },
      {
        "value": 73106,
        "label": "RETIRO"
      },
      {
        "value": 73107,
        "label": "VILLA ALEGRE"
      },
      {
        "value": 73108,
        "label": "SAN JAVIER"
      },
      {
        "value": 73201,
        "label": "CAUQUENES"
      },
      {
        "value": 73202,
        "label": "PELLUHUE"
      },
      {
        "value": 73203,
        "label": "CHANCO"
      },
      {
        "value": 83301,
        "label": "CHILLAN"
      },
      {
        "value": 83302,
        "label": "SAN CARLOS"
      },
      {
        "value": 83303,
        "label": "NIQUEN"
      },
      {
        "value": 83304,
        "label": "SAN FABIAN"
      },
      {
        "value": 83305,
        "label": "COIHUECO"
      },
      {
        "value": 83306,
        "label": "PINTO"
      },
      {
        "value": 83307,
        "label": "SAN IGNACIO"
      },
      {
        "value": 83308,
        "label": "EL CARMEN"
      },
      {
        "value": 83309,
        "label": "YUNGAY"
      },
      {
        "value": 83310,
        "label": "PEMUCO"
      },
      {
        "value": 83311,
        "label": "BULNES"
      },
      {
        "value": 83312,
        "label": "QUILLON"
      },
      {
        "value": 83313,
        "label": "RANQUIL"
      },
      {
        "value": 83314,
        "label": "PORTEZUELO"
      },
      {
        "value": 83315,
        "label": "COELEMU"
      },
      {
        "value": 83316,
        "label": "TREGUACO"
      },
      {
        "value": 83317,
        "label": "COBQUECURA"
      },
      {
        "value": 83318,
        "label": "QUIRIHUE"
      },
      {
        "value": 83319,
        "label": "NINHUE"
      },
      {
        "value": 83320,
        "label": "SAN NICOLAS"
      },
      {
        "value": 83321,
        "label": "CHILLAN VIEJO"
      },
      {
        "value": 83401,
        "label": "CONCEPCION"
      },
      {
        "value": 83402,
        "label": "TALCAHUANO"
      },
      {
        "value": 83403,
        "label": "PENCO"
      },
      {
        "value": 83404,
        "label": "TOME"
      },
      {
        "value": 83405,
        "label": "FLORvalueA"
      },
      {
        "value": 83406,
        "label": "HUALQUI"
      },
      {
        "value": 83407,
        "label": "SANTA JUANA"
      },
      {
        "value": 83408,
        "label": "LOTA"
      },
      {
        "value": 83409,
        "label": "CORONEL"
      },
      {
        "value": 83410,
        "label": "SAN PEDRO DE LA PAZ"
      },
      {
        "value": 83411,
        "label": "CHIGUAYANTE"
      },
      {
        "value": 83412,
        "label": "HUALPEN"
      },
      {
        "value": 83501,
        "label": "LOS ANGELES"
      },
      {
        "value": 83502,
        "label": "CABRERO"
      },
      {
        "value": 83503,
        "label": "TUCAPEL"
      },
      {
        "value": 83504,
        "label": "ANTUCO"
      },
      {
        "value": 83505,
        "label": "QUILLECO"
      },
      {
        "value": 83506,
        "label": "SANTA BARBARA"
      },
      {
        "value": 83507,
        "label": "QUILACO"
      },
      {
        "value": 83508,
        "label": "MULCHEN"
      },
      {
        "value": 83509,
        "label": "NEGRETE"
      },
      {
        "value": 83510,
        "label": "NACIMIENTO"
      },
      {
        "value": 83511,
        "label": "LAJA"
      },
      {
        "value": 83512,
        "label": "SAN ROSENDO"
      },
      {
        "value": 83513,
        "label": "YUMBEL"
      },
      {
        "value": 83514,
        "label": "ALTO BIOBIO"
      },
      {
        "value": 83601,
        "label": "LEBU"
      },
      {
        "value": 83602,
        "label": "ARAUCO"
      },
      {
        "value": 83603,
        "label": "CURANILAHUE"
      },
      {
        "value": 83604,
        "label": "LOS ALAMOS"
      },
      {
        "value": 83605,
        "label": "CANETE"
      },
      {
        "value": 83606,
        "label": "CONTULMO"
      },
      {
        "value": 83607,
        "label": "TIRUA"
      },
      {
        "value": 93701,
        "label": "ANGOL"
      },
      {
        "value": 93702,
        "label": "RENAICO"
      },
      {
        "value": 93703,
        "label": "COLLIPULLI"
      },
      {
        "value": 93704,
        "label": "LONQUIMAY"
      },
      {
        "value": 93705,
        "label": "CURACAUTIN"
      },
      {
        "value": 93706,
        "label": "ERCILLA"
      },
      {
        "value": 93707,
        "label": "VICTORIA"
      },
      {
        "value": 93708,
        "label": "TRAIGUEN"
      },
      {
        "value": 93709,
        "label": "LUMACO"
      },
      {
        "value": 93710,
        "label": "PUREN"
      },
      {
        "value": 93711,
        "label": "LOS SAUCES"
      },
      {
        "value": 93801,
        "label": "TEMUCO"
      },
      {
        "value": 93802,
        "label": "LAUTARO"
      },
      {
        "value": 93803,
        "label": "PERQUENCO"
      },
      {
        "value": 93804,
        "label": "VILCUN"
      },
      {
        "value": 93805,
        "label": "CUNCO"
      },
      {
        "value": 93806,
        "label": "MELIPEUCO"
      },
      {
        "value": 93807,
        "label": "CURARREHUE"
      },
      {
        "value": 93808,
        "label": "PUCON"
      },
      {
        "value": 93809,
        "label": "VILLARRICA"
      },
      {
        "value": 93810,
        "label": "FREIRE"
      },
      {
        "value": 93811,
        "label": "PITRUFQUEN"
      },
      {
        "value": 93812,
        "label": "GORBEA"
      },
      {
        "value": 93813,
        "label": "LONCOCHE"
      },
      {
        "value": 93814,
        "label": "TOLTEN"
      },
      {
        "value": 93815,
        "label": "TEODORO SCHMvalueT"
      },
      {
        "value": 93816,
        "label": "SAAVEDRA"
      },
      {
        "value": 93817,
        "label": "CARAHUE"
      },
      {
        "value": 93818,
        "label": "NUEVA IMPERIAL"
      },
      {
        "value": 93819,
        "label": "GALVARINO"
      },
      {
        "value": 93820,
        "label": "PADRE LAS CASAS"
      },
      {
        "value": 93821,
        "label": "CHOLCHOL"
      },
      {
        "value": 104001,
        "label": "OSORNO"
      },
      {
        "value": 104002,
        "label": "SAN PABLO"
      },
      {
        "value": 104003,
        "label": "PUYEHUE"
      },
      {
        "value": 104004,
        "label": "PUERTO OCTAY"
      },
      {
        "value": 104005,
        "label": "PURRANQUE"
      },
      {
        "value": 104006,
        "label": "RIO NEGRO"
      },
      {
        "value": 104007,
        "label": "SAN JUAN DE LA COSTA"
      },
      {
        "value": 104101,
        "label": "PUERTO MONTT"
      },
      {
        "value": 104102,
        "label": "PUERTO VARAS"
      },
      {
        "value": 104103,
        "label": "COCHAMO"
      },
      {
        "value": 104104,
        "label": "CALBUCO"
      },
      {
        "value": 104105,
        "label": "MAULLIN"
      },
      {
        "value": 104106,
        "label": "LOS MUERMOS"
      },
      {
        "value": 104107,
        "label": "FRESIA"
      },
      {
        "value": 104108,
        "label": "LLANQUIHUE"
      },
      {
        "value": 104109,
        "label": "FRUTILLAR"
      },
      {
        "value": 104201,
        "label": "CASTRO"
      },
      {
        "value": 104202,
        "label": "ANCUD"
      },
      {
        "value": 104203,
        "label": "QUEMCHI"
      },
      {
        "value": 104204,
        "label": "DALCAHUE"
      },
      {
        "value": 104205,
        "label": "CURACO DE VELEZ"
      },
      {
        "value": 104206,
        "label": "QUINCHAO"
      },
      {
        "value": 104207,
        "label": "PUQUELDON"
      },
      {
        "value": 104208,
        "label": "CHONCHI"
      },
      {
        "value": 104209,
        "label": "QUEILEN"
      },
      {
        "value": 104210,
        "label": "QUELLON"
      },
      {
        "value": 104301,
        "label": "CHAITEN"
      },
      {
        "value": 104302,
        "label": "HUALAIHUE"
      },
      {
        "value": 104303,
        "label": "FUTALEUFU"
      },
      {
        "value": 104304,
        "label": "PALENA"
      },
      {
        "value": 114401,
        "label": "COYHAIQUE"
      },
      {
        "value": 114402,
        "label": "LAGO VERDE"
      },
      {
        "value": 114501,
        "label": "AISEN"
      },
      {
        "value": 114502,
        "label": "CISNES"
      },
      {
        "value": 114503,
        "label": "GUAITECAS"
      },
      {
        "value": 114601,
        "label": "CHILE CHICO"
      },
      {
        "value": 114602,
        "label": "RIO IBANEZ"
      },
      {
        "value": 114701,
        "label": "COCHRANE"
      },
      {
        "value": 114702,
        "label": "OHIGGINS"
      },
      {
        "value": 114703,
        "label": "TORTEL"
      },
      {
        "value": 124801,
        "label": "NATALES"
      },
      {
        "value": 124802,
        "label": "TORRES DEL PAINE"
      },
      {
        "value": 124901,
        "label": "PUNTA ARENAS"
      },
      {
        "value": 124902,
        "label": "RIO VERDE"
      },
      {
        "value": 124903,
        "label": "LAGUNA BLANCA"
      },
      {
        "value": 124904,
        "label": "SAN GREGORIO"
      },
      {
        "value": 125001,
        "label": "PORVENIR"
      },
      {
        "value": 125002,
        "label": "PRIMAVERA"
      },
      {
        "value": 125003,
        "label": "TIMAUKEL"
      },
      {
        "value": 125101,
        "label": "CABO DE HORNOS"
      },
      {
        "value": 132001,
        "label": "COLINA"
      },
      {
        "value": 132002,
        "label": "LAMPA"
      },
      {
        "value": 132003,
        "label": "TILTIL"
      },
      {
        "value": 132101,
        "label": "SANTIAGO"
      },
      {
        "value": 132102,
        "label": "INDEPENDENCIA"
      },
      {
        "value": 132103,
        "label": "RECOLETA"
      },
      {
        "value": 132104,
        "label": "ESTACION CENTRAL"
      },
      {
        "value": 132105,
        "label": "CONCHALI"
      },
      {
        "value": 132106,
        "label": "HUECHURABA"
      },
      {
        "value": 132107,
        "label": "QUILICURA"
      },
      {
        "value": 132108,
        "label": "RENCA"
      },
      {
        "value": 132109,
        "label": "QUINTA NORMAL"
      },
      {
        "value": 132110,
        "label": "PUDAHUEL"
      },
      {
        "value": 132111,
        "label": "CERRO NAVIA"
      },
      {
        "value": 132112,
        "label": "LO PRADO"
      },
      {
        "value": 132113,
        "label": "MAIPU"
      },
      {
        "value": 132114,
        "label": "CERRILLOS"
      },
      {
        "value": 132115,
        "label": "LA CISTERNA"
      },
      {
        "value": 132116,
        "label": "LO ESPEJO"
      },
      {
        "value": 132117,
        "label": "EL BOSQUE"
      },
      {
        "value": 132118,
        "label": "LA GRANJA"
      },
      {
        "value": 132119,
        "label": "LA PINTANA"
      },
      {
        "value": 132120,
        "label": "SAN RAMON"
      },
      {
        "value": 132121,
        "label": "SAN MIGUEL"
      },
      {
        "value": 132122,
        "label": "PEDRO AGUIRRE CERDA"
      },
      {
        "value": 132123,
        "label": "SAN JOAQUIN"
      },
      {
        "value": 132124,
        "label": "LA FLORvalueA"
      },
      {
        "value": 132125,
        "label": "NUNOA"
      },
      {
        "value": 132126,
        "label": "MACUL"
      },
      {
        "value": 132127,
        "label": "PENALOLEN"
      },
      {
        "value": 132128,
        "label": "LA REINA"
      },
      {
        "value": 132129,
        "label": "Providencia"
      },
      {
        "value": 132130,
        "label": "LAS CONDES"
      },
      {
        "value": 132131,
        "label": "VITACURA"
      },
      {
        "value": 132132,
        "label": "LO BARNECHEA"
      },
      {
        "value": 132201,
        "label": "PUENTE ALTO"
      },
      {
        "value": 132202,
        "label": "PIRQUE"
      },
      {
        "value": 132203,
        "label": "SAN JOSE DE MAIPO"
      },
      {
        "value": 132301,
        "label": "SAN BERNARDO"
      },
      {
        "value": 132302,
        "label": "CALERA DE TANGO"
      },
      {
        "value": 132303,
        "label": "BUIN"
      },
      {
        "value": 132304,
        "label": "PAINE"
      },
      {
        "value": 132401,
        "label": "MELIPILLA"
      },
      {
        "value": 132402,
        "label": "ALHUE"
      },
      {
        "value": 132403,
        "label": "CURACAVI"
      },
      {
        "value": 132404,
        "label": "MARIA PINTO"
      },
      {
        "value": 132405,
        "label": "SAN PEDRO"
      },
      {
        "value": 132501,
        "label": "TALAGANTE"
      },
      {
        "value": 132502,
        "label": "PENAFLOR"
      },
      {
        "value": 132503,
        "label": "EL MONTE"
      },
      {
        "value": 132504,
        "label": "ISLA DE MAIPO"
      },
      {
        "value": 132505,
        "label": "PADRE HURTADO"
      },
      {
        "value": 143901,
        "label": "VALDIVIA"
      },
      {
        "value": 143902,
        "label": "MARIQUINA"
      },
      {
        "value": 143903,
        "label": "LANCO"
      },
      {
        "value": 143904,
        "label": "LOS LAGOS"
      },
      {
        "value": 143905,
        "label": "CORRAL"
      },
      {
        "value": 143906,
        "label": "MAFIL"
      },
      {
        "value": 143907,
        "label": "PANGUIPULLI"
      },
      {
        "value": 143908,
        "label": "PAILLACO"
      },
      {
        "value": 145301,
        "label": "FUTRONO"
      },
      {
        "value": 145302,
        "label": "LA UNION"
      },
      {
        "value": 145303,
        "label": "RIO BUENO"
      },
      {
        "value": 145304,
        "label": "LAGO RANCO"
      },
      {
        "value": 150101,
        "label": "ARICA"
      },
      {
        "value": 150102,
        "label": "CAMARONES"
      },
      {
        "value": 150201,
        "label": "PUTRE"
      },
      {
        "value": 150202,
        "label": "GENERAL LAGOS"
      }
]

export default comunas;